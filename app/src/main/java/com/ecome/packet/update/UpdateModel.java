package com.ecome.packet.update;

import com.mk.core.model.BaseDataModel;

/**
 * Created by mingkg21 on 2017/8/29.
 */

public class UpdateModel extends BaseDataModel {

    private String mUpdateKey;

    public void checkUpdate() {
        UpdateRequest request = new UpdateRequest();
        mUpdateKey = request.getDataKey();
        startTask(request);
    }

    public boolean isUpdateKey(String key) {
        return key.equals(mUpdateKey);
    }

    @Override
    public boolean dispatchSuccess(String key, Object data) {
        if (isUpdateKey(key)) {
            UpdateInfoData updateInfoData = (UpdateInfoData) data;
            UpdateInfo updateInfo = updateInfoData.getData();
            //TODO 提示客户端更新
        }
        return super.dispatchSuccess(key, data);
    }
}
