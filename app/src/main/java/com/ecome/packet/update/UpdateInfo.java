package com.ecome.packet.update;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

/**
 * Created by mingkg21 on 2017/8/29.
 */

public class UpdateInfo extends BaseEntity {

    private static final long serialVersionUID = -1908831099243917729L;

    @SerializedName("version")
    private long version;

    @SerializedName("url")
    private String url;

    public long getVersion() {
        return version;
    }

    public String getUrl() {
        return url;
    }
}
