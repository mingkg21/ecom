package com.ecome.packet.update;

import com.ecome.packet.net.request.AppRequest;

import java.util.Map;

/**
 * Created by mingkg21 on 2017/8/29.
 */

public class UpdateRequest extends AppRequest {

    @Override
    public String getDataKey() {
        return "UpdateRequest";
    }

//    @Override
//    public Call getCall(ApiStores apiStores) {
//        return apiStores.checkVersion();
//    }

    @Override
    protected void setParameters(Map<String, String> parameters) {

    }

    @Override
    protected String getActionName() {
        return null;
    }

    @Override
    protected Class getParseClass() {
        return null;
    }
}
