package com.ecome.packet.util;

import android.util.Log;

/**
 * 打印日志的工具类
 *
 * @author donkor
 */
public class LogUtil {
    //规定每段显示的长度
    private static int LOG_MAXLENGTH = 2000;

    public static int e(String TAG, String msg) {
        int strLength = msg.length();
        int start = 0;
        int end = LOG_MAXLENGTH;

        int i = 0;
        while (start < strLength) {
            if (strLength > end) {
                Log.e(TAG + "_" + i, msg.substring(start, end));
                start = end;
                end = end + LOG_MAXLENGTH;
            } else {
                Log.e(TAG, msg.substring(start, strLength));
                break;
            }
            i++;
        }

        return 0;
    }
}

