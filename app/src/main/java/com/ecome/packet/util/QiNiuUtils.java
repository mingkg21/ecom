package com.ecome.packet.util;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.mk.core.app.BaseApp;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.util.Auth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import top.zibin.luban.Luban;

public class QiNiuUtils {

    private static String token = "一般是找自己服务器要";
    /**
     * 七牛云SDK
     */
    private static UploadManager uploadManager = new UploadManager();

    /**
     * 回调接口
     */
    public interface QiNiuCallback {

        /**
         * 上传完成
         */
        void onSuccess(List<String> picUrls);

        /**
         * 上传失败
         */
        void onError(String msg);
    }

    public static void putImgs(String img, final QiNiuCallback qiNiuCallback) {
        List<String> images = new ArrayList<>();
        images.add(img);
        putImgs(images, qiNiuCallback);
    }

    /**
     * 上传图片到七牛
     *
     * @param images        图片地址
     * @param qiNiuCallback 回调接口
     */
    @SuppressLint("CheckResult")
    public static void putImgs(final List<String> images, final QiNiuCallback qiNiuCallback) {
        final String token = Auth.create(Constants.QINIU_AK,
                Constants.QINIU_SK).uploadToken(Constants.QINIU_BUCKET);
        // 七牛返回的文件名
        final ArrayList<String> resultImagePath = new ArrayList<>();
        Observable
                // 依次发送list中的数据
                .fromIterable(images)
                // 变换，在这里上传图片
                // 修改为concatMap确保图片顺序
                .concatMap(new Function<String, ObservableSource<String>>() {
                    @Override
                    public ObservableSource<String> apply(final String path) throws Exception {
                        return Observable.create(new ObservableOnSubscribe<String>() {
                            @Override
                            public void subscribe(ObservableEmitter<String> emitter) throws Exception {
                                if (TextUtils.isEmpty(path) || path.startsWith("http://")) {
                                    emitter.onNext(path);
                                    emitter.onComplete();
                                } else {
                                    String key = UUID.randomUUID().toString() + "." + path.split("\\.")[1];
                                    ResponseInfo responseInfo = uploadManager.syncPut(Luban.with(BaseApp.getInstance()).load(path).get().get(0), key, token, null);
                                    if (responseInfo.isOK()) {
                                        // 上传成功，发送这张图片的文件名
                                        emitter.onNext(key);
                                        emitter.onComplete();
                                    } else {
                                        // 上传失败，告辞
                                        emitter.onError(new IOException(responseInfo.error));
                                    }
                                }
                            }
                        }).subscribeOn(Schedulers.io());
                    }
                })
                // 线程切换
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(String s) {
                        resultImagePath.add(s);
                        // 如果全部完成，调用成功接口
                        if (resultImagePath.size() == images.size()) {
                            qiNiuCallback.onSuccess(resultImagePath);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        qiNiuCallback.onError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }
}
