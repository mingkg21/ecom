package com.ecome.packet.util;

import com.mk.core.util.FileUtil;

import java.io.File;

/**
 * 文件操作工具
 *
 * @author lyw
 * @date 2014-1-10
 */
public final class AppFileUtil {
    /**
     * 软件根目录
     */
    private static final String APP_PATH_NAME = "Ecom/";
    /**
     * 多媒体缓存目录
     */
    private static final String MEDIA_CACHE_PATH = "MediaCache/";
    /**
     * 录音缓存文件名
     */
    private static final String RECORD_CACHE_FILE = "RecordCacheFile";

    private static String APP_PATH;

    public static String getAppPath() {
        if (APP_PATH == null) {
            APP_PATH = initAppPath();
        }
        return APP_PATH;
    }

    public static String getMediaCachePath() {
        String path = getAppPath() + MEDIA_CACHE_PATH;
        FileUtil.checkPath(path);
        return path;
    }

    public static String initAppPath() {
        APP_PATH = FileUtil.getDcimPath() + File.separator + APP_PATH_NAME;
        FileUtil.checkPath(APP_PATH);
        return APP_PATH;
    }

}
