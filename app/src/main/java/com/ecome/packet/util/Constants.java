package com.ecome.packet.util;

import android.text.TextUtils;

import java.text.SimpleDateFormat;

public class Constants {

    public static final String QINIU_AK = "4_aYuuFk0q5GAyaqtBNzSV-UvNG2knaHD88zr2cf";
    public static final String QINIU_SK = "CTZN8ZtxIIZCw-4NjGaTvUupUgqWYqx3lpOuGtxW";
    public static final String QINIU_BUCKET = "ecom";

    public static final String getQiniuImageUrl(String key) {
        if (TextUtils.isEmpty(key) || key.startsWith("http://")) {
            return key;
        }
//        return String.format("http://osahmjfu4.bkt.clouddn.com/%s", key);
        return String.format("http://qn.51yicong.com/%s", key);
    }

    public static final long getTime(String str) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sdf.parse(str).getTime();
        } catch (Exception e) {

        }
        return 0;
    }

}
