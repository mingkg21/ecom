package com.ecome.packet.model;

import com.mk.core.entity.BaseListEntity;
import com.mk.core.model.AbsAppPagingLoadModel;

/**
 * Created by mingkg21 on 2017/10/22.
 */

public abstract class AppPagingLoadModel<T> extends AbsAppPagingLoadModel<BaseListEntity<T>, T> {
}
