package com.ecome.packet.model;

import com.ecome.packet.net.request.ReportChargeDetailRequest;
import com.ecome.packet.net.request.ReportFeeRequest;
import com.ecome.packet.net.request.ReportRiBaoRequest;
import com.ecome.packet.net.request.ReportRiZhiRequest;
import com.mk.core.model.BaseDataModel;

public class ReportModel extends BaseDataModel {

    private String mReportChargeDetailKey;
    private String mReportRiBaoKey;
    private String mReportRiZhiKey;
    private String mReportFeeKey;

    public void reportChargeDetail(String startTime, String endTime) {
        ReportChargeDetailRequest request = new ReportChargeDetailRequest(startTime, endTime);
        mReportChargeDetailKey = request.getDataKey();
        startTask(request);
    }

    public boolean isReportChargeDetailKey(String key) {
        return isKey(key, mReportChargeDetailKey);
    }

    public void reportRiBao(String startTime, String endTime) {
        ReportRiBaoRequest request = new ReportRiBaoRequest(startTime, endTime);
        mReportRiBaoKey = request.getDataKey();
        startTask(request);
    }

    public boolean isReportRiBaoKey(String key) {
        return isKey(key, mReportRiBaoKey);
    }

    public void reportRiZhi(String startTime, String endTime) {
        ReportRiZhiRequest request = new ReportRiZhiRequest(startTime, endTime);
        mReportRiZhiKey = request.getDataKey();
        startTask(request);
    }

    public boolean isReportRiZhiKey(String key) {
        return isKey(key, mReportRiZhiKey);
    }

    public void reportFee(String startTime, String endTime) {
        ReportFeeRequest request = new ReportFeeRequest(startTime, endTime);
        mReportFeeKey = request.getDataKey();
        startTask(request);
    }

    public boolean isReportFeeKey(String key) {
        return isKey(key, mReportFeeKey);
    }

}
