package com.ecome.packet.model;

import com.ecome.packet.db.SystemInfoPreferences;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.SystemInfo;
import com.ecome.packet.net.request.FeedbackRequest;
import com.ecome.packet.net.request.SystemRequest;
import com.mk.core.model.BaseDataModel;

import java.util.ArrayList;

public class SystemModel extends BaseDataModel {

    private static SystemModel instance;

    private String mGetSystemKey;
    private String mFeedbackKey;

    private SystemModel() {

    }

    public static SystemModel getInstance() {
        if (instance == null) {
            synchronized (SystemModel.class) {
                if (instance == null) {
                    instance = new SystemModel();
                }
            }
        }
        return instance;
    }

    public void getSystemInfo() {
        SystemRequest request = new SystemRequest();
        mGetSystemKey = request.getDataKey();
        startTask(request);
    }

    private boolean isGetSystemKey(String key) {
        return isKey(key, mGetSystemKey);
    }

    public void feedback(String content) {
        FeedbackRequest request = new FeedbackRequest(content);
        mFeedbackKey = request.getDataKey();
        startTask(request);
    }

    private boolean isFeedbackKey(String key) {
        return isKey(key, mFeedbackKey);
    }

    @Override
    public boolean dispatchSuccess(String key, Object data) {
        if (isGetSystemKey(key)) {
            AppResponseEntity<ArrayList<SystemInfo>> responseEntity = (AppResponseEntity<ArrayList<SystemInfo>>) data;
            ArrayList<SystemInfo> list = responseEntity.getData();
            if (list != null) {
                SystemInfoPreferences.getInstance().save(list);
            }
        }
        return super.dispatchSuccess(key, data);
    }
}
