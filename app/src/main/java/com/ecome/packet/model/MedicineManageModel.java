package com.ecome.packet.model;

import com.ecome.packet.net.request.GetMdVoucherRequest;
import com.mk.core.model.BaseDataModel;

public class MedicineManageModel extends BaseDataModel {

    private String mQueryAllMdVoucherKey;
    private String mConfirmMdVoucherKey;
    private String mQueryMdVoucherDetailKey;

    public void queryAllMdVoucher() {
        GetMdVoucherRequest request = new GetMdVoucherRequest(GetMdVoucherRequest.FLAG_QUERY_ALL, "");
        mQueryAllMdVoucherKey = request.getDataKey();
        startTask(request);
    }

    public boolean isQueryAllMdVoucherKey(String key) {
        return isKey(key, mQueryAllMdVoucherKey);
    }

    public void confirmMdVoucher(String voucherNo) {
        GetMdVoucherRequest request = new GetMdVoucherRequest(GetMdVoucherRequest.FLAG_CONFIRM, voucherNo);
        mConfirmMdVoucherKey = request.getDataKey();
        startTask(request);
    }

    public boolean isConfirmMdVoucherKey(String key) {
        return isKey(key, mConfirmMdVoucherKey);
    }

    public void queryMdVoucherDetail(String voucherNo) {
        GetMdVoucherRequest request = new GetMdVoucherRequest(GetMdVoucherRequest.FLAG_QUERY_ONE, voucherNo);
        mQueryMdVoucherDetailKey = request.getDataKey();
        startTask(request);
    }

    public boolean isQueryMdVoucherDetailKey(String key) {
        return isKey(key, mQueryMdVoucherDetailKey);
    }

}
