package com.ecome.packet.model;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppBaseEntity;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.net.request.AddPatientRequest;
import com.ecome.packet.net.request.AddPrescriptionRequest;
import com.ecome.packet.net.request.ChargeRequest;
import com.ecome.packet.net.request.FindPatientRequest;
import com.ecome.packet.net.request.GetDoctorWorkloadRequest;
import com.ecome.packet.net.request.GetDrugRequest;
import com.ecome.packet.net.request.GetFriedRequest;
import com.ecome.packet.net.request.GetPatientHistoryRegistrationRequest;
import com.ecome.packet.net.request.GetPrescriptionTempDetailRequest;
import com.ecome.packet.net.request.GetPrescriptionTempListRequest;
import com.ecome.packet.net.request.InsertPicRequest;
import com.ecome.packet.net.request.ModifyDiagnoseFeeRequest;
import com.ecome.packet.net.request.ModifyPatientAddressRequest;
import com.ecome.packet.net.request.PrescriptionRequest;
import com.ecome.packet.net.request.RegistrationRequest;
import com.ecome.packet.net.request.SavePrescriptionTempRequest;
import com.mk.core.model.BaseDataModel;

public class DoctorWorkspaceModel extends BaseDataModel {

    public static final String NOTIFY_CHANGE_CHARGE_SUCCESS = "NOTIFY_CHANGE_CHARGE_SUCCESS";

    private String mFindPatientKey;
    private String mGetPatientHistoryRegistrationKey;
    private String mGetPrescriptionKey;
    private String mAddPatientKey;
    private String mRegistrationKey;
    private String mGetFriedKey;
    private String mGetDrugKey;
    private String mAddPrescriptionKey;
    private String mModifyDiagnoseFeeKey;
    private String mModifyPatientAddressKey;
    private String mSavePrescriptionTempKey;
    private String mGetTempListKey;
    private String mGetTempDetailKey;
    private String mGetDoctorWorkloadKey;
    private String mChargeKey;
    private String mInsertImageKey;

    public void findPatient(String key) {
        FindPatientRequest request = new FindPatientRequest(key);
        mFindPatientKey = request.getDataKey();
        startTask(request);
    }

    public boolean isFindPatientKey(String key) {
        return isKey(key, mFindPatientKey);
    }

    public void getPatientHistoryRegistration(String id) {
        GetPatientHistoryRegistrationRequest request = new GetPatientHistoryRegistrationRequest(id);
        mGetPatientHistoryRegistrationKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetPatientHistoryRegistrationKey(String key) {
        return isKey(key, mGetPatientHistoryRegistrationKey);
    }

    public void getPrescription(String id) {
        PrescriptionRequest request = new PrescriptionRequest(id);
        mGetPrescriptionKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetPrescriptionKey(String key) {
        return isKey(key, mGetPrescriptionKey);
    }

    public void addPatient(String name, String tel, String sex, String age, String id, String address) {
        AddPatientRequest request = new AddPatientRequest(name, tel, sex, age, id, address);
        mAddPatientKey = request.getDataKey();
        startTask(request);
    }

    public boolean isAddPatientKey(String key) {
        return isKey(key, mAddPatientKey);
    }

    public void registration(int doctorId, int patientId) {
        RegistrationRequest request = new RegistrationRequest(doctorId, patientId);
        mRegistrationKey = request.getDataKey();
        startTask(request);
    }

    public boolean isRegistrationKey(String key) {
        return isKey(key, mRegistrationKey);
    }

    public void getFried() {
        GetFriedRequest request = new GetFriedRequest();
        mGetFriedKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetFriedKey(String key) {
        return isKey(key, mGetFriedKey);
    }

    public void getDrug() {
        GetDrugRequest request = new GetDrugRequest();
        mGetDrugKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetDrugKey(String key) {
        return isKey(key, mGetDrugKey);
    }

    public void addPrescription(int registrationId, int patientId, String diagnose,
                                String symptom, String prescriptionDetail, String useNumber,
                                String gaofang, String daijian, String remark) {
        AddPrescriptionRequest request = new AddPrescriptionRequest(UserManager.getInstance().getUserId(), registrationId,
                patientId, diagnose, symptom, prescriptionDetail, useNumber, gaofang, daijian, remark);
        mAddPrescriptionKey = request.getDataKey();
        startTask(request);
    }

    public boolean isAddPrescriptionKey(String key) {
        return isKey(key, mAddPrescriptionKey);
    }

    public void modifyDiagnoseFee(String registrationId, String doctorId, String fee) {
        ModifyDiagnoseFeeRequest request = new ModifyDiagnoseFeeRequest(registrationId, doctorId, fee);
        mModifyDiagnoseFeeKey = request.getDataKey();
        startTask(request);
    }

    public boolean isModifyDiagnoseFeeKey(String key) {
        return isKey(key, mModifyDiagnoseFeeKey);
    }

    public void modifyPatientAddress(String patientId, String tel, String address) {
        ModifyPatientAddressRequest request = new ModifyPatientAddressRequest(patientId, tel, address);
        mModifyPatientAddressKey = request.getDataKey();
        startTask(request);
    }

    public boolean isModifyPatientAddressKey(String key) {
        return isKey(key, mModifyPatientAddressKey);
    }

    public void savePrescriptionTemp(String zhuzhi, String gongxiao, String chufang_zhifa,
                                     String tempName, String temp_quanxian, String chufang_remark,
                                     String prescriptionDetail, String useNumber,
                                     String gaofang, String daijian, String remark) {
        SavePrescriptionTempRequest request = new SavePrescriptionTempRequest(UserManager.getInstance().getUserId(), zhuzhi,
                gongxiao, chufang_zhifa, tempName, temp_quanxian, chufang_remark, prescriptionDetail, useNumber, gaofang,
                daijian, remark);
        mSavePrescriptionTempKey = request.getDataKey();
        startTask(request);
    }

    public boolean isSavePrescriptionTempKey(String key) {
        return isKey(key, mSavePrescriptionTempKey);
    }

    public void getTempList() {
        GetPrescriptionTempListRequest request = new GetPrescriptionTempListRequest();
        mGetTempListKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetTempListKey(String key) {
        return isKey(key, mGetTempListKey);
    }

    public void getTempDetail(int tempId) {
        GetPrescriptionTempDetailRequest request = new GetPrescriptionTempDetailRequest(tempId);
        mGetTempDetailKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetTempDetailKey(String key) {
        return isKey(key, mGetTempDetailKey);
    }

    public void getDoctorWorkload(String startTime, String endTime) {
        GetDoctorWorkloadRequest request = new GetDoctorWorkloadRequest(startTime, endTime);
        mGetDoctorWorkloadKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetDoctorWorkloadKey(String key) {
        return isKey(key, mGetDoctorWorkloadKey);
    }

    public void charge(String chargeReg, String registerId, String payType, String fee, String patientId) {
        ChargeRequest request = new ChargeRequest(chargeReg, registerId, payType, fee, patientId);
        mChargeKey = request.getDataKey();
        startTask(request);
    }

    public boolean isChargeKey(String key) {
        return isKey(key, mChargeKey);
    }

    public void insertImage(int patientId, int guahaoId, String pic) {
        InsertPicRequest request = new InsertPicRequest(patientId, guahaoId, pic);
        mInsertImageKey = request.getDataKey();
        startTask(request);
    }

    public boolean isInsertImageKey(String key) {
        return isKey(key, mInsertImageKey);
    }

    @Override
    public boolean dispatchSuccess(String key, Object data) {
        if (isChargeKey(key)) {
            AppResponseEntity<AppBaseEntity> appResponseEntity = (AppResponseEntity<AppBaseEntity>) data;
            AppBaseEntity appBaseEntity = appResponseEntity.getData();
            if (appBaseEntity != null && appBaseEntity.isSuccess()) {
                notifyDataChange(NOTIFY_CHANGE_CHARGE_SUCCESS, data);
            }
        }
        return super.dispatchSuccess(key, data);
    }
}
