package com.ecome.packet.model;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.net.request.MenuRequest;
import com.mk.core.model.BaseDataModel;

public class MenuModel extends BaseDataModel {

    private String mMenuKey;

    public void getMenu(int levelId, int levelPre) {
        int loginId = 157;
        if (UserManager.getInstance().isLogin()) {
            loginId = UserManager.getInstance().getUserId();
        }
        MenuRequest request = new MenuRequest(loginId, levelId, levelPre);
        mMenuKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetMenuKey(String key) {
        return isKey(key, mMenuKey);
    }

}
