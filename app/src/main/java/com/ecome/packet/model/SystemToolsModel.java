package com.ecome.packet.model;

import com.ecome.packet.net.request.FeedbackRequest;
import com.mk.core.model.BaseDataModel;

public class SystemToolsModel extends BaseDataModel {

    private String mFeedbackKey;

    public void feedback(String content) {
        FeedbackRequest request = new FeedbackRequest(content);
        mFeedbackKey = request.getDataKey();
        startTask(request);
    }

    public boolean isFeedbackKey(String key) {
        return isKey(key, mFeedbackKey);
    }

}
