package com.ecome.packet.model;

import com.ecome.packet.entity.Drug;
import com.ecome.packet.entity.Staff;
import com.ecome.packet.net.request.AddMdRukuRequest;
import com.ecome.packet.net.request.CmisPaibanRequest;
import com.ecome.packet.net.request.ConfigModifyRequest;
import com.ecome.packet.net.request.GetClsCodeRequest;
import com.ecome.packet.net.request.GetCodeRequest;
import com.ecome.packet.net.request.GetConfigRequest;
import com.ecome.packet.net.request.GetDoctorPaibanRequest;
import com.ecome.packet.net.request.GetDoctorRequest;
import com.ecome.packet.net.request.GetDoctorWorkRequest;
import com.ecome.packet.net.request.GetDoctorWorkWeekRequest;
import com.ecome.packet.net.request.GetLogRequest;
import com.ecome.packet.net.request.GetStaffRequest;
import com.ecome.packet.net.request.ModifyDoctorWorkRequest;
import com.ecome.packet.net.request.ModifyFeeRequest;
import com.ecome.packet.net.request.ModifyStaffRequest;
import com.mk.core.model.BaseDataModel;

public class BackendManageModel extends BaseDataModel {

    public static final String KEY_MODIFY_STAFF = "KEY_MODIFY_STAFF";

    private String mGetConfigKey;
    private String mModifyConfigKey;
    private String mGetStaffKey;
    private String mGetDoctorKey;

    private String mGetZiDianKey;
    private String mGetRoleKey;
    private String mGetDepartmentKey;
    private String mGetZhiChengKey;
    private String mGetFeeCategoryKey;
    private String mGetUnitKey;
    private String mGetFeeKey;
    private String mGetKuFangKey;
    private String mGetProviderKey;
    private String mGetBusinessKey;
    private String mGetRemarkKey;
    private String mGetUsekKey;
    private String mGetPinCikKey;

    private String mModifyStaffKey;
    private String mModifyFeeKey;
    private String mModifyMdRuKuKey;

    private String mGetDoctorWorkWeekKey;
    private String mGetDoctorWorkKey;
    private String mModifyDoctorWorkKey;
    private String mGetDoctorPaibanKey;
    private String mCmisPaibanKey;

    private String mGetLogKey;

    public void getConfig() {
        GetConfigRequest request = new GetConfigRequest();
        mGetConfigKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetConfigKey(String key) {
        return isKey(key, mGetConfigKey);
    }

    public void modifyConfig(String configId, String configValue) {
        ConfigModifyRequest request = new ConfigModifyRequest(configId, configValue);
        mModifyConfigKey = request.getDataKey();
        startTask(request);
    }

    public boolean isModifyConfigKey(String key) {
        return isKey(key, mModifyConfigKey);
    }

    public void getStaff() {
        GetStaffRequest request = new GetStaffRequest();
        mGetStaffKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetStaffKey(String key) {
        return isKey(key, mGetStaffKey);
    }

    public void getDoctor() {
        GetDoctorRequest request = new GetDoctorRequest();
        mGetDoctorKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetDoctorKey(String key) {
        return isKey(key, mGetDoctorKey);
    }

    public void getDepartment() {
        GetClsCodeRequest request = new GetClsCodeRequest(GetClsCodeRequest.CLS_DEPARTMENT, GetClsCodeRequest.OPER_CLS_QUERY);
        mGetDepartmentKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetDepartmentKey(String key) {
        return isKey(key, mGetDepartmentKey);
    }

    public void getZiDian() {
        GetClsCodeRequest request = new GetClsCodeRequest(GetClsCodeRequest.CLS_ALL, GetClsCodeRequest.OPER_CLS_QUERY);
        mGetZiDianKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetZiDianKey(String key) {
        return isKey(key, mGetZiDianKey);
    }

    public void getRole() {
        GetClsCodeRequest request = new GetClsCodeRequest(GetClsCodeRequest.CLS_ROLE, GetClsCodeRequest.OPER_CLS_QUERY);
        mGetRoleKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetRoleKey(String key) {
        return isKey(key, mGetRoleKey);
    }

    public void getZhiCheng() {
        GetClsCodeRequest request = new GetClsCodeRequest(GetClsCodeRequest.CLS_ZHICHENG, GetClsCodeRequest.OPER_CLS_QUERY);
        mGetZhiChengKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetZhiChengKey(String key) {
        return isKey(key, mGetZhiChengKey);
    }

    public void modifyStaff(Staff staff) {
        ModifyStaffRequest request = new ModifyStaffRequest(staff);
        mModifyStaffKey = request.getDataKey();
        startTask(request);
    }

    public boolean isModifyStaffKey(String key) {
        return isKey(key, mModifyStaffKey);
    }

    public void getFee(String mdClsId) {
        GetCodeRequest request = new GetCodeRequest(mdClsId);
        mGetFeeKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetFeeKey(String key) {
        return isKey(key, mGetFeeKey);
    }

    public void getFeeCategory() {
        GetClsCodeRequest request = new GetClsCodeRequest(GetClsCodeRequest.CLS_FEE, GetClsCodeRequest.OPER_CLS_QUERY);
        mGetFeeCategoryKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetFeeCategoryKey(String key) {
        return isKey(key, mGetFeeCategoryKey);
    }

    public void getUnit() {
        GetClsCodeRequest request = new GetClsCodeRequest(GetClsCodeRequest.CLS_UNIT, GetClsCodeRequest.OPER_CLS_QUERY);
        mGetUnitKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetUnitKey(String key) {
        return isKey(key, mGetUnitKey);
    }

    public void getKuFang() {
        GetClsCodeRequest request = new GetClsCodeRequest(GetClsCodeRequest.CLS_KUFANG, GetClsCodeRequest.OPER_CLS_QUERY);
        mGetKuFangKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetKuFangKey(String key) {
        return isKey(key, mGetKuFangKey);
    }

    public void getProvider() {
        GetClsCodeRequest request = new GetClsCodeRequest(GetClsCodeRequest.CLS_PROVIDER, GetClsCodeRequest.OPER_CLS_QUERY);
        mGetProviderKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetProviderKey(String key) {
        return isKey(key, mGetProviderKey);
    }

    public void getBusiness() {
        GetClsCodeRequest request = new GetClsCodeRequest(GetClsCodeRequest.CLS_BUSINESS, GetClsCodeRequest.OPER_CLS_QUERY);
        mGetBusinessKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetBusinessKey(String key) {
        return isKey(key, mGetBusinessKey);
    }

    public void getRemark() {
        GetClsCodeRequest request = new GetClsCodeRequest(GetClsCodeRequest.CLS_REMARK, GetClsCodeRequest.OPER_CLS_QUERY);
        mGetRemarkKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetRemarkKey(String key) {
        return isKey(key, mGetRemarkKey);
    }

    public void getUse() {
        GetClsCodeRequest request = new GetClsCodeRequest(GetClsCodeRequest.CLS_USE, GetClsCodeRequest.OPER_CLS_QUERY);
        mGetUsekKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetUseKey(String key) {
        return isKey(key, mGetUsekKey);
    }

    public void getPinCi() {
        GetClsCodeRequest request = new GetClsCodeRequest(GetClsCodeRequest.CLS_PINCI, GetClsCodeRequest.OPER_CLS_QUERY);
        mGetPinCikKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetPinCiKey(String key) {
        return isKey(key, mGetPinCikKey);
    }

    public void modifyFee(Drug drug) {
        ModifyFeeRequest request = new ModifyFeeRequest(drug);
        mModifyFeeKey = request.getDataKey();
        startTask(request);
    }

    public boolean isModifyFeeKey(String key) {
        return isKey(key, mModifyFeeKey);
    }

    public void modifyMkRuKu(int providerId, String voucherNo, String fapiao, String remark, String detail, int kuFangId, int businessId) {
        AddMdRukuRequest request = new AddMdRukuRequest(providerId, voucherNo, fapiao, remark, detail, kuFangId, businessId);
        mModifyMdRuKuKey = request.getDataKey();
        startTask(request);
    }

    public boolean isModifyMdRuKuKey(String key) {
        return isKey(key, mModifyMdRuKuKey);
    }

    public void getDoctorWorkWeek(int doctorId) {
        GetDoctorWorkWeekRequest request = new GetDoctorWorkWeekRequest(doctorId);
        mGetDoctorWorkWeekKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetDoctorWorkWeekKey(String key) {
        return isKey(key, mGetDoctorWorkWeekKey);
    }

    public void getDoctorWork(int doctorId) {
        GetDoctorWorkRequest request = new GetDoctorWorkRequest(doctorId);
        mGetDoctorWorkKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetDoctorWorkKey(String key) {
        return isKey(key, mGetDoctorWorkKey);
    }

    public void modifyDoctorWork(int doctorId,
                                 String morningTimeStart, String morningTimeEnd,
                                 String afternoonTimeStart, String afternoonTimeEnd,
                                 String nightTimeStart, String nightTimeEnd,
                                 String day1Morning, String day1Afternoon,
                                 String day1Night, String day2Morning, String day2Afternoon,
                                 String day2Night, String day3Morning, String day3Afternoon,
                                 String day3Night, String day4Morning, String day4Afternoon,
                                 String day4Night, String day5Morning, String day5Afternoon,
                                 String day5Night, String day6Morning, String day6Afternoon,
                                 String day6Night, String day7Morning, String day7Afternoon, String day7Night) {
        ModifyDoctorWorkRequest request = new ModifyDoctorWorkRequest(doctorId, morningTimeStart, morningTimeEnd,
                afternoonTimeStart, afternoonTimeEnd, nightTimeStart, nightTimeEnd, day1Morning, day1Afternoon, day1Night,
                day2Morning, day2Afternoon, day2Night, day3Morning, day3Afternoon, day3Night, day4Morning, day4Afternoon,
                day4Night, day5Morning, day5Afternoon, day5Night, day6Morning, day6Afternoon, day6Night, day7Morning, day7Afternoon,
                day7Night);
        mModifyDoctorWorkKey = request.getDataKey();
        startTask(request);
    }

    public boolean isModifyDoctorWorkKey(String key) {
        return isKey(key, mModifyDoctorWorkKey);
    }

    public void cmisPaiban(int doctorId, String beginDate, String endDate) {
        CmisPaibanRequest request = new CmisPaibanRequest(doctorId, beginDate, endDate);
        mCmisPaibanKey = request.getDataKey();
        startTask(request);
    }

    public boolean isCmisPaibanKey(String key) {
        return isKey(key, mCmisPaibanKey);
    }

    public void getDoctorPaiban(int doctorId) {
        GetDoctorPaibanRequest request = new GetDoctorPaibanRequest(doctorId);
        mGetDoctorPaibanKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetDoctorPaibanKey(String key) {
        return isKey(key, mGetDoctorPaibanKey);
    }

    public void getLog() {
        GetLogRequest request = new GetLogRequest();
        mGetLogKey = request.getDataKey();
        startTask(request);
    }

    public boolean isGetLogKey(String key) {
        return isKey(key, mGetLogKey);
    }

    @Override
    public boolean dispatchSuccess(String key, Object data) {
        if (isModifyStaffKey(key)) {
            notifyDataChange(KEY_MODIFY_STAFF, data);
        }
        return super.dispatchSuccess(key, data);
    }
}
