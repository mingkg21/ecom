package com.ecome.packet.model;

import android.content.Intent;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.UserInfo;
import com.ecome.packet.net.request.CompleteInfoRequest;
import com.ecome.packet.net.request.LoginRequest;
import com.ecome.packet.net.request.ModifyPasswordRequest;
import com.ecome.packet.net.request.RegisterRequest;
import com.ecome.packet.net.request.SendSMSRequest;
import com.mk.core.app.AppBroadcast;
import com.mk.core.model.BaseDataModel;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class UserModel extends BaseDataModel {

    public static final String USER_INFO_CHANGE = "USER_INFO_CHANGE";

    private String mRegisterKey;
    private String mLoginKey;
    private String mCompleteInfoInRegisterKey;
    private String mCompleteInfoKey;
    private String mGetUserInfoKey;
    private String mEditUserInfoKey;
    private String mSendCodeKey;
    private String mUpdatePasswordKey;

    public void register(String name, String adminName, String phone, String introducerId, String picPath) {
        RegisterRequest request = new RegisterRequest(name, adminName, phone, introducerId, picPath);
        mRegisterKey = request.getDataKey();
        startTask(request);
    }

    public boolean isRegisterKey(String key) {
        return isKey(key, mRegisterKey);
    }

    public void login(String userName, String password) {
        LoginRequest request = new LoginRequest(userName, password);
        mLoginKey = request.getDataKey();
        startTask(request);
    }

    public boolean isLoginKey(String key) {
        return isKey(key, mLoginKey);
    }

    public void completeInfo(String loginId, String name, String adminName, String phone, String introducerId,
                             String picPath, String pic1Path, String pic2Path, String pic3Path,
                             String payWXPath, String payAlpayPath, String outpatientAddress,
                             String outpatientTel, String adminEmail, String outpatientIntro, boolean isRegister) {
        CompleteInfoRequest request = new CompleteInfoRequest(loginId, name, adminName, phone,
                introducerId, picPath, pic1Path, pic2Path, pic3Path, payWXPath, payAlpayPath,
                outpatientAddress, outpatientTel, adminEmail, outpatientIntro);
        if (isRegister) {
            mCompleteInfoInRegisterKey = request.getDataKey();
        } else {
            mCompleteInfoKey = request.getDataKey();
        }
        startTask(request);
    }

    public boolean isCompleteInfoInRegisterKey(String key) {
        return isKey(key, mCompleteInfoInRegisterKey);
    }

    public boolean isCompleteInfoKey(String key) {
        return isKey(key, mCompleteInfoKey);
    }

    public void updatePassword(String oldPwd, String new1Pwd, String new2Pwd) {
        ModifyPasswordRequest request = new ModifyPasswordRequest(oldPwd, new1Pwd, new2Pwd);
        mUpdatePasswordKey = request.getDataKey();
        startTask(request);
    }

    public boolean isUpdatePasswordKey(String key) {
        return isKey(key, mUpdatePasswordKey);
    }

    public void sendSMS(String tel, String code) {
        SendSMSRequest request = new SendSMSRequest(tel, code);
        mSendCodeKey = request.getDataKey();
        startTask(request);
    }

    public boolean isSendSMSKey(String key) {
        return isKey(key, mSendCodeKey);
    }

    public void logout() {
        UserManager.getInstance().clearUser();
        getContext().sendBroadcast(new Intent(AppBroadcast.ACTION_USER_LOGIN_STATE_CHANGE));
    }

    @Override
    public boolean dispatchSuccess(String key, Object data) {

        if (isLoginKey(key) || isCompleteInfoKey(key)) {
            if (data != null) {
                AppResponseEntity<UserInfo> responseEntity = (AppResponseEntity<UserInfo>) data;
                UserInfo userInfo = responseEntity.getData();
                responseEntity.setMsg(userInfo.getMsg());
                if (userInfo.isSuccess()) {
                    UserManager.getInstance().saveUser(userInfo);
                    notifyDataChange(USER_INFO_CHANGE, userInfo);
                } else {
                    dispatchFail(key, responseEntity);
                    return false;
                }
            }
        }

        boolean result = super.dispatchSuccess(key, data);

        if (isLoginKey(key)) {
            getContext().sendBroadcast(new Intent(AppBroadcast.ACTION_USER_LOGIN_STATE_CHANGE));
        }
        return result;
    }

    @Override
    public boolean dispatchFail(String key, Object data) {
//        if (isSendCodeKey(key)) {
//            ResponseEntity responseEntity = (ResponseEntity) data;
//            ToastUtil.showToast(responseEntity.getMsg());
//        }
        return super.dispatchFail(key, data);
    }
}
