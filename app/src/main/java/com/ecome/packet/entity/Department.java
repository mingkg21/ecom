package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class Department extends BaseEntity {

    @SerializedName("dept_id")
    private int id;

    @SerializedName("dept_cls")
    private int cls;

    @SerializedName("dept_name")
    private String name;

    @SerializedName("fendian_id")
    private int fendianId;

    public int getId() {
        return id;
    }

    public int getCls() {
        return cls;
    }

    public String getName() {
        return name;
    }

    public int getFendianId() {
        return fendianId;
    }
}
