package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class SystemInfo extends BaseEntity {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("database_jiekou")
    private String api;

    @SerializedName("stop_flag")
    private int stopFlag;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public int getStopFlag() {
        return stopFlag;
    }

    public void setStopFlag(int stopFlag) {
        this.stopFlag = stopFlag;
    }
}
