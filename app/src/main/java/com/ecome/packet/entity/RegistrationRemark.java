package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class RegistrationRemark extends BaseEntity {

//    {
//        "chufang_remark_id": 0,
//            "chufang_remark_name": "123123"
//    }

    @SerializedName("chufang_remark_id")
    private int id;

    @SerializedName("chufang_remark_name")
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
