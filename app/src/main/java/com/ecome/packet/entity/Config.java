package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;

public class Config extends AppBaseEntity {

//    {
//        "ID": -22,
//            "CONFIG_id": "-22",
//            "config_code": "weixin_secret:",
//            "config_value": "76",
//            "cls": 0,
//            "remark": ""
//    }

    @SerializedName("ID")
    private int id;

    @SerializedName("CONFIG_id")
    private String configId;

    @SerializedName("cls")
    private int cls;

    @SerializedName("config_code")
    private String code;

    @SerializedName("config_value")
    private String value;

    @SerializedName("remark")
    private String remark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public int getCls() {
        return cls;
    }

    public void setCls(int cls) {
        this.cls = cls;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
