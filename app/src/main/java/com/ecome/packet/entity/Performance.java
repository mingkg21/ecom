package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

/** 业绩
 *
 **/
public class Performance extends BaseEntity {

//    {
//        "fendian_id": 0,
//            "dor_name": "super",
//            "fee_sum": 252.62,
//            "fee_zhekou": 0
//    }

    @SerializedName("fendian_id")
    private int fendianId;

    @SerializedName("dor_name")
    private String doctorName;

    @SerializedName("fee_sum")
    private double feeSum;

    @SerializedName("fee_zhekou")
    private double discount;

    public int getFendianId() {
        return fendianId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public double getFeeSum() {
        return feeSum;
    }

    public double getDiscount() {
        return discount;
    }
}
