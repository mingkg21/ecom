package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class FeeCategory extends BaseEntity {

    @SerializedName("fee_cls_id")
    private int id;

    @SerializedName("fee_cls_name")
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
