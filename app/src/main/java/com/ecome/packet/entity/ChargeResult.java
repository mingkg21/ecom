package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;

public class ChargeResult extends AppBaseEntity {

//    {
//        "pid": 177,
//            "guahao_id": 2336,
//            "fee_sum": 4.760,
//            "oper_time": "2018-08-25 14:05:51.14",
//            "ms_return": "成功执行",
//            "oper_name": "王医生"
//    }

    @SerializedName("pid")
    private int patientId;

    @SerializedName("guahao_id")
    private int registrationId;

    @SerializedName("fee_sum")
    private double feeSum;

    @SerializedName("oper_time")
    private String time;

    @SerializedName("ms_return")
    private String msreturn;

    @SerializedName("oper_name")
    private String operationName;

    public int getPatientId() {
        return patientId;
    }

    public int getRegistrationId() {
        return registrationId;
    }

    public double getFeeSum() {
        return feeSum;
    }

    public String getTime() {
        return time;
    }

    public String getMsreturn() {
        return msreturn;
    }

    public String getOperationName() {
        return operationName;
    }
}
