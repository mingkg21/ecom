package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;

public class PinCi extends ClsEntity {

    @SerializedName("pinci_id")
    private int id;

    @SerializedName("pinci_name")
    private String name;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }
}
