package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;

public class TempDetail extends AppBaseEntity {

//    {
//        "sucess": "T",
//            "dor_id": 152,
//            "temp_id": 8,
//            "code_item_id": 1469,
//            "code_item_name": "熟地黄",
//            "code_item_cls": 1,
//            "code_item_count": 1,
//            "temp_jianfa_id": 0,
//            "temp_jianfa_name": null,
//            "temp_zibei": 0
//    },

    @SerializedName("temp_id")
    private int id;

    @SerializedName("dor_id")
    private int doctorId;

    @SerializedName("code_item_id")
    private int itemId;

    @SerializedName("code_item_name")
    private String itemName;

    @SerializedName("code_item_cls")
    private int itemCls;

    @SerializedName("code_item_count")
    private int itemCount;

    @SerializedName("temp_jianfa_id")
    private int tempJianfaId;

    @SerializedName("temp_jianfa_name")
    private String tempJianfaName;

    @SerializedName("temp_zibei")
    private int tmepZibei;

    private boolean isCheck;

    public int getId() {
        return id;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public int getItemId() {
        return itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public int getItemCls() {
        return itemCls;
    }

    public int getItemCount() {
        return itemCount;
    }

    public int getTempJianfaId() {
        return tempJianfaId;
    }

    public String getTempJianfaName() {
        return tempJianfaName;
    }

    public int getTmepZibei() {
        return tmepZibei;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }
}
