package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class ZiDian extends BaseEntity {

//    {
//        "zidian_code": 1,
//            "zidian_name": "中医诊断维护",
//            "zidian_jibie": 1,
//            "zidian_remark": null,
//            "app_falg": 1
//    },

    @SerializedName("zidian_code")
    private int code;

    @SerializedName("zidian_name")
    private String name;

    @SerializedName("zidian_jibie")
    private int level;

    @SerializedName("zidian_remark")
    private String remark;

    @SerializedName("app_falg")
    private int appFlag;

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public String getRemark() {
        return remark;
    }

    public int getAppFlag() {
        return appFlag;
    }
}
