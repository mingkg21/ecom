package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;

public class Temp extends AppBaseEntity {

//    {
//        "sucess": "T",
//            "dor_id": 152,
//            "temp_id": 23,
//            "temp_name": "病人:王五的处方模板",
//            "create_datetime": "2018-08-21 21:50:41.597",
//            "temp_chuchu": 1,
//            "temp_gongxiao": "手机",
//            "temp_jishu": 1,
//            "temp_zhuzhi": "是你",
//            "temp_remark": ""
//    }

    @SerializedName("temp_id")
    private int id;

    @SerializedName("temp_name")
    private String name;

    @SerializedName("temp_chuchu")
    private int chuchu;

    @SerializedName("temp_gongxiao")
    private String gongxiao;

    @SerializedName("temp_jishu")
    private int useNum;

    @SerializedName("temp_zhuzhi")
    private String zhuzhi;

    @SerializedName("temp_remark")
    private String remark;

    @SerializedName("dor_id")
    private int doctorId;

    @SerializedName("create_datetime")
    private String createDatetime;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getChuchu() {
        return chuchu;
    }

    public String getGongxiao() {
        return gongxiao;
    }

    public int getUseNum() {
        return useNum;
    }

    public String getZhuzhi() {
        return zhuzhi;
    }

    public String getRemark() {
        return remark;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public String getCreateDatetime() {
        return createDatetime;
    }
}
