package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class Staff extends BaseEntity {

//    {
//        "id": 1,
//            "name": "1648",
//            "disabled": 0,
//            "login_name": "1648",
//            "tel": "",
//            "sex": "男",
//            "fendian_id": 0,
//            "zhicheng_id": "中医师",
//            "kanzheng_minute": 25,
//            "dept": 1,
//            "dept_name": "中医内科",
//            "role_name": "系统管理员",
//            "role_id": 0,
//            "people_pic": "脾胃肾、脑梗塞。",
//            "jianjie": "毕业于北京中医药大学"
//    }

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("disabled")
    private int disabled;

    @SerializedName("login_name")
    private String loginName;

    @SerializedName("tel")
    private String tel;

    @SerializedName("sex")
    private String sex;

    @SerializedName("fendian_id")
    private int fendianId;

    @SerializedName("zhicheng_id")
    private String zhichengId;

    @SerializedName("kanzheng_minute")
    private int kanzhengMinute;

    @SerializedName("dept")
    private int deptId;

    @SerializedName("dept_name")
    private String deptName;

    @SerializedName("role_name")
    private String roleName;

    @SerializedName("role_id")
    private int roleId;

    @SerializedName("people_pic")
    private String peoplePic;

    @SerializedName("jianjie")
    private String jianjie;

    @SerializedName("shangchang")
    private String feature;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDisabled() {
        return disabled;
    }

    public void setDisabled(int disabled) {
        this.disabled = disabled;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getFendianId() {
        return fendianId;
    }

    public void setFendianId(int fendianId) {
        this.fendianId = fendianId;
    }

    public String getZhichengId() {
        return zhichengId;
    }

    public void setZhichengId(String zhichengId) {
        this.zhichengId = zhichengId;
    }

    public int getKanzhengMinute() {
        return kanzhengMinute;
    }

    public void setKanzhengMinute(int kanzhengMinute) {
        this.kanzhengMinute = kanzhengMinute;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getPeoplePic() {
        return peoplePic;
    }

    public void setPeoplePic(String peoplePic) {
        this.peoplePic = peoplePic;
    }

    public String getJianjie() {
        return jianjie;
    }

    public void setJianjie(String jianjie) {
        this.jianjie = jianjie;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }
}
