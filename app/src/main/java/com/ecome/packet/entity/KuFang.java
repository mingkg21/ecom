package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class KuFang extends BaseEntity {

    @SerializedName("kufang_id")
    private int id;

    @SerializedName("kufang_name")
    private String name;

    @SerializedName("remark")
    private String remark;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRemark() {
        return remark;
    }
}
