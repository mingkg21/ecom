package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;

public class Voucher extends AppBaseEntity {

//    {
//        "voucher_no": "APP-RK00000814",
//            "kufang_cls": 1,
//            "yewu_cls": 1,
//            "gongyingshang_id": 1,
//            "gongyingshang_name": "三明鹭燕",
//            "queren_flag": 0,
//            "queren_ren_id": null,
//            "queren_ren_name": null,
//            "input_datetime": "2018-09-07 09:37:11.06",
//            "fapiao_no": "的回",
//            "remark": "得不到的",
//            "voucher_cls": 1
//    }

    @SerializedName("voucher_no")
    private String voucherNo;

    @SerializedName("kufang_cls")
    private int kufangId;

    @SerializedName("yewu_cls")
    private int businessId;

    @SerializedName("gongyingshang_id")
    private int gongYingShangId;

    @SerializedName("gongyingshang_name")
    private String gongYingShangName;

    @SerializedName("queren_flag")
    private int queRenFlag;

    @SerializedName("queren_ren_id")
    private String queRenRenId;

    @SerializedName("queren_ren_name")
    private String querenRenName;

    @SerializedName("input_datetime")
    private String inputDatetime;

    @SerializedName("fapiao_no")
    private String faPiaoNo;

    @SerializedName("remark")
    private String remark;

    @SerializedName("voucher_cls")
    private String voucherCls;

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public int getGongYingShangId() {
        return gongYingShangId;
    }

    public void setGongYingShangId(int gongYingShangId) {
        this.gongYingShangId = gongYingShangId;
    }

    public String getGongYingShangName() {
        return gongYingShangName;
    }

    public void setGongYingShangName(String gongYingShangName) {
        this.gongYingShangName = gongYingShangName;
    }

    public int getQueRenFlag() {
        return queRenFlag;
    }

    public void setQueRenFlag(int queRenFlag) {
        this.queRenFlag = queRenFlag;
    }

    public void queRenFlag() {
        queRenFlag = 1;
    }

    public String getQueRenRenId() {
        return queRenRenId;
    }

    public void setQueRenRenId(String queRenRenId) {
        this.queRenRenId = queRenRenId;
    }

    public String getQuerenRenName() {
        return querenRenName;
    }

    public void setQuerenRenName(String querenRenName) {
        this.querenRenName = querenRenName;
    }

    public String getInputDatetime() {
        return inputDatetime;
    }

    public void setInputDatetime(String inputDatetime) {
        this.inputDatetime = inputDatetime;
    }

    public String getFaPiaoNo() {
        return faPiaoNo;
    }

    public void setFaPiaoNo(String faPiaoNo) {
        this.faPiaoNo = faPiaoNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getVoucherCls() {
        return voucherCls;
    }

    public void setVoucherCls(String voucherCls) {
        this.voucherCls = voucherCls;
    }

    public boolean isQueRen() {
        return queRenFlag == 1;
    }

    public int getKufangId() {
        return kufangId;
    }

    public void setKufangId(int kufangId) {
        this.kufangId = kufangId;
    }

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }
}
