package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;

public class Fried extends ClsEntity {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
