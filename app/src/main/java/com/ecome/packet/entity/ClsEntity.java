package com.ecome.packet.entity;

import com.mk.core.entity.BaseEntity;

public abstract class ClsEntity extends BaseEntity {

    public abstract int getId();

    public abstract String getName();
}
