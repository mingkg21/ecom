package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;

public class DiagnoseResult extends AppBaseEntity {

//    "ms": "0",
//            "ret_text": "执行成功！",
//            "ret_guahao_id": 2352,
//            "money_sum": 122.440,
//            "yaoping_fee": 0.420,
//            "daijian_fee": 2.000,
//            "gaofang_fee": 120.000,
//            "zhenjin_fee": 0.020

    @SerializedName("ret_guahao_id")
    private int registrationId;

    @SerializedName("money_sum")
    private double money;

    @SerializedName("yaoping_fee")
    private double drugFee;

    @SerializedName("daijian_fee")
    private double daijianFee;

    @SerializedName("gaofang_fee")
    private double gaofangFee;

    @SerializedName("zhenjin_fee")
    private double diagnoseFee;

    private int patientId;

    public int getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(int registrationId) {
        this.registrationId = registrationId;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public double getDrugFee() {
        return drugFee;
    }

    public void setDrugFee(double drugFee) {
        this.drugFee = drugFee;
    }

    public double getDaijianFee() {
        return daijianFee;
    }

    public void setDaijianFee(double daijianFee) {
        this.daijianFee = daijianFee;
    }

    public double getGaofangFee() {
        return gaofangFee;
    }

    public void setGaofangFee(double gaofangFee) {
        this.gaofangFee = gaofangFee;
    }

    public double getDiagnoseFee() {
        return diagnoseFee;
    }

    public void setDiagnoseFee(double diagnoseFee) {
        this.diagnoseFee = diagnoseFee;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }
}
