package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class Workload extends BaseEntity {

//    {
//        "pid": 177,
//            "zhifu": 0,
//            "guahao_id": 2337,
//            "pat_name": "王五",
//            "price_sum": 15.150,
//            "caozuo_datetime": "2018-08-18 17:32:37.673",
//            "caozuoren_id": 152,
//            "caozuoren": "王医生"
//    },

//    {
//        "fendian_id": 0,
//            "pid": 203,
//            "zhifu": 0,
//            "guahao_id": 4501,
//            "pat_name": "陈病人",
//            "price_sum": 8.78,
//            "caozuo_datetime": "2018-08-31 15:43:03.31",
//            "caozuoren_id": 157,
//            "caozuoren": "super"
//    }

    @SerializedName("caozuoren_id")
    private int id;

    @SerializedName("fendian_id")
    private int fendianId;

    @SerializedName("pid")
    private int patientId;

    @SerializedName("pat_name")
    private String patientName;

    @SerializedName("zhifu")
    private int zhifu;

    @SerializedName("guahao_id")
    private int registrationId;

    @SerializedName("price_sum")
    private double price;

    @SerializedName("caozuo_datetime")
    private String datetime;

    @SerializedName("caozuoren")
    private String doctorName;

    public int getId() {
        return id;
    }

    public int getFendianId() {
        return fendianId;
    }

    public int getPatientId() {
        return patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public int getZhifu() {
        return zhifu;
    }

    public int getRegistrationId() {
        return registrationId;
    }

    public double getPrice() {
        return price;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public boolean hadPay() {
        return zhifu == 1;
    }
}
