package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class Fee extends BaseEntity {

//    {
//        "fendian_id": 0,
//            "fee_cls": "草药",
//            "fee_yingshou": 195.614,
//            "fee_zhekou": 0,
//            "fee_shishou": 195.614
//    }


    @SerializedName("fendian_id")
    private int fendianId;

    @SerializedName("fee_cls")
    private String cls;

    @SerializedName("fee_yingshou")
    private double yingshou;

    @SerializedName("fee_zhekou")
    private double discount;

    @SerializedName("fee_shishou")
    private double shishou;

    public int getFendianId() {
        return fendianId;
    }

    public String getCls() {
        return cls;
    }

    public double getYingshou() {
        return yingshou;
    }

    public double getDiscount() {
        return discount;
    }

    public double getShishou() {
        return shishou;
    }
}
