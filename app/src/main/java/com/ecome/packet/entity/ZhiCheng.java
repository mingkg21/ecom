package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class ZhiCheng extends BaseEntity {

    @SerializedName("zhicheng_id")
    private int id;

    @SerializedName("zhicheng_name")
    private String name;

    @SerializedName("fendian_id")
    private int fendianId;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getFendianId() {
        return fendianId;
    }
}
