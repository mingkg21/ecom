package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class Provider extends BaseEntity {

    @SerializedName("gongyingshang_id")
    private int id;

    @SerializedName("gongyingshang_name")
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
