package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class WorkTimeItem extends BaseEntity {

//    {
//        "dor_date": "2018-09-11周二  上午(剩12)",
//            "id": 5750,
//            "qingjia": 0,
//            "shangban": 1
//    }

    @SerializedName("id")
    private int id;

    @SerializedName("dor_date")
    private String date;

    @SerializedName("qingjia")
    private int qingjia;

    @SerializedName("shangban")
    private int shangban;

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public boolean isQingjia() {
        return qingjia == 1;
    }

    public boolean isShangban() {
        return shangban ==1;
    }
}
