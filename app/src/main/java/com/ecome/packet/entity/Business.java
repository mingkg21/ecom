package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class Business extends BaseEntity {

    @SerializedName("voucher_id")
    private int id;

    @SerializedName("voucher_name")
    private String name;

    @SerializedName("didian")
    private int didian;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getDidian() {
        return didian;
    }
}
