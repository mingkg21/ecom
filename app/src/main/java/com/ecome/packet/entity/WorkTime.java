package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class WorkTime extends BaseEntity {

//    {
//        "id": 157,
//            "fendian_id": 0,
//            "name": "super",
//            "login_name": "super",
//            "login_count": 727,
//            "role_id": 0,
//            "login_disabled": 0,
//            "kanzhen_min": 5,
//            "morning_time1": "1900-01-01 08:30:00",
//            "morning_time2": "1900-01-01 12:00:00",
//            "afternon_time1": "1900-01-01 14:30:00",
//            "afternon_time2": "1900-01-01 17:30:00",
//            "night_time1": "1900-01-01 19:30:00",
//            "night_time2": "1900-01-01 21:30:00"
//    }

    @SerializedName("id")
    private int id;

    @SerializedName("fendian_id")
    private int fendianId;

    @SerializedName("name")
    private String name;

    @SerializedName("login_name")
    private String loginName;

    @SerializedName("login_count")
    private int loginCount;

    @SerializedName("role_id")
    private int roleId;

    @SerializedName("login_disabled")
    private int loginDisabled;

    @SerializedName("kanzhen_min")
    private int kanzhenMin;

    @SerializedName("morning_time1")
    private String morningTimeStart;

    @SerializedName("morning_time2")
    private String morningTimeEnd;

    @SerializedName("afternon_time1")
    private String afternoonTimeStart;

    @SerializedName("afternon_time2")
    private String afternoonTimeEnd;

    @SerializedName("night_time1")
    private String nightTimeStart;

    @SerializedName("night_time2")
    private String nightTimeEnd;

    @SerializedName("paiban_data_max")
    private String paibanDataMax;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFendianId() {
        return fendianId;
    }

    public void setFendianId(int fendianId) {
        this.fendianId = fendianId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public int getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(int loginCount) {
        this.loginCount = loginCount;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getLoginDisabled() {
        return loginDisabled;
    }

    public void setLoginDisabled(int loginDisabled) {
        this.loginDisabled = loginDisabled;
    }

    public int getKanzhenMin() {
        return kanzhenMin;
    }

    public void setKanzhenMin(int kanzhenMin) {
        this.kanzhenMin = kanzhenMin;
    }

    public String getMorningTimeStart() {
        return morningTimeStart;
    }

    public void setMorningTimeStart(String morningTimeStart) {
        this.morningTimeStart = morningTimeStart;
    }

    public String getMorningTimeEnd() {
        return morningTimeEnd;
    }

    public void setMorningTimeEnd(String morningTimeEnd) {
        this.morningTimeEnd = morningTimeEnd;
    }

    public String getAfternoonTimeStart() {
        return afternoonTimeStart;
    }

    public void setAfternoonTimeStart(String afternoonTimeStart) {
        this.afternoonTimeStart = afternoonTimeStart;
    }

    public String getAfternoonTimeEnd() {
        return afternoonTimeEnd;
    }

    public void setAfternoonTimeEnd(String afternoonTimeEnd) {
        this.afternoonTimeEnd = afternoonTimeEnd;
    }

    public String getNightTimeStart() {
        return nightTimeStart;
    }

    public void setNightTimeStart(String nightTimeStart) {
        this.nightTimeStart = nightTimeStart;
    }

    public String getNightTimeEnd() {
        return nightTimeEnd;
    }

    public void setNightTimeEnd(String nightTimeEnd) {
        this.nightTimeEnd = nightTimeEnd;
    }

    public String getPaibanDataMax() {
        return paibanDataMax;
    }

    public void setPaibanDataMax(String paibanDataMax) {
        this.paibanDataMax = paibanDataMax;
    }
}
