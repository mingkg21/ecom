package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class Unit extends BaseEntity {

    @SerializedName("unit_id")
    private int id;

    @SerializedName("unit_name")
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
