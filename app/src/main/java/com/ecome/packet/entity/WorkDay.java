package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class WorkDay extends BaseEntity {

//    {
//        "id": 99,
//            "dor_id": 152,
//            "morning": 1,
//            "afternoon": 1,
//            "night": 1,
//            "weeken": "星期一",
//            "remark": "0"
//    }

    @SerializedName("id")
    private int id;

    @SerializedName("dor_id")
    private int doctorId;

    @SerializedName("morning")
    private int morning;

    @SerializedName("afternoon")
    private int afternoon;

    @SerializedName("night")
    private int night;

    @SerializedName("weeken")
    private String week;

    @SerializedName("remark")
    private String remark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public boolean isMorning() {
        return morning == 1;
    }

    public void setMorning(int morning) {
        this.morning = morning;
    }

    public boolean isAfternoon() {
        return afternoon == 1;
    }

    public void setAfternoon(int afternoon) {
        this.afternoon = afternoon;
    }

    public boolean isNight() {
        return night == 1;
    }

    public void setNight(int night) {
        this.night = night;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getWeekDay() {
        if ("星期一".equals(week)) {
            return 1;
        } else if ("星期二".equals(week)) {
            return 2;
        } else if ("星期三".equals(week)) {
            return 3;
        } else if ("星期四".equals(week)) {
            return 4;
        } else if ("星期五".equals(week)) {
            return 5;
        } else if ("星期六".equals(week)) {
            return 6;
        }
        return 7;
    }
}
