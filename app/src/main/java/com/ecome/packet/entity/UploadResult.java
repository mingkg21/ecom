package com.ecome.packet.entity;

import com.mk.core.entity.BaseEntity;

/**
 * Created by mingkg21 on 2017/11/12.
 */

public class UploadResult extends BaseEntity {

    private static final long serialVersionUID = -2016783190243373749L;

    private boolean flag;
    private String msg;

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
