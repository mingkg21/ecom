package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class Role extends BaseEntity {

    @SerializedName("id")
    private int id;

    @SerializedName("role_name")
    private String name;

    @SerializedName("fendian_id")
    private int fendianId;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getFendianId() {
        return fendianId;
    }
}
