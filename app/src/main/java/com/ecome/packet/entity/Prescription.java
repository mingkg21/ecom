package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class Prescription extends BaseEntity {

    @SerializedName("id")
    private int id;

    @SerializedName("chufang_no")
    private String no;

    @SerializedName("chufang_cls")
    private int cls;

    @SerializedName("name")
    private String name;

    @SerializedName("yongliang")
    private float dosage;

    @SerializedName("unit")
    private String unit;

    @SerializedName("input_datetime")
    private String inputDatetime;

    @SerializedName("charge_datetime")
    private String chargeDatetime;

    @SerializedName("drugId")
    private int drugId;

    @SerializedName("jishu")
    private int count;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public int getCls() {
        return cls;
    }

    public void setCls(int cls) {
        this.cls = cls;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getDosage() {
        return dosage;
    }

    public void setDosage(float dosage) {
        this.dosage = dosage;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getInputDatetime() {
        return inputDatetime;
    }

    public void setInputDatetime(String inputDatetime) {
        this.inputDatetime = inputDatetime;
    }

    public String getChargeDatetime() {
        return chargeDatetime;
    }

    public void setChargeDatetime(String chargeDatetime) {
        this.chargeDatetime = chargeDatetime;
    }

    public int getDrugId() {
        return drugId;
    }

    public void setDrugId(int drugId) {
        this.drugId = drugId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
