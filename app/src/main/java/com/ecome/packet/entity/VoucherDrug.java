package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class VoucherDrug extends BaseEntity {

//    {
//        "voucher_no": "APP-RK00000718",
//            "code_item_id": 1005,
//            "code_item_name": "艾叶",
//            "code_item_count": 12.12,
//            "ruku_price": 1.45,
//            "ruku_price_sum": 17.574,
//            "code_item_pihao": "20180722-001"
//    }

    @SerializedName("voucher_no")
    private String voucherNo;

    @SerializedName("code_item_id")
    private int id;

    @SerializedName("code_item_name")
    private String name;

    @SerializedName("code_item_count")
    private String count;

    @SerializedName("ruku_price")
    private String price;

    @SerializedName("ruku_price_sum")
    private String priceSum;

    @SerializedName("code_item_pihao")
    private String pihao;

    public String getVoucherNo() {
        return voucherNo;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCount() {
        return count;
    }

    public String getPrice() {
        return price;
    }

    public String getPriceSum() {
        return priceSum;
    }

    public String getPihao() {
        return pihao;
    }
}
