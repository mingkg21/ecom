package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class AppBaseEntity extends BaseEntity {

    @SerializedName("ret_string")
    private String msg;

    @SerializedName("success")
    private String success;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isSuccess() {
        return "T".equals(success);
    }
}
