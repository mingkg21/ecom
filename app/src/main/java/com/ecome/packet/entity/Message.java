package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

/**
 * Created by mingkg21 on 2017/11/28.
 */

public class Message extends BaseEntity {

    private static final long serialVersionUID = -6197564190391670227L;

    @SerializedName("sm_id")
    private int id;

    @SerializedName("sm_title")
    private String title;

    @SerializedName("sm_content")
    private String content;

    @SerializedName("sm_addtime")
    private long time;

    @SerializedName("sm_isshow")
    private int isShow;

    @SerializedName("sm_isread")
    private int isRead;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public long getTime() {
        return time * 1000;
    }

    public boolean isRead() {
        return isRead == 1;
    }
}
