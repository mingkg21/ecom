package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;

public class Patient extends AppBaseEntity {

    @SerializedName("pid")
    private int id;

    @SerializedName("pat_name")
    private String name;

    @SerializedName("pat_sex")
    private String sex;

    @SerializedName("pat_age")
    private String age;

    @SerializedName("pat_tel")
    private String tel;

    @SerializedName("pat_merry")
    private String merry;

    @SerializedName("reg_datetime")
    private String datetime;

    @SerializedName("remark")
    private String remark;

    @SerializedName(value = "home_addr", alternate = "pat_address")
    private String address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getMerry() {
        return merry;
    }

    public void setMerry(String merry) {
        this.merry = merry;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isMan() {
        return "男".equals(sex);
    }
}
