package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class LogInfo extends BaseEntity {

//    {
//        "log_id": 4328,
//            "login_name": "super",
//            "login_time": "2017-12-06 13:47:40.097",
//            "login_ip": "WIN-3OPPCNIEU1J",
//            "exit_time": "2017-12-06 14:10:52.017",
//            "remark": "  【易聪软件Ver.20171118】  1.8_系统日志及客户端  8.4_挂号相关统计报表  1.3_系统人员管理  1.5_字典维护与设置  1.8_系统日志及客户端  1.4_收费项目维护  1.4_收费项目维护  1.5_字典维护与设置  1.4_收费项目维护",
//            "mac_address": "WIN-3OPPCNIEU1J",
//            "log_cls": 0,
//            "computer_name": "WIN-3OPPCNIEU1J",
//            "pid": null
//    }

    @SerializedName("log_id")
    private int id;

    @SerializedName("login_name")
    private String loginName;

    @SerializedName("login_time")
    private String loginTime;

    @SerializedName("login_ip")
    private String loginIp;

    @SerializedName("exit_time")
    private String exitTime;

    @SerializedName("remark")
    private String remark;

    @SerializedName("mac_address")
    private String macAddress;

    @SerializedName("computer_name")
    private String computerName;

    @SerializedName("log_cls")
    private int logCls;

    public int getId() {
        return id;
    }

    public String getLoginName() {
        return loginName;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public String getExitTime() {
        return exitTime;
    }

    public String getRemark() {
        return remark;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public String getComputerName() {
        return computerName;
    }

    public int getLogCls() {
        return logCls;
    }
}
