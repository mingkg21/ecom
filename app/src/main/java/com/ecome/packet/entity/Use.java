package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;

public class Use extends ClsEntity {

    @SerializedName("yongfa_id")
    private int id;

    @SerializedName("yongfa_name")
    private String name;

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }
}
