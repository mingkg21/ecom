package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mingkg21 on 2017/8/25.
 */

public class UserInfo extends AppBaseEntity {

    private static final long serialVersionUID = -1737345921561320508L;

    @SerializedName("login_state")
    private String loginState;

    @SerializedName("login_id")
    private int loginId;

    @SerializedName("role_id")
    private int roleId;

    @SerializedName("role_name")
    private String roleName;

    @SerializedName("name")
    private String name;

    @SerializedName("tel")
    private String tel;

    @SerializedName("sex")
    private String sex;

    @SerializedName(value = "login_name", alternate = "admin_name")
    private String loginName;

    @SerializedName("admin_tel")
    private String phone;

    @SerializedName("dept_id")
    private int departmentId;

    @SerializedName("dept_name")
    private String departmentName;

    @SerializedName("zhicheng_id")
    private String zhicheng;

    @SerializedName("pic_head")
    private String picHead;

    @SerializedName("jianjie")
    private String jianjie;

    @SerializedName("shangchang")
    private String shangchang;

    @SerializedName("kanzheng_minute")
    private int kanzhengMinute;

    @SerializedName("reg_datetime")
    private String regDatetime;

    @SerializedName("dead_datetime")
    private String deadDatetime;

    @SerializedName("zhenjin")
    private String zhenjin;

    @SerializedName("fendian_id")
    private int fendDianId;

    @SerializedName("fendian_name")
    private String fendianName;

    @SerializedName("fendian_address")
    private String fendianAddress;

    @SerializedName("fendian_yuyue_tel")
    private String fendianYuyueTel;

    @SerializedName("fendian_jianjie")
    private String fendianJianjie;

    @SerializedName("fendian_address2")
    private String fendianAddress2;

    @SerializedName("app_url")
    private String appUrl;

    @SerializedName("yuyue_url")
    private String yuyueUrl;

    @SerializedName("pic_0")
    private String pic0;

    @SerializedName("pic_1")
    private String pic1;

    @SerializedName("pic_2")
    private String pic2;

    @SerializedName("pic_3")
    private String pic3;

    @SerializedName("pic_weixin")
    private String picWeixin;

    @SerializedName("pic_zhifubao")
    private String picAlipay;

    @SerializedName("fee_sum")
    private double feeSum;

    @SerializedName("fee_month_sum")
    private double feeMonthSum;

    @SerializedName("fee_today_sum")
    private double feeTodaySum;

    @SerializedName("fee_sum_self")
    private double feeSumSelf;

    @SerializedName("fee_month_sum_self")
    private double feeMonthSumSelf;

    @SerializedName("fee_today_sum_self")
    private double feeTodaySumSelf;

    public String getLoginState() {
        return loginState;
    }

    public void setLoginState(String loginState) {
        this.loginState = loginState;
    }

    public int getLoginId() {
        return loginId;
    }

    public void setLoginId(int loginId) {
        this.loginId = loginId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getZhicheng() {
        return zhicheng;
    }

    public void setZhicheng(String zhicheng) {
        this.zhicheng = zhicheng;
    }

    public String getPicHead() {
        return picHead;
    }

    public void setPicHead(String picHead) {
        this.picHead = picHead;
    }

    public String getJianjie() {
        return jianjie;
    }

    public void setJianjie(String jianjie) {
        this.jianjie = jianjie;
    }

    public String getShangchang() {
        return shangchang;
    }

    public void setShangchang(String shangchang) {
        this.shangchang = shangchang;
    }

    public int getKanzhengMinute() {
        return kanzhengMinute;
    }

    public void setKanzhengMinute(int kanzhengMinute) {
        this.kanzhengMinute = kanzhengMinute;
    }

    public String getRegDatetime() {
        return regDatetime;
    }

    public void setRegDatetime(String regDatetime) {
        this.regDatetime = regDatetime;
    }

    public String getDeadDatetime() {
        return deadDatetime;
    }

    public void setDeadDatetime(String deadDatetime) {
        this.deadDatetime = deadDatetime;
    }

    public String getZhenjin() {
        return zhenjin;
    }

    public void setZhenjin(String zhenjin) {
        this.zhenjin = zhenjin;
    }

    public int getFendDianId() {
        return fendDianId;
    }

    public void setFendDianId(int fendDianId) {
        this.fendDianId = fendDianId;
    }

    public String getFendianName() {
        return fendianName;
    }

    public void setFendianName(String fendianName) {
        this.fendianName = fendianName;
    }

    public String getFendianAddress() {
        return fendianAddress;
    }

    public void setFendianAddress(String fendianAddress) {
        this.fendianAddress = fendianAddress;
    }

    public String getFendianYuyueTel() {
        return fendianYuyueTel;
    }

    public void setFendianYuyueTel(String fendianYuyueTel) {
        this.fendianYuyueTel = fendianYuyueTel;
    }

    public String getFendianJianjie() {
        return fendianJianjie;
    }

    public void setFendianJianjie(String fendianJianjie) {
        this.fendianJianjie = fendianJianjie;
    }

    public String getFendianAddress2() {
        return fendianAddress2;
    }

    public void setFendianAddress2(String fendianAddress2) {
        this.fendianAddress2 = fendianAddress2;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public String getYuyueUrl() {
        return yuyueUrl;
    }

    public void setYuyueUrl(String yuyueUrl) {
        this.yuyueUrl = yuyueUrl;
    }

    public String getPic0() {
        return pic0;
    }

    public void setPic0(String pic0) {
        this.pic0 = pic0;
    }

    public String getPic1() {
        return pic1;
    }

    public void setPic1(String pic1) {
        this.pic1 = pic1;
    }

    public String getPic2() {
        return pic2;
    }

    public void setPic2(String pic2) {
        this.pic2 = pic2;
    }

    public String getPic3() {
        return pic3;
    }

    public void setPic3(String pic3) {
        this.pic3 = pic3;
    }

    public double getFeeSum() {
        return feeSum;
    }

    public void setFeeSum(double feeSum) {
        this.feeSum = feeSum;
    }

    public double getFeeMonthSum() {
        return feeMonthSum;
    }

    public void setFeeMonthSum(double feeMonthSum) {
        this.feeMonthSum = feeMonthSum;
    }

    public double getFeeTodaySum() {
        return feeTodaySum;
    }

    public void setFeeTodaySum(double feeTodaySum) {
        this.feeTodaySum = feeTodaySum;
    }

    public double getFeeSumSelf() {
        return feeSumSelf;
    }

    public void setFeeSumSelf(double feeSumSelf) {
        this.feeSumSelf = feeSumSelf;
    }

    public double getFeeMonthSumSelf() {
        return feeMonthSumSelf;
    }

    public void setFeeMonthSumSelf(double feeMonthSumSelf) {
        this.feeMonthSumSelf = feeMonthSumSelf;
    }

    public double getFeeTodaySumSelf() {
        return feeTodaySumSelf;
    }

    public void setFeeTodaySumSelf(double feeTodaySumSelf) {
        this.feeTodaySumSelf = feeTodaySumSelf;
    }

    public String getPicWeixin() {
        return picWeixin;
    }

    public void setPicWeixin(String picWeixin) {
        this.picWeixin = picWeixin;
    }

    public String getPicAlipay() {
        return picAlipay;
    }

    public void setPicAlipay(String picAlipay) {
        this.picAlipay = picAlipay;
    }
}
