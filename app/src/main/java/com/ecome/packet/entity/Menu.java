package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class Menu extends BaseEntity {

    @SerializedName("id")
    private int id;

    @SerializedName("role_id")
    private int roleId;

    @SerializedName("menu_name")
    private String name;

    @SerializedName("menu_name_identi")
    private String nameId;

    @SerializedName("menu_level")
    private int level;

    @SerializedName("fendian_id")
    private int fendDianId;

    @SerializedName("level_pre")
    private int levelPre;

    @SerializedName("remark")
    private String remark;

    @SerializedName("pic_path")
    private String picPath;

    @SerializedName("pic")
    private String pic;

    @SerializedName("login_falg")
    private int loginFalg;

    public int getId() {
        return id;
    }

    public int getRoleId() {
        return roleId;
    }

    public String getName() {
        return name;
    }

    public String getNameId() {
        return nameId;
    }

    public int getLevel() {
        return level;
    }

    public int getFendDianId() {
        return fendDianId;
    }

    public int getLevelPre() {
        return levelPre;
    }

    public String getRemark() {
        return remark;
    }

    public String getPicPath() {
        return picPath;
    }

    public String getPic() {
        return pic;
    }

    public int getLoginFalg() {
        return loginFalg;
    }
}
