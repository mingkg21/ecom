package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class Registration extends BaseEntity {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("tel")
    private String tel;

    @SerializedName("sex")
    private String sex;

    @SerializedName("age")
    private String age;

    @SerializedName("chufuzhen")
    private String state;

    @SerializedName(value = "dor_name", alternate = "guahao_doctor")
    private String doctorName;

    @SerializedName("dor_dept")
    private String doctorDepament;

    @SerializedName("guahao_datetime")
    private String datetime;

    @SerializedName("pid")
    private int patientId;

    @SerializedName("pat_name")
    private String patientName;

    @SerializedName("pat_tel")
    private String patientTel;

    @SerializedName("pat_address")
    private String patientAddress;

    @SerializedName("jiuzhen_datetime")
    private String jiuzhenDatetime;

    @SerializedName("zhenduan_1")
    private String zhenduan1;

    @SerializedName("zhiye")
    private String zhiye;

    @SerializedName("zhenduan_2")
    private String zhenduan2;

    @SerializedName("fendian_id")
    private String fendianId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDoctorDepament() {
        return doctorDepament;
    }

    public void setDoctorDepament(String doctorDepament) {
        this.doctorDepament = doctorDepament;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientTel() {
        return patientTel;
    }

    public void setPatientTel(String patientTel) {
        this.patientTel = patientTel;
    }

    public String getPatientAddress() {
        return patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getJiuzhenDatetime() {
        return jiuzhenDatetime;
    }

    public String getZhenduan1() {
        return zhenduan1;
    }

    public String getZhiye() {
        return zhiye;
    }

    public String getZhenduan2() {
        return zhenduan2;
    }

    public String getFendianId() {
        return fendianId;
    }
}
