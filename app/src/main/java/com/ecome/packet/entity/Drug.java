package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.BaseEntity;

public class Drug extends BaseEntity {

//    {
//        "code_item_id": 1196,
//            "fee_cls_name": "中草药",
//            "cls_id": 1,
//            "name": "党参",
//            "guige": "1g",
//            "price": 0.345,
//            "unit": "1",
//            "yaoping_kucun": 9242,
//            "py": "DS",
//            "use_times": 55367,
//            "wb": "IC",
//            "fendian_id": 0,
//            "disabled": 0,
//            "login_id": 157
//    }

    @SerializedName(value = "id", alternate = "code_item_id")
    private int id;

    @SerializedName(value = "cls", alternate = "cls_id")
    private int cls;

    @SerializedName("fee_cls_name")
    private String feeClsName;

    @SerializedName("name")
    private String name;

    @SerializedName("guige")
    private String guige;

    @SerializedName("price")
    private double price;

    @SerializedName("unit")
    private String unit;

    @SerializedName("yaoping_kucun")
    private double stock;

    @SerializedName("py")
    private String py;

    @SerializedName("use_times")
    private int useTimes;

    @SerializedName("wb")
    private String wb;

    @SerializedName("fendian_id")
    private int fendianId;

    @SerializedName("login_id")
    private int loginId;

    @SerializedName("disabled")
    private int disabled;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCls() {
        return cls;
    }

    public void setCls(int cls) {
        this.cls = cls;
    }

    public String getFeeClsName() {
        return feeClsName;
    }

    public void setFeeClsName(String feeClsName) {
        this.feeClsName = feeClsName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGuige() {
        return guige;
    }

    public void setGuige(String guige) {
        this.guige = guige;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getStock() {
        return stock;
    }

    public void setStock(double stock) {
        this.stock = stock;
    }

    public String getPy() {
        return py;
    }

    public void setPy(String py) {
        this.py = py;
    }

    public int getUseTimes() {
        return useTimes;
    }

    public void setUseTimes(int useTimes) {
        this.useTimes = useTimes;
    }

    public String getWb() {
        return wb;
    }

    public void setWb(String wb) {
        this.wb = wb;
    }

    public int getFendianId() {
        return fendianId;
    }

    public void setFendianId(int fendianId) {
        this.fendianId = fendianId;
    }

    public int getLoginId() {
        return loginId;
    }

    public void setLoginId(int loginId) {
        this.loginId = loginId;
    }

    public int getDisabled() {
        return disabled;
    }

    public void setDisabled(int disabled) {
        this.disabled = disabled;
    }
}
