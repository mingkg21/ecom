package com.ecome.packet.entity;

import com.google.gson.annotations.SerializedName;
import com.mk.core.entity.AbsResponseEntity;

import java.io.Serializable;

/** 请求数据响应类
 * Created by mingkg21 on 2017/8/25.
 */

public class AppResponseEntity<Data> extends AbsResponseEntity<Data> {

    private static final long serialVersionUID = -4378844811166132960L;

    @SerializedName("err")
    private String msg;

    @SerializedName("success")
    private boolean success;

    @SerializedName("resultset")
    private Data data;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public Data getData() {
        return data;
    }

    @Override
    public boolean isSuccess() {
        return success;
    }

}
