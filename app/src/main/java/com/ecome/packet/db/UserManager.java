package com.ecome.packet.db;


import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.ecome.packet.entity.UserInfo;
import com.google.gson.Gson;
import com.mk.core.app.AppBroadcast;
import com.mk.core.app.BaseApp;
import com.mk.core.db.AbsPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class UserManager extends AbsPreferences {

    private static final String KEY_USER_INFO = "key_id";

    private static UserManager mInstance;

    private UserInfo mUserInfo;

    private UserManager() {

    }

    public static UserManager getInstance() {
        if (mInstance == null) {
            mInstance = new UserManager();
        }
        return mInstance;
    }

    @Override
    protected String getFileName() {
        return "USER";
    }

    public boolean isLogin() {
        return getUserId() != 0;
    }

    public int getUserId() {
        UserInfo userInfo = getUserInfo();
        if (userInfo != null) {
            return userInfo.getLoginId();
        }
        return 0;
    }

    public boolean saveUser(UserInfo userInfo) {
        if (userInfo == null) {
            return false;
        }

        mUserInfo = userInfo;

        SharedPreferences.Editor editor = getEdit();
        Gson gson = new Gson();
        String json = gson.toJson(userInfo);
        editor.putString(KEY_USER_INFO, json);

        return editor.commit();
    }

    public UserInfo getUserInfo() {
        if (mUserInfo == null) {
            String json = getSharedPreferences().getString(KEY_USER_INFO, "");
            if (!TextUtils.isEmpty(json)) {
                Gson gson = new Gson();
                mUserInfo = gson.fromJson(json, UserInfo.class);
            }
        }
        return mUserInfo;
    }

    public List<String> getPics() {
        UserInfo userInfo = getUserInfo();
        if (userInfo != null) {
            ArrayList<String> pics = new ArrayList<>();
            if (!TextUtils.isEmpty(userInfo.getPic1())) {
                pics.add(userInfo.getPic1());
            }
            if (!TextUtils.isEmpty(userInfo.getPic2())) {
                pics.add(userInfo.getPic2());
            }
            if (!TextUtils.isEmpty(userInfo.getPic3())) {
                pics.add(userInfo.getPic3());
            }
            return pics;
        }
        return null;
    }

    public String getWXPayUrl() {
        UserInfo userInfo = getUserInfo();
        if (userInfo != null) {
            return userInfo.getPicWeixin();
        }
        return null;
    }
    public String getAliPayUrl() {
        UserInfo userInfo = getUserInfo();
        if (userInfo != null) {
            return userInfo.getPicAlipay();
        }
        return null;
    }

    public void logout() {
        clearUser();
        BaseApp.getInstance().sendBroadcast(new Intent(AppBroadcast.ACTION_USER_LOGIN_STATE_CHANGE));
    }

    public boolean clearUser() {
        mUserInfo = null;

        SharedPreferences.Editor editor = getEdit();
//        editor.putString(KEY_USER_INFO, "");
        editor.clear();
        return editor.commit();
    }

}
