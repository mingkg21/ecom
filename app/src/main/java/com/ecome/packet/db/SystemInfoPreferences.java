package com.ecome.packet.db;

import android.text.TextUtils;

import com.ecome.packet.entity.SystemInfo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mk.core.db.AbsPreferences;

import java.util.ArrayList;
import java.util.List;

public class SystemInfoPreferences extends AbsPreferences {

    private static final String KEY_SYSTEM_INFOS = "KEY_SYSTEM_INFOS";
    private static final String KEY_SYSTEM_INFO_ID_CHOICE = "KEY_SYSTEM_INFO_ID_CHOICE";

    private static SystemInfoPreferences instance;

    private ArrayList<SystemInfo> mSystemInfos;

    private SystemInfoPreferences() {
        mSystemInfos = new ArrayList<>();
    }

    public static SystemInfoPreferences getInstance() {
        if (instance == null) {
            synchronized (SystemInfoPreferences.class) {
                if (instance == null) {
                    instance = new SystemInfoPreferences();
                }
            }
        }
        return instance;
    }

    @Override
    protected String getFileName() {
        return "SYSTEM_INFO";
    }

    public String getBaseApiUrl() {
        return "http://cxc3561713.gnway.cc:28199/restful/rpc";
    }

    public String getApiUrl() {
        SystemInfo systemInfo = getChoiceSystemInfo();
        if (systemInfo != null) {
            return systemInfo.getApi();
        }
        return getBaseApiUrl();
    }

    public SystemInfo getChoiceSystemInfo() {
        mSystemInfos = getSystemInfos();
        int choiceId = getInt(KEY_SYSTEM_INFO_ID_CHOICE);
        if (mSystemInfos != null && !mSystemInfos.isEmpty()) {
            for (SystemInfo info : mSystemInfos) {
                if (info.getId() == choiceId) {
                    return info;
                }
            }
            return mSystemInfos.get(0);
        }
        return null;
    }

    public void choiceSystemInfo(int id) {
        putInt(KEY_SYSTEM_INFO_ID_CHOICE, id);
    }

    public void save(ArrayList<SystemInfo> infos) {
        mSystemInfos.clear();
        mSystemInfos.addAll(infos);
        Gson gson = new Gson();
        String str = gson.toJson(infos);
        putString(KEY_SYSTEM_INFOS, str);
    }

    public ArrayList<SystemInfo> getSystemInfos() {
        if (mSystemInfos.isEmpty()) {
            String str = getString(KEY_SYSTEM_INFOS);
            if (!TextUtils.isEmpty(str)) {
                Gson gson = new Gson();
                List<SystemInfo> jsonListObject = gson.fromJson(str, new TypeToken<List<SystemInfo>>() {
                }.getType());
                mSystemInfos.addAll(jsonListObject);
            }
        }
        return mSystemInfos;
    }

}
