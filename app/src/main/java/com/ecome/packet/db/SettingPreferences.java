package com.ecome.packet.db;

import com.mk.core.app.BaseApp;
import com.mk.core.db.AbsPreferences;
import com.mk.core.util.AppUtil;

public class SettingPreferences extends AbsPreferences {

    private static final String KEY_FEATURE = "key_feature";

    private static SettingPreferences instance;

    private SettingPreferences() {

    }

    public static SettingPreferences getInstance() {

        if (instance == null) {
            synchronized (SettingPreferences.class) {
                if (instance == null) {
                    instance = new SettingPreferences();
                }
            }
        }

        return instance;
    }

    @Override
    protected String getFileName() {
        return "setting";
    }

    public boolean isShowFeature() {
        int currentVersion = AppUtil.getApkVersionCode(BaseApp.getInstance());
        return currentVersion != getInt(KEY_FEATURE);
    }

    public void setShowFeature() {
        int currentVersion = AppUtil.getApkVersionCode(BaseApp.getInstance());
        putInt(KEY_FEATURE, currentVersion);
    }

}
