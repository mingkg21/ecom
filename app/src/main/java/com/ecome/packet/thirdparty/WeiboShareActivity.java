package com.ecome.packet.thirdparty;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.mk.core.util.ToastUtil;

/**
 * 微博分享
 */
public class WeiboShareActivity extends Activity {

    public static final String SHARE_ID    = "id";
    public static final String SHARE_TITLE    = "title";
    public static final String SHARE_CONTENT  = "content";
    public static final String SHARE_URL      = "url";
    public static final String SHARE_ICON_URL = "iconUrl";

    protected MyUiListener mMyUiListener;
    protected String mCommentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WeiBoUtils.getInst().install(this);
        WeiBoUtils.getInst().register(this);
        if (!WeiBoUtils.getInst().isWbAppInstalled()) {
            ToastUtil.showToast("未安装微博");
            finish();
            return;
        }

        mCommentId = getIntent().getStringExtra(SHARE_ID);
        String title = getIntent().getStringExtra(SHARE_TITLE);
        String content = getIntent().getStringExtra(SHARE_CONTENT);
        String url = getIntent().getStringExtra(SHARE_URL);
        String iconUrl = getIntent().getStringExtra(SHARE_ICON_URL);
        if (TextUtils.isEmpty(title)) {
            ShareUtils.shareToWeiBoImg(this, iconUrl);
        } else {
            ShareUtils.shareToWeiBo(this, title, content, url, iconUrl);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        doResultIntent(data);
    }

    protected MyUiListener getMyUiListener() {
        if (mMyUiListener == null) {
            mMyUiListener = new MyUiListener(this);
        }
        mMyUiListener.setCommentId(mCommentId);
        return mMyUiListener;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        doResultIntent(intent);
    }

    protected void doResultIntent(Intent data) {
        WeiBoUtils.getInst().doResultIntent(this, data, getMyUiListener());
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
