package com.ecome.packet.thirdparty;

public interface ThreePartAction {

    /**
	* 成功返回的监听
	*
	* @param token
	*/
    void onThreePartActionSuccess(String token, String openId);

    /**
	* 失败返回的监听
	*/
    void onThreePartActionFail();

    /**
	* 取消返回的监听
	*/
    void onThreePartActionCancel();
}
