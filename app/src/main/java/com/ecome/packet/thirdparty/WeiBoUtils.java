package com.ecome.packet.thirdparty;

import android.app.Activity;
import android.content.Intent;

import com.mk.core.app.BaseApp;
import com.mk.core.util.AppUtil;
import com.ecome.packet.BuildConfig;
import com.sina.weibo.sdk.WbSdk;
import com.sina.weibo.sdk.api.WeiboMultiMessage;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.share.WbShareCallback;
import com.sina.weibo.sdk.share.WbShareHandler;

/**
 * 新浪微博
 */
public class WeiBoUtils {

    private static final String TAG = WeiBoUtils.class.getName();
    public static final String APP_KEY = BuildConfig.WEIBO_APP_KEY;
    public static final String REDIRECT_URL = "https://api.weibo.com/oauth2/default.htm";
    public static final String SCOPE = "all";

    private static WeiBoUtils mInst = null;
    protected AuthInfo mAuthInfo;
    private WbShareHandler mWbShareHandler;

    private WeiBoUtils() {
    }

    public void install(Activity activity) {
        if (mAuthInfo == null) {
            mAuthInfo = new AuthInfo(activity, APP_KEY, REDIRECT_URL, SCOPE);
            WbSdk.install(activity, mAuthInfo);
        }
    }

    public static WeiBoUtils getInst() {
        synchronized (WeiBoUtils.class) {
            if (mInst == null) {
                mInst = new WeiBoUtils();
            }
        }
        return mInst;
    }

    public void register(Activity activity) {
        mWbShareHandler = new WbShareHandler(activity);
        mWbShareHandler.registerApp();
    }

    public boolean isWbAppInstalled() {
        return AppUtil.isInstalled(BaseApp.getInstance(), "com.sina.weibo");
    }

    public void shareMessage(Activity activity, WeiboMultiMessage weiboMessage) {
        getWbShareHandler(activity).shareMessage(weiboMessage, true);
    }

    public void doResultIntent(Activity activity, Intent intent, WbShareCallback wbShareCallback) {
        getWbShareHandler(activity).doResultIntent(intent, wbShareCallback);
    }

    private WbShareHandler getWbShareHandler(Activity activity) {
        if (mWbShareHandler == null) {
            register(activity);
        }
        return mWbShareHandler;
    }

}
