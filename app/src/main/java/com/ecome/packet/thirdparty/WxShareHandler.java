package com.ecome.packet.thirdparty;

import android.content.Context;

import com.mk.core.util.LogUtil;
import com.mk.core.util.ToastUtil;
import com.tencent.mm.sdk.modelbase.BaseResp;

public class WxShareHandler {

    private static final String TAG = WxShareHandler.class.getName();
    private Context mContext;
    private String mCommentId;

    public WxShareHandler(Context context) {
        mContext = context;
    }

    public void onResp(BaseResp baseResp) {
        LogUtil.i(TAG, "onResp");
        switch (baseResp.errCode) {
            case BaseResp.ErrCode.ERR_OK:
                LogUtil.i(TAG, "ERR_OK");
                ToastUtil.showToast("分享成功~");
                onShareSuccess();
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
                LogUtil.i(TAG, "ERR_USER_CANCEL");
                ToastUtil.showToast("分享取消~");
                onShareCancel();
                break;
            case BaseResp.ErrCode.ERR_AUTH_DENIED:
            default:
                LogUtil.i(TAG, "ERR_AUTH_DENIED");
                ToastUtil.showToast("分享失败~");
                onShareFail();
                break;
        }
    }

    public void setCommentId(String commentId) {
        mCommentId = commentId;
    }

    public void onShareSuccess() {
    }

    public void onShareFail() {

    }

    public void onShareCancel() {

    }


}
