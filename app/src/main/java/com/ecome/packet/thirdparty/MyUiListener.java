package com.ecome.packet.thirdparty;

import android.content.Context;

import com.mk.core.util.ToastUtil;
import com.sina.weibo.sdk.share.WbShareCallback;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;

public class MyUiListener implements IUiListener, WbShareCallback {

    private Context mContext;
    private String mCommentId;

    public MyUiListener(Context context) {
	   mContext = context.getApplicationContext();
    }

    public void setCommentId(String commentId) {
        mCommentId = commentId;
    }

    @Override
    public void onError(UiError uiError) {
        ToastUtil.showToast("分享失败~");
    }

    @Override
    public void onComplete(Object object) {
	   ToastUtil.showToast( "分享成功~");
    }

    @Override
    public void onCancel() {
        ToastUtil.showToast("分享取消~");
    }


    //----------------weibo回调

    @Override
    public void onWbShareSuccess() {
        ToastUtil.showToast("分享成功~");
    }

    @Override
    public void onWbShareCancel() {
        ToastUtil.showToast("分享取消~");
    }

    @Override
    public void onWbShareFail() {
        ToastUtil.showToast("分享失败~");
    }
}
