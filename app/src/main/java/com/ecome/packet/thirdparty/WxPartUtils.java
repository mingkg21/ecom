package com.ecome.packet.thirdparty;

import android.content.Context;
import android.content.Intent;

import com.mk.core.util.ToastUtil;
import com.ecome.packet.BuildConfig;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/**
 * 微信操作
 *
 * @author cgh
 */
public class WxPartUtils {

    private static final String TAG = WxPartUtils.class.getName();
    public static final String APP_ID = BuildConfig.WX_APP_ID;

    private static WxPartUtils mInst = null;

    private Context mContext;
    private IWXAPI mIwxapi;
    private ThreePartAction mAction;
    private WxShareHandler mWxShareHandler;

    private WxPartUtils(Context context) {
        mContext = context.getApplicationContext();
        mIwxapi = WXAPIFactory.createWXAPI(context, APP_ID);
    }

    public void register() {
        mIwxapi.registerApp(APP_ID);
    }

    public static WxPartUtils getInst(Context context) {
        synchronized (WxPartUtils.class) {
            if (mInst == null) {
                mInst = new WxPartUtils(context);
            }
        }
        return mInst;
    }

    /**
     * 检测微信是否有效
     *
     * @return
     */
    public boolean checkWxValid() {
        if (mIwxapi.isWXAppInstalled()) {
            int wxSdkVersion = mIwxapi.getWXAppSupportAPI();
            if (wxSdkVersion < 0x21020001) {
                ToastUtil.showToast("微信版本过低！");
                return false;
            } else {
                return true;
            }
        } else {
            ToastUtil.showToast("未安装微信！");
            return false;
        }
    }

    public IWXAPI getIwxapi() {
        return mIwxapi;
    }


    public void share(WxShareHandler wxShareHandler) {
        mWxShareHandler = wxShareHandler;
        mAction = null;
    }

    /**
     * 处理微信回调
     *
     * @param intent
     * @param iwxapiEventHandler
     */
    public void handleIntent(Intent intent, final IWXAPIEventHandler iwxapiEventHandler) {
        mIwxapi.handleIntent(intent, new IWXAPIEventHandler() {

            @Override
            public void onResp(BaseResp baseResp) {
                iwxapiEventHandler.onResp(baseResp);
//			 Logger.i(TAG, "baseResp.openId:" + baseResp.openId);
                if (mWxShareHandler != null) {
                    mWxShareHandler.onResp(baseResp);
                }
                switch (baseResp.errCode) {
                    case BaseResp.ErrCode.ERR_OK:
//				    Logger.i(TAG, "ERR_OK");
                        if (mAction != null) {
                            mAction.onThreePartActionSuccess(((SendAuth.Resp) baseResp).code, "");
                        }
                        break;
                    case BaseResp.ErrCode.ERR_USER_CANCEL:
//				    Logger.i(TAG, "ERR_USER_CANCEL");
                        if (mAction != null) {
                            mAction.onThreePartActionCancel();
                        }
                        break;
                    case BaseResp.ErrCode.ERR_AUTH_DENIED:
//				    Logger.i(TAG, "ERR_USER_CANCEL");
                    default:
//				    Logger.i(TAG, "default");
                        if (mAction != null) {
                            mAction.onThreePartActionFail();
                        }
                        break;
                }
            }

            @Override
            public void onReq(BaseReq baseReq) {
                iwxapiEventHandler.onReq(baseReq);
//			 Logger.i(TAG, "onReq");
            }
        });
    }

}
