package com.ecome.packet.thirdparty;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.R;
import com.sina.weibo.sdk.api.ImageObject;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.WeiboMultiMessage;
import com.tencent.connect.share.QQShare;
import com.tencent.connect.share.QzoneShare;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXImageObject;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;

import java.util.ArrayList;

import static com.ecome.packet.thirdparty.Util.bmpToByteArray;


/**
 * 分享
 */
public class ShareUtils {

    public static final int TYPE_QQ = 1;
    public static final int TYPE_WX = 2;
    public static final int TYPE_WX_TIMELINE = 3;
    public static final int TYPE_WEIBO = 4;
    public static final int TYPE_QZONE = 5;

    private static final int THUMB_SIZE = 150;
    private static final int WEIBO_IMG = 4096000;
    private MyUiListener mUiListener;
    private static final String SHARE_TITLE = "【%s】";
    private static final String SHARE_TEXT = "- 发掘最好玩的游戏，汇聚最深度的玩家 | 游范儿";

    private static final String SHARE_TITLE_GAME = "快来玩（%s分 %s人下载）- 游范儿，一起撩游戏";
    private static final String SHARE_TITLE_GROUP = "邀请你加入小组：%s - 游范儿，有组织的游戏社区";
    private static final String SHARE_TITLE_POST = "%s - 游范儿";
    private static final String SHARE_URL = "http://www.youfanr.net";

    public void onItemShare(Context context, String title, String content, String shareUrl, String iconUrl, String imgUrl, int type) {
        switch (type) {
            case TYPE_QQ: {
                mUiListener = new MyUiListener(context);
                shareToQQ((Activity) context, title, content, shareUrl, iconUrl, mUiListener, false);
                break;
            }
            case TYPE_QZONE: {
                mUiListener = new MyUiListener(context);
                shareToQQ((Activity) context, title, content, shareUrl, iconUrl, mUiListener, true);
                break;
            }
            case TYPE_WX: {
                shareToWeixin(context, title, content, shareUrl, iconUrl, false);
                WxPartUtils.getInst(context).share(new WxShareHandler(context));
                break;
            }
            case TYPE_WX_TIMELINE: {
                shareToWeixin(context, title, content, shareUrl, iconUrl, true);
                WxPartUtils.getInst(context).share(new WxShareHandler(context));
                break;
            }
            case TYPE_WEIBO:
                String img = imgUrl;
                if (TextUtils.isEmpty(img)) {
                    img = iconUrl;
                }
                ActivityRouter.startWeiboShareActivity(context, title, content, shareUrl, img);
                break;
        }
    }

    public void onActivityResult(Context context, int requestCode, int resultCode, Intent data) {
        if (data != null) {
            TencentPartUtils.getInst(context).handleShareIntent(data, mUiListener);
        } else if (mUiListener != null) {
            mUiListener.onError(null);
        }
    }

    public static void shareToQQ(Activity activity, String title, String content, String url, String iconUrl, MyUiListener uiListener, boolean isQzone) {
        Bundle params = new Bundle();
        params.putString(QQShare.SHARE_TO_QQ_TITLE, title);
        params.putString(QQShare.SHARE_TO_QQ_SUMMARY, content);
        params.putString(QQShare.SHARE_TO_QQ_TARGET_URL, url);
        params.putString(QQShare.SHARE_TO_QQ_APP_NAME, activity.getString(R.string.app_name));
        if (isQzone) {
            ArrayList<String> imageUrls = new ArrayList();
            imageUrls.add(iconUrl);
            params.putStringArrayList(QzoneShare.SHARE_TO_QQ_IMAGE_URL, imageUrls);
            TencentPartUtils.getInst(activity).getTencent().shareToQzone(activity, params, uiListener);
        } else {
            params.putString(QQShare.SHARE_TO_QQ_IMAGE_URL, iconUrl);
            TencentPartUtils.getInst(activity).getTencent().shareToQQ(activity, params, uiListener);
        }
    }

    public static void shareToQQImg(Activity activity, String title, String content, String fileUrl, MyUiListener uiListener) {
        Bundle params = new Bundle();
        params.putInt(QQShare.SHARE_TO_QQ_KEY_TYPE, QQShare.SHARE_TO_QQ_TYPE_IMAGE);
        params.putString(QQShare.SHARE_TO_QQ_TITLE, title);
        params.putString(QQShare.SHARE_TO_QQ_SUMMARY, content);
        params.putString(QQShare.SHARE_TO_QQ_IMAGE_LOCAL_URL, fileUrl);
        params.putString(QQShare.SHARE_TO_QQ_APP_NAME, activity.getString(R.string.app_name));
        TencentPartUtils.getInst(activity).getTencent().shareToQQ(activity, params, uiListener);
    }

    public static void shareToWeixin(final Context context, final String title, final String content, final String url, String iconUrl, final boolean shareToFriendCircle) {
        SimpleTarget target = new SimpleTarget<GlideBitmapDrawable>() {
            @Override
            public void onResourceReady(GlideBitmapDrawable resource, GlideAnimation glideAnimation) {
                WXWebpageObject webpage = new WXWebpageObject();
                webpage.webpageUrl = url;
                WXMediaMessage msg = new WXMediaMessage(webpage);
                msg.title = title;
                msg.description = content;
                Bitmap thumbBmp = resource.getBitmap();
                thumbBmp = Bitmap.createBitmap(thumbBmp);
                msg.thumbData = bmpToByteArray(thumbBmp, true);

                SendMessageToWX.Req req = new SendMessageToWX.Req();
                req.transaction = String.valueOf(System.currentTimeMillis());
                req.message = msg;
                req.scene = shareToFriendCircle ? SendMessageToWX.Req.WXSceneTimeline : SendMessageToWX.Req.WXSceneSession;
                WxPartUtils.getInst(context).getIwxapi().sendReq(req);
            }
        };

        Glide.with(context).load(iconUrl).into(target);
    }

    public static void shareToWeixinImg(final Context context, final Bitmap bitmap, final boolean shareToFriendCircle) {
        WXMediaMessage msg = new WXMediaMessage();

        WXImageObject image = new WXImageObject(bitmap);
        msg.mediaObject = image;

        Bitmap thumbBmp = Bitmap.createScaledBitmap(bitmap, THUMB_SIZE, THUMB_SIZE, true);
        msg.thumbData = bmpToByteArray(thumbBmp, true);

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = String.valueOf(System.currentTimeMillis());
        req.message = msg;
        req.scene = shareToFriendCircle ? SendMessageToWX.Req.WXSceneTimeline : SendMessageToWX.Req.WXSceneSession;
        WxPartUtils.getInst(context).getIwxapi().sendReq(req);
    }


    /**
     * 第三方应用发送请求消息到微博，唤起微博分享界面。
     */
    public static void shareToWeiBo(final Activity context, final String title, final String content, final String shareUrl, String iconUrl) {
        SimpleTarget target = new SimpleTarget<GlideBitmapDrawable>() {
            @Override
            public void onResourceReady(GlideBitmapDrawable resource, GlideAnimation glideAnimation) {
                WeiboMultiMessage weiboMessage = new WeiboMultiMessage();

                StringBuilder builder = new StringBuilder();
                builder.append("【");
                builder.append(title);
                builder.append("】");
                builder.append(content);
                builder.append(SHARE_TEXT);
                builder.append(" @" + context.getString(R.string.app_name) + "APP ");
                builder.append(shareUrl);

                TextObject textObject = new TextObject();
                textObject.text = builder.toString();
                textObject.title = "";
                weiboMessage.textObject = textObject;

                ImageObject imageObject = new ImageObject();
                Bitmap bitmap = Bitmap.createBitmap(resource.getBitmap());
                if (bitmap.getByteCount() > WEIBO_IMG) {
                    int scale = bitmap.getByteCount() / WEIBO_IMG;//缩放比例
                    Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / scale, bitmap.getHeight() / scale, false);
                    bitmap.recycle();
                    bitmap = bitmap2;
                }
                imageObject.setImageObject(bitmap);
                bitmap.recycle();
                weiboMessage.imageObject = imageObject;

                WeiBoUtils.getInst().shareMessage(context, weiboMessage);
            }
        };

        Glide.with(context).load(iconUrl).into(target);
    }

    public static void shareToWeiBoImg(final Activity context, String iconUrl) {
        WeiboMultiMessage weiboMessage = new WeiboMultiMessage();

        StringBuilder builder = new StringBuilder();
        builder.append(" @" + context.getString(R.string.app_name) + "APP ");

        TextObject textObject = new TextObject();
        textObject.text = builder.toString();
        textObject.title = "";
        weiboMessage.textObject = textObject;

        ImageObject imageObject = new ImageObject();
        Bitmap bitmap = null;
        int count = 0;
        while (bitmap == null && count < 5) {
            try {
                bitmap = BitmapFactory.decodeFile(iconUrl);
                if (bitmap.getByteCount() > WEIBO_IMG) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = bitmap.getByteCount() / WEIBO_IMG;//缩放比例
                    bitmap = BitmapFactory.decodeFile(iconUrl, options);
                }
            } catch (OutOfMemoryError error) {
                error.printStackTrace();
            }
            count++;
        }
        imageObject.setImageObject(bitmap);
        bitmap.recycle();
        weiboMessage.imageObject = imageObject;

        WeiBoUtils.getInst().shareMessage(context, weiboMessage);
    }

}
