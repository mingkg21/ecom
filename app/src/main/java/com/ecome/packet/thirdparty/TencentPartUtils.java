package com.ecome.packet.thirdparty;

import android.content.Context;
import android.content.Intent;

import com.ecome.packet.BuildConfig;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * QQ操作
 *
 * @author cgh
 */
public class TencentPartUtils {
    private static final String TAG = TencentPartUtils.class.getName();
    public static final String APP_ID = BuildConfig.QQ_ID;
    public static final String SCOPE = "all";

    private static TencentPartUtils mInst = null;

    private Context mContext;
    private Tencent mTencent;

    private TencentPartUtils(Context context) {
        mContext = context;
        mTencent = Tencent.createInstance(APP_ID, context);
    }

    public static TencentPartUtils getInst(Context context) {
        synchronized (TencentPartUtils.class) {
            if (mInst == null) {
                mInst = new TencentPartUtils(context);
            }
        }
        return mInst;
    }

    public void handleResultData(Intent paramIntent, final ThreePartAction action) {
        Tencent.handleResultData(paramIntent, new IUiListener() {

            @Override
            public void onError(UiError uiError) {
//			 Logger.i(TAG, "onError:");
                if (action != null) {
                    action.onThreePartActionFail();
                }
            }

            @Override
            public void onComplete(Object object) {
                if (object instanceof JSONObject) {
                    JSONObject jsonObject = (JSONObject) object;
                    try {
                        String openId = jsonObject.getString("openid");
                        String token = jsonObject.getString("access_token");
                        if (action != null) {
                            action.onThreePartActionSuccess(token, openId);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (action != null) {
                        action.onThreePartActionFail();
                    }
                }
            }

            @Override
            public void onCancel() {
//			 Logger.i(TAG, "onCancel:");
                if (action != null) {
                    action.onThreePartActionCancel();
                }
            }
        });
    }

    public void handleShareIntent(Intent data, MyUiListener myUiListener) {
        Tencent.handleResultData(data, myUiListener);
    }

    public Tencent getTencent() {
        return mTencent;
    }

    /**
     * 退出登录
     *
     * @param context
     */
    public void logout(Context context) {
        mTencent.logout(mContext);
    }

}
