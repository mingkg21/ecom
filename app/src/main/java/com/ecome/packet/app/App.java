package com.ecome.packet.app;

import android.content.Context;
import android.graphics.Color;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ecome.packet.R;
import com.ecome.packet.db.SystemInfoPreferences;
import com.ecome.packet.db.UserManager;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.util.AppFileUtil;
import com.mk.core.app.BaseApp;
import com.mk.core.util.ToastUtil;
import com.mk.library.imgSelector.ISNav;
import com.mk.library.imgSelector.common.ImageLoader;
import com.mk.library.imgSelector.config.ISListConfig;
import com.tencent.bugly.Bugly;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class App extends BaseApp {

    @Override
    public void onCreate() {
        super.onCreate();

        AppFileUtil.initAppPath();

        Bugly.init(this, "e7f0ba3d79", true);
    }

    @Override
    public void onEnterApp() {
    }

    @Override
    public void onExitApp() {
    }

    @Override
    public boolean isLogin() {
        return UserManager.getInstance().isLogin();
    }

    @Override
    protected void onLogin(Context context) {
        ToastUtil.showToast("登录后才能操作哦~");
        ActivityRouter.openLoginActivity(context);
    }

    public static String getApi() {
        return SystemInfoPreferences.getInstance().getApiUrl();
    }

    public static String getBaseApi() {
        return SystemInfoPreferences.getInstance().getBaseApiUrl();
    }

    public static void openPhotoListActivity(Context context, int requestCode) {
        openPhotoListActivity(context, false, requestCode);
    }

    public static void openPhotoListActivity(Context context, boolean isNeedScrop, int requestCode) {
        // 自定义图片加载器
        ISNav.getInstance().init(new ImageLoader() {

            private static final long serialVersionUID = -1859501350870904757L;

            @Override
            public void displayImage(Context context, String path, ImageView imageView) {
                Glide.with(context).load(path).into(imageView);
            }
        });

        // 自由配置选项
        ISListConfig config = new ISListConfig.Builder()
                // 是否多选, 默认true
                .multiSelect(false)
                // 是否记住上次选中记录, 仅当multiSelect为true的时候配置，默认为true
                .rememberSelected(false)
                // “确定”按钮背景色
                .btnBgColor(Color.TRANSPARENT)
                // “确定”按钮文字颜色
                .btnTextColor(context.getResources().getColor(R.color.common_text))
                // 使用沉浸式状态栏
                .statusBarColor(context.getResources().getColor(R.color.colorPrimaryDark))
                // 返回图标ResId
                .backResId(R.drawable.btn_back)
                // 标题
                .title("选择图片")
                // 标题文字颜色
                .titleColor(context.getResources().getColor(R.color.common_text))
                .titleHeight(context.getResources().getDimensionPixelOffset(R.dimen.common_title_bar_height))
                // TitleBar背景色
                .titleBgColor(context.getResources().getColor(R.color.colorPrimary))
                // 裁剪大小。needCrop为true的时候配置
                .cropSize(1, 1, 200, 200)
                .needCrop(isNeedScrop)
                // 第一个是否显示相机，默认true
                .needCamera(true)
                // 最大选择图片数量，默认9
                .maxNum(1)
                .build();

        // 跳转到图片选择器
        ISNav.getInstance().toListActivity(context, config, requestCode);
    }
}
