package com.ecome.packet.ui.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ecome.packet.R;

/**
 * Created by mingkg21 on 2017/10/10.
 */

public class PasswordView extends LinearLayout {

    private EditText mEditText;
    private ImageView mImageView;

    public PasswordView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(context).inflate(R.layout.layout_password, this);

        init();
    }

    private void init() {
        mEditText = (EditText) findViewById(R.id.layout_password_et);

        mImageView = (ImageView) findViewById(R.id.layout_password_iv);
        mImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageView.setSelected(!mImageView.isSelected());
                if(mImageView.isSelected()){
                    //选择状态 显示明文--设置为可见的密码
                    mEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else {
                    //默认状态显示密码--设置文本 要一起写才能起作用 InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD
                    mEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                mEditText.setSelection(mEditText.getText().length());
            }
        });
    }

    public void setHint(int resId) {
        mEditText.setHint(resId);
    }

    public void setHint(String hint) {
        mEditText.setHint(hint);
    }

    public Editable getText() {
        return mEditText.getText();
    }

    public EditText getEditText() {
        return mEditText;
    }
}