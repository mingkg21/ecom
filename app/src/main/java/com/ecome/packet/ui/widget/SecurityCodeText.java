package com.ecome.packet.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppBaseEntity;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.model.UserModel;
import com.mk.core.model.LoadCallback;
import com.mk.core.util.SMSUtil;
import com.mk.core.util.ToastUtil;
import com.mk.core.util.ValidationUtil;

/**
 * 验证码
 */
public class SecurityCodeText extends TextView implements View.OnClickListener, Runnable, LoadCallback {

    protected final static long DELAY_TIME = 1000;
    private final static int MSG_WHAT = 1;
    private long mWaitTime = 60;

    private String mGetSecurityCode;
    protected TextView mPhoneEt;
    private String mCode;

    private UserModel mUserModel;

    public SecurityCodeText(Context context, AttributeSet attrs) {
        super(context, attrs);
        mWaitTime = 60;
        mGetSecurityCode = getResources().getString(R.string.login_text_security_code);
        setText(mGetSecurityCode);
        setOnClickListener(this);

        mUserModel = new UserModel();
        mUserModel.addCallback(this);
    }

    public void setPhoneEt(TextView phoneEt) {
        this.mPhoneEt = phoneEt;
        removeCallbacks(this);
        setWaitTime(-1);
    }

    public String getCode() {
        return mCode;
    }

    @Override
    public void run() {
        if (mWaitTime >= 0) {
            setText(mWaitTime + "s");
            postDelayed(this, DELAY_TIME);
            mWaitTime--;
        } else {
            reset();
        }
    }

    public void reset() {
        removeCallbacks(this);
        mWaitTime = 60;
        setEnabled(true);
        setText(mGetSecurityCode);
    }

    protected void setWaitTime(long time) {
        mWaitTime = time;
    }

    @Override
    public final void onClick(View v) {
        if (mPhoneEt == null || !ValidationUtil.matchingMobil(mPhoneEt.getText().toString())) {
            return;
        }
        String phone = mPhoneEt.getText().toString().trim();

        mCode = SMSUtil.getSMSCode(4);

        mUserModel.sendSMS(phone, mCode);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this);
        mWaitTime = 60;
    }

    @Override
    public boolean onPreLoad(String key) {
        return false;
    }

    @Override
    public boolean onPostLoad(String key) {
        return false;
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mUserModel.isSendSMSKey(key)) {
            AppResponseEntity<AppBaseEntity> responseEntity = (AppResponseEntity<AppBaseEntity>) data;
            AppBaseEntity appBaseEntity = responseEntity.getData();
            if (appBaseEntity != null && appBaseEntity.isSuccess()) {
                mWaitTime = 60;
                setEnabled(false);
                post(this);
                ToastUtil.showToast(appBaseEntity.getMsg());
            } else {
                setEnabled(true);
                setText(mGetSecurityCode);
                ToastUtil.showToast("发送短信验证码失败，请稍后重试！");
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mUserModel.isSendSMSKey(key)) {
            setEnabled(true);
            setText(mGetSecurityCode);
            ToastUtil.showToast("发送短信验证码失败，请稍后重试！");
            return true;
        }
        return false;
    }

    @Override
    public boolean onDataChange(String key, Object data) {
        return false;
    }
}