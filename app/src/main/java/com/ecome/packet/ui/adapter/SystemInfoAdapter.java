package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.SystemInfo;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class SystemInfoAdapter extends BaseAdapter<SystemInfo> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_system_info;
    }

    @Override
    public BaseViewHolder<SystemInfo> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView);
    }

    public static class Holder extends BaseViewHolder<SystemInfo> {

        private TextView mTextView;

        public Holder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView;
        }

        @Override
        public void onBind(SystemInfo data, int position) {
            mTextView.setText(data.getName());
        }
    }
}
