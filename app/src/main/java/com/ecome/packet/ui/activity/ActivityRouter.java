package com.ecome.packet.ui.activity;

import android.content.Context;
import android.content.Intent;

import com.ecome.packet.entity.DiagnoseResult;
import com.ecome.packet.entity.Drug;
import com.ecome.packet.entity.KuFang;
import com.ecome.packet.entity.Menu;
import com.ecome.packet.entity.Patient;
import com.ecome.packet.entity.Registration;
import com.ecome.packet.entity.Staff;
import com.ecome.packet.entity.Temp;
import com.ecome.packet.entity.UserInfo;
import com.ecome.packet.entity.Voucher;
import com.ecome.packet.thirdparty.WeiboShareActivity;
import com.ecome.packet.ui.fragment.AddFeeFragment;
import com.ecome.packet.ui.fragment.AddPatientFragment;
import com.ecome.packet.ui.fragment.AddStaffFragment;
import com.ecome.packet.ui.fragment.AddVoucherFragment;
import com.ecome.packet.ui.fragment.ChargeFragment;
import com.ecome.packet.ui.fragment.CompleteInfoFragment;
import com.ecome.packet.ui.fragment.ConfigManageFragment;
import com.ecome.packet.ui.fragment.DoctorManageFragment;
import com.ecome.packet.ui.fragment.FeeManageFragment;
import com.ecome.packet.ui.fragment.FeedbackFragment;
import com.ecome.packet.ui.fragment.LogFragment;
import com.ecome.packet.ui.fragment.LoginFragment;
import com.ecome.packet.ui.fragment.MainFragment;
import com.ecome.packet.ui.fragment.MedicineVoucherFragment;
import com.ecome.packet.ui.fragment.MenuFragment;
import com.ecome.packet.ui.fragment.ModifyPasswordFragment;
import com.ecome.packet.ui.fragment.PaibanListFragment;
import com.ecome.packet.ui.fragment.PaibanModifyFragment;
import com.ecome.packet.ui.fragment.PatientHistoryRegistrationFragment;
import com.ecome.packet.ui.fragment.PrescribeFragment;
import com.ecome.packet.ui.fragment.PrescriptionFragment;
import com.ecome.packet.ui.fragment.PrescriptionTempDetailFragment;
import com.ecome.packet.ui.fragment.PrescriptionTempListFragment;
import com.ecome.packet.ui.fragment.RegisterFragment;
import com.ecome.packet.ui.fragment.RegistrationFragment;
import com.ecome.packet.ui.fragment.RegistrationResultFragment;
import com.ecome.packet.ui.fragment.ReportChargeDetailFragment;
import com.ecome.packet.ui.fragment.ReportFeeFragment;
import com.ecome.packet.ui.fragment.ReportRiBaoFragment;
import com.ecome.packet.ui.fragment.ReportRiZhiFragment;
import com.ecome.packet.ui.fragment.StaffManageFragment;
import com.ecome.packet.ui.fragment.StockQueryFragment;
import com.ecome.packet.ui.fragment.SystemInfoFragment;
import com.ecome.packet.ui.fragment.UserInfoFragment;
import com.ecome.packet.ui.fragment.WebViewFragment;
import com.ecome.packet.ui.fragment.WorkloadFragment;
import com.ecome.packet.ui.fragment.ZiDianFragment;
import com.mk.core.ui.activity.BaseActivityRouter;

/**
 * Created by mingkg21 on 2017/8/26.
 */

public class ActivityRouter extends BaseActivityRouter {

    public static final String EXTRA_NAME_TYPE = "EXTRA_NAME_TYPE";
    public static final String EXTRA_NAME_MENU = "EXTRA_NAME_MENU";
    public static final String EXTRA_NAME_POSITION = "EXTRA_NAME_POSITION";
    public static final String EXTRA_NAME_PATIENT = "EXTRA_NAME_PATIENT";
    public static final String EXTRA_NAME_REGISTRATION = "EXTRA_NAME_REGISTRATION";
    public static final String EXTRA_NAME_REGISTRATION_ID = "EXTRA_NAME_REGISTRATION_ID";
    public static final String EXTRA_NAME_REGISTRATION_RESULT = "EXTRA_NAME_REGISTRATION_RESULT";
    public static final String EXTRA_NAME_STAFF = "EXTRA_NAME_STAFF";
    public static final String EXTRA_NAME_DRUG = "EXTRA_NAME_DRUG";
    public static final String EXTRA_NAME_KUFANG = "EXTRA_NAME_KUFANG";
    public static final String EXTRA_NAME_VOUCHER = "EXTRA_NAME_VOUCHER";
    public static final String EXTRA_NAME_NAME = "EXTRA_NAME_NAME";
    public static final String EXTRA_NAME_USER = "EXTRA_NAME_USER";
    public static final String EXTRA_NAME_TEMP = "EXTRA_NAME_TEMP";
    public static final String EXTRA_NAME_TITLE = "EXTRA_NAME_TITLE";
    public static final String EXTRA_NAME_ID = "EXTRA_NAME_ID";
    public static final String EXTRA_NAME_RESULT = "EXTRA_NAME_RESULT";
    public static final String EXTRA_NAME_URL = "EXTRA_NAME_URL";
    public static final String EXTRA_NAME_VALUE = "EXTRA_NAME_VALUE";

    public static void openRegisterActivity(Context context) {
        Intent intent = new Intent();
        startActivity(context, RegisterFragment.class, intent);
    }

    public static void openCompleteInfoActivity(Context context, UserInfo userInfo) {
        Intent intent = new Intent();
        boolean isRegister = userInfo != null;
        intent.putExtra(EXTRA_NAME_VALUE, isRegister);
        intent.putExtra(EXTRA_NAME_USER, userInfo);
        startActivity(context, CompleteInfoFragment.class, intent);
    }

    public static void openMenuActivity(Context context, Menu menu, int position) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_MENU, menu);
        intent.putExtra(EXTRA_NAME_POSITION, position);
        startActivity(context, MenuFragment.class, intent);
    }

    public static void openConfigManageActivity(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, ConfigManageFragment.class, intent);
    }

    public static void openDoctorManageActivity(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, DoctorManageFragment.class, intent);
    }

    public static void openPaiBanModifyActivity(Context context, Staff staff) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_STAFF, staff);
        startActivity(context, PaibanModifyFragment.class, intent);
    }

    public static void openPaiBanListActivity(Context context, Staff staff) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_STAFF, staff);
        startActivity(context, PaibanListFragment.class, intent);
    }

    public static void openStaffManageActivity(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, StaffManageFragment.class, intent);
    }

    public static void openAddStaffFragmentActivity(Context context, Staff staff) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_STAFF, staff);
        startActivity(context, AddStaffFragment.class, intent);
    }

    public static void openFeeManageActivity(Context context, String title, KuFang kuFang) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        intent.putExtra(EXTRA_NAME_KUFANG, kuFang);
        startActivity(context, FeeManageFragment.class, intent);
    }

    public static void openAddFeeActivity(Context context, Drug drug) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_DRUG, drug);
        startActivity(context, AddFeeFragment.class, intent);
    }

    public static void openZiDianActivity(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, ZiDianFragment.class, intent);
    }

    public static void openLogActivity(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, LogFragment.class, intent);
    }

    public static void openModifyPasswordActivity(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, ModifyPasswordFragment.class, intent);
    }

    public static void openPrescribeActivity(Context context) {
        startActivity(context, PrescribeFragment.class);
    }

    public static void openMyPatientActivity(Context context) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_VALUE, true);
        startActivity(context, PrescribeFragment.class, intent);
    }

    public static void openPrescriptionTempListActivity(Context context, boolean isChoiceMode) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_VALUE, isChoiceMode);
        startActivity(context, PrescriptionTempListFragment.class, intent);
    }

    public static void openWorkloadActivity(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, WorkloadFragment.class, intent);
    }

    public static void openPrescriptionTempDetailActivity(Context context, Temp temp, boolean isChoiceMode) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TEMP, temp);
        intent.putExtra(EXTRA_NAME_VALUE, isChoiceMode);
        startActivity(context, PrescriptionTempDetailFragment.class, intent);
    }

    public static void openAddPatientActivity(Context context) {
        startActivity(context, AddPatientFragment.class);
    }

    public static void openWebViewActivity(Context context, String url) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_URL, url);
        startActivity(context, WebViewFragment.class, intent);
    }

    public static void openRegistrationActivity(Context context, Patient patient, Registration registration) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_PATIENT, patient);
        intent.putExtra(EXTRA_NAME_REGISTRATION, registration);
        startActivity(context, RegistrationFragment.class, intent);
    }

    public static void openRegistrationResultActivity(Context context, Patient patient,
                                                      Registration registration, DiagnoseResult diagnoseResult) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_PATIENT, patient);
        intent.putExtra(EXTRA_NAME_REGISTRATION, registration);
        intent.putExtra(EXTRA_NAME_REGISTRATION_RESULT, diagnoseResult);
        startActivity(context, RegistrationResultFragment.class, intent);
    }

    public static void openRegistrationHistoryActivity(Context context, Patient patient) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_PATIENT, patient);
        startActivity(context, PatientHistoryRegistrationFragment.class, intent);
    }

    public static void openPrescriptionActivity(Context context, int registrationId, String name) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_REGISTRATION_ID, registrationId);
        intent.putExtra(EXTRA_NAME_NAME, name);
        startActivity(context, PrescriptionFragment.class, intent);
    }

    public static void openChargeActivity(Context context, DiagnoseResult diagnoseResult, String payType) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_REGISTRATION_RESULT, diagnoseResult);
        intent.putExtra(EXTRA_NAME_VALUE, payType);
        startActivity(context, ChargeFragment.class, intent);
    }

    public static void openMedicineVoucherActivity(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, MedicineVoucherFragment.class, intent);
    }

    public static void openStockQueryActivity(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, StockQueryFragment.class, intent);
    }

    public static void openAddVoucherActivity(Context context, Voucher voucher) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_VOUCHER, voucher);
        startActivity(context, AddVoucherFragment.class, intent);
    }

    public static void openForgetActivity(Context context) {
    }

    public static void openUserInfoActivity(Context context) {
        startActivity(context, UserInfoFragment.class);
    }

    public static void openMainActivity(Context context) {
        startActivity(context, MainFragment.class);
    }

    public static void openLoginActivity(Context context) {
        startActivity(context, LoginFragment.class);
    }

    public static void openSystemInfoActivity(Context context, int requestCode) {
        startActivityForResult(context, SystemInfoFragment.class, requestCode);
    }

    public static void openReportChargeDetailActivity(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, ReportChargeDetailFragment.class, intent);
    }

    public static void openReportRiBaoActivity(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, ReportRiBaoFragment.class, intent);
    }

    public static void openReportRiZhiActivity(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, ReportRiZhiFragment.class, intent);
    }

    public static void openReportFeeActivity(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, ReportFeeFragment.class, intent);
    }

    public static void openFeedbackFragment(Context context, String title) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NAME_TITLE, title);
        startActivity(context, FeedbackFragment.class, intent);
    }

    //---------------------------------------------------------------------------------------------------
    //--------------------------------------------我的----------------------------------------------------
    //---------------------------------------------------------------------------------------------------

    public static void startWeiboShareActivity(Context context, String title, String content, String shareUrl, String iconUrl) {
        Intent intent = new Intent(context, WeiboShareActivity.class);
        intent.putExtra(WeiboShareActivity.SHARE_TITLE, title);
        intent.putExtra(WeiboShareActivity.SHARE_CONTENT, content);
        intent.putExtra(WeiboShareActivity.SHARE_URL, shareUrl);
        intent.putExtra(WeiboShareActivity.SHARE_ICON_URL, iconUrl);
        context.startActivity(intent);
    }

}
