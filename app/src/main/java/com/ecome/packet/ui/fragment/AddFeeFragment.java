package com.ecome.packet.ui.fragment;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Drug;
import com.ecome.packet.entity.FeeCategory;
import com.ecome.packet.entity.Unit;
import com.ecome.packet.model.BackendManageModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.dialog.DialogManager;
import com.ecome.packet.ui.widget.BottomSheetListLayout;
import com.mk.core.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class AddFeeFragment extends AppFragment {

    private BackendManageModel mBackendManageModel;

    private ArrayList<FeeCategory> mFeeCategories = new ArrayList<>();
    private ArrayList<Unit> mUnits = new ArrayList<>();

    private EditText mNameET;
    private EditText mGuigeET;
    private EditText mPriceET;

    private TextView mCategoryTV;
    private TextView mUnitTV;

    private Drug mDrug;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_fee_modify;
    }

    @Override
    protected void initData() {
        super.initData();
        mDrug = (Drug) getArguments().get(ActivityRouter.EXTRA_NAME_DRUG);
    }

    @Override
    protected void initView(final View view) {
        super.initView(view);

        if (mDrug == null) {
            setTitle("新增收费项目");
        } else {
            setTitle("修改收费项目");
        }

        mBackendManageModel = new BackendManageModel();
        mBackendManageModel.addCallback(this);

        mNameET = view.findViewById(R.id.fragment_fee_modify_name_et);
        mGuigeET = view.findViewById(R.id.fragment_fee_modify_guige_et);
        mPriceET = view.findViewById(R.id.fragment_fee_modify_price_et);

        mCategoryTV = view.findViewById(R.id.fragment_fee_modify_category_tv);
        mCategoryTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showFeeCategoryListDialog(getContext(), mFeeCategories, new BottomSheetListLayout.OnItemListener<FeeCategory>() {
                    @Override
                    public void onItemClick(FeeCategory data) {
                        mCategoryTV.setText(data.getName());
                        mCategoryTV.setTag(data);
                    }

                    @Override
                    public String getItemContent(FeeCategory data) {
                        return null;
                    }
                });
            }
        });
        mUnitTV = view.findViewById(R.id.fragment_fee_modify_unit_tv);
        mUnitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showUnitListDialog(getContext(), mUnits, new BottomSheetListLayout.OnItemListener<Unit>() {
                    @Override
                    public void onItemClick(Unit data) {
                        mUnitTV.setText(data.getName());
                        mUnitTV.setTag(data);
                    }

                    @Override
                    public String getItemContent(Unit data) {
                        return null;
                    }
                });
            }
        });
        view.findViewById(R.id.fragment_fee_modify_submit_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String name = mNameET.getText().toString().trim();
                String guige = mGuigeET.getText().toString().trim();
                String price = mPriceET.getText().toString().trim();

                String role = mCategoryTV.getText().toString().trim();

                if (TextUtils.isEmpty(name)) {
                    ToastUtil.showToast("请输入项目名称！");
                    return;
                }
                if (TextUtils.isEmpty(guige)) {
                    ToastUtil.showToast("请输入规格！");
                    return;
                }
                if (TextUtils.isEmpty(price)) {
                    ToastUtil.showToast("请输入单价！");
                    return;
                }

                if (TextUtils.isEmpty(role)) {
                    ToastUtil.showToast("请选择类别！");
                    return;
                }

                int cls = ((FeeCategory) mCategoryTV.getTag()).getId();
                if (mDrug == null) {
                    mDrug = new Drug();
                }

                try {
                    mDrug.setName(name);
                    mDrug.setGuige(guige);
                    mDrug.setPrice(Double.valueOf(price));
                    mDrug.setCls(cls);
                    mDrug.setUnit(((Unit)mUnitTV.getTag()).getName());

                    mBackendManageModel.modifyFee(mDrug);
                } catch (Exception e) {

                }

            }
        });

        loadData();
    }

    @Override
    protected void loadData() {
        mBackendManageModel.getFeeCategory();
        mBackendManageModel.getUnit();
        showLoadingView();
    }

    private void checkData() {
        if (mFeeCategories.isEmpty()) {
            return;
        }
        if (mUnits.isEmpty()) {
            return;
        }
        if (mDrug != null) {

            mNameET.setText(mDrug.getName());
            mGuigeET.setText(mDrug.getGuige());
            mPriceET.setText(mDrug.getPrice() + "");

            for (FeeCategory feeCategory : mFeeCategories) {
                if (feeCategory.getId() == mDrug.getCls()) {
                    mCategoryTV.setText(feeCategory.getName());
                    mCategoryTV.setTag(feeCategory);
                    break;
                }
            }

            for (Unit unit : mUnits) {
                if (unit.getName().equals(mDrug.getUnit())) {
                    mUnitTV.setText(unit.getName());
                    mUnitTV.setTag(unit);
                    break;
                }
            }

        }
        hideLoadingView();
    }

    @Override
    public boolean onPreLoad(String key) {
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
       if(mBackendManageModel.isModifyStaffKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mBackendManageModel.isGetFeeCategoryKey(key)
                || mBackendManageModel.isGetUnitKey(key)) {
            showErrorView();
            return true;
        } else if(mBackendManageModel.isModifyFeeKey(key)) {
            AppResponseEntity appResponseEntity = (AppResponseEntity) data;
            ToastUtil.showToast(appResponseEntity.getMsg());
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mBackendManageModel.isGetFeeCategoryKey(key)) {
            AppResponseEntity<List<FeeCategory>> appResponseEntity = (AppResponseEntity<List<FeeCategory>>) data;
            mFeeCategories.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mBackendManageModel.isGetUnitKey(key)) {
            AppResponseEntity<List<Unit>> appResponseEntity = (AppResponseEntity<List<Unit>>) data;
            mUnits.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mBackendManageModel.isModifyFeeKey(key)) {
            if (mDrug != null && mDrug.getId() > 0) {
                ToastUtil.showToast("修改收费项目成功！");
            } else {
                ToastUtil.showToast("新增收费项目成功！");
            }
            EventBus.getDefault().post(new Drug());
            finish();
            return true;
        }
        return super.onSuccess(key, data);
    }

}
