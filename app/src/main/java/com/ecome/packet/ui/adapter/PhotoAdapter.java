package com.ecome.packet.ui.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mk.core.util.ImageUtil;
import com.ecome.packet.R;
import com.ecome.packet.ui.dialog.PhotoDialog;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by mingkg21 on 2017/10/11.
 */

public class PhotoAdapter extends PagerAdapter {

    private ArrayList<String> mImagePaths;

    private LayoutInflater mInflater;
    private Context mContext;
    private PhotoDialog.OnDoneListener mOnDoneListener;
    private View.OnClickListener mOnFinishListener;

    private boolean mHandChange;

    public PhotoAdapter(Context context, ArrayList<String> imagePaths) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mImagePaths = imagePaths;
    }

    public void setOnDoneListener(PhotoDialog.OnDoneListener onDoneListener) {
        mOnDoneListener = onDoneListener;
    }

    public void setOnFinishListener(View.OnClickListener onFinishListener) {
        mOnFinishListener = onFinishListener;
    }

    @Override
    public void notifyDataSetChanged() {
        mHandChange = true;
        super.notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(Object object) {
        if (mHandChange) {
            mHandChange = false;
            return POSITION_NONE;
        }
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return mImagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View view = mInflater.inflate(R.layout.item_photo, null);

        PhotoView imageView = (PhotoView) view;
        ImageUtil.loadImage(mImagePaths.get(position), imageView);
        imageView.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
            @Override
            public void onPhotoTap(View view, float v, float v1) {
                finish();
            }

            @Override
            public void onOutsidePhotoTap() {
                finish();
            }
        });

        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new PhotoDialog(v.getContext(), position, mOnDoneListener).show();
                return false;
            }
        });

        container.addView(view);
        return view;
    }

    private void finish() {
       if (mOnFinishListener != null) {
           mOnFinishListener.onClick(null);
       }
    }

}
