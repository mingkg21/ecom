package com.ecome.packet.ui.fragment;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppBaseEntity;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.model.UserModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.mk.core.util.ToastUtil;

public class ModifyPasswordFragment extends AppFragment {

    private UserModel mUserModel;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_modify_password;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        mUserModel = new UserModel();
        mUserModel.addCallback(this);

        setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));

        final EditText oldET = view.findViewById(R.id.fragment_modify_password_password_old_et);
        final EditText new1ET = view.findViewById(R.id.fragment_modify_password_password_new_1_et);
        final EditText new2ET = view.findViewById(R.id.fragment_modify_password_password_new_2_et);

        view.findViewById(R.id.fragment_modify_password_sure_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPwd = oldET.getText().toString().trim();
                String new1Pwd = new1ET.getText().toString().trim();
                String new2Pwd = new2ET.getText().toString().trim();

                if (TextUtils.isEmpty(oldPwd)) {
                    ToastUtil.showToast("请输入旧密码！");
                    return;
                }
                if (TextUtils.isEmpty(new1Pwd)) {
                    ToastUtil.showToast("请输入新密码！");
                    return;
                }
                if (!new1Pwd.equals(new2Pwd)) {
                    ToastUtil.showToast("两次输入的密码不一致！");
                    return;
                }
                mUserModel.updatePassword(oldPwd, new1Pwd, new2Pwd);
            }
        });

        view.findViewById(R.id.fragment_modify_password_cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void loadData() {

    }

    @Override
    public boolean onPreLoad(String key) {
        if (mUserModel.isUpdatePasswordKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mUserModel.isUpdatePasswordKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mUserModel.isUpdatePasswordKey(key)) {
            AppResponseEntity<AppBaseEntity> appResponseEntity = (AppResponseEntity<AppBaseEntity>) data;

            AppBaseEntity appBaseEntity = appResponseEntity.getData();
            if (appBaseEntity != null) {
                ToastUtil.showToast(appBaseEntity.getMsg());
                if (appBaseEntity.isSuccess()) {
                    finish();
                }
            }
            return true;
        }
        return super.onSuccess(key, data);
    }
}
