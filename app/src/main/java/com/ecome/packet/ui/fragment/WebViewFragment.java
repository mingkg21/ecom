package com.ecome.packet.ui.fragment;

import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ecome.packet.R;
import com.ecome.packet.ui.activity.ActivityRouter;

public class WebViewFragment extends AppFragment {

    private WebView mWebView;
    private String mUrl;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_webview;
    }

    @Override
    protected void initData() {
        super.initData();
        mUrl = getArguments().getString(ActivityRouter.EXTRA_NAME_URL);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        mWebView = view.findViewById(R.id.fragment_webview_wb);
        mWebView.getSettings().setSupportZoom(false);
        mWebView.getSettings().setNeedInitialFocus(false);
        mWebView.getSettings().setBuiltInZoomControls(false);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//		mWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);//视频缓存有问题
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setDomStorageEnabled(true);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideLoadingView();
                try {
                    mWebView.getSettings().setBlockNetworkImage(false);
                } catch (Exception e) {
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showLoadingView();
                try {
                    mWebView.getSettings().setBlockNetworkImage(true);
                } catch (Exception e) {
                }
            }

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, final String failingUrl) {
                if (!isAdded()) {
                    return;
                }
                showErrorView();
                super.onReceivedError(view, errorCode, description, failingUrl);
            }

        });
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if (getUserVisibleHint()) {
                    setTitle(title);
                }
            }

        });

        loadData();
    }

    @Override
    protected void loadData() {
        mWebView.loadUrl(mUrl);
    }

}
