package com.ecome.packet.ui.fragment;

import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ecome.packet.R;
import com.mk.core.util.IMMUtils;

public abstract class AppSearchFragment<T> extends AppRecyclerFragment<T> {

    protected EditText mSearchET;
    protected View mSearchLayout;

    @Override
    protected void initView(View view) {
        super.initView(view);

        mSearchLayout = LayoutInflater.from(getContext()).inflate(R.layout.layout_search, null);

        mSearchET = mSearchLayout.findViewById(R.id.search_et);
        mSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search(v);
                    return true;
                }
                return false;
            }
        });
        if (!TextUtils.isEmpty(getSearchHint())) {
            mSearchET.setHint(getSearchHint());
        }

        ((LinearLayout) view).addView(mSearchLayout, 1);

        mRecyclerView.setVisibility(View.VISIBLE);

    }

    private void search(View v) {
        onSearch(mSearchET.getText().toString().trim());
        IMMUtils.hideSoftInput(getContext(), v);
    }

    protected abstract String getSearchHint();

    protected abstract void onSearch(String key);
}
