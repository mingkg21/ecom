package com.ecome.packet.ui.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Performance;
import com.ecome.packet.model.ReportModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.PerformanceAdapter;
import com.mk.core.ui.widget.BaseAdapter;

import java.util.List;

public class ReportRiBaoFragment extends AppDateSearchFragment<Performance> {

    private ReportModel mReportModel;

    private TextView mTotalFeeTV;

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));

        View totalFeeView = LayoutInflater.from(getContext()).inflate(R.layout.layout_total_fee, null);
        mTotalFeeTV = totalFeeView.findViewById(R.id.layout_total_fee_tv);

        mDataTimeLayout.addView(totalFeeView);
    }

    @Override
    protected void initLoadModel() {
        mReportModel = new ReportModel();
        mReportModel.addCallback(this);
    }

    @Override
    protected boolean isLoadData(String key) {
        return mReportModel.isReportRiBaoKey(key);
    }

    @Override
    protected BaseAdapter<Performance> getAdapter() {
        return new PerformanceAdapter();
    }

    @Override
    protected void loadData(String startDateTime, String endDateTime) {
        mReportModel.reportRiBao(startDateTime, endDateTime);
    }

    @Override
    public void setAll(List<Performance> datas) {
        super.setAll(datas);

        double totalFee = 0;
        for (Performance performance : datas) {
            totalFee += performance.getFeeSum();
        }

        mTotalFeeTV.setText("总收入：" + totalFee + "元");
    }
}
