package com.ecome.packet.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.ecome.packet.R;

/**
 * Created by mingkg21 on 2017/10/6.
 */

public class PhotoDialog extends Dialog {

    private OnDoneListener mOnDoneListener;
    private int mPosition;

    public PhotoDialog(@NonNull Context context, int position, OnDoneListener onDoneListener) {
        super(context, R.style.dialogstyle);

        setContentView(R.layout.dialog_photo);

        mPosition = position;
        mOnDoneListener = onDoneListener;

        Window window = getWindow();
        window.setGravity(Gravity.BOTTOM);

        findViewById(R.id.dialog_photo_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        findViewById(R.id.dialog_photo_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDoneListener != null) {
                    mOnDoneListener.onDelete(mPosition);
                }
                dismiss();
            }
        });
        findViewById(R.id.dialog_photo_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDoneListener != null) {
                    mOnDoneListener.onSave(mPosition);
                }
                dismiss();
            }
        });
    }

    @Override
    public void show() {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = getContext().getResources().getDisplayMetrics().widthPixels;
        getWindow().setAttributes(lp);
        super.show();
    }

    public interface OnDoneListener {
        void onDelete(int position);
        void onSave(int position);
    }

}
