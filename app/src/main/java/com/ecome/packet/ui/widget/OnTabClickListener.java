package com.ecome.packet.ui.widget;

/**
 * Created by mingkg21 on 2017/9/29.
 */

public interface OnTabClickListener {

    void onTabClick(int position);

}
