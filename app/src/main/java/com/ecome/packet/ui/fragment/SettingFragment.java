package com.ecome.packet.ui.fragment;

import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;

public class SettingFragment extends MenuFragment {

    private TextView mTitleTV;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_main_list;
    }

    @Override
    protected void initData() {
        position = 0;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        mTitleTV = view.findViewById(R.id.fragment_main_list_title_tv);
        setTitle("设置");
    }

    @Override
    protected void setTitle(String title) {
        mTitleTV.setText(title);
    }

    @Override
    protected boolean isMain() {
        return true;
    }

    @Override
    public void onUserLoginStateChange(boolean isLogin) {
        loadData();
        super.onUserLoginStateChange(isLogin);
    }
}
