package com.ecome.packet.ui.fragment;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppBaseEntity;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Business;
import com.ecome.packet.entity.Drug;
import com.ecome.packet.entity.KuFang;
import com.ecome.packet.entity.Provider;
import com.ecome.packet.entity.Voucher;
import com.ecome.packet.entity.VoucherDrug;
import com.ecome.packet.model.BackendManageModel;
import com.ecome.packet.model.DoctorWorkspaceModel;
import com.ecome.packet.model.MedicineManageModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.DrugAdapter;
import com.ecome.packet.ui.dialog.ConfirmDialog;
import com.ecome.packet.ui.dialog.DialogManager;
import com.ecome.packet.ui.dialog.KuFangDialog;
import com.ecome.packet.ui.widget.BottomSheetListLayout;
import com.ecome.packet.ui.widget.DrugInputLayout;
import com.mk.core.util.IMMUtils;
import com.mk.core.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class AddVoucherFragment extends AppFragment {

    private DoctorWorkspaceModel mDoctorWorkspaceModel;
    private BackendManageModel mBackendManageModel;
    private MedicineManageModel mMedicineManageModel;

    private EditText mFapiaoET;
    private EditText mRemarkET;

    private TextView mCategoryTV;
    private TextView mTypeTV;
    private TextView mProviderTV;

    private DrugInputLayout mDrugInputLayout;
    private LinearLayout mDrugsLayout;

    private EditText mDrugET;
    private ListView mDrugLV;
    private View mDrugLayout;
    private DrugAdapter mDrugAdapter;

    private ArrayList<Drug> mDrugs = new ArrayList<>();
    private ArrayList<KuFang> mKuFangs = new ArrayList<>();
    private ArrayList<Provider> mProviders = new ArrayList<>();
    private ArrayList<Business> mBusinesses = new ArrayList<>();
    private ArrayList<VoucherDrug> mVoucherDrugs = new ArrayList<>();

    private KuFang mKuFang;
    private Voucher mVoucher;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_voucher_modify;
    }

    @Override
    protected void initData() {
        super.initData();
        mVoucher = (Voucher) getArguments().get(ActivityRouter.EXTRA_NAME_VOUCHER);
    }

    @Override
    protected void initView(final View view) {
        super.initView(view);

        setTitle("新增单据");

        mBackendManageModel = new BackendManageModel();
        mBackendManageModel.addCallback(this);

        mDoctorWorkspaceModel = new DoctorWorkspaceModel();
        mDoctorWorkspaceModel.addCallback(this);

        mMedicineManageModel = new MedicineManageModel();
        mMedicineManageModel.addCallback(this);

        mFapiaoET = view.findViewById(R.id.fragment_voucher_modify_fapiao_et);
        mRemarkET = view.findViewById(R.id.fragment_voucher_modify_remark_et);

        mDrugsLayout = view.findViewById(R.id.drugs_layout);
        addDrugInputLayout();
        view.findViewById(R.id.add_drug_input_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DrugInputLayout layout = addDrugInputLayout();
                layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        layout.onClickName();
                    }
                });
            }
        });

        mCategoryTV = view.findViewById(R.id.fragment_voucher_modify_category_tv);
//        mCategoryTV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DialogManager.showKuFangListDialog(getContext(), mKuFangs, new BottomSheetListLayout.OnItemListener<KuFang>() {
//                    @Override
//                    public void onItemClick(KuFang data) {
//                        mCategoryTV.setText(data.getName());
//                        mCategoryTV.setTag(data);
//                    }
//
//                    @Override
//                    public String getItemContent(KuFang data) {
//                        return null;
//                    }
//                });
//            }
//        });
        mTypeTV = view.findViewById(R.id.fragment_voucher_modify_type_tv);
        mProviderTV = view.findViewById(R.id.fragment_voucher_modify_provider_tv);
        mProviderTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showProviderListDialog(getContext(), mProviders, new BottomSheetListLayout.OnItemListener<Provider>() {
                    @Override
                    public void onItemClick(Provider data) {
                        mProviderTV.setText(data.getName());
                        mProviderTV.setTag(data);
                    }

                    @Override
                    public String getItemContent(Provider data) {
                        return null;
                    }
                });
            }
        });

        mDrugLayout = view.findViewById(R.id.drug_layout);
        mDrugET = view.findViewById(R.id.drug_et);
        mDrugLV = view.findViewById(R.id.drug_lv);
        mDrugAdapter = new DrugAdapter(getContext());
        mDrugLV.setAdapter(mDrugAdapter);
        mDrugLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mDrugInputLayout != null) {
                    mDrugInputLayout.setDrug(mDrugAdapter.getItem(position));
                    mDrugLayout.setVisibility(View.GONE);
                }
            }
        });
        mDrugET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                ArrayList<Drug> drugs = new ArrayList<>();
                if (!TextUtils.isEmpty(str)) {
                    for (Drug drug : mDrugs) {
                        if (drug.getName().contains(str) || (drug.getPy() != null && drug.getPy().contains(str.toUpperCase()))) {
                            drugs.add(drug);
                        }
                    }
                } else {
                    drugs.addAll(mDrugs);
                }

                mDrugAdapter.setAll(drugs);
            }
        });

        view.findViewById(R.id.fragment_vocher_modify_submit_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String fapiao = mFapiaoET.getText().toString().trim();
                String remark = mRemarkET.getText().toString().trim();

                String providerName = mProviderTV.getText().toString().trim();

                if (TextUtils.isEmpty(providerName)) {
                    ToastUtil.showToast("请选择供应商！");
                    return;
                }

                Provider provider = (Provider) mProviderTV.getTag();

                StringBuilder detailSB = new StringBuilder();

                for (int i = 0; i < mDrugsLayout.getChildCount(); i++) {
                    View childView = mDrugsLayout.getChildAt(i);
                    if (childView instanceof DrugInputLayout) {
                        String data = ((DrugInputLayout) childView).getData();
                        if (TextUtils.isEmpty(data)) {
                            return;
                        }
                        detailSB.append(data);
                        detailSB.append("|");
                    }
                }

                String voucherNo = "";
                if (mVoucher != null) {
                    voucherNo = mVoucher.getVoucherNo();
                }

                mBackendManageModel.modifyMkRuKu(provider.getId(), voucherNo, fapiao, remark, detailSB.toString(), mKuFang.getId(), 6);

            }
        });

        loadData();
    }

    @Override
    protected void loadData() {
        mDoctorWorkspaceModel.getDrug();
        mBackendManageModel.getKuFang();
        mBackendManageModel.getBusiness();
        mBackendManageModel.getProvider();
        if (mVoucher != null) {
            mMedicineManageModel.queryMdVoucherDetail(mVoucher.getVoucherNo());
        }
        showLoadingView();
    }

    private DrugInputLayout addDrugInputLayout() {
        final DrugInputLayout layout = new DrugInputLayout(getContext());
        layout.setOnDrugInputLayoutListener(new DrugInputLayout.OnDrugInputLayoutListener() {

            @Override
            public void onNameClick() {
                mDrugET.setText("");
                mDrugAdapter.setAll(mDrugs);
                mDrugInputLayout = layout;
                mDrugLayout.setVisibility(View.VISIBLE);
                mDrugET.post(new Runnable() {
                    @Override
                    public void run() {
                        mDrugET.requestFocus();
                        IMMUtils.showSoftInput(getActivity(), mDrugET);
                    }
                });
            }

            @Override
            public void onNumChanged() {
            }
        });
        mDrugsLayout.addView(layout);
        return layout;
    }

    private void checkData() {
        if (mDrugs.isEmpty()) {
            return;
        }
        if (mKuFangs.isEmpty()) {
            return;
        }
        if (mProviders.isEmpty()) {
            return;
        }
        if (mBusinesses.isEmpty()) {
            return;
        }
        if (mVoucher != null) {
            if (mVoucherDrugs.isEmpty()) {
                return;
            }
        }
        for (Business business : mBusinesses) {
            if (business.getId() == 6) {
                mTypeTV.setText(business.getName());
                break;
            }
        }

        if (mVoucher == null) {
            new KuFangDialog(getContext(), mKuFangs, new KuFangDialog.OnDataListener() {
                @Override
                public void onSelected(KuFang data) {
                    mKuFang = data;
                    mCategoryTV.setText(data.getName());

                    ArrayList<Drug> tempDrugs = new ArrayList<>();
                    for (Drug drug : mDrugs) {
                        if (drug.getCls() == mKuFang.getId()) {
                            tempDrugs.add(drug);
                        }
                    }

                    mDrugs.clear();
                    mDrugs.addAll(tempDrugs);
                }

                @Override
                public void onCancel() {
                    finish();
                }
            }).show();
        } else {
            for (KuFang kuFang : mKuFangs) {
                if (kuFang.getId() == mVoucher.getKufangId()) {
                    mKuFang = kuFang;
                    mCategoryTV.setText(mKuFang.getName());
                    break;
                }
            }

            ArrayList<Drug> tempDrugs = new ArrayList<>();
            for (Drug drug : mDrugs) {
                if (drug.getCls() == mKuFang.getId()) {
                    tempDrugs.add(drug);
                }
            }

            mDrugs.clear();
            mDrugs.addAll(tempDrugs);

            for (Provider provider : mProviders) {
                if (provider.getId() == mVoucher.getGongYingShangId()) {
                    mProviderTV.setText(provider.getName());
                    mProviderTV.setTag(provider);
                    break;
                }
            }

            mFapiaoET.setText(mVoucher.getFaPiaoNo());
            mRemarkET.setText(mVoucher.getRemark());

            for (VoucherDrug tempDetail : mVoucherDrugs) {
                DrugInputLayout inputLayout = null;
                for (int i = 0; i < mDrugsLayout.getChildCount(); i++) {
                    View childView = mDrugsLayout.getChildAt(i);
                    if (childView instanceof DrugInputLayout) {
                        inputLayout = (DrugInputLayout) childView;
                        if (!inputLayout.hasData()) {
                            break;
                        } else {
                            inputLayout = null;
                        }
                    }
                }

                if (tempDetail == null) {
                    inputLayout = addDrugInputLayout();
                }

                for (Drug drug : mDrugs) {
                    if (drug.getId() == tempDetail.getId()) {
                        inputLayout.setDrug(drug);
                        break;
                    }
                }

                inputLayout.setNum(tempDetail.getCount());
                inputLayout.setPrice(tempDetail.getPrice());

            }
        }
        hideLoadingView();
    }

    @Override
    public boolean onPreLoad(String key) {
        if(mBackendManageModel.isModifyMdRuKuKey(key)) {
            showLoadingDialog();
            return true;
        } else if (mMedicineManageModel.isConfirmMdVoucherKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
       if(mBackendManageModel.isModifyMdRuKuKey(key)) {
            hideLoadingDialog();
            return true;
        } else if (mMedicineManageModel.isConfirmMdVoucherKey(key)) {
           hideLoadingDialog();
           return true;
       }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mDoctorWorkspaceModel.isGetDrugKey(key)
                || mBackendManageModel.isGetKuFangKey(key)
                || mBackendManageModel.isGetProviderKey(key)
                || mBackendManageModel.isGetBusinessKey(key)
                || mMedicineManageModel.isQueryMdVoucherDetailKey(key)) {
            showErrorView();
            return true;
        } else if(mBackendManageModel.isModifyMdRuKuKey(key)) {
            AppResponseEntity appResponseEntity = (AppResponseEntity) data;
            if (appResponseEntity != null) {
                ToastUtil.showToast(appResponseEntity.getMsg());
            } else {
                ToastUtil.showToast("请求失败，请稍后重试！");
            }
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mDoctorWorkspaceModel.isGetDrugKey(key)) {
            AppResponseEntity<List<Drug>> appResponseEntity = (AppResponseEntity<List<Drug>>) data;
            mDrugs.clear();
            mDrugs.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mBackendManageModel.isGetKuFangKey(key)) {
            AppResponseEntity<List<KuFang>> appResponseEntity = (AppResponseEntity<List<KuFang>>) data;
            mKuFangs.clear();
            mKuFangs.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mBackendManageModel.isGetProviderKey(key)) {
            AppResponseEntity<List<Provider>> appResponseEntity = (AppResponseEntity<List<Provider>>) data;
            mProviders.clear();
            mProviders.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mBackendManageModel.isGetBusinessKey(key)) {
            AppResponseEntity<List<Business>> appResponseEntity = (AppResponseEntity<List<Business>>) data;
            mBusinesses.clear();
            mBusinesses.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mMedicineManageModel.isQueryMdVoucherDetailKey(key)) {
            AppResponseEntity<List<VoucherDrug>> appResponseEntity = (AppResponseEntity<List<VoucherDrug>>) data;
            mVoucherDrugs.clear();
            mVoucherDrugs.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mBackendManageModel.isModifyMdRuKuKey(key)) {
            AppResponseEntity<Voucher> appResponseEntity = (AppResponseEntity<Voucher>) data;
            final Voucher voucher = appResponseEntity.getData();
            ToastUtil.showToast(voucher.getMsg());
            new ConfirmDialog(getContext()).setContent("是否确认单据？").setSureListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMedicineManageModel.confirmMdVoucher(voucher.getVoucherNo());
                }
            }).setCancelListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            }).show();
            return true;
        } else if (mMedicineManageModel.isConfirmMdVoucherKey(key)) {
            AppResponseEntity<List<AppBaseEntity>> appResponseEntity = (AppResponseEntity<List<AppBaseEntity>>) data;
            List<AppBaseEntity> list = appResponseEntity.getData();
            if (list != null && list.size() > 0){
                AppBaseEntity appBaseEntity = list.get(0);
                ToastUtil.showToast(appBaseEntity.getMsg());
            }
            finish();
            return true;
        }
        return super.onSuccess(key, data);
    }

    @Override
    public void finish() {
        super.finish();
        EventBus.getDefault().post(new Voucher());
    }

    @Override
    public boolean onBackPressed() {
        if (mDrugLayout != null && mDrugLayout.getVisibility() == View.VISIBLE) {
            mDrugLayout.setVisibility(View.GONE);
            IMMUtils.hideSoftInput(getActivity());
            return true;
        }
        return super.onBackPressed();
    }

}
