package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Performance;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class PerformanceAdapter extends BaseAdapter<Performance> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_workload;
    }

    @Override
    public BaseViewHolder<Performance> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView);
    }

    private static class Holder extends BaseViewHolder<Performance> {

        private TextView mNameTV;
        private TextView mValueTV;
        private Button mPayBtn;

        public Holder(View itemView) {
            super(itemView);


            mNameTV = itemView.findViewById(R.id.item_workload_name_tv);
            mValueTV = itemView.findViewById(R.id.item_workload_value_tv);
            mPayBtn = itemView.findViewById(R.id.item_workload_pay_btn);

            mPayBtn.setVisibility(View.GONE);
        }

        @Override
        public void onBind(final Performance data, int position) {
            mNameTV.setText(data.getDoctorName());
            StringBuilder sb = new StringBuilder();
            sb.append(String.format("费用总额：%.2f元", data.getFeeSum()));
            sb.append("\n");
            sb.append("费用折扣：");
            sb.append(data.getDiscount());
            mValueTV.setText(sb);
        }
    }

}
