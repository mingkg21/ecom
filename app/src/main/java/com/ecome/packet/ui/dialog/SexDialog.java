package com.ecome.packet.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.ecome.packet.R;

/**
 * Created by mingkg21 on 2017/10/6.
 */

public class SexDialog extends Dialog implements View.OnClickListener {

    private OnDoneListener mOnDoneListener;

    public SexDialog(@NonNull Context context, OnDoneListener onDoneListener) {
        super(context, R.style.dialogstyle);

        setContentView(R.layout.dialog_sex);

        mOnDoneListener = onDoneListener;

        Window window = getWindow();
        window.setGravity(Gravity.BOTTOM);

        findViewById(R.id.dialog_sex_man).setOnClickListener(this);
        findViewById(R.id.dialog_sex_woman).setOnClickListener(this);
        findViewById(R.id.dialog_sex_cancel).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.dialog_sex_man) {
            if (mOnDoneListener != null) {
                mOnDoneListener.onDone(1);
            }
        } else if (v.getId() == R.id.dialog_sex_woman) {
            if (mOnDoneListener != null) {
                mOnDoneListener.onDone(2);
            }
        }
        dismiss();
    }

    @Override
    public void show() {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = getContext().getResources().getDisplayMetrics().widthPixels;
        getWindow().setAttributes(lp);
        super.show();
    }


    public interface OnDoneListener {
        void onDone(int sex);
    }

}
