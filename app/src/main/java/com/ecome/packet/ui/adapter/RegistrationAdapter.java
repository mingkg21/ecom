package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Registration;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class RegistrationAdapter extends BaseAdapter<Registration> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_common_info;
    }

    @Override
    public BaseViewHolder<Registration> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView);
    }

    private static class Holder extends BaseViewHolder<Registration> {

        private TextView mNameTV;
        private TextView mContentTV;

        public Holder(View itemView) {
            super(itemView);
            mNameTV = findViewById(R.id.name_tv);
            mContentTV = findViewById(R.id.content_tv);

        }

        @Override
        public void onBind(Registration data, int position) {
            mNameTV.setText(data.getState());

            StringBuilder sb = new StringBuilder();
            sb.append("科室名称：");
            sb.append(data.getDoctorDepament());
            sb.append("\n");
            sb.append("挂号医生：");
            sb.append(data.getDoctorName());
            sb.append("\n");
            sb.append("挂号时间：");
            sb.append(data.getDatetime());

            mContentTV.setText(sb);
        }
    }
}
