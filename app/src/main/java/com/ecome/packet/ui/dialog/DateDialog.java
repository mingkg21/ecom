package com.ecome.packet.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.ecome.packet.R;

public class DateDialog extends Dialog {

    private DatePicker mDatePicker;

    public DateDialog(@NonNull Context context, boolean isDateStart, int year, int month, int day, final OnDateChoiceListener onDateChoiceListener) {
        super(context);

        setContentView(R.layout.dialog_date);

        TextView titleTV = findViewById(R.id.dialog_date_title);
        if (isDateStart) {
            titleTV.setText("选择开始日期");
        } else {
            titleTV.setText("选择结束日期");
        }

        mDatePicker = findViewById(R.id.dialog_date_dp);
        mDatePicker.init(year, month, day, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            }
        });

        findViewById(R.id.dialog_date_sure).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onDateChoiceListener != null) {
                    onDateChoiceListener.onChoice(mDatePicker.getYear(), mDatePicker.getMonth(), mDatePicker.getDayOfMonth());
                }
                dismiss();
            }
        });

        findViewById(R.id.dialog_date_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface OnDateChoiceListener {
        void onChoice(int year, int month, int day);
    }

}
