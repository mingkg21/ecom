package com.ecome.packet.ui.adapter;

import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.entity.Patient;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class MyPatientAdapter extends BaseAdapter<Patient> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_my_patient;
    }

    @Override
    public BaseViewHolder<Patient> getBaseViewHolder(View itemView, int viewType) {
        return new MyPatientViewHolder(itemView);
    }

}
