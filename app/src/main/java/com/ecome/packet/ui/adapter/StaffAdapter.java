package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Staff;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;
import com.mk.core.util.ImageUtil;

public class StaffAdapter extends BaseAdapter<Staff> {

    private OnModifyClickListener mOnModifyClickListener;

    private boolean mIsPaiBan;

    public StaffAdapter(OnModifyClickListener onModifyClickListener, boolean isPaiBan) {
        mOnModifyClickListener = onModifyClickListener;
        this.mIsPaiBan = isPaiBan;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_config;
    }

    @Override
    public BaseViewHolder<Staff> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView, mOnModifyClickListener, mIsPaiBan);
    }

    private static class Holder extends BaseViewHolder<Staff> {

        private TextView mNameTV;
        private TextView mValueTV;
        private ImageView mIconIV;
        private Button mModifyBtn;

        private OnModifyClickListener mOnModifyClickListener;

        public Holder(View itemView, OnModifyClickListener onModifyClickListener, boolean isPaiBan) {
            super(itemView);

            mOnModifyClickListener = onModifyClickListener;

            mIconIV = itemView.findViewById(R.id.icon_iv);
            mNameTV = itemView.findViewById(R.id.name_tv);
            mValueTV = itemView.findViewById(R.id.value_tv);
            mModifyBtn = itemView.findViewById(R.id.modify_btn);

            mIconIV.setVisibility(View.VISIBLE);

            if (isPaiBan) {
                mModifyBtn.setText("排班");
            }
        }

        @Override
        public void onBind(final Staff data, int position) {
            mNameTV.setText(data.getName());
            StringBuilder sb = new StringBuilder();
            sb.append("部门：");
            sb.append(data.getDeptName());
            sb.append("\n");
            sb.append("角色：");
            sb.append(data.getRoleName());
            sb.append("\n");
            sb.append("看诊能力：");
            sb.append(data.getKanzhengMinute());
            sb.append("分钟");
            mValueTV.setText(sb);
            mModifyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnModifyClickListener != null) {
                        mOnModifyClickListener.onClick(data);
                    }
                }
            });
            ImageUtil.loadImage(data.getPeoplePic(), mIconIV, R.drawable.ic_doctor_default);
        }
    }

    public interface OnModifyClickListener {
        void onClick(Staff config);
    }

}
