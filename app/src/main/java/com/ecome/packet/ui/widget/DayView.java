package com.ecome.packet.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.WorkDay;

public class DayView extends LinearLayout {

    private CheckBox mMorningCB;
    private CheckBox mAfternoonCB;
    private CheckBox mNightCB;

    public DayView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(getContext()).inflate(R.layout.layout_day, this);

        TextView dayTV = findViewById(R.id.day_tv);

        mMorningCB = findViewById(R.id.layout_day_morning_cb);
        mAfternoonCB = findViewById(R.id.layout_day_afternoon_cb);
        mNightCB = findViewById(R.id.layout_day_night_cb);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.DayView);
        String day = typedArray.getString(R.styleable.DayView_day);
        typedArray.recycle();

        dayTV.setText(day);
    }

    public void setData(WorkDay workDay) {
        mMorningCB.setChecked(workDay.isMorning());
        mAfternoonCB.setChecked(workDay.isAfternoon());
        mNightCB.setChecked(workDay.isNight());
    }

    public String getMorning() {
        return mMorningCB.isChecked() ? "1" : "0";
    }

    public String getAfternoon() {
        return mAfternoonCB.isChecked() ? "1" : "0";
    }

    public String getNight() {
        return mNightCB.isChecked() ? "1" : "0";
    }

}
