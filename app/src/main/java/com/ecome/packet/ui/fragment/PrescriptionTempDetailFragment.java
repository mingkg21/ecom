package com.ecome.packet.ui.fragment;

import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Temp;
import com.ecome.packet.entity.TempDetail;
import com.ecome.packet.model.DoctorWorkspaceModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.PrescriptionTempDetailAdapter;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class PrescriptionTempDetailFragment extends AppRecyclerFragment<TempDetail> {

    private DoctorWorkspaceModel mDoctorWorkspaceModel;

    private Temp mTemp;

    private boolean mIsChoiceMode;

    @Override
    protected BaseAdapter<TempDetail> getAdapter() {
        return new PrescriptionTempDetailAdapter(mIsChoiceMode);
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected void initData() {
        super.initData();
        mTemp = (Temp) getArguments().get(ActivityRouter.EXTRA_NAME_TEMP);
        mIsChoiceMode = getArguments().getBoolean(ActivityRouter.EXTRA_NAME_VALUE, false);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle(mTemp.getName());

        setTopDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_list_top_height));

        mRecyclerView.setVisibility(View.VISIBLE);
        setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Object data) {
            }
        });

        if (mIsChoiceMode) {
            setTitleBarRightText("选择药品");
        }

        mDoctorWorkspaceModel = new DoctorWorkspaceModel();
        mDoctorWorkspaceModel.addCallback(this);

        loadData();
    }

    @Override
    protected void onTitleBarRightAction() {
        super.onTitleBarRightAction();

        List<TempDetail> tempDetails = new ArrayList<>();

        for (TempDetail tempDetail : mAdapter.getDatas()) {
            if (tempDetail.isCheck()) {
                tempDetails.add(tempDetail);
            }
        }

        if (tempDetails.isEmpty()) {
            ToastUtil.showToast("请选择药品！");
            return;
        }

        EventBus.getDefault().post(tempDetails);
        finish();
    }

    @Override
    protected void loadData() {
        super.loadData();

        mDoctorWorkspaceModel.getTempDetail(mTemp.getId());
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mDoctorWorkspaceModel.isGetTempDetailKey(key)) {
            showLoadingView();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mDoctorWorkspaceModel.isGetTempDetailKey(key)) {
            hideLoadingView();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mDoctorWorkspaceModel.isGetTempDetailKey(key)) {
            AppResponseEntity<List<TempDetail>> responseEntity = (AppResponseEntity<List<TempDetail>>) data;
            List<TempDetail> tempDetails = responseEntity.getData();
            for (TempDetail detail : tempDetails) {
                detail.setCheck(true);
            }
            setAll(tempDetails);
            return true;
        }
        return super.onSuccess(key, data);
    }

}
