package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.TempDetail;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class  PrescriptionTempDetailAdapter extends BaseAdapter<TempDetail> {

    private boolean mIsChoiceMode;

    public PrescriptionTempDetailAdapter(boolean isChoiceMode) {
        mIsChoiceMode = isChoiceMode;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_temp;
    }

    @Override
    public BaseViewHolder<TempDetail> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView, mIsChoiceMode);
    }

    private static class Holder extends BaseViewHolder<TempDetail> {

        private TextView mNameTV;
        private TextView mContentTV;
        private CheckBox mCheckBox;

        public Holder(View itemView, boolean isChoiceMode) {
            super(itemView);
            mNameTV = findViewById(R.id.name_tv);
            mContentTV = findViewById(R.id.content_tv);
            mCheckBox = findViewById(R.id.choice_cb);
            mCheckBox.setVisibility(isChoiceMode ? View.VISIBLE : View.GONE);
        }

        @Override
        public void onBind(final TempDetail data, int position) {
            mNameTV.setText(data.getItemName());

            StringBuilder sb = new StringBuilder();
            sb.append("用量：");
            sb.append(data.getItemCount());
            sb.append("\n");
            sb.append("使用方式：");
            sb.append(data.getTempJianfaName());

            mContentTV.setText(sb);

            mCheckBox.setChecked(data.isCheck());

            mCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    data.setCheck(!data.isCheck());
                }
            });
        }
    }
}
