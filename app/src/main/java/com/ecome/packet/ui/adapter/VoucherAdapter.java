package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Voucher;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class VoucherAdapter extends BaseAdapter<Voucher> {

    private OnVoucherListener mOnVoucherListener;

    public VoucherAdapter(OnVoucherListener onVocherListener) {
        mOnVoucherListener = onVocherListener;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_vocher;
    }

    @Override
    public BaseViewHolder<Voucher> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView, mOnVoucherListener);
    }

    private static class Holder extends BaseViewHolder<Voucher> {

        private TextView mNameTV;
        private TextView mContentTV;
        private Button mConfirmBtn;
        private Button mModifyBtn;

        private OnVoucherListener mOnVoucherListener;

        public Holder(View itemView, OnVoucherListener onVoncherListener) {
            super(itemView);
            mOnVoucherListener = onVoncherListener;
            mNameTV = findViewById(R.id.name_tv);
            mContentTV = findViewById(R.id.content_tv);

            mConfirmBtn = findViewById(R.id.confirm_vocuher_btn);
            mModifyBtn = findViewById(R.id.modify_vocuher_btn);
        }

        @Override
        public void onBind(final Voucher data, int position) {
            mNameTV.setText("单据号：" + data.getVoucherNo());

            StringBuilder sb = new StringBuilder();
            sb.append("是否确认：");
            sb.append(data.isQueRen() ? "是" : "否");
            sb.append("\n");
            sb.append("确认人：");
            sb.append(data.getQuerenRenName());
            sb.append("\n");
            sb.append("操作人：");
            sb.append(data.getQuerenRenName() + " " + data.getInputDatetime());

            mContentTV.setText(sb);

            if (data.isQueRen()) {
                mConfirmBtn.setVisibility(View.GONE);
                mModifyBtn.setVisibility(View.GONE);
            } else {
                mConfirmBtn.setVisibility(View.VISIBLE);
                mModifyBtn.setVisibility(View.VISIBLE);
            }

            mConfirmBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnVoucherListener != null) {
                        mOnVoucherListener.onConfirm(data);
                    }
                }
            });

            mModifyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnVoucherListener != null) {
                        mOnVoucherListener.onModify(data);
                    }
                }
            });
        }
    }

    public interface OnVoucherListener {
        void onConfirm(Voucher voucher);
        void onModify(Voucher voucher);
    }
}
