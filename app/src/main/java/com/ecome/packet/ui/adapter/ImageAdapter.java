package com.ecome.packet.ui.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;
import com.mk.core.util.ImageUtil;
import com.ecome.packet.R;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class ImageAdapter extends BaseAdapter<String> {

    private static final int TYPE_FOOTER_VIEW = 100;

    private Context mContext;
    private View.OnClickListener mFooterClickListener;

    public ImageAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        if (viewType == TYPE_FOOTER_VIEW) {
            return R.layout.item_image_footer;
        }
        return R.layout.item_image;
    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1) {
            return TYPE_FOOTER_VIEW;
        }
        return super.getItemViewType(position);
    }

    @Override
    protected String getItem(int position) {
        if (position >= mDatas.size()) {
            return "";
        }
        return super.getItem(position);
    }

    @Override
    public BaseViewHolder<String> getBaseViewHolder(View itemView, int viewType) {
        if (viewType == TYPE_FOOTER_VIEW) {
            return new FooterHolder(itemView);
        }
        return new Holder(itemView);
    }

    public void setFooterClickListener(View.OnClickListener footerClickListener) {
        mFooterClickListener = footerClickListener;
    }

    private class FooterHolder extends BaseViewHolder<String> {

        public FooterHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mFooterClickListener != null) {
                        mFooterClickListener.onClick(v);
                    }
                }
            });
        }

        @Override
        public void onBind(String data, int position) {

        }
    }

    private class Holder extends BaseViewHolder<String> {

        private ImageView mImageView;
        private ImageView mDeleteIV;

        public Holder(View itemView) {
            super(itemView);

            mImageView = (ImageView) itemView.findViewById(R.id.item_image_image);
            mDeleteIV = (ImageView) itemView.findViewById(R.id.item_image_delete);
        }

        @Override
        public void onBind(final String data, int position) {
            ImageUtil.loadImage(data, mImageView);

            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            mDeleteIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    remove(data);
                }
            });
        }
    }

}
