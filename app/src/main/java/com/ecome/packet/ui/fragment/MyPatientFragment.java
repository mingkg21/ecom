package com.ecome.packet.ui.fragment;

import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Patient;
import com.ecome.packet.ui.adapter.MyPatientAdapter;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.util.DimensionUtil;

public class MyPatientFragment extends PrescribeFragment {

    private TextView mTitleTV;

    @Override
    protected BaseAdapter<Patient> getAdapter() {
        return new MyPatientAdapter();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_my_patient;
    }

    @Override
    protected void initData() {
        super.initData();
        setMain();
    }

    @Override
    public boolean isMain() {
        return true;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        setDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_line_height));
        mTitleTV = view.findViewById(R.id.fragment_my_patient_title_tv);
        mTitleTV.setText("我的病人");
        int padding = DimensionUtil.DIPToPX(15);
        mSearchLayout.setPadding(padding, 0, padding, padding);
        if (isMain()) {
            loadData();
        }
    }

}
