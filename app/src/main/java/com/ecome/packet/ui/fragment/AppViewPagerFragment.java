package com.ecome.packet.ui.fragment;

import com.mk.core.ui.fragment.BaseViewPagerFragment;
import com.ecome.packet.R;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public abstract class AppViewPagerFragment extends BaseViewPagerFragment {

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_com_viewpager;
    }

    @Override
    protected int getTitleBarTitleResId() {
        return R.id.title_bar_title;
    }

    @Override
    protected int getTabResId() {
        return R.id.fragment_com_viewpager_tab;
    }

    @Override
    protected int getViewPagerResId() {
        return R.id.fragment_com_viewpager_vp;
    }

    @Override
    protected int getTabLayoutResId() {
        return 0;
    }

    @Override
    protected int getTitleBarBackResId() {
        return R.id.title_bar_back;
    }

}
