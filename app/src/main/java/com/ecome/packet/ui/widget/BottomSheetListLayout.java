package com.ecome.packet.ui.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ecome.packet.R;

import java.util.ArrayList;
import java.util.List;

public class BottomSheetListLayout<T> extends LinearLayout {

    private BottomSheetDialog mBottomSheetDialog;
    private Adapter mAdapter;

    private OnItemListener<T> mOnItemListener;

    public BottomSheetListLayout(Context context) {
        super(context);
        init();
    }

    public BottomSheetListLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BottomSheetListLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_fried_list, this);

        findViewById(R.id.cancel_tv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBottomSheetDialog != null) {
                    mBottomSheetDialog.dismiss();
                }
            }
        });

        mAdapter = new Adapter(getContext());
        mAdapter.setOnItemListener(mOnItemListener);

        ListView listView = findViewById(R.id.data_lv);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mOnItemListener != null) {
                    mOnItemListener.onItemClick((T) parent.getAdapter().getItem(position));
                }
                if (mBottomSheetDialog != null) {
                    mBottomSheetDialog.dismiss();
                }
            }
        });
    }

    public void setDatas(List<T> datas) {
        mAdapter.setAll(datas);
    }

    public void setBottomSheetDialog(BottomSheetDialog bottomSheetDialog) {
        mBottomSheetDialog = bottomSheetDialog;
    }

    private static class Adapter<T> extends BaseAdapter {

        private ArrayList<T> mList;
        private Context mContext;
        private OnItemListener<T> mOnItemListener;

        public Adapter(Context context) {
            mContext = context;
            mList = new ArrayList<>();
        }

        public void setAll(List<T> datas) {
            mList.clear();
            mList.addAll(datas);
            notifyDataSetChanged();
        }

        public void setOnItemListener(OnItemListener<T> onItemListener) {
            mOnItemListener = onItemListener;
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public T getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_fried, null);
            }

            TextView tv = (TextView) convertView;

            T data = getItem(position);
            if (mOnItemListener != null) {
                tv.setText(mOnItemListener.getItemContent(data));
            }

            return convertView;
        }

    }

    public void setOnItemListener(OnItemListener<T> onItemListener) {
        mOnItemListener = onItemListener;
        if (mAdapter != null) {
            mAdapter.setOnItemListener(onItemListener);
        }
    }

    public interface OnItemListener<T> {
        void onItemClick(T data);
        String getItemContent(T data);
    }
}
