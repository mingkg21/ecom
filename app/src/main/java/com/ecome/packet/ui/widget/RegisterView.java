package com.ecome.packet.ui.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ecome.packet.R;
import com.mk.core.util.IMMUtils;

/**
 * Created by mingkg21 on 2017/10/10.
 */

public class RegisterView extends LinearLayout {

    private EditText mPhoneET;
    private PasswordView mPasswordET;
    private EditText mSecurityET;
    private TextView mRegisterBtn;

    private RegisterViewAction mRegisterViewAction;

    public RegisterView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(getContext()).inflate(R.layout.layout_register, this);

        init();
    }

    private void init() {
        mPhoneET = (EditText) findViewById(R.id.layout_register_phone);
        mPasswordET = (PasswordView) findViewById(R.id.layout_register_password);
        mSecurityET = (EditText) findViewById(R.id.layout_register_security_code);
        mRegisterBtn = (TextView) findViewById(R.id.layout_register_btn);
    }

    public void setType(int type) {
    }

    public void setConfirmText(String text) {
        mRegisterBtn.setText(text);
    }

    public void setRegisterViewAction(RegisterViewAction registerViewAction) {
        mRegisterViewAction = registerViewAction;
    }

    public interface RegisterViewAction {
        public void register(String phone, String password, String smsCode);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        IMMUtils.hideSoftInput(getContext(), mPhoneET);
        IMMUtils.hideSoftInput(getContext(), mPasswordET);
        IMMUtils.hideSoftInput(getContext(), mSecurityET);
    }

}
