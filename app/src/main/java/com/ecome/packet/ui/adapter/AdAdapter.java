package com.ecome.packet.ui.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ecome.packet.R;
import com.mk.core.ui.widget.RatioImageView;
import com.mk.core.util.ImageUtil;

import java.util.ArrayList;
import java.util.List;

public class AdAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<String> datas;

    private boolean mHasChange;

    public AdAdapter(Context context) {
        mContext = context;
        datas = new ArrayList<>();
    }

    public void setAll(List<String> datas) {
        this.datas.clear();
        if (datas != null) {
            this.datas.addAll(datas);
        }
        mHasChange = true;
        notifyDataSetChanged();
    }

    public void clear() {
        datas.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (!datas.isEmpty()) {
            return datas.size();
        }
        return 1;
    }

    @Override
    public int getItemPosition(Object object) {
//        if (mHasChange) {
//            mHasChange = false;
//            return POSITION_NONE;
//        }
//        return super.getItemPosition(object);
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        RatioImageView picImg = new RatioImageView(mContext);
        picImg.setRatio(360, 138);
        picImg.setScaleType(ImageView.ScaleType.FIT_XY);

        if (datas.isEmpty()) {
            picImg.setImageResource(R.drawable.ic_ecome);
            picImg.setScaleType(ImageView.ScaleType.FIT_XY);
        } else {
            ImageUtil.loadImage(datas.get(position), picImg, R.drawable.ic_ecome);
            picImg.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }

        container.removeView(picImg);
        container.addView(picImg);
        return picImg;
    }

    @Override
    public void destroyItem(ViewGroup container, int position,
                            Object object) {
    }

}
