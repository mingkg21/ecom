package com.ecome.packet.ui.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Drug;
import com.mk.core.util.ToastUtil;

public class DrugInputLayout extends LinearLayout {

    private TextView mNameET;
    private EditText mNumberET;
    private EditText mPriceET;

    private Drug mDrug;

    private OnDrugInputLayoutListener mOnDrugInputLayoutListener;

    public DrugInputLayout(Context context) {
        super(context);
        init();
    }

    public DrugInputLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrugInputLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_drug_input, this);

        mNameET = findViewById(R.id.name_et);
        mNumberET = findViewById(R.id.number_et);
        mPriceET = findViewById(R.id.price_et);

        findViewById(R.id.delete_iv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewGroup)getParent()).removeView(DrugInputLayout.this);
            }
        });

        mNameET.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDrugInputLayoutListener != null) {
                    mOnDrugInputLayoutListener.onNameClick();
                }
            }
        });

        mNumberET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mOnDrugInputLayoutListener != null) {
                    mOnDrugInputLayoutListener.onNumChanged();
                }
            }
        });
    }

    /**
     *  "|药品id,入库数量（按销售的最小单位）,进货单价（按销售的最小单位）,批号, 生产厂家id,生产日期,有效期,产地id,备注|药品id,入库数量（按销售的最小单位）,进货单价（按销售的最小单位）,批号, 生产厂家,生产日期,产地,备注|"
     * @return
     */
    public String getData() {
        if (mDrug == null) {
            ToastUtil.showToast("请选择药品");
            return null;
        }
        String number = mNumberET.getText().toString().trim();
        if (TextUtils.isEmpty(number)) {
            ToastUtil.showToast("请填写数量");
            return null;
        }
        String price = mPriceET.getText().toString().trim();
        if (TextUtils.isEmpty(price)) {
            price = "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(mDrug.getId());
        sb.append(',');
        sb.append(number);
        sb.append(",");
        sb.append(price);
        sb.append(",");
        sb.append(",");
        sb.append(",,,,");
        return sb.toString();
    }

    public boolean hasData() {
        if (mDrug == null) {
            return false;
        }
        return true;
    }

    public void onClickName() {
        if (mNameET != null) {
            mNameET.performClick();
        }
    }

    public void setPrice(String price) {
        mPriceET.setText(price);
    }

    public void setNum(String num) {
        mNumberET.setText(num);
    }

    public void setDrug(Drug drug) {
        mDrug = drug;
        mNameET.setText(drug.getName());
        mNumberET.requestFocus();
    }

    public Drug getDrug() {
        return mDrug;
    }


    public int getNum() {
        try {
            return Integer.valueOf(mNumberET.getText().toString().trim());
        } catch (Exception e) {

        }
        return 0;
    }

    public void setOnDrugInputLayoutListener(OnDrugInputLayoutListener onDrugInputLayoutListener) {
        mOnDrugInputLayoutListener = onDrugInputLayoutListener;
    }

    public interface OnDrugInputLayoutListener {
        void onNameClick();
        void onNumChanged();
    }

}
