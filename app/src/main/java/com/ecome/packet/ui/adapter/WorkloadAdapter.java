package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Workload;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class WorkloadAdapter extends BaseAdapter<Workload> {

    private OnPayClickListener mOnPayClickListener;

    public WorkloadAdapter(OnPayClickListener onPayClickListener) {
        mOnPayClickListener = onPayClickListener;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_workload;
    }

    @Override
    public BaseViewHolder<Workload> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView, mOnPayClickListener);
    }

    private static class Holder extends BaseViewHolder<Workload> {

        private TextView mNameTV;
        private TextView mValueTV;
        private Button mPayBtn;

        private OnPayClickListener mOnModifyClickListener;

        public Holder(View itemView, OnPayClickListener onModifyClickListener) {
            super(itemView);

            mOnModifyClickListener = onModifyClickListener;

            mNameTV = itemView.findViewById(R.id.item_workload_name_tv);
            mValueTV = itemView.findViewById(R.id.item_workload_value_tv);
            mPayBtn = itemView.findViewById(R.id.item_workload_pay_btn);

            mPayBtn.setVisibility(mOnModifyClickListener == null ? View.GONE : View.VISIBLE);
        }

        @Override
        public void onBind(final Workload data, int position) {
            mNameTV.setText(data.getPatientName());
            StringBuilder sb = new StringBuilder();
            sb.append(String.format("处方价格：%.2f元", data.getPrice()));
            sb.append("\n");
            sb.append("开方医生：");
            sb.append(data.getDoctorName());
            sb.append("\n");
            sb.append("开方时间：");
            sb.append(data.getDatetime());
            mValueTV.setText(sb);
            mPayBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnModifyClickListener != null) {
                        mOnModifyClickListener.onClick(data);
                    }
                }
            });

            if (data.hadPay()) {
                mPayBtn.setText("已支付");
                mPayBtn.setEnabled(false);
            } else {
                mPayBtn.setText("未支付");
                mPayBtn.setEnabled(true);
            }
        }
    }

    public interface OnPayClickListener {
        void onClick(Workload workload);
    }

}
