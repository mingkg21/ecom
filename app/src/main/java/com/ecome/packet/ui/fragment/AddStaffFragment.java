package com.ecome.packet.ui.fragment;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.app.App;
import com.ecome.packet.entity.AppBaseEntity;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Department;
import com.ecome.packet.entity.Role;
import com.ecome.packet.entity.Staff;
import com.ecome.packet.entity.ZhiCheng;
import com.ecome.packet.model.BackendManageModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.dialog.DialogManager;
import com.ecome.packet.ui.widget.BottomSheetListLayout;
import com.ecome.packet.util.Constants;
import com.ecome.packet.util.QiNiuUtils;
import com.mk.core.util.ImageUtil;
import com.mk.core.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class AddStaffFragment extends AppFragment {

    private static final int REQUEST_CODE_SELECT_IMG = 3000;

    private BackendManageModel mBackendManageModel;

    private ArrayList<Role> mRoles = new ArrayList<>();
    private ArrayList<Department> mDepartments = new ArrayList<>();
    private ArrayList<ZhiCheng> mZhiChengs = new ArrayList<>();

    private ImageView mIconIV;

    private EditText mNameET;
    private EditText mAccountET;
    private EditText mTelET;
    private EditText mMinuteET;
    private EditText mFeatureET;
    private EditText mIntroET;

    private RadioGroup mSexRG;

    private TextView mRoleTV;
    private TextView mDepartmentTV;
    private TextView mZhiChengTV;

    private String mIconPath;

    private Staff mStaff;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_staff_modify;
    }

    @Override
    protected void initData() {
        super.initData();
        mStaff = (Staff) getArguments().get(ActivityRouter.EXTRA_NAME_STAFF);
    }

    @Override
    protected void initView(final View view) {
        super.initView(view);

        if (mStaff == null) {
            setTitle("新增员工信息");
        } else {
            setTitle("修改员工信息");
        }

        mBackendManageModel = new BackendManageModel();
        mBackendManageModel.addCallback(this);

        mIconIV = view.findViewById(R.id.fragment_staff_modify_icon_iv);

        mNameET = view.findViewById(R.id.fragment_staff_modify_name_et);
        mAccountET = view.findViewById(R.id.fragment_staff_modify_account_et);
        mTelET = view.findViewById(R.id.fragment_staff_modify_tel_et);
        mMinuteET = view.findViewById(R.id.fragment_staff_modify_minute_et);
        mFeatureET = view.findViewById(R.id.fragment_staff_modify_feature_et);
        mIntroET = view.findViewById(R.id.fragment_staff_modify_intro_et);

        mSexRG = view.findViewById(R.id.fragment_staff_modify_sex_rg);

        mRoleTV = view.findViewById(R.id.fragment_staff_modify_role_tv);
        mDepartmentTV = view.findViewById(R.id.fragment_staff_modify_department_tv);
        mZhiChengTV = view.findViewById(R.id.fragment_staff_modify_zhicheng_tv);

        mIconIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choiceImage();
            }
        });
        mRoleTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showRoleListDialog(getContext(), mRoles, new BottomSheetListLayout.OnItemListener<Role>() {
                    @Override
                    public void onItemClick(Role data) {
                        mRoleTV.setText(data.getName());
                        mRoleTV.setTag(data);
                    }

                    @Override
                    public String getItemContent(Role data) {
                        return null;
                    }
                });
            }
        });
        mDepartmentTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showDepartmentListDialog(getContext(), mDepartments, new BottomSheetListLayout.OnItemListener<Department>() {
                    @Override
                    public void onItemClick(Department data) {
                        mDepartmentTV.setText(data.getName());
                        mDepartmentTV.setTag(data);
                    }

                    @Override
                    public String getItemContent(Department data) {
                        return null;
                    }
                });
            }
        });
        mZhiChengTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showZhiChengListDialog(getContext(), mZhiChengs, new BottomSheetListLayout.OnItemListener<ZhiCheng>() {
                    @Override
                    public void onItemClick(ZhiCheng data) {
                        mZhiChengTV.setText(data.getName());
                        mZhiChengTV.setTag(data);
                    }

                    @Override
                    public String getItemContent(ZhiCheng data) {
                        return null;
                    }
                });
            }
        });
        view.findViewById(R.id.fragment_staff_modify_submit_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String name = mNameET.getText().toString().trim();
                String account = mAccountET.getText().toString().trim();
                String tel = mTelET.getText().toString().trim();

                String role = mRoleTV.getText().toString().trim();
                String department = mDepartmentTV.getText().toString().trim();
                String zhiCheng = mZhiChengTV.getText().toString().trim();

                if (TextUtils.isEmpty(name)) {
                    ToastUtil.showToast("请输入姓名！");
                    return;
                }
                if (TextUtils.isEmpty(account)) {
                    ToastUtil.showToast("请输入账号！");
                    return;
                }
                if (TextUtils.isEmpty(tel)) {
                    ToastUtil.showToast("请输入手机号！");
                    return;
                }

                if (TextUtils.isEmpty(zhiCheng)) {
                    ToastUtil.showToast("请选择职称！");
                    return;
                }
                if (TextUtils.isEmpty(department)) {
                    ToastUtil.showToast("请选择科室！");
                    return;
                }
                if (TextUtils.isEmpty(role)) {
                    ToastUtil.showToast("请选择角色！");
                    return;
                }

                String sex = ((RadioButton) view.findViewById(mSexRG.getCheckedRadioButtonId())).getText().toString();
                int departmentId = ((Department) mDepartmentTV.getTag()).getId();
                int roleId = ((Role) mRoleTV.getTag()).getId();
                int zhiChengId = ((ZhiCheng) mZhiChengTV.getTag()).getId();

                if (TextUtils.isEmpty(mIconPath)) {
                    mIconPath = "";
                }
                ArrayList<String> imgPaths = new ArrayList<>();
                imgPaths.add(mIconPath);

                if (mStaff == null) {
                    mStaff = new Staff();
                }

                mStaff.setName(name);
                mStaff.setLoginName(account);
                mStaff.setSex(sex);
                mStaff.setTel(tel);
                mStaff.setDeptId(departmentId);
                mStaff.setRoleId(roleId);
                mStaff.setZhichengId(zhiChengId + "");
                try {
                    mStaff.setKanzhengMinute(Integer.valueOf(mMinuteET.getText().toString().trim()));
                } catch (Exception e) {

                }
                mStaff.setFeature(mFeatureET.getText().toString().trim());
                mStaff.setJianjie(mIntroET.getText().toString().trim());

                showLoadingDialog();
                QiNiuUtils.putImgs(imgPaths, new QiNiuUtils.QiNiuCallback() {
                    @Override
                    public void onSuccess(List<String> picUrls) {
                        mStaff.setPeoplePic(Constants.getQiniuImageUrl(picUrls.get(0)));
                        mBackendManageModel.modifyStaff(mStaff);
                    }

                    @Override
                    public void onError(String msg) {
                        hideLoadingDialog();
                        ToastUtil.showToast("上传图片失败，请稍后重试！");
                    }
                });
            }
        });

        loadData();
    }

    @Override
    protected void loadData() {
        mBackendManageModel.getDepartment();
        mBackendManageModel.getRole();
        mBackendManageModel.getZhiCheng();
        showLoadingView();
    }

    private void choiceImage() {
        App.openPhotoListActivity(getContext(), true, REQUEST_CODE_SELECT_IMG);
    }

    private void checkData() {
        if (mRoles.isEmpty()) {
            return;
        }
        if (mDepartments.isEmpty()) {
            return;
        }
        if (mZhiChengs.isEmpty()) {
            return;
        }
        if (mStaff != null) {
            ImageUtil.loadImage(mStaff.getPeoplePic(), mIconIV);

            mNameET.setText(mStaff.getName());
            mAccountET.setText(mStaff.getLoginName());
            mTelET.setText(mStaff.getTel());
            mMinuteET.setText(mStaff.getKanzhengMinute() + "");
            mIntroET.setText(mStaff.getJianjie());
            mFeatureET.setText(mStaff.getFeature());

            mSexRG.check("男".equals(mStaff.getSex()) ? R.id.sex_man_rb : R.id.sex_women_rb);

            for (Role role : mRoles) {
                if (role.getId() == mStaff.getRoleId()) {
                    mRoleTV.setText(role.getName());
                    mRoleTV.setTag(role);
                    break;
                }
            }

            for (Department department : mDepartments) {
                if (department.getId() == mStaff.getDeptId()) {
                    mDepartmentTV.setText(department.getName());
                    mDepartmentTV.setTag(department);
                    break;
                }
            }

//            for (ZhiCheng zhiCheng : mZhiChengs) {
//                if (zhiCheng.getId() == mStaff.getZhichengId()) {
//                    mZhiChengTV.setText(zhiCheng.getName());
//                    mZhiChengTV.setTag(zhiCheng);
//                    break;
//                }
//            }

        }
        hideLoadingView();
    }

    @Override
    public boolean onPostLoad(String key) {
        if(mBackendManageModel.isModifyStaffKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mBackendManageModel.isGetRoleKey(key)
                || mBackendManageModel.isGetDepartmentKey(key)
                || mBackendManageModel.isGetZhiChengKey(key)) {
            showErrorView();
            return true;
        } else if(mBackendManageModel.isModifyStaffKey(key)) {
            AppResponseEntity appResponseEntity = (AppResponseEntity) data;
            ToastUtil.showToast(appResponseEntity.getMsg());
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mBackendManageModel.isGetRoleKey(key)) {
            AppResponseEntity<List<Role>> appResponseEntity = (AppResponseEntity<List<Role>>) data;
            mRoles.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mBackendManageModel.isGetDepartmentKey(key)) {
            AppResponseEntity<List<Department>> appResponseEntity = (AppResponseEntity<List<Department>>) data;
            mDepartments.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mBackendManageModel.isGetZhiChengKey(key)) {
            AppResponseEntity<List<ZhiCheng>> appResponseEntity = (AppResponseEntity<List<ZhiCheng>>) data;
            mZhiChengs.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mBackendManageModel.isModifyStaffKey(key)) {
            if (mStaff != null && mStaff.getId() > 0) {
                ToastUtil.showToast("修改员工信息成功！");
                finish();
            } else {
                AppResponseEntity<AppBaseEntity> appResponseEntity = (AppResponseEntity<AppBaseEntity>) data;
                AppBaseEntity appBaseEntity = appResponseEntity.getData();
                if (appBaseEntity.isSuccess()) {
                    finish();
                }
                ToastUtil.showToast(appBaseEntity.getMsg());
            }
            return true;
        }
        return super.onSuccess(key, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_SELECT_IMG) {
                final List<String> pathList = data.getStringArrayListExtra("result");
                if (!pathList.isEmpty()) {

                    String path = pathList.get(0);
                    mIconPath = path;
                    ImageUtil.loadImage(path, mIconIV);
                }
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
