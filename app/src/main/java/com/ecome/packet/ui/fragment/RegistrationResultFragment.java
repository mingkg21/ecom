package com.ecome.packet.ui.fragment;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.DiagnoseResult;
import com.ecome.packet.entity.Registration;
import com.ecome.packet.model.DoctorWorkspaceModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.dialog.DialogManager;
import com.mk.core.util.ToastUtil;

public class RegistrationResultFragment extends AppFragment {

    private EditText mPatientAddressET;
    private EditText mDiagnoseFeeET;
    private Button mSavePatientAddressBtn;
    private Button mModifyDiagnoseFeeBtn;

    private TextView mGaofanFeeTV;
    private TextView mDaijianTV;
    private TextView mDrugFeeTV;
    private TextView mTotalFeeTV;

    private Registration mRegistration;
    private DiagnoseResult mDiagnoseResult;

    private DoctorWorkspaceModel mDoctorWorkspaceModel;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_registration_result;
    }

    @Override
    protected void initData() {
        super.initData();
        mRegistration = (Registration) getArguments().get(ActivityRouter.EXTRA_NAME_REGISTRATION);
        mDiagnoseResult = (DiagnoseResult) getArguments().get(ActivityRouter.EXTRA_NAME_REGISTRATION_RESULT);
        mDiagnoseResult.setPatientId(mRegistration.getPatientId());
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        setTitle("确认收货信息");

        TextView patientNameTV = view.findViewById(R.id.patient_name_tv);
        TextView patientTelTV = view.findViewById(R.id.patient_tel_tv);
        TextView totalUserNumTV = view.findViewById(R.id.total_use_num_tv);
        mPatientAddressET = view.findViewById(R.id.patient_address_et);
        mDiagnoseFeeET = view.findViewById(R.id.diagnose_fee_et);

        mSavePatientAddressBtn = view.findViewById(R.id.save_address_btn);
        mModifyDiagnoseFeeBtn = view.findViewById(R.id.modify_diagnose_fee_btn);

        mPatientAddressET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mSavePatientAddressBtn.setEnabled(true);
            }
        });
        mDiagnoseFeeET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mModifyDiagnoseFeeBtn.setEnabled(true);
            }
        });

        mGaofanFeeTV = view.findViewById(R.id.gaofan_fee_tv);
        mDaijianTV = view.findViewById(R.id.daijian_fee_tv);
        mDrugFeeTV = view.findViewById(R.id.drug_fee_tv);
        mTotalFeeTV = view.findViewById(R.id.total_fee_tv);
        view.findViewById(R.id.pay_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showChoicePayDialog(getContext(), mDiagnoseResult);
            }
        });

        patientNameTV.setText("收货人：" + mRegistration.getPatientName());
        patientTelTV.setText("手机号：" + mRegistration.getPatientTel());
        mPatientAddressET.setText(mRegistration.getPatientAddress());
        mDiagnoseFeeET.setText(String.format("%.2f", mDiagnoseResult.getDiagnoseFee()));

        setFee();

        mSavePatientAddressBtn.setEnabled(false);
        mModifyDiagnoseFeeBtn.setEnabled(false);

        mSavePatientAddressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address = mPatientAddressET.getText().toString();
                if (TextUtils.isEmpty(address)) {
                    ToastUtil.showToast("请输入收药地址！");
                    return;
                }
                mDoctorWorkspaceModel.modifyPatientAddress(mRegistration.getPatientId() + "",
                        mRegistration.getPatientTel(), address);
            }
        });

        mModifyDiagnoseFeeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fee = mDiagnoseFeeET.getText().toString();
                if (TextUtils.isEmpty(fee)) {
                    ToastUtil.showToast("请输入诊金费！");
                    return;
                }
                mDoctorWorkspaceModel.modifyDiagnoseFee(mRegistration.getId() + "",
                        UserManager.getInstance().getUserId() + "", fee);
            }
        });

        mDoctorWorkspaceModel = new DoctorWorkspaceModel();
        mDoctorWorkspaceModel.addCallback(this);

    }

    private void setFee() {
        mGaofanFeeTV.setText(String.format("膏方费：%.2f", mDiagnoseResult.getGaofangFee()));
        mDaijianTV.setText(String.format("代煎费：%.2f", mDiagnoseResult.getDaijianFee()));
        mDrugFeeTV.setText(String.format("药    费：%.2f", mDiagnoseResult.getDrugFee()));
        mTotalFeeTV.setText(String.format("总金额：%.2f元", mDiagnoseResult.getMoney()));
    }

    @Override
    protected void loadData() {

    }

    @Override
    public boolean onPreLoad(String key) {

        if (mDoctorWorkspaceModel.isModifyDiagnoseFeeKey(key)
                || mDoctorWorkspaceModel.isModifyPatientAddressKey(key)) {
            showLoadingDialog();
            return true;
        }

        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mDoctorWorkspaceModel.isModifyDiagnoseFeeKey(key)
                || mDoctorWorkspaceModel.isModifyPatientAddressKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mDoctorWorkspaceModel.isModifyDiagnoseFeeKey(key)
                || mDoctorWorkspaceModel.isModifyPatientAddressKey(key)) {
            ToastUtil.showToast("修改成功！");
            if (mDoctorWorkspaceModel.isModifyDiagnoseFeeKey(key)) {
                AppResponseEntity<DiagnoseResult> appResponseEntity = (AppResponseEntity<DiagnoseResult>) data;
                mDiagnoseResult = appResponseEntity.getData();
                setFee();
            }
            return true;
        }
        return super.onSuccess(key, data);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mDoctorWorkspaceModel.isModifyDiagnoseFeeKey(key)
                || mDoctorWorkspaceModel.isModifyPatientAddressKey(key)) {
            ToastUtil.showToast("修改失败，请稍后重试！");
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onDataChange(String key, Object data) {
        if (DoctorWorkspaceModel.NOTIFY_CHANGE_CHARGE_SUCCESS.equals(key)) {
            finish();
            return true;
        }
        return super.onDataChange(key, data);
    }
}
