package com.ecome.packet.ui.fragment;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mk.core.ui.fragment.RecyclerFragment;
import com.ecome.packet.R;

/**
 * Created by mingkg21 on 2017/10/11.
 */

public abstract class AppRecyclerFragment<T> extends RecyclerFragment<T> {

    private TextView mTitleBarRightTV;
    private ImageView mTitleBarRightIV;

    @Override
    protected int getTitleBarBackResId() {
        return R.id.title_bar_back;
    }

    @Override
    protected int getTitleBarTitleResId() {
        return R.id.title_bar_title;
    }

    protected int getTitleBarRightResId() {
        return R.id.title_bar_right;
    }

    protected int getTitleBarRightImageResId() {
        return R.id.title_bar_right_iv;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_com_list;
    }

    @Override
    protected boolean canRefresh() {
        return false;
    }

    @Override
    protected boolean hasFooterView() {
        return false;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setDividerColor(getResources().getColor(R.color.common_line));

        mSwipeRefreshLayout.setBackgroundColor(getResources().getColor(R.color.common_basic_bg));

        View titleBarLayout = view.findViewById(R.id.com_title_bar);
        if (titleBarLayout != null) {
            titleBarLayout.setVisibility(hasTitleBar() ? View.VISIBLE : View.GONE);

            mTitleBarRightTV = (TextView) view.findViewById(getTitleBarRightResId());
            mTitleBarRightIV = view.findViewById(getTitleBarRightImageResId());

            if (mTitleBarRightTV != null) {
                mTitleBarRightTV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onTitleBarRightAction();
                    }
                });
            }
            if (mTitleBarRightIV != null) {
                mTitleBarRightIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onTitleBarRightAction();
                    }
                });
            }
        }
    }

    protected void setTitleBarRightText(String text) {
        setTitleBarRight(text, 0);
    }

    protected void setTitleBarRightImage(int resId) {
        setTitleBarRight("", resId);
    }

    protected void setTitleBarRight(String text, int resId) {
        if (!TextUtils.isEmpty(text)) {
            if (mTitleBarRightTV != null) {
                mTitleBarRightTV.setText(text);
                mTitleBarRightTV.setVisibility(View.VISIBLE);
            }
            if (mTitleBarRightIV != null) {
                mTitleBarRightIV.setVisibility(View.GONE);
            }
        } else if (resId > 0){
            if (mTitleBarRightTV != null) {
                mTitleBarRightTV.setVisibility(View.GONE);
            }
            if (mTitleBarRightIV != null) {
                mTitleBarRightIV.setImageResource(resId);
                mTitleBarRightIV.setVisibility(View.VISIBLE);
            }
        } else {
            if (mTitleBarRightTV != null) {
                mTitleBarRightTV.setVisibility(View.GONE);
            }
            if (mTitleBarRightIV != null) {
                mTitleBarRightIV.setVisibility(View.GONE);
            }
        }
    }

    protected void onTitleBarRightAction() {

    }

}
