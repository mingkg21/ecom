package com.ecome.packet.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ecome.packet.R;
import com.ecome.packet.app.App;
import com.ecome.packet.db.SettingPreferences;
import com.ecome.packet.model.SystemModel;

import java.util.ArrayList;

/**
 * Created by mingkg21 on 2017/8/26.
 */

public class SplashActivity extends Activity {

    private ViewPager mViewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        mViewPager = findViewById(R.id.feature_vp);

        FeaturePagerAdapter adapter = new FeaturePagerAdapter(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMain();
            }
        });
        adapter.addItem(R.drawable.ic_feature_1);
        adapter.addItem(R.drawable.ic_feature_2);
        adapter.addItem(R.drawable.ic_feature_3);
        mViewPager.setAdapter(adapter);

        if (SettingPreferences.getInstance().isShowFeature()) {
            mViewPager.setVisibility(View.VISIBLE);
            SettingPreferences.getInstance().setShowFeature();
        } else {
            mViewPager.setVisibility(View.GONE);
            new CountDownTimer(2500, 500) {

                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    gotoMain();
                }
            }.start();
        }

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        App.getInstance().onEnterApp();

        SystemModel.getInstance().getSystemInfo();

    }

    private void gotoMain() {
        ActivityRouter.openMainActivity(SplashActivity.this);
        finish();
    }

    private static class FeaturePagerAdapter extends PagerAdapter {

        private ArrayList<Integer> mImgs = new ArrayList<>();

        private Context mContext;
        private View.OnClickListener mGoToOnClickListener;

        public FeaturePagerAdapter(Context context, View.OnClickListener goToOnClickListener) {
            mContext = context;
            mGoToOnClickListener = goToOnClickListener;
        }

        public void addItem(int resId) {
            mImgs.add(resId);
        }

        @Override
        public int getCount() {
            return mImgs.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            ImageView imageView = new ImageView(mContext);

            imageView.setImageResource(mImgs.get(position));
            if (position == 2) {
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mGoToOnClickListener != null) {
                            mGoToOnClickListener.onClick(v);
                        }
                    }
                });
            } else {
                imageView.setOnClickListener(null);
            }

            container.removeView(imageView);
            container.addView(imageView);
            return imageView;
        }


    }
}
