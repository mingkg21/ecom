package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.LogInfo;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class LogAdapter extends BaseAdapter<LogInfo> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_log;
    }

    @Override
    public BaseViewHolder<LogInfo> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView);
    }

    private static class Holder extends BaseViewHolder<LogInfo> {

        private TextView mNameTV;
        private TextView mContentTV;

        public Holder(View itemView) {
            super(itemView);
            mNameTV = findViewById(R.id.item_log_name_tv);
            mContentTV = findViewById(R.id.item_log_content_tv);

        }

        @Override
        public void onBind(LogInfo data, int position) {
            mNameTV.setText(data.getLoginName());

            StringBuilder sb = new StringBuilder();
            sb.append("登录时间：");
            sb.append(data.getLoginTime());
            sb.append("\n");
            sb.append("登录IP：");
            sb.append(data.getLoginIp());
            sb.append("\n");
            sb.append("退出时间：");
            sb.append(data.getExitTime());
//            sb.append("\n");
//            sb.append("备注：");
//            sb.append(data.getRemark());

            mContentTV.setText(sb);
        }
    }
}
