package com.ecome.packet.ui.fragment;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.app.App;
import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.UserInfo;
import com.ecome.packet.model.UserModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.util.Constants;
import com.ecome.packet.util.QiNiuUtils;
import com.mk.core.util.ImageUtil;
import com.mk.core.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by mingkg21 on 2017/10/10.
 */

public class CompleteInfoFragment extends AppFragment implements View.OnClickListener {

    private static final int REQUEST_CODE_SELECT_IMG = 3000;

    private UserModel mUserModel;

    private EditText mNameET;
    private EditText mAccountET;
    private EditText mPhoneET;
    private EditText mIntroducerIdET;
    private EditText mOutpatientAddressET;
    private EditText mOutpatientPhoneET;
    private EditText mOutpatientIntroET;
    private EditText mAdminEmailET;
    private EditText mLoginIdET;
    private TextView mPasswordTipTV;

    private ImageView mImageView;
    private ImageView mLogo1View;
    private ImageView mLogo2View;
    private ImageView mLogo3View;

    private ImageView mPayWXView;
    private ImageView mPayAlpayView;

    private View mMoreInfoLayout;
    private View mLoginIdLayout;
    private View mIntroducerLayout;

    private Button mActionBtn;

    private String mImagePath0;
    private String mImagePath1;
    private String mImagePath2;
    private String mImagePath3;
    private String mImagePathPayWX;
    private String mImagePathPayAlpay;

    private int mChoiceImageId;
    private boolean mIsRegister;

    private UserInfo mUserInfo;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_complete_info;
    }

    @Override
    protected void initData() {
        super.initData();
        mIsRegister = getArguments().getBoolean(ActivityRouter.EXTRA_NAME_VALUE);
        mUserInfo = (UserInfo) getArguments().get(ActivityRouter.EXTRA_NAME_USER);
        if (mUserInfo == null) {
            mUserInfo = UserManager.getInstance().getUserInfo();
        }
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        setTitle("完善门诊部信息");

        mPasswordTipTV = view.findViewById(R.id.password_tip_tv);
        mNameET = view.findViewById(R.id.name_et);
        mAccountET = view.findViewById(R.id.account_et);
        mPhoneET = view.findViewById(R.id.phone_et);
        mIntroducerIdET = view.findViewById(R.id.introducer_id_et);
        mOutpatientAddressET = view.findViewById(R.id.outpatient_address_et);
        mOutpatientPhoneET = view.findViewById(R.id.outpatient_phone_et);
        mOutpatientIntroET = view.findViewById(R.id.outpatient_intro_et);
        mAdminEmailET = view.findViewById(R.id.email_et);
        mLoginIdET = view.findViewById(R.id.login_id_et);

        mActionBtn = view.findViewById(R.id.register_btn);

        mLoginIdLayout = view.findViewById(R.id.login_id_layout);
        mMoreInfoLayout = view.findViewById(R.id.more_info_layout);
        mIntroducerLayout = view.findViewById(R.id.introducer_id_layout);

        mActionBtn.setOnClickListener(this);

        mImageView = view.findViewById(R.id.add_image_iv);
        mImageView.setOnClickListener(this);

        mLogo1View = view.findViewById(R.id.add_logo_1_iv);
        mLogo1View.setOnClickListener(this);

        mLogo2View = view.findViewById(R.id.add_logo_2_iv);
        mLogo2View.setOnClickListener(this);

        mLogo3View = view.findViewById(R.id.add_logo_3_iv);
        mLogo3View.setOnClickListener(this);

        mPayWXView = view.findViewById(R.id.add_pay_wx_iv);
        mPayWXView.setOnClickListener(this);

        mPayAlpayView = view.findViewById(R.id.add_pay_alpay_iv);
        mPayAlpayView.setOnClickListener(this);

        mIntroducerLayout.setVisibility(View.GONE);
        mLoginIdLayout.setVisibility(View.VISIBLE);
        mMoreInfoLayout.setVisibility(View.VISIBLE);
        mActionBtn.setText(R.string.btn_submit);

        mPasswordTipTV.setVisibility(mIsRegister ? View.VISIBLE : View.GONE);

        if (mUserInfo != null) {
            mPasswordTipTV.setText(mUserInfo.getMsg());
            mLoginIdET.setText(mUserInfo.getLoginId() + "");
            mNameET.setText(mUserInfo.getFendianName());
            mAccountET.setText(mUserInfo.getLoginName());
            mPhoneET.setText(mUserInfo.getPhone());

            mOutpatientAddressET.setText(mUserInfo.getFendianAddress());
            mOutpatientPhoneET.setText(mUserInfo.getFendianYuyueTel());
            mOutpatientIntroET.setText(mUserInfo.getFendianJianjie());

            mImagePath0 = mUserInfo.getPic0();
            mImagePath1 = mUserInfo.getPic1();
            mImagePath2 = mUserInfo.getPic2();
            mImagePath3 = mUserInfo.getPic3();
            mImagePathPayWX = mUserInfo.getPicWeixin();
            mImagePathPayAlpay = mUserInfo.getPicAlipay();

            ImageUtil.loadImage(mUserInfo.getPic0(), mImageView, R.drawable.ic_add_img);
            ImageUtil.loadImage(mUserInfo.getPic1(), mLogo1View, R.drawable.ic_add_img);
            ImageUtil.loadImage(mUserInfo.getPic2(), mLogo2View, R.drawable.ic_add_img);
            ImageUtil.loadImage(mUserInfo.getPic3(), mLogo3View, R.drawable.ic_add_img);
            ImageUtil.loadImage(mUserInfo.getPicWeixin(), mPayWXView, R.drawable.ic_add_img);
            ImageUtil.loadImage(mUserInfo.getPicAlipay(), mPayAlpayView, R.drawable.ic_add_img);

        }

        mUserModel = new UserModel();
        mUserModel.addCallback(this);

    }

    @Override
    protected void loadData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_btn:
                final String name = mNameET.getText().toString().trim();
                final String account = mAccountET.getText().toString().trim();
                final String phone = mPhoneET.getText().toString().trim();
                String tempIntroducerId = mIntroducerIdET.getText().toString().trim();
                if (TextUtils.isEmpty(name)) {
                    ToastUtil.showToast(R.string.register_input_name_tip_is_null);
                    return;
                }
                if (TextUtils.isEmpty(account)) {
                    ToastUtil.showToast(R.string.register_input_account_tip_is_null);
                    return;
                }
                if (TextUtils.isEmpty(phone)) {
                    ToastUtil.showToast(R.string.register_input_phone_tip_is_null);
                    return;
                }
                if (TextUtils.isEmpty(tempIntroducerId)) {
                    tempIntroducerId = "0";
                }

                final String introducerId = tempIntroducerId;

                //先上传图片
                final String outpatientAddress = mOutpatientAddressET.getText().toString().trim();
                final String outpatientPhone = mOutpatientPhoneET.getText().toString().trim();
                final String outpatientIntro = mOutpatientIntroET.getText().toString().trim();
                final String adminEmail = mAdminEmailET.getText().toString().trim();

                final int loginId = UserManager.getInstance().getUserId();

                ArrayList<String> imgPaths = new ArrayList<>();
                imgPaths.add(mImagePath0);
                imgPaths.add(mImagePath1);
                imgPaths.add(mImagePath2);
                imgPaths.add(mImagePath3);
                imgPaths.add(mImagePathPayWX);
                imgPaths.add(mImagePathPayAlpay);

                if (imgPaths.isEmpty()) {
                    mUserModel.completeInfo("" + loginId, name, account, phone, introducerId,
                            mImagePath0, mImagePath1, mImagePath2, mImagePath3, mImagePathPayWX, mImagePathPayAlpay, outpatientAddress,
                            outpatientPhone, adminEmail, outpatientIntro, mIsRegister);
                } else {
                    showLoadingDialog();
                    QiNiuUtils.putImgs(imgPaths, new QiNiuUtils.QiNiuCallback() {
                        @Override
                        public void onSuccess(List<String> picUrls) {
                            mUserModel.completeInfo("" + loginId, name, account, phone, introducerId,
                                    Constants.getQiniuImageUrl(picUrls.get(0)), Constants.getQiniuImageUrl(picUrls.get(1)),
                                    Constants.getQiniuImageUrl(picUrls.get(2)), Constants.getQiniuImageUrl(picUrls.get(3)),
                                    Constants.getQiniuImageUrl(picUrls.get(4)), Constants.getQiniuImageUrl(picUrls.get(5)),
                                    outpatientAddress, outpatientPhone, adminEmail, outpatientIntro, mIsRegister);
                        }

                        @Override
                        public void onError(String msg) {
                            hideLoadingDialog();
                            ToastUtil.showToast("上传图片失败，请稍后重试！");
                        }
                    });
                }
                break;
            case R.id.add_image_iv:
            case R.id.add_logo_1_iv:
            case R.id.add_logo_2_iv:
            case R.id.add_logo_3_iv:
            case R.id.add_pay_wx_iv:
            case R.id.add_pay_alpay_iv:
                mChoiceImageId = v.getId();
                choiceImage();
                break;
        }
    }

    private void choiceImage() {
        App.openPhotoListActivity(getContext(), REQUEST_CODE_SELECT_IMG);
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mUserModel.isCompleteInfoInRegisterKey(key)
                || mUserModel.isCompleteInfoKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mUserModel.isCompleteInfoInRegisterKey(key)
                || mUserModel.isCompleteInfoKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mUserModel.isCompleteInfoInRegisterKey(key)) {
            AppResponseEntity<UserInfo> responseEntity = (AppResponseEntity<UserInfo>) data;
            UserInfo userInfo = responseEntity.getData();
            if (userInfo != null) {
                ToastUtil.showToast(userInfo.getMsg());
                if (userInfo.isSuccess()) {
                    ActivityRouter.openLoginActivity(getContext());
                    finish();
                }
                return true;
            }
            ToastUtil.showToast("请求失败，请稍后重试！");
            return true;
        } else if (mUserModel.isCompleteInfoKey(key)) {
            AppResponseEntity<UserInfo> responseEntity = (AppResponseEntity<UserInfo>) data;
            UserInfo userInfo = responseEntity.getData();
            ToastUtil.showToast(userInfo.getMsg());
            if (userInfo.isSuccess()) {
                finish();
            }
            return true;
        }
        return super.onSuccess(key, data);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mUserModel.isCompleteInfoInRegisterKey(key) || mUserModel.isCompleteInfoKey(key)) {
            AppResponseEntity responseEntity = (AppResponseEntity) data;
            ToastUtil.showToast(responseEntity.getMsg());
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_SELECT_IMG) {
                final List<String> pathList = data.getStringArrayListExtra("result");
                if (!pathList.isEmpty()) {

                    String path = pathList.get(0);
                    ImageView imageView = null;
                    switch (mChoiceImageId) {
                        case R.id.add_image_iv:
                            imageView = mImageView;
                            mImagePath0 = path;
                            break;
                        case R.id.add_logo_1_iv:
                            imageView = mLogo1View;
                            mImagePath1 = path;
                            break;
                        case R.id.add_logo_2_iv:
                            imageView = mLogo2View;
                            mImagePath2 = path;
                            break;
                        case R.id.add_logo_3_iv:
                            imageView = mLogo3View;
                            mImagePath3 = path;
                            break;
                        case R.id.add_pay_wx_iv:
                            imageView = mPayWXView;
                            mImagePathPayWX = path;
                            break;
                        case R.id.add_pay_alpay_iv:
                            imageView = mPayAlpayView;
                            mImagePathPayAlpay = path;
                            break;
                    }
                    if (imageView != null) {
                        ImageUtil.loadImage(path, imageView);
                    }
                }
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
