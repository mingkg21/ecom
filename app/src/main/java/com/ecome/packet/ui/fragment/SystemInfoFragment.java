package com.ecome.packet.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;

import com.ecome.packet.db.SystemInfoPreferences;
import com.ecome.packet.entity.SystemInfo;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.SystemInfoAdapter;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;

public class SystemInfoFragment extends AppRecyclerFragment<SystemInfo> {

    @Override
    protected BaseAdapter<SystemInfo> getAdapter() {
        return new SystemInfoAdapter();
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected boolean canRefresh() {
        return false;
    }

    @Override
    protected boolean hasFooterView() {
        return false;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        setTitle("选择服务器");

        setAll(SystemInfoPreferences.getInstance().getSystemInfos());
        mRecyclerView.setVisibility(View.VISIBLE);
        mRecyclerView.setBackgroundColor(Color.WHITE);

        setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Object data) {
                Intent intent = new Intent();
                intent.putExtra(ActivityRouter.EXTRA_NAME_RESULT, (SystemInfo) data);
                ((Activity) getContext()).setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }

}
