package com.ecome.packet.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.ecome.packet.R;
import com.ecome.packet.thirdparty.ShareUtils;

/**
 * Created by mingkg21 on 2017/10/6.
 */

public class ShareDialog extends Dialog implements View.OnClickListener {

    private Context mContext;

    public ShareDialog(@NonNull Context context) {
        super(context, R.style.dialogstyle);

        mContext = context;

        setContentView(R.layout.dialog_share);

        Window window = getWindow();
        window.setGravity(Gravity.BOTTOM);

        findViewById(R.id.dialog_share_qq).setOnClickListener(this);
        findViewById(R.id.dialog_share_wx).setOnClickListener(this);
        findViewById(R.id.dialog_share_timeline).setOnClickListener(this);
        findViewById(R.id.dialog_share_qzone).setOnClickListener(this);
        findViewById(R.id.dialog_share_cancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_share_qq:
                share(ShareUtils.TYPE_QQ);
                break;
            case R.id.dialog_share_wx:
                share(ShareUtils.TYPE_WX);
                break;
            case R.id.dialog_share_timeline:
                share(ShareUtils.TYPE_WX_TIMELINE);
                break;
            case R.id.dialog_share_qzone:
                share(ShareUtils.TYPE_QZONE);
                break;
        }
        dismiss();
    }

    private void share(int type) {
        ShareUtils shareUtils = new ShareUtils();
        shareUtils.onItemShare(mContext, "aaa", "bbb", "http://www.baidu.com", "http://qny.smzdm.com/201712/10/5719853a9c74a.jpeg_d200.jpg", null, type);
    }

    @Override
    public void show() {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = getContext().getResources().getDisplayMetrics().widthPixels;
        getWindow().setAttributes(lp);
        super.show();
    }

}
