package com.ecome.packet.ui.fragment;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.app.App;
import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.UserInfo;
import com.ecome.packet.model.UserModel;
import com.ecome.packet.ui.dialog.SexDialog;
import com.mk.core.util.ImageUtil;
import com.mk.core.util.ToastUtil;

import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by mingkg21 on 2017/10/10.
 */

public class UserInfoFragment extends AppFragment {

    private static final int REQUEST_CODE_SELECT_IMG = 3000;

    private ImageView mUserIconIV;
    private TextView mNickNameTV;
    private TextView mSexTV;
    private TextView mBirthdayTV;

    private UserModel mUserModel;
    private UserInfo mUserInfo;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_user_info;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle("个人信息");

        setTitleBarRightText("保存");

        mUserIconIV = findViewById(R.id.fragment_user_info_icon);
        mNickNameTV = findViewById(R.id.fragment_user_info_nick_name);
        mSexTV = findViewById(R.id.fragment_user_info_sex);
        mBirthdayTV = findViewById(R.id.fragment_user_info_birthday);

        findViewById(R.id.title_bar_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nickName = mNickNameTV.getText().toString().trim();
                if (TextUtils.isEmpty(nickName)) {
                    ToastUtil.showToast("昵称不能为空，请输入昵称！");
                    return;
                }

//                mUserInfo.setNickName(nickName);

//                mUserModel.editUserInfo(mUserInfo);
            }
        });

        mBirthdayTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Calendar calendar = Calendar.getInstance();
//                long birthday = mUserInfo.getBirthday();
//                if (String.valueOf(birthday).length() == 10) {
//                    birthday *= 1000;
//                }
//                calendar.setTimeInMillis(birthday);
            }
        });

        mSexTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SexDialog(getContext(), new SexDialog.OnDoneListener() {
                    @Override
                    public void onDone(int sex) {
//                        mUserInfo.setSex(sex);
                        fillData(mUserInfo);
                    }
                }).show();
            }
        });

        mUserIconIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        mUserModel = new UserModel();
        mUserModel.addCallback(this);

        mUserInfo = UserManager.getInstance().getUserInfo();

        fillData(mUserInfo);

    }

    private void fillData(UserInfo userInfo) {
        mUserInfo = userInfo;

//        ImageUtil.loadImage(userInfo.getIcon(), mUserIconIV);
//        mNickNameTV.setText(userInfo.getNickName());
//        mSexTV.setText((userInfo.getSex() == UserInfo.SEX_MAN) ? "男" : "女");
//        mBirthdayTV.setText(TimeUtil.formatYYYY_MM_DD(userInfo.getBirthday()));
    }

    private void selectImage() {
        App.openPhotoListActivity(getContext(), true, REQUEST_CODE_SELECT_IMG);
    }

    @Override
    protected void loadData() {

    }

    @Override
    public boolean onPreLoad(String key) {
//        if (mUserModel.isEditUserInfoKey(key)) {
//            showLoadingDialog();
//            return true;
//        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
//        if (mUserModel.isEditUserInfoKey(key)) {
//            hideLoadingDialog();
//            return true;
//        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
//        if (mUserModel.isEditUserInfoKey(key)) {
//            UserInfoData userInfoData = (UserInfoData) data;
//            fillData(userInfoData.getData());
//            ToastUtil.showToast("修改个人信息成功！");
//            return true;
//        }
        return super.onSuccess(key, data);
    }

    @Override
    public boolean onFail(String key, Object data) {
//        if (mUserModel.isEditUserInfoKey(key)) {
//            ToastUtil.showToast(((ResponseEntity) data).getMsg());
//            return true;
//        }
        return super.onFail(key, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_SELECT_IMG) {
                List<String> pathList = data.getStringArrayListExtra("result");
                if (!pathList.isEmpty()) {
                    ImageUtil.loadImage(pathList.get(0), mUserIconIV);
//                    mUserInfo.setIcon(pathList.get(0));
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
