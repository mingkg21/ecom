package com.ecome.packet.ui.fragment;

import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Staff;
import com.ecome.packet.entity.WorkTimeItem;
import com.ecome.packet.model.BackendManageModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.PianbanAdapter;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;

import java.util.List;

public class PaibanListFragment extends AppRecyclerFragment<WorkTimeItem> {

    private BackendManageModel mMenuModel;

    private Staff mStaff;

    @Override
    protected BaseAdapter<WorkTimeItem> getAdapter() {
        return new PianbanAdapter();
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected void initData() {
        super.initData();
        mStaff = (Staff) getArguments().get(ActivityRouter.EXTRA_NAME_STAFF);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTopDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_list_top_height));

        if (mStaff != null) {
            setTitle(mStaff.getName() + "的排班信息");
        }

        mMenuModel = new BackendManageModel();
        mMenuModel.addCallback(this);

        mRecyclerView.setVisibility(View.VISIBLE);

        loadData();
    }

    @Override
    protected void loadData() {
        super.loadData();

        mMenuModel.getDoctorPaiban(mStaff.getId());
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mMenuModel.isGetDoctorPaibanKey(key)) {
            showLoadingView();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mMenuModel.isGetDoctorPaibanKey(key)) {
            hideLoadingView();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public void onUserLoginStateChange(boolean isLogin) {
        super.onUserLoginStateChange(isLogin);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mMenuModel.isGetDoctorPaibanKey(key)) {
            showErrorView();
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mMenuModel.isGetDoctorPaibanKey(key)) {
            AppResponseEntity<List<WorkTimeItem>> menus = (AppResponseEntity<List<WorkTimeItem>>) data;
            setAll(menus.getData());
            return true;
        }
        return super.onSuccess(key, data);
    }
}
