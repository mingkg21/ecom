package com.ecome.packet.ui.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Patient;
import com.mk.core.ui.widget.BaseViewHolder;

public class MyPatientViewHolder extends BaseViewHolder<Patient> {

    protected TextView mNameTV;
    protected TextView mPhoneTV;
    protected TextView mAgeTV;
    protected TextView mSexTV;
    protected ImageView mIconIV;

    public MyPatientViewHolder(View itemView) {
        super(itemView);
        mIconIV = findViewById(R.id.item_my_patient_icon_iv);
        mNameTV = findViewById(R.id.item_my_patient_name_tv);
        mPhoneTV = findViewById(R.id.item_my_patient_phone_tv);
        mAgeTV = findViewById(R.id.item_my_patient_age_tv);
        mSexTV = findViewById(R.id.item_my_patient_sex_tv);

    }

    @Override
    public void onBind(final Patient data, int position) {
        mNameTV.setText(data.getName());
        if (TextUtils.isEmpty(data.getTel())) {
            mPhoneTV.setVisibility(View.GONE);
        } else {
            mPhoneTV.setText(data.getTel());
            mPhoneTV.setVisibility(View.VISIBLE);
        }
        mAgeTV.setText(data.getAge());
        mSexTV.setText(data.getSex());
        if (data.isMan()) {
            mIconIV.setImageResource(R.drawable.ic_patient_man);
            mSexTV.setTextColor(getContext().getResources().getColor(R.color.common_green));
        } else {
            mIconIV.setImageResource(R.drawable.ic_patient_woman);
            mSexTV.setTextColor(getContext().getResources().getColor(R.color.common_orange));
        }
    }
}
