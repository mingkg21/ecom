package com.ecome.packet.ui.fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Patient;
import com.ecome.packet.entity.Registration;
import com.ecome.packet.model.DoctorWorkspaceModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.RegistrationAdapter;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;

import java.util.List;

public class PatientHistoryRegistrationFragment extends AppRecyclerFragment<Registration> {

    private DoctorWorkspaceModel mDoctorWorkspaceModel;

    private Patient mPatient;

    @Override
    protected BaseAdapter<Registration> getAdapter() {
        return new RegistrationAdapter();
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected void initData() {
        super.initData();

        mPatient = (Patient) getArguments().get(ActivityRouter.EXTRA_NAME_PATIENT);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle(String.format("病人详情", mPatient.getName()));

        View patientInfoView = LayoutInflater.from(getContext()).inflate(R.layout.item_my_patient, null);
        patientInfoView.findViewById(R.id.item_my_patient_more_iv).setVisibility(View.GONE);

        ImageView iconIV = patientInfoView.findViewById(R.id.item_my_patient_icon_iv);
        TextView nameTV = patientInfoView.findViewById(R.id.item_my_patient_name_tv);
        TextView phoneTV = patientInfoView.findViewById(R.id.item_my_patient_phone_tv);
        TextView ageTV = patientInfoView.findViewById(R.id.item_my_patient_age_tv);
        TextView sexTV = patientInfoView.findViewById(R.id.item_my_patient_sex_tv);

        nameTV.setText(mPatient.getName());
        if (TextUtils.isEmpty(mPatient.getTel())) {
            phoneTV.setVisibility(View.GONE);
        } else {
            phoneTV.setText(mPatient.getTel());
            phoneTV.setVisibility(View.VISIBLE);
        }

        ageTV.setText(mPatient.getAge());
        sexTV.setText(mPatient.getSex());
        if (mPatient.isMan()) {
            iconIV.setImageResource(R.drawable.ic_patient_man);
            sexTV.setTextColor(getContext().getResources().getColor(R.color.common_green));
        } else {
            iconIV.setImageResource(R.drawable.ic_patient_woman);
            sexTV.setTextColor(getContext().getResources().getColor(R.color.common_orange));
        }

        ((LinearLayout) view).addView(patientInfoView, 1);

        setTopDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_list_top_height));

        mRecyclerView.setVisibility(View.VISIBLE);
        setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Object data) {
                ActivityRouter.openPrescriptionActivity(getContext(), ((Registration) data).getId(), mPatient.getName());
            }
        });

        mDoctorWorkspaceModel = new DoctorWorkspaceModel();
        mDoctorWorkspaceModel.addCallback(this);

        loadData();
    }

    @Override
    protected void loadData() {
        super.loadData();

        mDoctorWorkspaceModel.getPatientHistoryRegistration(mPatient.getId() + "");
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mDoctorWorkspaceModel.isGetPatientHistoryRegistrationKey(key)) {
            showLoadingView();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mDoctorWorkspaceModel.isGetPatientHistoryRegistrationKey(key)) {
            hideLoadingView();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mDoctorWorkspaceModel.isGetPatientHistoryRegistrationKey(key)) {
            AppResponseEntity<List<Registration>> responseEntity = (AppResponseEntity<List<Registration>>) data;
            setAll(responseEntity.getData());
            return true;
        }
        return super.onSuccess(key, data);
    }
}
