package com.ecome.packet.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TimePicker;

import com.ecome.packet.R;

public class TimeDialog extends Dialog {

    private TimePicker mTimePicker;

    public TimeDialog(@NonNull Context context, int hour, int minute, final OnTimeChoicedListener onTimeChoicedListener) {
        super(context);

        setContentView(R.layout.dialog_time);

        mTimePicker = findViewById(R.id.dialog_time_tp);
        mTimePicker.setIs24HourView(true);
        mTimePicker.setCurrentHour(hour);
        mTimePicker.setCurrentMinute(minute);

        findViewById(R.id.dialog_time_sure).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onTimeChoicedListener != null) {
                    onTimeChoicedListener.onChoiced(mTimePicker.getCurrentHour(), mTimePicker.getCurrentMinute());
                }
                dismiss();
            }
        });

        findViewById(R.id.dialog_time_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface OnTimeChoicedListener {
        void onChoiced(int hour, int minute);
    }

}
