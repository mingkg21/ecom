package com.ecome.packet.ui.fragment;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppBaseEntity;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.DiagnoseResult;
import com.ecome.packet.model.DoctorWorkspaceModel;
import com.ecome.packet.net.request.ChargeRequest;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.mk.core.util.ImageUtil;
import com.mk.core.util.ToastUtil;

public class ChargeFragment extends AppFragment {

    private DoctorWorkspaceModel mDoctorWorkspaceModel;

    private DiagnoseResult mDiagnoseResult;
    private String mPayType;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_charge;
    }

    @Override
    protected void initData() {
        super.initData();
        mDiagnoseResult = (DiagnoseResult) getArguments().get(ActivityRouter.EXTRA_NAME_REGISTRATION_RESULT);
        mPayType = getArguments().getString(ActivityRouter.EXTRA_NAME_VALUE);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        mDoctorWorkspaceModel = new DoctorWorkspaceModel();
        mDoctorWorkspaceModel.addCallback(this);

        TextView priceTV = view.findViewById(R.id.fragment_pay_price_tv);
        ImageView priceIV = view.findViewById(R.id.fragment_pay_price_iv);

        if (ChargeRequest.PAY_TYPE_ALPAY.equals(mPayType)) {
            setTitle("支付宝扫码支付");
            ImageUtil.loadImage(UserManager.getInstance().getAliPayUrl(), priceIV, R.drawable.ic_default);
        } else {
            setTitle("微信扫码支付");
            ImageUtil.loadImage(UserManager.getInstance().getWXPayUrl(), priceIV, R.drawable.ic_default);
        }

        SpannableString ss = new SpannableString(String.format("支付价格：%s元", mDiagnoseResult.getMoney()));
        ss.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.color_main)), 5, ss.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new AbsoluteSizeSpan(24, true), 5, ss.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        priceTV.setText(ss);

        view.findViewById(R.id.fragment_pay_done_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDoctorWorkspaceModel.charge("", mDiagnoseResult.getRegistrationId() + "",
                        mPayType, mDiagnoseResult.getMoney() + "", mDiagnoseResult.getPatientId() + "");
            }
        });
        view.findViewById(R.id.fragment_pay_pause_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDoctorWorkspaceModel.charge("", mDiagnoseResult.getRegistrationId() + "",
                        "-1", mDiagnoseResult.getMoney() + "", mDiagnoseResult.getPatientId() + "");
            }
        });
    }

    @Override
    protected void loadData() {

    }

    @Override
    public boolean onPreLoad(String key) {
        if (mDoctorWorkspaceModel.isChargeKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mDoctorWorkspaceModel.isChargeKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mDoctorWorkspaceModel.isChargeKey(key)) {
            AppResponseEntity appResponseEntity = (AppResponseEntity) data;
            ToastUtil.showToast(appResponseEntity.getMsg());
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mDoctorWorkspaceModel.isChargeKey(key)) {
            AppResponseEntity<AppBaseEntity> appResponseEntity = (AppResponseEntity<AppBaseEntity>) data;
            AppBaseEntity appBaseEntity = appResponseEntity.getData();
            if (appBaseEntity != null) {
                ToastUtil.showToast(appBaseEntity.getMsg());
                if (appBaseEntity.isSuccess()) {
                    ActivityRouter.openWorkloadActivity(getContext(), "我的工作量");
                    finish();
                }
            } else {
                ToastUtil.showToast(appResponseEntity.getMsg());
            }
            return true;
        }
        return super.onSuccess(key, data);
    }
}
