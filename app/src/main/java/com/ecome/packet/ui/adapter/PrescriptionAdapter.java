package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Prescription;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class PrescriptionAdapter extends BaseAdapter<Prescription> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_common_info;
    }

    @Override
    public BaseViewHolder<Prescription> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView);
    }

    private static class Holder extends BaseViewHolder<Prescription> {

        private TextView mNameTV;
        private TextView mContentTV;

        public Holder(View itemView) {
            super(itemView);
            mNameTV = findViewById(R.id.name_tv);
            mContentTV = findViewById(R.id.content_tv);

        }

        @Override
        public void onBind(Prescription data, int position) {
            mNameTV.setText(data.getNo());

            StringBuilder sb = new StringBuilder();
            sb.append("药品名称：");
            sb.append(data.getName());
            sb.append("\n");
            sb.append("药品用量：");
            sb.append(String.format("%s/%s", (int)data.getDosage(), data.getUnit()));
            sb.append("\n");
            sb.append("药品剂数：");
            sb.append(data.getCount());

            mContentTV.setText(sb);
        }
    }
}
