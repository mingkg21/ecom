package com.ecome.packet.ui.fragment;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.mk.core.ui.fragment.BaseFragment;
import com.ecome.packet.R;

/**
 * Created by mingkg21 on 2017/10/10.
 */

public abstract class AppFragment extends BaseFragment {

    protected TextView mTitleBarRightTV;

    @Override
    protected int getTitleBarBackResId() {
        return R.id.title_bar_back;
    }

    @Override
    protected int getTitleBarTitleResId() {
        return R.id.title_bar_title;
    }

    protected int getTitleBarRightResId() {
        return R.id.title_bar_right;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        mTitleBarRightTV = (TextView) view.findViewById(getTitleBarRightResId());

        if (mTitleBarRightTV != null) {
            mTitleBarRightTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onTitleBarRightAction();
                }
            });
        }
    }

    protected void setTitleBarRightText(String text) {
        if (!TextUtils.isEmpty(text)) {
            if (mTitleBarRightTV != null) {
                mTitleBarRightTV.setText(text);
                mTitleBarRightTV.setVisibility(View.VISIBLE);
            }
        } else {
            if (mTitleBarRightTV != null) {
                mTitleBarRightTV.setVisibility(View.GONE);
            }
        }
    }

    protected void onTitleBarRightAction() {

    }
}
