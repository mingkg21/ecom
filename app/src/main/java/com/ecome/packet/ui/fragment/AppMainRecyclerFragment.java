package com.ecome.packet.ui.fragment;

import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.fragment.RecyclerFragment;

public abstract class AppMainRecyclerFragment<T> extends RecyclerFragment<T> {

    protected TextView mTitleTV;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_main_list;
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        setTopDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_list_top_height));
        mTitleTV = view.findViewById(R.id.fragment_main_list_title_tv);
    }

    @Override
    protected void setTitle(String title) {
        if (mTitleTV != null) {
            mTitleTV.setText(title);
        }
    }
}
