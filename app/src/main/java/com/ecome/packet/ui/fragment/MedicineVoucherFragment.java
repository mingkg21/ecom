package com.ecome.packet.ui.fragment;

import android.app.Activity;
import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Voucher;
import com.ecome.packet.model.MedicineManageModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.VoucherAdapter;
import com.ecome.packet.ui.dialog.ConfirmDialog;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class MedicineVoucherFragment extends AppRecyclerFragment<Voucher> {

    private MedicineManageModel mMedicineManageModel;

    @Override
    protected BaseAdapter<Voucher> getAdapter() {
        return new VoucherAdapter(new VoucherAdapter.OnVoucherListener() {
            @Override
            public void onConfirm(final Voucher voucher) {
                new ConfirmDialog(getContext()).setContent("是否确认单据？").setSureListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMedicineManageModel.confirmMdVoucher(voucher.getVoucherNo());
                    }
                }).show();
            }

            @Override
            public void onModify(Voucher voucher) {
                ActivityRouter.openAddVoucherActivity(getContext(), voucher);
            }
        });
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected void initData() {
        super.initData();
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));

        setTopDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_list_top_height));

        mRecyclerView.setVisibility(View.VISIBLE);
        setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Object data) {

            }
        });

        setTitleBarRightText("新增单据");

        mMedicineManageModel = new MedicineManageModel();
        mMedicineManageModel.addCallback(this);

        loadData();
    }

    @Override
    protected void onTitleBarRightAction() {
        super.onTitleBarRightAction();
        ActivityRouter.openAddVoucherActivity(getContext(), null);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVoucherRefresh(Voucher voucher) {
        loadData();
    }

    @Override
    protected void loadData() {
        super.loadData();

        mMedicineManageModel.queryAllMdVoucher();
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mMedicineManageModel.isQueryAllMdVoucherKey(key)) {
            showLoadingView();
            return true;
        } else if (mMedicineManageModel.isConfirmMdVoucherKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mMedicineManageModel.isQueryAllMdVoucherKey(key)) {
            hideLoadingView();
            return true;
        } else if (mMedicineManageModel.isConfirmMdVoucherKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mMedicineManageModel.isQueryAllMdVoucherKey(key)) {
            AppResponseEntity<List<Voucher>> responseEntity = (AppResponseEntity<List<Voucher>>) data;
            setAll(responseEntity.getData());
            return true;
        } else if (mMedicineManageModel.isConfirmMdVoucherKey(key)) {
            AppResponseEntity<List<Voucher>> responseEntity = (AppResponseEntity<List<Voucher>>) data;
            List<Voucher> tempVouchers = responseEntity.getData();
            if (!tempVouchers.isEmpty()) {
                Voucher voucher = tempVouchers.get(0);
                ToastUtil.showToast(voucher.getMsg());
                if (voucher.isSuccess()) {
                    List<Voucher> vouchers = getAdapter().getDatas();
                    for (Voucher v : vouchers) {
                        if (v.getVoucherNo().equals(voucher.getVoucherNo())) {
                            v.queRenFlag();
                            notifyDataSetChanged();
                            break;
                        }
                    }
                }
            }
            return true;
        }
        return super.onSuccess(key, data);
    }
}
