package com.ecome.packet.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.ui.dialog.TimeDialog;
import com.ecome.packet.util.Constants;
import com.mk.core.util.TimeUtil;

import java.util.Calendar;

public class NoonView extends LinearLayout {

    private TextView mStartTV;
    private TextView mEndTV;

    private long mStartTime;
    private long mEndTime;

    public NoonView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(getContext()).inflate(R.layout.layout_noon, this);

        TextView noonTV = findViewById(R.id.roon_name_tv);

        mStartTV = findViewById(R.id.roon_time_start_tv);
        mEndTV = findViewById(R.id.roon_time_end_tv);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.NoonView);
        String day = typedArray.getString(R.styleable.NoonView_noon);
        Drawable dayDrawable = typedArray.getDrawable(R.styleable.NoonView_moonBackground);
        typedArray.recycle();

        noonTV.setText(day);
        noonTV.setBackground(dayDrawable);

        mStartTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDailog(true);
            }
        });
        mEndTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDailog(false);
            }
        });
    }

    private void showTimeDailog(final boolean isStart) {
        final Calendar calendar = Calendar.getInstance();
        if (isStart) {
            calendar.setTimeInMillis(mStartTime);
        } else {
            calendar.setTimeInMillis(mEndTime);
        }
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        new TimeDialog(getContext(), hour, minute, new TimeDialog.OnTimeChoicedListener() {
            @Override
            public void onChoiced(int hour, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.set(Calendar.MINUTE, minute);
                if (isStart) {
                    mStartTime = calendar.getTimeInMillis();
                    mStartTV.setText(TimeUtil.formatHH_MM(mStartTime));
                } else {
                    mEndTime = calendar.getTimeInMillis();
                    mEndTV.setText(TimeUtil.formatHH_MM(mEndTime));
                }
            }
        }).show();
    }

    public void setData(String startTime, String endTime) {
        this.mStartTime = Constants.getTime(startTime);
        this.mEndTime = Constants.getTime(endTime);
        mStartTV.setText(TimeUtil.formatHH_MM(mStartTime));
        mEndTV.setText(TimeUtil.formatHH_MM(mEndTime));
    }

    public String getStartTime() {
        return TimeUtil.formatYYYY_MM_DD_HH_MM_SS(mStartTime);
    }

    public String getEndTime() {
        return TimeUtil.formatYYYY_MM_DD_HH_MM_SS(mEndTime);
    }

}
