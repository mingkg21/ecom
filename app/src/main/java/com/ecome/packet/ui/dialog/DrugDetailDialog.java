package com.ecome.packet.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Drug;
import com.ecome.packet.entity.Fried;
import com.ecome.packet.entity.PinCi;
import com.ecome.packet.entity.Use;
import com.ecome.packet.ui.widget.BottomSheetListLayout;

import java.util.ArrayList;

public class DrugDetailDialog extends Dialog {

    private Use mUse;
    private Fried mFried;
    private PinCi mPinCi;

    public DrugDetailDialog(@NonNull final Context context, Drug drug, String guige, final String yongliang,
                            final String unit, final Use use, final Fried fried, final PinCi pinCi, int zibei,
                            final ArrayList<Fried> frieds, final ArrayList<Use> uses, final ArrayList<PinCi> pinCis, final OnDrugDetailListener onDrugDetailListener) {
        super(context);

        setContentView(R.layout.dialog_drug_detail);

        TextView nameTV = findViewById(R.id.dialog_drug_detail_name_tv);
        final EditText guigeET = findViewById(R.id.dialog_drug_detail_guige_et);
        final EditText yongliangET = findViewById(R.id.dialog_drug_detail_yongliang_et);
        final TextView useTV = findViewById(R.id.dialog_drug_detail_use_tv);
        final TextView jianfaTV = findViewById(R.id.dialog_drug_detail_jianfa_tv);
        final TextView pinciTV = findViewById(R.id.dialog_drug_detail_pinci_tv);
        final EditText unitET = findViewById(R.id.dialog_drug_detail_uint_et);
        final RadioGroup zibeiRG = findViewById(R.id.dialog_drug_detail_zibei_rg);

        guigeET.setText(guige);
        yongliangET.setText(yongliang);
        unitET.setText(unit);
        nameTV.setText(drug.getName());

        mUse = use;
        mFried = fried;
        mPinCi = pinCi;

        zibeiRG.check(R.id.dialog_drug_detail_zibei_no_rb);
        if (zibei == 1) {
            zibeiRG.check(R.id.dialog_drug_detail_zibei_yes_rb);
        }

        useTV.setText(mUse != null ? mUse.getName() : "");
        jianfaTV.setText(mFried != null ? mFried.getName() : "");
        pinciTV.setText(mPinCi != null ? mPinCi.getName() : "");

        useTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showClsListDialog(context, uses, new BottomSheetListLayout.OnItemListener<Use>() {
                    @Override
                    public void onItemClick(Use data) {
                        useTV.setText(data.getName());
                        mUse = data;
                    }

                    @Override
                    public String getItemContent(Use data) {
                        return null;
                    }
                });
            }
        });

        jianfaTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showClsListDialog(context, frieds, new BottomSheetListLayout.OnItemListener<Fried>() {
                    @Override
                    public void onItemClick(Fried data) {
                        jianfaTV.setText(data.getName());
                        mFried = data;
                    }

                    @Override
                    public String getItemContent(Fried data) {
                        return null;
                    }
                });
            }
        });

        pinciTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showClsListDialog(context, pinCis, new BottomSheetListLayout.OnItemListener<PinCi>() {
                    @Override
                    public void onItemClick(PinCi data) {
                        pinciTV.setText(data.getName());
                        mPinCi = data;
                    }

                    @Override
                    public String getItemContent(PinCi data) {
                        return null;
                    }
                });
            }
        });

        findViewById(R.id.dialog_confirm_sure).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String guige = guigeET.getText().toString().trim();
                String yonglianTemp = yongliangET.getText().toString().trim();
                String unitTemp = yongliangET.getText().toString().trim();
                int zibei = zibeiRG.getCheckedRadioButtonId() == R.id.dialog_drug_detail_zibei_yes_rb ? 1 : 0;

                if (onDrugDetailListener != null) {
                    onDrugDetailListener.onClick(guige, yonglianTemp, unitTemp, mUse, mFried, mPinCi, zibei);
                }
                dismiss();
            }
        });
        findViewById(R.id.dialog_confirm_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface OnDrugDetailListener {
        void onClick(String guige, String yongliang, String unit, Use use, Fried fried, PinCi pinCi, int zibei);
    }

}
