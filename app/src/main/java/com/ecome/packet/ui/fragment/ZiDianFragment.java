package com.ecome.packet.ui.fragment;

import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.ZiDian;
import com.ecome.packet.model.BackendManageModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.ZiDianAdapter;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;

import java.util.List;

/** 字典
 *
 */
public class ZiDianFragment extends AppRecyclerFragment<ZiDian> {

    private BackendManageModel mBackendManageModel;

    @Override
    protected BaseAdapter<ZiDian> getAdapter() {
        return new ZiDianAdapter();
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected void initView(final View view) {
        super.initView(view);

        setTopDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_list_top_height));

        setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));

        mRecyclerView.setVisibility(View.VISIBLE);

        mBackendManageModel = new BackendManageModel();
        mBackendManageModel.addCallback(this);
        loadData();
    }

    @Override
    protected void loadData() {
        mBackendManageModel.getZiDian();
    }

    @Override
    public boolean onPreLoad(String key) {
        if(mBackendManageModel.isGetZiDianKey(key)) {
            showLoadingView();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if(mBackendManageModel.isGetZiDianKey(key)) {
            hideLoadingView();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mBackendManageModel.isGetZiDianKey(key)) {
            showErrorView();
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mBackendManageModel.isGetZiDianKey(key)) {
            AppResponseEntity<List<ZiDian>> appResponseEntity = (AppResponseEntity<List<ZiDian>>) data;
            setAll(appResponseEntity.getData());
            return true;
        }
        return super.onSuccess(key, data);
    }
}
