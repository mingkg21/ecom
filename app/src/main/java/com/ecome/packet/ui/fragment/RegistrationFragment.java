package com.ecome.packet.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppBaseEntity;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.DiagnoseResult;
import com.ecome.packet.entity.Drug;
import com.ecome.packet.entity.Fried;
import com.ecome.packet.entity.Patient;
import com.ecome.packet.entity.PinCi;
import com.ecome.packet.entity.Registration;
import com.ecome.packet.entity.RegistrationRemark;
import com.ecome.packet.entity.TempDetail;
import com.ecome.packet.entity.Use;
import com.ecome.packet.model.BackendManageModel;
import com.ecome.packet.model.DoctorWorkspaceModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.DrugAdapter;
import com.ecome.packet.ui.dialog.DialogManager;
import com.ecome.packet.ui.widget.BottomSheetListLayout;
import com.ecome.packet.ui.widget.ImageItemView;
import com.ecome.packet.ui.widget.PrescriptionInputLayout;
import com.ecome.packet.util.Constants;
import com.ecome.packet.util.QiNiuUtils;
import com.mk.core.util.IMMUtils;
import com.mk.core.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class RegistrationFragment extends AppFragment {

    private LinearLayout mPrescriptionLayout;
    private EditText mDrugET;
    private ListView mDrugLV;
    private View mDrugLayout;
    private DrugAdapter mDrugAdapter;
    private PrescriptionInputLayout mCurrentInputLayout;

    private EditText mDiagnoseET;
    private EditText mSymptomET;
    private EditText mUseNumberET;
    private EditText mRemarkET;

    private LinearLayout mImageLayout;

    private TextView mPriceTV;

    private CheckBox mDaijianCB;
    private CheckBox mGaofanCB;

    private Patient mPatient;
    private Registration mRegistration;

    private DoctorWorkspaceModel mDoctorWorkspaceModel;
    private BackendManageModel mBackendManageModel;

    private ArrayList<Fried> mFrieds = new ArrayList<>();
    private ArrayList<Drug> mDrugs = new ArrayList<>();
    private ArrayList<RegistrationRemark> mRemarks = new ArrayList<>();
    private ArrayList<Use> mUses = new ArrayList<>();
    private ArrayList<PinCi> mPinCis = new ArrayList<>();

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_registration;
    }

    @Override
    protected void initData() {
        super.initData();
        mPatient = (Patient) getArguments().get(ActivityRouter.EXTRA_NAME_PATIENT);
        mRegistration = (Registration) getArguments().get(ActivityRouter.EXTRA_NAME_REGISTRATION);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSelectedTemp(List<TempDetail> tempDetailList) {
        if (tempDetailList == null) {
            return;
        }
        for (TempDetail tempDetail : tempDetailList) {
            PrescriptionInputLayout inputLayout = null;
            for (int i = 0; i < mPrescriptionLayout.getChildCount(); i++) {
                View childView = mPrescriptionLayout.getChildAt(i);
                if (childView instanceof PrescriptionInputLayout) {
                    inputLayout = (PrescriptionInputLayout) childView;
                    if (!inputLayout.hasData()) {
                        break;
                    } else {
                        inputLayout = null;
                    }
                }
            }

            if (inputLayout == null) {
                inputLayout = addPrescriptionInputLayout();
            }

            for (Drug drug : mDrugs) {
                if (drug.getId() == tempDetail.getItemId()) {
                    inputLayout.setDrug(drug);
                    break;
                }
            }

            for (Fried fried : mFrieds) {
                if (fried.getId() == tempDetail.getTempJianfaId()) {
                    inputLayout.setFried(fried);
                    break;
                }
            }

            inputLayout.setNum(tempDetail.getItemCount());

        }
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle("处方录入");

        mDoctorWorkspaceModel = new DoctorWorkspaceModel();
        mDoctorWorkspaceModel.addCallback(this);

        mBackendManageModel = new BackendManageModel();
        mBackendManageModel.addCallback(this);

        TextView nameTV = view.findViewById(R.id.name_tv);
        TextView contentTV = view.findViewById(R.id.content_tv);
        mPriceTV = view.findViewById(R.id.price_tv);

        mImageLayout = view.findViewById(R.id.image_layout);

        mDiagnoseET = view.findViewById(R.id.diagnose_et);
        mSymptomET = view.findViewById(R.id.symptom_et);
        mUseNumberET = view.findViewById(R.id.use_number_et);
        mRemarkET = view.findViewById(R.id.remark_et);

        mDaijianCB = view.findViewById(R.id.daijian_cb);
        mGaofanCB = view.findViewById(R.id.gaofan_cb);

        addImageItemView();

        StringBuilder sb = new StringBuilder();
        if (mPatient != null) {
            nameTV.setText(mPatient.getName());

            sb.append("性别：");
            sb.append(mPatient.getSex());
            sb.append("\n");
            sb.append("年龄：");
            sb.append(mPatient.getAge());
            sb.append("\n");
            sb.append("手机号：");
            sb.append(mPatient.getTel());
            sb.append("\n");
            sb.append("登记时间：");
            sb.append(mPatient.getDatetime());
            sb.append("\n");
            sb.append("收药地址：");
            sb.append(mPatient.getAddress());
        } else {
            nameTV.setText(mRegistration.getPatientName());

            sb.append("性别：");
            sb.append("\n");
            sb.append("年龄：");
            sb.append("\n");
            sb.append("手机号：");
            sb.append(mRegistration.getPatientTel());
            sb.append("\n");
            sb.append("登记时间：");
            sb.append(mRegistration.getDatetime());
            sb.append("\n");
            sb.append("收药地址：");
            sb.append(mRegistration.getPatientAddress());
        }

        contentTV.setText(sb);

        mPrescriptionLayout = view.findViewById(R.id.prescription_layout);

        view.findViewById(R.id.add_prescription_input_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PrescriptionInputLayout layout = addPrescriptionInputLayout();
                layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        layout.onClickName();
                    }
                });
            }
        });

        mUseNumberET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkPrice();
            }
        });

        mDrugLayout = view.findViewById(R.id.drug_layout);
        mDrugET = view.findViewById(R.id.drug_et);
        mDrugLV = view.findViewById(R.id.drug_lv);
        mDrugAdapter = new DrugAdapter(getContext());
        mDrugLV.setAdapter(mDrugAdapter);
        mDrugLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mCurrentInputLayout != null) {
                    mCurrentInputLayout.setDrug(mDrugAdapter.getItem(position));
                    mDrugLayout.setVisibility(View.GONE);
                }
            }
        });
        mDrugET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String str = s.toString();
                ArrayList<Drug> drugs = new ArrayList<>();
                if (!TextUtils.isEmpty(str)) {
                    for (Drug drug : mDrugs) {
                        if (drug.getName().contains(str) || (drug.getPy() != null && drug.getPy().contains(str.toUpperCase()))) {
                            drugs.add(drug);
                            checkPrice();
                        }
                    }
                } else {
                    drugs.addAll(mDrugs);
                }

                mDrugAdapter.setAll(drugs);
            }
        });

        view.findViewById(R.id.get_temp_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityRouter.openPrescriptionTempListActivity(getContext(), true);
            }
        });

        view.findViewById(R.id.done_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String diagnose = mDiagnoseET.getText().toString().trim();
                String symptom = mSymptomET.getText().toString().trim();
                String userNumber = mUseNumberET.getText().toString().trim();
                String remark = mRemarkET.getText().toString().trim();

                StringBuilder detailSB = new StringBuilder();

                for (int i = 0; i < mPrescriptionLayout.getChildCount(); i++) {
                    View childView = mPrescriptionLayout.getChildAt(i);
                    if (childView instanceof PrescriptionInputLayout) {
                        String data = ((PrescriptionInputLayout) childView).getData();
                        if (TextUtils.isEmpty(data)) {
                            return;
                        }
                        detailSB.append(data);
                        detailSB.append("|");
                    }

                }

                String daijian = mDaijianCB.isChecked() ? "1" : "0";
                String gaofan = mGaofanCB.isChecked() ? "1" : "0";

                mDoctorWorkspaceModel.addPrescription(mRegistration.getId(), mRegistration.getPatientId(),
                        diagnose, symptom, detailSB.toString(), userNumber, daijian, gaofan,remark
                        );
            }
        });
        view.findViewById(R.id.cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        view.findViewById(R.id.save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String diagnose = mDiagnoseET.getText().toString().trim();
                String symptom = mSymptomET.getText().toString().trim();
                String userNumber = mUseNumberET.getText().toString().trim();
                String remark = mRemarkET.getText().toString().trim();

                StringBuilder detailSB = new StringBuilder();

                for (int i = 0; i < mPrescriptionLayout.getChildCount(); i++) {
                    View childView = mPrescriptionLayout.getChildAt(i);
                    if (childView instanceof PrescriptionInputLayout) {
                        String data = ((PrescriptionInputLayout) childView).getData();
                        if (TextUtils.isEmpty(data)) {
                            return;
                        }
                        detailSB.append(data);
                        detailSB.append("|");
                    }
                }

                String daijian = mDaijianCB.isChecked() ? "1" : "0";
                String gaofan = mGaofanCB.isChecked() ? "1" : "0";

                mDoctorWorkspaceModel.savePrescriptionTemp(diagnose, symptom, "1", String.format("病人:%s的处方模板", mRegistration.getPatientName()),
                        "1", "1", detailSB.toString(), userNumber, gaofan, daijian, remark);

            }
        });

        findViewById(R.id.remark_choice_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.showRemarkListDialog(getContext(), mRemarks, new BottomSheetListLayout.OnItemListener<RegistrationRemark>() {
                    @Override
                    public void onItemClick(RegistrationRemark data) {
                        mRemarkET.setText(data.getName());
                    }

                    @Override
                    public String getItemContent(RegistrationRemark data) {
                        return null;
                    }
                });
            }
        });

        setTitleBarRightText("历史处方");

        loadData();
    }

    @Override
    protected void onTitleBarRightAction() {
        super.onTitleBarRightAction();
        ActivityRouter.openRegistrationHistoryActivity(getContext(), mPatient);
    }

    @Override
    protected void loadData() {
        mDoctorWorkspaceModel.getFried();
        mDoctorWorkspaceModel.getDrug();
        mBackendManageModel.getRemark();
        mBackendManageModel.getUse();
        mBackendManageModel.getPinCi();
        showLoadingView();
    }

    private void checkData() {
        if (mFrieds.isEmpty()) {
            return;
        }
        if (mDrugs.isEmpty()) {
            return;
        }
        if (mRemarks.isEmpty()) {
            return;
        }
        if (mUses.isEmpty()) {
            return;
        }
        if (mPinCis.isEmpty()) {
            return;
        }
        addPrescriptionInputLayout();
        hideLoadingView();
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mDoctorWorkspaceModel.isAddPrescriptionKey(key)
                || mDoctorWorkspaceModel.isSavePrescriptionTempKey(key)
                || mDoctorWorkspaceModel.isInsertImageKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mDoctorWorkspaceModel.isAddPrescriptionKey(key)
                || mDoctorWorkspaceModel.isSavePrescriptionTempKey(key)
                || mDoctorWorkspaceModel.isInsertImageKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mDoctorWorkspaceModel.isAddPrescriptionKey(key)) {
            AppResponseEntity appResponseEntity = (AppResponseEntity) data;
            ToastUtil.showToast(appResponseEntity.getMsg());
            return true;
        } else if (mDoctorWorkspaceModel.isGetDrugKey(key)
                || mDoctorWorkspaceModel.isGetFriedKey(key)
                || mBackendManageModel.isGetRemarkKey(key)
                || mBackendManageModel.isGetUseKey(key)
                || mBackendManageModel.isGetPinCiKey(key)) {
            showErrorView();
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mDoctorWorkspaceModel.isGetDrugKey(key)) {
            AppResponseEntity<List<Drug>> appResponseEntity = (AppResponseEntity<List<Drug>>) data;
            mDrugs.clear();
            mDrugs.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mDoctorWorkspaceModel.isGetFriedKey(key)) {
            AppResponseEntity<List<Fried>> appResponseEntity = (AppResponseEntity<List<Fried>>) data;
            mFrieds.clear();
            mFrieds.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mBackendManageModel.isGetRemarkKey(key)) {
            AppResponseEntity<List<RegistrationRemark>> appResponseEntity = (AppResponseEntity<List<RegistrationRemark>>) data;
            mRemarks.clear();
            mRemarks.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mBackendManageModel.isGetUseKey(key)) {
            AppResponseEntity<List<Use>> appResponseEntity = (AppResponseEntity<List<Use>>) data;
            mUses.clear();
            mUses.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mBackendManageModel.isGetPinCiKey(key)) {
            AppResponseEntity<List<PinCi>> appResponseEntity = (AppResponseEntity<List<PinCi>>) data;
            mPinCis.clear();
            mPinCis.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mDoctorWorkspaceModel.isAddPrescriptionKey(key)) {
            AppResponseEntity<DiagnoseResult> appResponseEntity = (AppResponseEntity<DiagnoseResult>) data;
            DiagnoseResult diagnoseResult = appResponseEntity.getData();
            if (diagnoseResult != null) {
                ActivityRouter.openRegistrationResultActivity(getContext(), mPatient, mRegistration, appResponseEntity.getData());
                finish();
            } else {
                ToastUtil.showToast("请求失败，请稍后重试！");
            }
            return true;
        } else if (mDoctorWorkspaceModel.isSavePrescriptionTempKey(key)) {
            AppResponseEntity<AppBaseEntity> appResponseEntity = (AppResponseEntity<AppBaseEntity>) data;

            AppBaseEntity appBaseEntity = appResponseEntity.getData();
            if (appBaseEntity != null && appBaseEntity.isSuccess()) {
                ToastUtil.showToast("保存失败，请稍后重试！");
            }
            return true;
        }
        return super.onSuccess(key, data);
    }

    private void addImageItemView() {
        int count = mImageLayout.getChildCount();
        if (count < 4) {
            final ImageItemView imageItemView = new ImageItemView(getContext());
            imageItemView.setOnImageItemListener(new ImageItemView.OnImageItemListener() {
                @Override
                public void onDelete(ImageItemView view) {
                    if (mImageLayout.getChildCount() == 1) {
                        view.reset();
                    } else {
                        mImageLayout.removeView(view);
                        for (int i = 0; i < mImageLayout.getChildCount(); i++) {
                            ImageItemView iiv = (ImageItemView) mImageLayout.getChildAt(i);
                            if (!iiv.hasImage()) {
                                return;
                            }
                        }
                        addImageItemView();
                    }
                }

                @Override
                public void onChoiceImage(String path) {
                    addImageItemView();

                    QiNiuUtils.putImgs(path, new QiNiuUtils.QiNiuCallback() {
                        @Override
                        public void onSuccess(List<String> picUrls) {
                            mDoctorWorkspaceModel.insertImage(mRegistration.getPatientId(), mRegistration.getId(), Constants.getQiniuImageUrl(picUrls.get(0)));
                        }

                        @Override
                        public void onError(String msg) {

                        }
                    });
                }

                @Override
                public void onChoiceImage() {
                    imageItemView.choiceImage(RegistrationFragment.this);
                }
            });
            int tag = 0;
            if (mImageLayout.getTag() != null) {
                tag = (Integer) mImageLayout.getTag();
            }
            mImageLayout.setTag(tag + 1);
            imageItemView.setCode(tag);
            mImageLayout.addView(imageItemView);
        }
    }

    private PrescriptionInputLayout addPrescriptionInputLayout() {
        final PrescriptionInputLayout layout = new PrescriptionInputLayout(getContext());
        layout.setFrieds(mFrieds);
        layout.setUses(mUses);
        layout.setPinCis(mPinCis);
        layout.setOnPrescriptionInputLayoutListener(new PrescriptionInputLayout.OnPrescriptionInputLayoutListener() {
            @Override
            public void onFriedClick() {
                IMMUtils.hideSoftInput(getActivity());
                showFriedList(layout);
            }

            @Override
            public void onNameClick() {
                mDrugET.setText("");
                mDrugAdapter.setAll(mDrugs);
                mCurrentInputLayout = layout;
                mDrugLayout.setVisibility(View.VISIBLE);
                mDrugET.post(new Runnable() {
                    @Override
                    public void run() {
                        mDrugET.requestFocus();
                        IMMUtils.showSoftInput(getActivity(), mDrugET);
                    }
                });
            }

            @Override
            public void onNumChanged() {
                checkPrice();
            }
        });
        mPrescriptionLayout.addView(layout);
        return layout;
    }

    private void checkPrice() {
        try {
            int useNum = Integer.valueOf(mUseNumberET.getText().toString().trim());
            double price = 0;
            if (useNum > 0) {
                for (int i = 0; i < mPrescriptionLayout.getChildCount(); i++) {
                    View childView = mPrescriptionLayout.getChildAt(i);
                    if (childView instanceof PrescriptionInputLayout) {
                        PrescriptionInputLayout inputLayout = ((PrescriptionInputLayout) childView);
                        if (inputLayout.getDrug() != null) {
                            price += inputLayout.getDrug().getPrice() * inputLayout.getNum();
                        }
                    }
                }

                price *= useNum;
            }

            mPriceTV.setText(String.format("价格：%s元", price));
        } catch (Exception e) {

        }
    }

    private void showFriedList(final PrescriptionInputLayout view) {
        DialogManager.showFriedListDialog(getContext(), mFrieds, new BottomSheetListLayout.OnItemListener<Fried>() {
            @Override
            public void onItemClick(Fried data) {
                view.setFried(data);
            }

            @Override
            public String getItemContent(Fried data) {
                return null;
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (mImageLayout != null) {
            for (int i = 0; i < mImageLayout.getChildCount(); i++) {
                View view = mImageLayout.getChildAt(i);
                if (view instanceof ImageItemView) {
                    ((ImageItemView) view).onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mDrugLayout != null && mDrugLayout.getVisibility() == View.VISIBLE) {
            mDrugLayout.setVisibility(View.GONE);
            IMMUtils.hideSoftInput(getActivity());
            return true;
        }
        return super.onBackPressed();
    }

}
