package com.ecome.packet.ui.fragment;

import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.entity.Registration;
import com.ecome.packet.model.ReportModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.ReportRiZhiAdapter;
import com.mk.core.ui.widget.BaseAdapter;

public class ReportRiZhiFragment extends AppDateSearchFragment<Registration> {

    private ReportModel mReportModel;

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));

        setDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_line_large_height));

    }

    @Override
    protected void initLoadModel() {
        mReportModel = new ReportModel();
        mReportModel.addCallback(this);
    }

    @Override
    protected boolean isLoadData(String key) {
        return mReportModel.isReportRiZhiKey(key);
    }

    @Override
    protected BaseAdapter<Registration> getAdapter() {
        return new ReportRiZhiAdapter();
    }

    @Override
    protected void loadData(String startDateTime, String endDateTime) {
        mReportModel.reportRiZhi(startDateTime, endDateTime);
    }

}
