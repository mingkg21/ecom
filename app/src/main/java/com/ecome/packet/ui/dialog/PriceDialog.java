package com.ecome.packet.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ecome.packet.R;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class PriceDialog extends Dialog {

    private EditText mTargetEditText;
    private EditText mEditText;

    private DoneListener mDoneListener;

    public PriceDialog(@NonNull Context context, int type, EditText editText) {
        super(context);

        mTargetEditText = editText;

        setContentView(R.layout.dialog_price);

        TextView titleTV = (TextView) findViewById(R.id.dialog_price_title);
        if (type == 0) {
            titleTV.setText("请输入价格");
        } else {
            titleTV.setText("请输入数量");
        }

        mEditText = (EditText) findViewById(R.id.dialog_price_et);

        mEditText.setText(mTargetEditText.getText());

        mEditText.setSelection(mTargetEditText.getText().length());

        findViewById(R.id.dialog_price_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTargetEditText.setText(mEditText.getText());
                if (mDoneListener != null) {
                    mDoneListener.onDone(mEditText.getText().toString());
                }
                dismiss();
            }
        });

        findViewById(R.id.dialog_price_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void setDoneListener(DoneListener doneListener) {
        mDoneListener = doneListener;
    }

    public interface DoneListener {
        void onDone(String content);
    }

}
