package com.ecome.packet.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.db.SystemInfoPreferences;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.SystemInfo;
import com.ecome.packet.model.UserModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.mk.core.util.ToastUtil;

public class LoginFragment extends AppFragment implements View.OnClickListener {

    private static final int REQUEST_CODE_CHOICE = 1000;

    private UserModel mUserModel;

    private EditText mUserNameET;
    private EditText mUserPwdET;
    private TextView mSystemInfoTV;
    private Button mLoginBTN;

    private SystemInfo mSystemInfo;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_login;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        setTitle("");

        mUserModel = new UserModel();
        mUserModel.addCallback(this);

        mSystemInfoTV = view.findViewById(R.id.service_info_tv);

        mUserNameET = view.findViewById(R.id.user_name_et);
        mUserPwdET = view.findViewById(R.id.user_password_et);

        mLoginBTN = view.findViewById(R.id.login_btn);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (TextUtils.isEmpty(mUserNameET.getText()) || TextUtils.isEmpty(mUserPwdET.getText())) {
                    mLoginBTN.setEnabled(false);
                } else {
                    mLoginBTN.setEnabled(true);
                }

            }
        };

        mUserNameET.addTextChangedListener(textWatcher);
        mUserPwdET.addTextChangedListener(textWatcher);

        mSystemInfoTV.setOnClickListener(this);
        mLoginBTN.setOnClickListener(this);
        view.findViewById(R.id.register_tv).setOnClickListener(this);

        mSystemInfo = SystemInfoPreferences.getInstance().getChoiceSystemInfo();
        if (mSystemInfo != null) {
            mSystemInfoTV.setText(mSystemInfo.getName());
        }
    }

    @Override
    protected void loadData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_btn:
                String userName = mUserNameET.getText().toString().trim();
                String pwd = mUserPwdET.getText().toString().trim();
                if (TextUtils.isEmpty(userName)) {
                    ToastUtil.showToast(R.string.login_input_name_tip_is_null);
                    return;
                }
                if (TextUtils.isEmpty(pwd)) {
                    ToastUtil.showToast(R.string.login_input_password_tip_is_null);
                    return;
                }
                mUserModel.login(userName, pwd);
                break;
            case R.id.cancel_btn:
                finish();
                break;
            case R.id.register_tv:
                ActivityRouter.openRegisterActivity(getContext());
                break;
            case R.id.service_info_tv:
                ActivityRouter.openSystemInfoActivity(getContext(), REQUEST_CODE_CHOICE);
                break;
        }
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mUserModel.isLoginKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mUserModel.isLoginKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mUserModel.isLoginKey(key)) {
            ToastUtil.showToast(R.string.toast_login_success);
        }
        return super.onSuccess(key, data);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mUserModel.isLoginKey(key)) {
            AppResponseEntity responseEntity = (AppResponseEntity) data;
            String msg = responseEntity.getMsg();
            if (TextUtils.isEmpty(msg)) {
                msg = "登录失败";
            }
            ToastUtil.showToast(msg);
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public void onUserLoginStateChange(boolean isLogin) {
        super.onUserLoginStateChange(isLogin);
        if (isLogin) {
            finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_CHOICE) {
            if (resultCode == Activity.RESULT_OK) {
                mSystemInfo = (SystemInfo) data.getSerializableExtra(ActivityRouter.EXTRA_NAME_RESULT);
                mSystemInfoTV.setText(mSystemInfo.getName());
                SystemInfoPreferences.getInstance().choiceSystemInfo(mSystemInfo.getId());
            }
        }
    }
}
