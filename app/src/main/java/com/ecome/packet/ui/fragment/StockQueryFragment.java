package com.ecome.packet.ui.fragment;

import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.KuFang;
import com.ecome.packet.model.BackendManageModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.KuFangAdapter;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;

import java.util.List;

/** 库存查询
 *
 */
public class StockQueryFragment extends AppRecyclerFragment<KuFang> {

    private BackendManageModel mBackendManageModel;

    @Override
    protected BaseAdapter<KuFang> getAdapter() {
        return new KuFangAdapter();
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected void initView(final View view) {
        super.initView(view);

        setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));

        setTopDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_list_top_height));

        mRecyclerView.setVisibility(View.VISIBLE);

        setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Object data) {
                ActivityRouter.openFeeManageActivity(getContext(), "", (KuFang) data);
            }
        });

        mBackendManageModel = new BackendManageModel();
        mBackendManageModel.addCallback(this);
        loadData();
    }

    @Override
    protected void loadData() {
        mBackendManageModel.getKuFang();
    }

    @Override
    public boolean onPreLoad(String key) {
        if(mBackendManageModel.isGetKuFangKey(key)) {
            showLoadingView();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if(mBackendManageModel.isGetKuFangKey(key)) {
            hideLoadingView();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mBackendManageModel.isGetKuFangKey(key)) {
            showErrorView();
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mBackendManageModel.isGetKuFangKey(key)) {
            AppResponseEntity<List<KuFang>> appResponseEntity = (AppResponseEntity<List<KuFang>>) data;
            setAll(appResponseEntity.getData());
            return true;
        }
        return super.onSuccess(key, data);
    }
}
