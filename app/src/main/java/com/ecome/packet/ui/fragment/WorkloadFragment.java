package com.ecome.packet.ui.fragment;

import android.view.View;

import com.ecome.packet.entity.DiagnoseResult;
import com.ecome.packet.entity.Workload;
import com.ecome.packet.model.DoctorWorkspaceModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.WorkloadAdapter;
import com.ecome.packet.ui.dialog.DialogManager;
import com.mk.core.ui.widget.BaseAdapter;

public class WorkloadFragment extends AppDateSearchFragment<Workload> {

    private DoctorWorkspaceModel mDoctorWorkspaceModel;

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));

        setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Object data) {
                Workload workload = (Workload) data;
                ActivityRouter.openPrescriptionActivity(getContext(), workload.getRegistrationId(), workload.getPatientName());
            }
        });
    }

    @Override
    protected boolean isLoadData(String key) {
        return mDoctorWorkspaceModel.isGetDoctorWorkloadKey(key);
    }

    @Override
    protected BaseAdapter<Workload> getAdapter() {
        return new WorkloadAdapter(new WorkloadAdapter.OnPayClickListener() {
            @Override
            public void onClick(Workload workload) {
                DiagnoseResult diagnoseResult = new DiagnoseResult();
                diagnoseResult.setPatientId(workload.getPatientId());
                diagnoseResult.setMoney(workload.getPrice());
                diagnoseResult.setRegistrationId(workload.getRegistrationId());
                DialogManager.showChoicePayDialog(getContext(), diagnoseResult);
            }
        });
    }

    @Override
    protected void initLoadModel() {
        mDoctorWorkspaceModel = new DoctorWorkspaceModel();
        mDoctorWorkspaceModel.addCallback(this);
    }

    @Override
    protected void loadData(String startDateTime, String endDateTime) {
        mDoctorWorkspaceModel.getDoctorWorkload(startDateTime, endDateTime);
    }

    @Override
    public boolean onDataChange(String key, Object data) {
        if (DoctorWorkspaceModel.NOTIFY_CHANGE_CHARGE_SUCCESS.equals(key)) {
            finish();
            return true;
        }
        return super.onDataChange(key, data);
    }
}
