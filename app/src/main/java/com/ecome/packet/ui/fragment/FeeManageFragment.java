package com.ecome.packet.ui.fragment;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;

import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Config;
import com.ecome.packet.entity.Drug;
import com.ecome.packet.entity.KuFang;
import com.ecome.packet.model.BackendManageModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.FeeAdapter;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class FeeManageFragment extends AppSearchFragment<Drug> {

    private BackendManageModel mBackendManageModel;

    private ArrayList<Drug> mDrugs = new ArrayList<>();

    private KuFang mKuFang;

    @Override
    protected String getSearchHint() {
        return "输入汉字检索";
    }

    @Override
    protected void onSearch(String key) {
        if (TextUtils.isEmpty(key)) {
            ToastUtil.showToast("输入汉字检索！");
            return;
        }

        ArrayList<Drug> results = new ArrayList<>();
        for (Drug drug : mDrugs) {
            if (containsStr(key, drug.getName())) {
                results.add(drug);
            }
        }
        setAll(results);
    }

    private boolean containsStr(String key, String value) {
        if (TextUtils.isEmpty(value)) {
            return false;
        }
        if (value.contains(key)) {
            return true;
        }
        return false;
    }

    @Override
    protected BaseAdapter<Drug> getAdapter() {
        FeeAdapter.OnModifyClickListener onModifyClickListener = null;
        if (mKuFang == null) {
            onModifyClickListener = new FeeAdapter.OnModifyClickListener() {
                @Override
                public void onClick(Drug drug) {
                    ActivityRouter.openAddFeeActivity(getContext(), drug);
                }
            };
        }
        return new FeeAdapter(onModifyClickListener);
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVoucherRefresh(Drug drug) {
        loadData();
    }

    @Override
    protected void initData() {
        super.initData();
        mKuFang = (KuFang) getArguments().get(ActivityRouter.EXTRA_NAME_KUFANG);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        if (mKuFang == null) {
            setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));
            setTitleBarRightText("新增");
        } else {
            setTitle(mKuFang.getName() + "药品查询");
        }

        mBackendManageModel = new BackendManageModel();
        mBackendManageModel.addCallback(this);

        loadData();
    }

    @Override
    protected void onTitleBarRightAction() {
        super.onTitleBarRightAction();

        ActivityRouter.openAddFeeActivity(getContext(), null);
    }

    @Override
    protected void loadData() {
        super.loadData();

        String id = null;
        if (mKuFang != null) {
            id = mKuFang.getId() + "";
        }
        mBackendManageModel.getFee(id);
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mBackendManageModel.isGetFeeKey(key)) {
            showLoadingView();
            return true;
        } else if (mBackendManageModel.isModifyConfigKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mBackendManageModel.isGetFeeKey(key)) {
            hideLoadingView();
            return true;
        } else if (mBackendManageModel.isModifyConfigKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mBackendManageModel.isModifyConfigKey(key)) {
            ToastUtil.showToast("修改失败，请稍后重试！");
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mBackendManageModel.isGetFeeKey(key)) {
            AppResponseEntity<List<Drug>> responseEntity = (AppResponseEntity<List<Drug>>) data;
            mDrugs.addAll(responseEntity.getData());
            setAll(mDrugs);
            return true;
        } else if (mBackendManageModel.isModifyConfigKey(key)) {
            AppResponseEntity<Config> responseEntity = (AppResponseEntity<Config>) data;
            Config result = responseEntity.getData();
//            for (Staff staff : mDrugs) {
//                if (staff.getConfigId().equals(result.getConfigId())) {
//                    staff.setValue(result.getValue());
//                    notifyDataSetChanged();
//                    break;
//                }
//            }
            ToastUtil.showToast("修改成功！");
            return true;
        }
        return super.onSuccess(key, data);
    }
}
