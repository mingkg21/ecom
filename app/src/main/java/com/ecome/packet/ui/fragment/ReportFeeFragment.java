package com.ecome.packet.ui.fragment;

import android.view.View;

import com.ecome.packet.entity.Fee;
import com.ecome.packet.model.ReportModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.ReportFeeAdapter;
import com.mk.core.ui.widget.BaseAdapter;

public class ReportFeeFragment extends AppDateSearchFragment<Fee> {

    private ReportModel mReportModel;

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));
    }

    @Override
    protected void initLoadModel() {
        mReportModel = new ReportModel();
        mReportModel.addCallback(this);
    }

    @Override
    protected boolean isLoadData(String key) {
        return mReportModel.isReportFeeKey(key);
    }

    @Override
    protected BaseAdapter<Fee> getAdapter() {
        return new ReportFeeAdapter();
    }

    @Override
    protected void loadData(String startDateTime, String endDateTime) {
        mReportModel.reportFee(startDateTime, endDateTime);
    }

}
