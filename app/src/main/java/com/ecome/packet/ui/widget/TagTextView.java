package com.ecome.packet.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mk.core.util.DimensionUtil;

public class TagTextView extends TextView {

    private Paint mBGPaint;
    private int mStrokeWidth;

    public TagTextView(Context context) {
        super(context);
        init();
    }

    public TagTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TagTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mStrokeWidth = DimensionUtil.DIPToPX(0.5f);
        mBGPaint = new Paint();
        mBGPaint.setStyle(Paint.Style.STROKE);
        mBGPaint.setStrokeWidth(mStrokeWidth);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        RectF rectF = new RectF(mStrokeWidth, mStrokeWidth, getWidth() - mStrokeWidth, getHeight() - mStrokeWidth);
        mBGPaint.setColor(getCurrentTextColor());
        canvas.drawRoundRect(rectF, 5, 5, mBGPaint);
        super.onDraw(canvas);
    }
}
