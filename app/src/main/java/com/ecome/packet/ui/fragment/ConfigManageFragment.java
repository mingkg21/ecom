package com.ecome.packet.ui.fragment;

import android.text.TextUtils;
import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Config;
import com.ecome.packet.model.BackendManageModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.ConfigAdapter;
import com.ecome.packet.ui.dialog.ConfigModifyDialog;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

public class ConfigManageFragment extends AppSearchFragment<Config> {

    private BackendManageModel mBackendManageModel;

    private ArrayList<Config> mConfigs = new ArrayList<>();

    @Override
    protected String getSearchHint() {
        return "输入汉字检索";
    }

    @Override
    protected void onSearch(String key) {
        if (TextUtils.isEmpty(key)) {
            ToastUtil.showToast("输入汉字检索！");
            return;
        }

        ArrayList<Config> results = new ArrayList<>();
        for (Config config : mConfigs) {
            if (containsStr(key, config.getCode()) || containsStr(key, config.getRemark())) {
                results.add(config);
            }
        }
        setAll(results);
    }

    private boolean containsStr(String key, String value) {
        if (TextUtils.isEmpty(value)) {
            return false;
        }
        if (value.contains(key)) {
            return true;
        }
        return false;
    }

    @Override
    protected BaseAdapter<Config> getAdapter() {
        return new ConfigAdapter(new ConfigAdapter.OnModifyClickListener() {
            @Override
            public void onClick(Config config) {
                new ConfigModifyDialog(getContext(), config, new ConfigModifyDialog.OnConfigModifyListener() {
                    @Override
                    public void onModify(Config config) {
                        mBackendManageModel.modifyConfig(config.getConfigId(), config.getValue());
                    }
                }).show();
            }
        });
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));

        setTopDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_list_top_height));

        mBackendManageModel = new BackendManageModel();
        mBackendManageModel.addCallback(this);

        loadData();
    }

    @Override
    protected void loadData() {
        super.loadData();

        mBackendManageModel.getConfig();
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mBackendManageModel.isGetConfigKey(key)) {
            showLoadingView();
            return true;
        } else if (mBackendManageModel.isModifyConfigKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mBackendManageModel.isGetConfigKey(key)) {
            hideLoadingView();
            return true;
        } else if (mBackendManageModel.isModifyConfigKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mBackendManageModel.isModifyConfigKey(key)) {
            ToastUtil.showToast("修改失败，请稍后重试！");
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mBackendManageModel.isGetConfigKey(key)) {
            AppResponseEntity<List<Config>> responseEntity = (AppResponseEntity<List<Config>>) data;
            mConfigs.addAll(responseEntity.getData());
            setAll(mConfigs);
            return true;
        } else if (mBackendManageModel.isModifyConfigKey(key)) {
            AppResponseEntity<Config> responseEntity = (AppResponseEntity<Config>) data;
            Config result = responseEntity.getData();
            for (Config config : mConfigs) {
                if (config.getConfigId().equals(result.getConfigId())) {
                    config.setValue(result.getValue());
                    notifyDataSetChanged();
                    break;
                }
            }
            ToastUtil.showToast("修改成功！");
            return true;
        }
        return super.onSuccess(key, data);
    }
}
