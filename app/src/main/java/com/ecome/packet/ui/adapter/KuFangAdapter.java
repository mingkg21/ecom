package com.ecome.packet.ui.adapter;

import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckedTextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.KuFang;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;
import com.mk.core.util.DimensionUtil;

public class KuFangAdapter extends BaseAdapter<KuFang> {


    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_kufang;
    }

    @Override
    public BaseViewHolder<KuFang> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView);
    }

    private static class Holder extends BaseViewHolder<KuFang> {

        private CheckedTextView mNameTV;

        public Holder(View itemView) {
            super(itemView);
            mNameTV = (CheckedTextView) itemView;
            mNameTV.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            mNameTV.setCheckMarkDrawable(null);
            mNameTV.setGravity(Gravity.LEFT);
            mNameTV.setPadding(DimensionUtil.DIPToPX(13), DimensionUtil.DIPToPX(15), DimensionUtil.DIPToPX(13), DimensionUtil.DIPToPX(15));
        }

        @Override
        public void onBind(final KuFang data, int position) {
            mNameTV.setText(data.getName());
        }
    }
}
