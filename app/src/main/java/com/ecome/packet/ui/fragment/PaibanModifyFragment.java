package com.ecome.packet.ui.fragment;

import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppBaseEntity;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Staff;
import com.ecome.packet.entity.WorkDay;
import com.ecome.packet.entity.WorkTime;
import com.ecome.packet.model.BackendManageModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.dialog.DateDialog;
import com.ecome.packet.ui.widget.DayView;
import com.ecome.packet.ui.widget.NoonView;
import com.ecome.packet.util.Constants;
import com.mk.core.util.TimeUtil;
import com.mk.core.util.ToastUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PaibanModifyFragment extends AppFragment {

    private BackendManageModel mBackendManageModel;

    private Staff mStaff;
    private ArrayList<WorkDay> mWorkDays = new ArrayList<>();
    private WorkTime mWorkTime;

    private TextView mNameTV;
    private TextView mMinuteTV;
    private TextView mInfoTV;

    private DayView mDayView1;
    private DayView mDayView2;
    private DayView mDayView3;
    private DayView mDayView4;
    private DayView mDayView5;
    private DayView mDayView6;
    private DayView mDayView7;

    private NoonView mMorningNoonView;
    private NoonView mAfternoonNoonView;
    private NoonView mNightNoonView;

    private TextView mDateStartTV;
    private TextView mDateEndTV;

    private long mDateStartTime;
    private long mDateEndTime;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_paiban_modify;
    }

    @Override
    protected void initData() {
        super.initData();
        mStaff = (Staff) getArguments().get(ActivityRouter.EXTRA_NAME_STAFF);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle("修改排班作息");

        mBackendManageModel = new BackendManageModel();
        mBackendManageModel.addCallback(this);

        mNameTV = view.findViewById(R.id.fragment_paiban_modify_name_tv);
        mMinuteTV = view.findViewById(R.id.fragment_paiban_modify_minite_tv);
        mInfoTV = view.findViewById(R.id.fragment_paiban_modify_info_tv);

        mDayView1 = view.findViewById(R.id.fragment_paiban_modify_day_1);
        mDayView2 = view.findViewById(R.id.fragment_paiban_modify_day_2);
        mDayView3 = view.findViewById(R.id.fragment_paiban_modify_day_3);
        mDayView4 = view.findViewById(R.id.fragment_paiban_modify_day_4);
        mDayView5 = view.findViewById(R.id.fragment_paiban_modify_day_5);
        mDayView6 = view.findViewById(R.id.fragment_paiban_modify_day_6);
        mDayView7 = view.findViewById(R.id.fragment_paiban_modify_day_7);

        mMorningNoonView = view.findViewById(R.id.fragment_paiban_modify_noon_morning);
        mAfternoonNoonView = view.findViewById(R.id.fragment_paiban_modify_noon_afternoon);
        mNightNoonView = view.findViewById(R.id.fragment_paiban_modify_noon_night);

        mDateStartTV = view.findViewById(R.id.paibantime_start_tv);
        mDateEndTV = view.findViewById(R.id.paibantime_end_tv);

        mDateStartTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog(true);
            }
        });
        mDateEndTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog(false);
            }
        });

        setTitleBarRightText("排班");

        loadData();
    }

    @Override
    protected void onTitleBarRightAction() {
        super.onTitleBarRightAction();

        if (mDateStartTime > mDateEndTime) {
            ToastUtil.showToast("开始日期不能大意结束日期！");
            return;
        }

        mBackendManageModel.modifyDoctorWork(mStaff.getId(),
                mMorningNoonView.getStartTime(), mMorningNoonView.getEndTime(),
                mAfternoonNoonView.getStartTime(), mAfternoonNoonView.getEndTime(),
                mNightNoonView.getStartTime(), mNightNoonView.getEndTime(),
                mDayView1.getMorning(), mDayView1.getAfternoon(), mDayView1.getNight(),
                mDayView2.getMorning(), mDayView2.getAfternoon(), mDayView2.getNight(),
                mDayView3.getMorning(), mDayView3.getAfternoon(), mDayView3.getNight(),
                mDayView4.getMorning(), mDayView4.getAfternoon(), mDayView4.getNight(),
                mDayView5.getMorning(), mDayView5.getAfternoon(), mDayView5.getNight(),
                mDayView6.getMorning(), mDayView6.getAfternoon(), mDayView6.getNight(),
                mDayView7.getMorning(), mDayView7.getAfternoon(), mDayView7.getNight());
    }

    @Override
    protected void loadData() {
        mBackendManageModel.getDoctorWorkWeek(mStaff.getId());
        mBackendManageModel.getDoctorWork(mStaff.getId());

        showLoadingView();
    }

    private void showDateDialog(final boolean isDateStart) {
        final Calendar calendar = Calendar.getInstance();
        if (isDateStart) {
            calendar.setTimeInMillis(mDateStartTime);
        } else {
            calendar.setTimeInMillis(mDateEndTime);
        }
        new DateDialog(getContext(), isDateStart, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DateDialog.OnDateChoiceListener() {
            @Override
            public void onChoice(int year, int month, int day) {
                calendar.set(year, month, day);
                if (isDateStart) {
                    mDateStartTime = calendar.getTimeInMillis();
                    mDateStartTV.setText(TimeUtil.formatYYYY_MM_DD(mDateStartTime));
                } else {
                    mDateEndTime = calendar.getTimeInMillis();
                    mDateEndTV.setText(TimeUtil.formatYYYY_MM_DD(mDateEndTime));
                }
            }
        }).show();

    }

    private void checkData() {
        if (mWorkDays.isEmpty()) {
            return;
        }
        if (mWorkTime == null) {
            return;
        }

        mNameTV.setText(mStaff.getName());
        mMinuteTV.setText("看诊能力：" + mStaff.getKanzhengMinute() + "分钟");
        mInfoTV.setText(mStaff.getSex() + " " + mStaff.getRoleName());

        for (WorkDay workDay : mWorkDays) {
            DayView dayView = null;
            switch (workDay.getWeekDay()) {
                case 1:
                    dayView = mDayView1;
                    break;
                case 2:
                    dayView = mDayView2;
                    break;
                case 3:
                    dayView = mDayView3;
                    break;
                case 4:
                    dayView = mDayView4;
                    break;
                case 5:
                    dayView = mDayView5;
                    break;
                case 6:
                    dayView = mDayView6;
                    break;
                case 7:
                    dayView = mDayView7;
                    break;
            }
            dayView.setData(workDay);
        }

        mMorningNoonView.setData(mWorkTime.getMorningTimeStart(), mWorkTime.getMorningTimeEnd());
        mAfternoonNoonView.setData(mWorkTime.getAfternoonTimeStart(), mWorkTime.getAfternoonTimeEnd());
        mNightNoonView.setData(mWorkTime.getNightTimeStart(), mWorkTime.getNightTimeEnd());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        mDateStartTime = calendar.getTimeInMillis();
        mDateEndTime = Constants.getTime(mWorkTime.getPaibanDataMax());

        mDateStartTV.setText(TimeUtil.formatYYYY_MM_DD(mDateStartTime));
        mDateEndTV.setText(TimeUtil.formatYYYY_MM_DD(mDateEndTime));

        hideLoadingView();
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mBackendManageModel.isCmisPaibanKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mBackendManageModel.isModifyDoctorWorkKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mBackendManageModel.isGetDoctorWorkWeekKey(key)) {
            showErrorView();
            return true;
        } else if (mBackendManageModel.isGetDoctorWorkKey(key)) {
            showErrorView();
            return true;
        } else if (mBackendManageModel.isModifyDoctorWorkKey(key)
                || mBackendManageModel.isCmisPaibanKey(key)) {
            AppResponseEntity appResponseEntity = (AppResponseEntity) data;
            ToastUtil.showToast(appResponseEntity.getMsg());
            hideLoadingDialog();
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mBackendManageModel.isGetDoctorWorkWeekKey(key)) {
            AppResponseEntity<List<WorkDay>> appResponseEntity = (AppResponseEntity<List<WorkDay>>) data;
            mWorkDays.addAll(appResponseEntity.getData());
            checkData();
            return true;
        } else if (mBackendManageModel.isGetDoctorWorkKey(key)) {
            AppResponseEntity<List<WorkTime>> appResponseEntity = (AppResponseEntity<List<WorkTime>>) data;
            if (!appResponseEntity.getData().isEmpty()) {
                mWorkTime = appResponseEntity.getData().get(0);
            }
            checkData();
            return true;
        } else if (mBackendManageModel.isModifyDoctorWorkKey(key)) {
//            AppResponseEntity<AppBaseEntity> appResponseEntity = (AppResponseEntity<AppBaseEntity>) data;
//            AppBaseEntity appBaseEntity = appResponseEntity.getData();
//            ToastUtil.showToast(appBaseEntity.getMsg());
            mBackendManageModel.cmisPaiban(mStaff.getId(), TimeUtil.formatYYYY_MM_DD_HH_MM_SS(mDateStartTime), TimeUtil.formatYYYY_MM_DD_HH_MM_SS(mDateEndTime));
            return true;
        } else if (mBackendManageModel.isCmisPaibanKey(key)) {
            AppResponseEntity<AppBaseEntity> appResponseEntity = (AppResponseEntity<AppBaseEntity>) data;
            AppBaseEntity appBaseEntity = appResponseEntity.getData();
            ToastUtil.showToast(appBaseEntity.getMsg());
            finish();
            return true;
        }
        return super.onSuccess(key, data);
    }
}
