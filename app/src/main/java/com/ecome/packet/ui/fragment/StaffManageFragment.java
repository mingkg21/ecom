package com.ecome.packet.ui.fragment;

import android.text.TextUtils;
import android.view.View;

import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Staff;
import com.ecome.packet.model.BackendManageModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.StaffAdapter;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

public class StaffManageFragment extends AppSearchFragment<Staff> {

    private BackendManageModel mBackendManageModel;

    private ArrayList<Staff> mStaffs = new ArrayList<>();

    @Override
    protected String getSearchHint() {
        return "输入汉字检索";
    }

    @Override
    protected void onSearch(String key) {
        if (TextUtils.isEmpty(key)) {
            ToastUtil.showToast("输入汉字检索！");
            return;
        }

        ArrayList<Staff> results = new ArrayList<>();
        for (Staff staff : mStaffs) {
            if (containsStr(key, staff.getName()) || containsStr(key, staff.getDeptName())) {
                results.add(staff);
            }
        }
        setAll(results);
    }

    private boolean containsStr(String key, String value) {
        if (TextUtils.isEmpty(value)) {
            return false;
        }
        if (value.contains(key)) {
            return true;
        }
        return false;
    }

    @Override
    protected BaseAdapter<Staff> getAdapter() {
        return new StaffAdapter(new StaffAdapter.OnModifyClickListener() {
            @Override
            public void onClick(Staff staff) {
                ActivityRouter.openAddStaffFragmentActivity(getContext(), staff);
            }
        }, false);
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));

        mBackendManageModel = new BackendManageModel();
        mBackendManageModel.addCallback(this);

        setTitleBarRightText("新增");

        loadData();
    }

    @Override
    protected void onTitleBarRightAction() {
        super.onTitleBarRightAction();

        ActivityRouter.openAddStaffFragmentActivity(getContext(), null);
    }

    @Override
    protected void loadData() {
        super.loadData();

        mBackendManageModel.getStaff();
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mBackendManageModel.isGetStaffKey(key)) {
            showLoadingView();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mBackendManageModel.isGetStaffKey(key)) {
            hideLoadingView();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mBackendManageModel.isModifyConfigKey(key)) {
            ToastUtil.showToast("修改失败，请稍后重试！");
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mBackendManageModel.isGetStaffKey(key)) {
            AppResponseEntity<List<Staff>> responseEntity = (AppResponseEntity<List<Staff>>) data;
            mStaffs.clear();
            mStaffs.addAll(responseEntity.getData());
            setAll(mStaffs);
            return true;
        }
        return super.onSuccess(key, data);
    }

    @Override
    public boolean onDataChange(String key, Object data) {
        if (BackendManageModel.KEY_MODIFY_STAFF.equals(key)) {
            loadData();
            return true;
        }
        return super.onDataChange(key, data);
    }
}
