package com.ecome.packet.ui.fragment;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppBaseEntity;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.model.SystemToolsModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.mk.core.util.ToastUtil;

public class FeedbackFragment extends AppFragment {

    private SystemToolsModel mSystemToolsModel;

    private EditText mEditText;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_feedback;
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        mSystemToolsModel = new SystemToolsModel();
        mSystemToolsModel.addCallback(this);

        setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));

        mEditText = view.findViewById(R.id.fragment_feedback_content_et);

        findViewById(R.id.fragment_feedback_submit_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = mEditText.getText().toString().trim();
                if (TextUtils.isEmpty(content)) {
                    ToastUtil.showToast("请输入反馈内容！");
                    return;
                }
                mSystemToolsModel.feedback(content);
            }
        });
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mSystemToolsModel.isFeedbackKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mSystemToolsModel.isFeedbackKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public void onUserLoginStateChange(boolean isLogin) {
        super.onUserLoginStateChange(isLogin);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mSystemToolsModel.isFeedbackKey(key)) {

            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mSystemToolsModel.isFeedbackKey(key)) {
            AppResponseEntity<AppBaseEntity> menus = (AppResponseEntity<AppBaseEntity>) data;
            AppBaseEntity appBaseEntity = menus.getData();
            ToastUtil.showToast(appBaseEntity.getMsg());
            if (appBaseEntity.isSuccess()) {
                mEditText.setText("");
            }
            return true;
        }
        return super.onSuccess(key, data);
    }
}
