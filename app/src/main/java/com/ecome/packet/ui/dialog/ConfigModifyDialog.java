package com.ecome.packet.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Config;
import com.mk.core.util.ToastUtil;

public class ConfigModifyDialog extends Dialog {

    public ConfigModifyDialog(@NonNull Context context, final Config config, final OnConfigModifyListener onConfigModifyListener) {
        super(context);

        setContentView(R.layout.dialog_config);

        TextView nameTV = findViewById(R.id.dialog_config_name);
        TextView remarkTV = findViewById(R.id.dialog_config_remark);
        final EditText valueET = findViewById(R.id.dialog_config_value);

        nameTV.setText(config.getCode());
        if (TextUtils.isEmpty(config.getRemark())) {
            remarkTV.setVisibility(View.GONE);
        } else {
            remarkTV.setText(config.getRemark());
        }
        valueET.setText(config.getValue());

        findViewById(R.id.dialog_config_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.dialog_config_sure).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String value = valueET.getText().toString().trim();
                if (TextUtils.isEmpty(value)) {
                    ToastUtil.showToast("请输入内容！");
                    return;
                }

                config.setValue(value);

                if (onConfigModifyListener != null) {
                    onConfigModifyListener.onModify(config);
                }

                dismiss();
            }
        });
    }

    public interface OnConfigModifyListener {
        void onModify(Config config);
    }

}
