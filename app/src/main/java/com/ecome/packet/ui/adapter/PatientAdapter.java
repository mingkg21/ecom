package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Patient;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class PatientAdapter extends BaseAdapter<Patient> {

    private OnItemClick mOnItemClick;

    private boolean mIsMyPatient;

    public PatientAdapter(OnItemClick onItemClick) {
        mOnItemClick = onItemClick;
    }

    public PatientAdapter setMyPatient(boolean myPatient) {
        mIsMyPatient = myPatient;
        return this;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_patient;
    }

    @Override
    public BaseViewHolder<Patient> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView, mOnItemClick, mIsMyPatient);
    }

    private static class Holder extends MyPatientViewHolder {

        private View registrationBtn;
        private View registrationHistoryBtn;

        private TextView mPhone2TV;
        private TextView mRegisterTimeTV;
        private TextView mAddressTV;

        private OnItemClick mOnItemClick;

        private boolean mIsMyPatient;

        public Holder(View itemView, OnItemClick onItemClick, boolean isMyPatient) {
            super(itemView);

            mPhoneTV.setVisibility(View.GONE);

            mPhone2TV = findViewById(R.id.item_patient_phone_tv);
            mRegisterTimeTV = findViewById(R.id.item_patient_register_time_tv);
            mAddressTV = findViewById(R.id.item_patient_address_tv);

            registrationBtn = findViewById(R.id.registration_btn);
            registrationHistoryBtn = findViewById(R.id.history_btn);

            findViewById(R.id.item_my_patient_more_iv).setVisibility(View.GONE);

            mOnItemClick = onItemClick;

            mIsMyPatient = isMyPatient;

        }

        @Override
        public void onBind(final Patient data, int position) {
            super.onBind(data, position);

            if (mIsMyPatient) {
                mPhoneTV.setVisibility(View.GONE);
            }

            mPhone2TV.setText(data.getTel());
            mRegisterTimeTV.setText(data.getDatetime());
            mAddressTV.setText(data.getAddress());

            registrationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClick != null) {
                        mOnItemClick.onRegistration(data);
                    }
                }
            });
            registrationHistoryBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClick != null) {
                        mOnItemClick.onCheckRegistrationHistory(data);
                    }
                }
            });
        }
    }

    public interface OnItemClick {
        void onRegistration(Patient patient);
        void onCheckRegistrationHistory(Patient patient);
    }
}
