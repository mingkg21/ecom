package com.ecome.packet.ui.fragment;

import android.app.Activity;
import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Temp;
import com.ecome.packet.entity.TempDetail;
import com.ecome.packet.model.DoctorWorkspaceModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.PrescriptionTempAdapter;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class PrescriptionTempListFragment extends AppRecyclerFragment<Temp> {

    private DoctorWorkspaceModel mDoctorWorkspaceModel;

    private boolean mIsChoiceMode;

    @Override
    protected BaseAdapter<Temp> getAdapter() {
        return new PrescriptionTempAdapter();
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSelectedTemp(List<TempDetail> tempDetailList) {
        finish();
    }

    @Override
    protected void initData() {
        super.initData();
        mIsChoiceMode = getArguments().getBoolean(ActivityRouter.EXTRA_NAME_VALUE, false);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle("我的模板");

        setTopDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_list_top_height));

        mRecyclerView.setVisibility(View.VISIBLE);
        setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Object data) {
                ActivityRouter.openPrescriptionTempDetailActivity(getContext(), (Temp) data, mIsChoiceMode);
            }
        });

        mDoctorWorkspaceModel = new DoctorWorkspaceModel();
        mDoctorWorkspaceModel.addCallback(this);

        loadData();
    }

    @Override
    protected void loadData() {
        super.loadData();

        mDoctorWorkspaceModel.getTempList();
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mDoctorWorkspaceModel.isGetTempListKey(key)) {
            showLoadingView();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mDoctorWorkspaceModel.isGetTempListKey(key)) {
            hideLoadingView();
            if (isEmpty()) {
                showEmptyView("暂无模板");
            }
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mDoctorWorkspaceModel.isGetTempListKey(key)) {
            AppResponseEntity<List<Temp>> responseEntity = (AppResponseEntity<List<Temp>>) data;
            setAll(responseEntity.getData());
            return true;
        }
        return super.onSuccess(key, data);
    }

}
