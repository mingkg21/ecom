package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Config;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class ConfigAdapter extends BaseAdapter<Config> {

    private OnModifyClickListener mOnModifyClickListener;

    public ConfigAdapter(OnModifyClickListener onModifyClickListener) {
        mOnModifyClickListener = onModifyClickListener;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_config;
    }

    @Override
    public BaseViewHolder<Config> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView, mOnModifyClickListener);
    }

    private static class Holder extends BaseViewHolder<Config> {

        private TextView mNameTV;
        private TextView mValueTV;
        private TextView mRemarkTV;
        private Button mModifyBtn;

        private OnModifyClickListener mOnModifyClickListener;

        public Holder(View itemView, OnModifyClickListener onModifyClickListener) {
            super(itemView);

            mOnModifyClickListener = onModifyClickListener;

            mNameTV = itemView.findViewById(R.id.name_tv);
            mValueTV = itemView.findViewById(R.id.value_tv);
            mRemarkTV = itemView.findViewById(R.id.remark_tv);
            mModifyBtn = itemView.findViewById(R.id.modify_btn);
            mRemarkTV.setVisibility(View.VISIBLE);
        }

        @Override
        public void onBind(final Config data, int position) {
            mNameTV.setText(data.getCode());
            mValueTV.setText(data.getValue());
            mRemarkTV.setText(data.getRemark());
            mModifyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnModifyClickListener != null) {
                        mOnModifyClickListener.onClick(data);
                    }
                }
            });
        }
    }

    public interface OnModifyClickListener {
        void onClick(Config config);
    }

}
