package com.ecome.packet.ui.fragment;

import android.app.DatePickerDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.util.DimensionUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public abstract class AppDateSearchFragment<T> extends AppRecyclerFragment<T> {

    private TextView mStartTimeTV;
    private TextView mEndTimeTV;

    protected LinearLayout mDataTimeLayout;

    private int mStartYear;
    private int mStartMonth;
    private int mStartDay;

    private int mEndYear;
    private int mEndMonth;
    private int mEndDay;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));

        mDataTimeLayout = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.layout_workload_datatime, null);

        mStartTimeTV = mDataTimeLayout.findViewById(R.id.layout_workload_datatime_start_tv);
        mEndTimeTV = mDataTimeLayout.findViewById(R.id.layout_workload_datatime_end_tv);

        mStartYear = Calendar.getInstance().get(Calendar.YEAR);
        mStartMonth = Calendar.getInstance().get(Calendar.MONTH);
        mStartDay = 1;

        mEndYear = mStartYear;
        mEndMonth = mStartMonth;
        mEndDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

        mStartTimeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getContext(), DatePickerDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        mStartYear = year;
                        mStartMonth = month;
                        mStartDay = dayOfMonth;
                        loadData();
                    }
                }, mStartYear, mStartMonth, mStartDay).show();
            }
        });

        mEndTimeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getContext(), DatePickerDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        mEndYear = year;
                        mEndMonth = month;
                        mEndDay = dayOfMonth;
                        loadData();
                    }
                }, mEndYear, mEndMonth, mEndDay).show();
            }
        });

        ((LinearLayout) view).addView(mDataTimeLayout, 1);

        mRecyclerView.setVisibility(View.VISIBLE);

        setTopDividerHeight(DimensionUtil.DIPToPX(8));

        initLoadModel();

        loadData();
    }

    protected abstract void initLoadModel();

    @Override
    protected void loadData() {
        super.loadData();

        Calendar calendar = Calendar.getInstance();
        calendar.set(mStartYear, mStartMonth, mStartDay, 0, 0, 0);
        String startDateTime = sdf.format(calendar.getTimeInMillis());

        calendar.set(mEndYear, mEndMonth, mEndDay, 23, 59, 59);
        String endDateTime = sdf.format(calendar.getTimeInMillis());

        mStartTimeTV.setText(startDateTime);
        mEndTimeTV.setText(endDateTime);

        loadData(startDateTime, endDateTime);
    }

    protected abstract void loadData(String startDateTime, String endDateTime);

    protected abstract boolean isLoadData(String key);

    @Override
    public boolean onPreLoad(String key) {
        if (isLoadData(key)) {
            showLoadingView();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (isLoadData(key)) {
            hideLoadingView();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (isLoadData(key)) {
            AppResponseEntity<List<T>> responseEntity = (AppResponseEntity<List<T>>) data;
            setAll(responseEntity.getData());
            return true;
        }
        return super.onSuccess(key, data);
    }

}
