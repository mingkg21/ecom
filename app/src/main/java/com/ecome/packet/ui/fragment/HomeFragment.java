package com.ecome.packet.ui.fragment;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.UserInfo;
import com.ecome.packet.model.MenuModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.AdAdapter;
import com.ecome.packet.ui.adapter.MenuAdapter;
import com.ecome.packet.ui.dialog.ConfirmDialog;
import com.ecome.packet.ui.widget.ViewPagerToImageCarousel;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;

import java.util.List;

public class HomeFragment extends AppMainRecyclerFragment<Menu> {

    private MenuModel mMenuModel;

    private ViewPagerToImageCarousel mViewPagerToImageCarousel;
    private AdAdapter mAdAdapter;

    private TextView mRegisterTimeTV;

    @Override
    protected boolean isMain() {
        return true;
    }

    @Override
    protected boolean hasTitleBar() {
        return false;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTopDividerHeight(0);

        View headerView = LayoutInflater.from(getContext()).inflate(R.layout.header_home, null);
        setHeaderView(headerView);

        View footerView = LayoutInflater.from(getContext()).inflate(R.layout.footer_home, null);
        setFooterView(footerView);

        mRegisterTimeTV = footerView.findViewById(R.id.footer_home_register_date);

        mViewPagerToImageCarousel = headerView.findViewById(R.id.layout_ad);

        mAdAdapter = new AdAdapter(getContext());
        mViewPagerToImageCarousel.setAdapter(mAdAdapter);

        mRecyclerView.setBackgroundColor(Color.WHITE);

        setHeaderData();

        mMenuModel = new MenuModel();
        mMenuModel.addCallback(this);

        mRecyclerView.setVisibility(View.VISIBLE);
        setDividerHeight(0);

        setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position, final Object data) {
                checkLogin(new Runnable() {
                    @Override
                    public void run() {
                        ActivityRouter.openMenuActivity(getContext(), (com.ecome.packet.entity.Menu) data, position);
                    }
                });
            }
        });

    }

    @Override
    protected BaseAdapter getAdapter() {
        return new MenuAdapter().setSecond(false);
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected int getGridSpanCount() {
        return 3;
    }

    private String getDate(String time) {
        if (TextUtils.isEmpty(time)) {
            return time;
        }
        return time.substring(0, time.indexOf(" "));
    }

    private void setHeaderData() {
        UserInfo userInfo = UserManager.getInstance().getUserInfo();
        if (userInfo != null) {
            setTitle("欢迎您，" + userInfo.getName() + "（" + userInfo.getRoleName() + "）");
            if (!TextUtils.isEmpty(userInfo.getRegDatetime())) {
                mRegisterTimeTV.setText("分店：" + userInfo.getFendDianId() +  "    注册时间：" + getDate(userInfo.getRegDatetime()) + "    试用结束：" + getDate(userInfo.getDeadDatetime()));
                mRegisterTimeTV.setVisibility(View.VISIBLE);
            } else {
                mRegisterTimeTV.setVisibility(View.GONE);
            }
            mTitleTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new ConfirmDialog(getContext()).setContent("安全退出？").setSureListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            UserManager.getInstance().logout();
                        }
                    }).show();
                }
            });
        } else {
            setTitle("欢迎您");
            mRegisterTimeTV.setVisibility(View.GONE);
            mTitleTV.setOnClickListener(null);
        }
        if (UserManager.getInstance().isLogin()) {
            mAdAdapter.setAll(UserManager.getInstance().getPics());
        } else {
            mAdAdapter.clear();
        }
    }

    @Override
    protected void loadData() {
        mMenuModel.getMenu(1, 0);
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mMenuModel.isGetMenuKey(key)) {
            showLoadingView();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mMenuModel.isGetMenuKey(key)) {
            stopRefresh();
            hideLoadingView();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mMenuModel.isGetMenuKey(key)) {
            AppResponseEntity<List<Menu>> menus = (AppResponseEntity<List<Menu>>) data;
            setAll(menus.getData());
            hideLoadingView();
        }
        return super.onSuccess(key, data);
    }

    @Override
    public void onUserLoginStateChange(boolean isLogin) {
        setHeaderData();
        mMenuModel.getMenu(1, 0);
        super.onUserLoginStateChange(isLogin);
    }
}
