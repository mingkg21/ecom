package com.ecome.packet.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Drug;

import java.util.ArrayList;
import java.util.List;

public class DrugAdapter extends BaseAdapter {

    private ArrayList<Drug> mDrugs;

    private Context mContext;

    public DrugAdapter(Context context) {
        mContext = context;
        mDrugs = new ArrayList<>();
    }

    public void setAll(List<Drug> drugs) {
        mDrugs.clear();
        mDrugs.addAll(drugs);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mDrugs.size();
    }

    @Override
    public Drug getItem(int position) {
        return mDrugs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_drug, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.bind(getItem(position));

        return convertView;
    }

    private static class ViewHolder {

        private TextView mNameTV;
        private TextView mPriceTV;
        private TextView mStoreTV;

        public ViewHolder(View itemView) {
            mNameTV = itemView.findViewById(R.id.name_tv);
            mPriceTV = itemView.findViewById(R.id.price_tv);
            mStoreTV = itemView.findViewById(R.id.store_tv);
        }

        public void bind(Drug drug) {
            mNameTV.setText(drug.getName());
            mPriceTV.setText(String.format("价格:%.2f元", drug.getPrice()));
            mStoreTV.setText(String.format("库存:%s%s", drug.getStock(), drug.getUnit()));

        }

    }
}