package com.ecome.packet.ui.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Menu;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;
import com.mk.core.util.ImageUtil;

public class MenuAdapter extends BaseAdapter<Menu> {

    private boolean mIsSecond;

    public MenuAdapter setSecond(boolean isSecond) {
        mIsSecond = isSecond;
        return this;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        if (mIsSecond) {
            return R.layout.item_menu_second;
        }
        return R.layout.item_menu;
    }

    @Override
    public BaseViewHolder<Menu> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView);
    }

    private static class Holder extends BaseViewHolder<Menu> {

        private TextView mNameTV;
        private TextView mDescTV;
        private ImageView mIconIV;

        public Holder(View itemView) {
            super(itemView);
            mNameTV = findViewById(R.id.menu_name_tv);
            mDescTV = findViewById(R.id.menu_desc_tv);
            mIconIV = findViewById(R.id.menu_icon_iv);

        }

        @Override
        public void onBind(Menu data, int position) {
            mNameTV.setText(data.getName());
            if (TextUtils.isEmpty(data.getPicPath())) {
                mIconIV.setImageResource(R.drawable.ic_default);
            } else {
                ImageUtil.loadImage(data.getPicPath(), mIconIV);
            }
            if (mDescTV != null) {
                mDescTV.setText(data.getRemark());
            }
        }
    }
}
