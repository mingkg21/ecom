package com.ecome.packet.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.ecome.packet.R;


public class ViewPagerToImageCarousel extends CustomViewPager {

    private static final int IMAGE_CAROUSEL = 0;

    protected int mRatio_x = -1;
    protected int mRatio_y = -1;

    private Handler mHandler;

    public ViewPagerToImageCarousel(Context context, AttributeSet attrs) {
        super(context, attrs);
        mHandler = new Handler();
        mHandler.sendEmptyMessageDelayed(IMAGE_CAROUSEL, 3000);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ViewPagerToImageCarousel);
        mRatio_x = typedArray.getInt(R.styleable.ViewPagerToImageCarousel_ViewPagerToImageCarousel_Ratio_X, -1);
        mRatio_y = typedArray.getInt(R.styleable.ViewPagerToImageCarousel_ViewPagerToImageCarousel_Ratio_Y, -1);
        typedArray.recycle();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mHandler.removeMessages(IMAGE_CAROUSEL);
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                mHandler.sendEmptyMessageDelayed(IMAGE_CAROUSEL, 3000);
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        getParent().requestDisallowInterceptTouchEvent(true);
        final int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mHandler.removeMessages(IMAGE_CAROUSEL);
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                mHandler.sendEmptyMessageDelayed(IMAGE_CAROUSEL, 3000);
                break;
        }
        return super.onTouchEvent(ev);
    }

    public void handleMessage(Message msg) {
        switch (msg.what) {
            case IMAGE_CAROUSEL:
                mHandler.removeMessages(IMAGE_CAROUSEL);
                if (getChildCount() > 1) {
                    setCurrentItem(getCurrentItem() + 1, true);
                    mHandler.sendEmptyMessageDelayed(IMAGE_CAROUSEL, 3000);
                }
                break;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        if (mRatio_y > 0 && mRatio_x > 0) {
            if (widthMode == MeasureSpec.EXACTLY) {
                heightMeasureSpec = MeasureSpec.makeMeasureSpec(widthSize * mRatio_y / mRatio_x, MeasureSpec.EXACTLY);
            } else if (heightMode == MeasureSpec.EXACTLY) {
                widthMeasureSpec = MeasureSpec.makeMeasureSpec(heightSize * mRatio_x / mRatio_y, MeasureSpec.EXACTLY);
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void startCarousel() {
        mHandler.sendEmptyMessageDelayed(IMAGE_CAROUSEL, 3000);
    }

    public void stopCarousel() {
        mHandler.removeMessages(IMAGE_CAROUSEL);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mHandler.removeMessages(IMAGE_CAROUSEL);
    }

}