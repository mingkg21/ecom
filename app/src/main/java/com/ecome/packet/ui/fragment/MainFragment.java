package com.ecome.packet.ui.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;

import com.ecome.packet.app.App;
import com.mk.core.ui.fragment.BaseFragment;
import com.mk.core.ui.widget.FragmentAdapter;
import com.ecome.packet.R;
import com.ecome.packet.ui.widget.CustomViewPager;
import com.ecome.packet.ui.widget.OnTabClickListener;
import com.ecome.packet.update.UpdateManager;

import java.util.ArrayList;

/**
 * Created by mingkg21 on 2018/2/27.
 */

public class MainFragment extends BaseFragment implements View.OnClickListener, OnTabClickListener {

    private long mPreTime;

    private View mGardenTabTV;
    private View mManageTabTV;
    private View mFmTabTV;
    private View mMeTabTV;

    private CustomViewPager mViewPager;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        mGardenTabTV = findViewById(R.id.main_tab_garden);
        mManageTabTV = findViewById(R.id.main_tab_manage);
        mFmTabTV = findViewById(R.id.main_tab_fm);
        mMeTabTV = findViewById(R.id.main_tab_me);

        mGardenTabTV.setOnClickListener(this);
        mManageTabTV.setOnClickListener(this);
        mFmTabTV.setOnClickListener(this);
        mMeTabTV.setOnClickListener(this);

        ArrayList<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new HomeFragment());
        fragmentList.add(new MyPatientFragment());
        fragmentList.add(new MyMessageFragment());
        fragmentList.add(new SettingFragment());

        mViewPager = (CustomViewPager) findViewById(R.id.main_vr);
        FragmentAdapter adapter = new FragmentAdapter(getChildFragmentManager(), fragmentList);
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(4);
        mViewPager.setCanScroll(false);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setTabStatus(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setTabStatus(0);
    }

    private void setTabStatus(final int position) {
        mGardenTabTV.setSelected(position == 0);
        mManageTabTV.setSelected(position == 1);
        mFmTabTV.setSelected(position == 2);
        mMeTabTV.setSelected(position == 3);
    }

    @Override
    public boolean isSwipeToClose() {
        return false;
    }

    @Override
    protected void loadData() {

    }

    @Override
    public void onClick(final View v) {
//        if (v == mGardenTabTV) {
//            setTabStatus(0);
//            onTabClick(0);
//        } else if (v == mManageTabTV) {
//            setTabStatus(1);
//            onTabClick(1);
//        } else if (v == mFmTabTV) {
//            setTabStatus(2);
//            onTabClick(2);
//        } else if (v == mMeTabTV) {
//            setTabStatus(3);
//            onTabClick(3);
//        }
        if (v != mGardenTabTV) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if (v == mManageTabTV) {
                        setTabStatus(1);
                        onTabClick(1);
                    } else if (v == mFmTabTV) {
                        setTabStatus(2);
                        onTabClick(2);
                    } else if (v == mMeTabTV) {
                        setTabStatus(3);
                        onTabClick(3);
                    }
                }
            };
            checkLogin(runnable);
        } else {
            setTabStatus(0);
            onTabClick(0);
        }
    }

    @Override
    public void onTabClick(int position) {
        mViewPager.setCurrentItem(position);
    }

    @Override
    public boolean onBackPressed() {
        long now = System.currentTimeMillis();
        if (now - mPreTime > 1500) {
            mPreTime = now;
            Toast.makeText(getContext(), R.string.toast_exit_one_more_time, Toast.LENGTH_SHORT).show();
            return true;
        } else {
            UpdateManager.getInstance().release();
            App.getInstance().onExitApp();
        }
        return super.onBackPressed();
    }
}
