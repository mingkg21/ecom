package com.ecome.packet.ui.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Patient;
import com.ecome.packet.entity.Registration;
import com.ecome.packet.model.DoctorWorkspaceModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.PatientAdapter;
import com.ecome.packet.ui.dialog.ConfirmDialog;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.util.ToastUtil;

import java.util.List;

public class PrescribeFragment extends AppSearchFragment<Patient> {

    private DoctorWorkspaceModel mDoctorWorkspaceModel;

    private Patient mPatient;

    private boolean mIsMyPatient;

    private boolean mIsMain;

    @Override
    protected BaseAdapter<Patient> getAdapter() {
        return new PatientAdapter(new PatientAdapter.OnItemClick() {
            @Override
            public void onRegistration(final Patient patient) {
                mPatient = patient;
                new ConfirmDialog(getContext()).setContent("是否挂号").setSureListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDoctorWorkspaceModel.registration(UserManager.getInstance().getUserId(), patient.getId());
                    }
                }).show();
            }

            @Override
            public void onCheckRegistrationHistory(Patient patient) {
                ActivityRouter.openRegistrationHistoryActivity(getContext(),patient);
            }
        }).setMyPatient(mIsMyPatient);
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    public PrescribeFragment setMain() {
        mIsMain = true;
        mIsMyPatient = true;
        return this;
    }

    @Override
    public boolean isMain() {
        return mIsMain;
    }

    @Override
    protected void initData() {
        super.initData();
        Bundle bundle = getArguments();
        if (bundle != null) {
            mIsMyPatient = bundle.getBoolean(ActivityRouter.EXTRA_NAME_VALUE, false);
        }
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_line_large_height));

        mDoctorWorkspaceModel = new DoctorWorkspaceModel();
        mDoctorWorkspaceModel.addCallback(this);

        if (mIsMyPatient) {
            setTitle("我的病人");
            setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position, Object data) {
                    ActivityRouter.openRegistrationHistoryActivity(getContext(), (Patient) data);
                }
            });
        } else {
            setTitle("处方录入");
            setTitleBarRightText("新增病人");
        }

        loadData();
    }

    @Override
    protected void loadData() {
        super.loadData();
        mDoctorWorkspaceModel.findPatient("");
    }

    @Override
    protected void onTitleBarRightAction() {
        super.onTitleBarRightAction();
        ActivityRouter.openAddPatientActivity(getContext());
    }

    @Override
    protected String getSearchHint() {
        return "请输入手机号码或姓名";
    }

    @Override
    protected void onSearch(String key) {
        if (TextUtils.isEmpty(key)) {
            ToastUtil.showToast("请输入手机号码或姓名");
            return;
        }
        mDoctorWorkspaceModel.findPatient(key);
    }


    @Override
    public boolean onPreLoad(String key) {
        if (mDoctorWorkspaceModel.isFindPatientKey(key)) {
            showLoadingView();
            return true;
        } else if (mDoctorWorkspaceModel.isRegistrationKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mDoctorWorkspaceModel.isFindPatientKey(key)) {
            hideLoadingView();
            return true;
        } else if (mDoctorWorkspaceModel.isRegistrationKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mDoctorWorkspaceModel.isFindPatientKey(key)) {
            AppResponseEntity<List<Patient>> responseEntity = (AppResponseEntity<List<Patient>>) data;
            setAll(responseEntity.getData());
            return true;
        } else if (mDoctorWorkspaceModel.isRegistrationKey(key)) {
            AppResponseEntity<Registration> appResponseEntity = (AppResponseEntity<Registration>) data;
            ActivityRouter.openRegistrationActivity(getContext(), mPatient, appResponseEntity.getData());
        }
        return super.onSuccess(key, data);
    }

    @Override
    public boolean onDataChange(String key, Object data) {
        if (DoctorWorkspaceModel.NOTIFY_CHANGE_CHARGE_SUCCESS.equals(key)) {
            if (!isMain()) {
                finish();
                return true;
            }
        }
        return super.onDataChange(key, data);
    }
}
