package com.ecome.packet.ui.fragment;

import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Prescription;
import com.ecome.packet.model.DoctorWorkspaceModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.PrescriptionAdapter;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;

import java.util.List;

/** 处方信息
 *
 */
public class PrescriptionFragment extends AppRecyclerFragment<Prescription> {

    private DoctorWorkspaceModel mDoctorWorkspaceModel;

    private int mRegistrationId;
    private String mName;

    @Override
    protected BaseAdapter<Prescription> getAdapter() {
        return new PrescriptionAdapter();
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected void initData() {
        super.initData();

        mRegistrationId = getArguments().getInt(ActivityRouter.EXTRA_NAME_REGISTRATION_ID);
        mName = getArguments().getString(ActivityRouter.EXTRA_NAME_NAME);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTopDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_list_top_height));

        setTitle(String.format("<<%s>>处方信息", mName));

        mRecyclerView.setVisibility(View.VISIBLE);

        mDoctorWorkspaceModel = new DoctorWorkspaceModel();
        mDoctorWorkspaceModel.addCallback(this);

        loadData();
    }

    @Override
    protected void loadData() {
        super.loadData();

        mDoctorWorkspaceModel.getPrescription(mRegistrationId + "");
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mDoctorWorkspaceModel.isGetPrescriptionKey(key)) {
            showLoadingView();
            return true;
        }
        return super.onPreLoad(key);
    }

//    @Override
//    public boolean onPostLoad(String key) {
//        if (mDoctorWorkspaceModel.isGetPrescriptionKey(key)) {
//            hideLoadingView();
//            return true;
//        }
//        return super.onPostLoad(key);
//    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mDoctorWorkspaceModel.isGetPrescriptionKey(key)) {
            AppResponseEntity<List<Prescription>> responseEntity = (AppResponseEntity<List<Prescription>>) data;
            if (responseEntity.getData().isEmpty()) {
                showEmptyView("暂无处方信息");
            } else {
                setAll(responseEntity.getData());
                hideLoadingView();
            }
            return true;
        }
        return super.onSuccess(key, data);
    }
}
