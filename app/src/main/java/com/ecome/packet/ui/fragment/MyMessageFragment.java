package com.ecome.packet.ui.fragment;

import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.LogInfo;
import com.ecome.packet.model.BackendManageModel;
import com.ecome.packet.ui.adapter.LogAdapter;
import com.mk.core.ui.widget.BaseAdapter;

import java.util.List;

public class MyMessageFragment extends AppMainRecyclerFragment<LogInfo> {

    private BackendManageModel mBackendManageModel;

    @Override
    protected boolean isMain() {
        return true;
    }

    @Override
    protected BaseAdapter<LogInfo> getAdapter() {
        return new LogAdapter();
    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        setTitle(R.string.main_tab_my_message);

        mRecyclerView.setVisibility(View.VISIBLE);

        mBackendManageModel = new BackendManageModel();
        mBackendManageModel.addCallback(this);

        loadData();
    }

    @Override
    protected void loadData() {
        if (UserManager.getInstance().isLogin()) {
            mBackendManageModel.getLog();
        }
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mBackendManageModel.isGetLogKey(key)) {
            showLoadingView();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mBackendManageModel.isGetLogKey(key)) {
            stopRefresh();
            hideLoadingView();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public void onUserLoginStateChange(boolean isLogin) {
        super.onUserLoginStateChange(isLogin);
        if (isLogin) {
            loadData();
        } else {
            getAdapter().clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mBackendManageModel.isGetLogKey(key)) {
            showErrorView();
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mBackendManageModel.isGetLogKey(key)) {
            AppResponseEntity<List<LogInfo>> menus = (AppResponseEntity<List<LogInfo>>) data;
            setAll(menus.getData());
            return true;
        }
        return super.onSuccess(key, data);
    }
}
