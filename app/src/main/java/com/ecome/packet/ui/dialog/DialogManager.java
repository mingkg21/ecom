package com.ecome.packet.ui.dialog;

import android.content.Context;
import android.support.design.widget.BottomSheetDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.entity.Business;
import com.ecome.packet.entity.ClsEntity;
import com.ecome.packet.entity.Department;
import com.ecome.packet.entity.DiagnoseResult;
import com.ecome.packet.entity.FeeCategory;
import com.ecome.packet.entity.Fried;
import com.ecome.packet.entity.KuFang;
import com.ecome.packet.entity.Provider;
import com.ecome.packet.entity.RegistrationRemark;
import com.ecome.packet.entity.Role;
import com.ecome.packet.entity.Unit;
import com.ecome.packet.entity.ZhiCheng;
import com.ecome.packet.net.request.ChargeRequest;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.widget.BottomSheetListLayout;

import java.util.List;

public class DialogManager {

    public static void showChoicePayDialog(final Context context, final DiagnoseResult diagnoseResult) {
        View layout = LayoutInflater.from(context).inflate(R.layout.dialog_choice_pay, null);
        final BottomSheetDialog sheetDialog = new BottomSheetDialog(context);
        sheetDialog.setContentView(layout);
        sheetDialog.setCanceledOnTouchOutside(true);

        layout.findViewById(R.id.dialog_choice_pay_wx).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityRouter.openChargeActivity(context, diagnoseResult, ChargeRequest.PAY_TYPE_WX);
                sheetDialog.dismiss();
            }
        });

        layout.findViewById(R.id.dialog_choice_pay_alpay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityRouter.openChargeActivity(context, diagnoseResult, ChargeRequest.PAY_TYPE_ALPAY);
                sheetDialog.dismiss();
            }
        });

        layout.findViewById(R.id.dialog_choice_pay_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetDialog.dismiss();
            }
        });

        sheetDialog.show();
    }

    private static <T> void showBottomSheetDialog(Context context, List<T> datas, final BottomSheetListLayout.OnItemListener onItemListener) {
        BottomSheetListLayout<T> layout = new BottomSheetListLayout(context);
        layout.setDatas(datas);
        layout.setOnItemListener(onItemListener);
        BottomSheetDialog sheetDialog = new BottomSheetDialog(context);
        sheetDialog.setContentView(layout);
        sheetDialog.setCanceledOnTouchOutside(true);
        layout.setBottomSheetDialog(sheetDialog);
        sheetDialog.show();
    }

    public static void showFriedListDialog(Context context, List<Fried> datas, final BottomSheetListLayout.OnItemListener<Fried> onItemListener) {
        showBottomSheetDialog(context, datas, new BottomSheetListLayout.OnItemListener<Fried>() {
            @Override
            public void onItemClick(Fried data) {
                if (onItemListener != null) {
                    onItemListener.onItemClick(data);
                }
            }

            @Override
            public String getItemContent(Fried data) {
                return data.getName();
            }
        });
    }

    public static void showRoleListDialog(Context context, List<Role> datas, final BottomSheetListLayout.OnItemListener<Role> onItemListener) {
        showBottomSheetDialog(context, datas, new BottomSheetListLayout.OnItemListener<Role>() {
            @Override
            public void onItemClick(Role data) {
                if (onItemListener != null) {
                    onItemListener.onItemClick(data);
                }
            }

            @Override
            public String getItemContent(Role data) {
                return data.getName();
            }
        });
    }

    public static void showDepartmentListDialog(Context context, List<Department> datas, final BottomSheetListLayout.OnItemListener<Department> onItemListener) {
        showBottomSheetDialog(context, datas, new BottomSheetListLayout.OnItemListener<Department>() {
            @Override
            public void onItemClick(Department data) {
                if (onItemListener != null) {
                    onItemListener.onItemClick(data);
                }
            }

            @Override
            public String getItemContent(Department data) {
                return data.getName();
            }
        });
    }

    public static void showZhiChengListDialog(Context context, List<ZhiCheng> datas, final BottomSheetListLayout.OnItemListener<ZhiCheng> onItemListener) {
        showBottomSheetDialog(context, datas, new BottomSheetListLayout.OnItemListener<ZhiCheng>() {
            @Override
            public void onItemClick(ZhiCheng data) {
                if (onItemListener != null) {
                    onItemListener.onItemClick(data);
                }
            }

            @Override
            public String getItemContent(ZhiCheng data) {
                return data.getName();
            }
        });
    }

    public static void showFeeCategoryListDialog(Context context, List<FeeCategory> datas, final BottomSheetListLayout.OnItemListener<FeeCategory> onItemListener) {
        showBottomSheetDialog(context, datas, new BottomSheetListLayout.OnItemListener<FeeCategory>() {
            @Override
            public void onItemClick(FeeCategory data) {
                if (onItemListener != null) {
                    onItemListener.onItemClick(data);
                }
            }

            @Override
            public String getItemContent(FeeCategory data) {
                return data.getName();
            }
        });
    }

    public static void showUnitListDialog(Context context, List<Unit> datas, final BottomSheetListLayout.OnItemListener<Unit> onItemListener) {
        showBottomSheetDialog(context, datas, new BottomSheetListLayout.OnItemListener<Unit>() {
            @Override
            public void onItemClick(Unit data) {
                if (onItemListener != null) {
                    onItemListener.onItemClick(data);
                }
            }

            @Override
            public String getItemContent(Unit data) {
                return data.getName();
            }
        });
    }

    public static void showKuFangListDialog(Context context, List<KuFang> datas, final BottomSheetListLayout.OnItemListener<KuFang> onItemListener) {
        showBottomSheetDialog(context, datas, new BottomSheetListLayout.OnItemListener<KuFang>() {
            @Override
            public void onItemClick(KuFang data) {
                if (onItemListener != null) {
                    onItemListener.onItemClick(data);
                }
            }

            @Override
            public String getItemContent(KuFang data) {
                return data.getName();
            }
        });
    }

    public static void showProviderListDialog(Context context, List<Provider> datas, final BottomSheetListLayout.OnItemListener<Provider> onItemListener) {
        showBottomSheetDialog(context, datas, new BottomSheetListLayout.OnItemListener<Provider>() {
            @Override
            public void onItemClick(Provider data) {
                if (onItemListener != null) {
                    onItemListener.onItemClick(data);
                }
            }

            @Override
            public String getItemContent(Provider data) {
                return data.getName();
            }
        });
    }

    public static void showBusinessListDialog(Context context, List<Business> datas, final BottomSheetListLayout.OnItemListener<Business> onItemListener) {
        showBottomSheetDialog(context, datas, new BottomSheetListLayout.OnItemListener<Business>() {
            @Override
            public void onItemClick(Business data) {
                if (onItemListener != null) {
                    onItemListener.onItemClick(data);
                }
            }

            @Override
            public String getItemContent(Business data) {
                return data.getName();
            }
        });
    }

    public static void showRemarkListDialog(Context context, List<RegistrationRemark> datas, final BottomSheetListLayout.OnItemListener<RegistrationRemark> onItemListener) {
        showBottomSheetDialog(context, datas, new BottomSheetListLayout.OnItemListener<RegistrationRemark>() {
            @Override
            public void onItemClick(RegistrationRemark data) {
                if (onItemListener != null) {
                    onItemListener.onItemClick(data);
                }
            }

            @Override
            public String getItemContent(RegistrationRemark data) {
                return data.getName();
            }
        });
    }

    public static <T extends ClsEntity> void showClsListDialog(Context context, List<T> datas, final BottomSheetListLayout.OnItemListener<T> onItemListener) {
        showBottomSheetDialog(context, datas, new BottomSheetListLayout.OnItemListener<T>() {
            @Override
            public void onItemClick(T data) {
                if (onItemListener != null) {
                    onItemListener.onItemClick(data);
                }
            }

            @Override
            public String getItemContent(T data) {
                return data.getName();
            }
        });
    }
}
