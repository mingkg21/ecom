package com.ecome.packet.ui.fragment;

import android.view.View;

import com.ecome.packet.R;
import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Menu;
import com.ecome.packet.model.MenuModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.MenuAdapter;
import com.ecome.packet.ui.dialog.ConfirmDialog;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;

import java.util.List;

public class MenuFragment extends AppRecyclerFragment<Menu> {

    private MenuModel mMenuModel;

    private Menu mMenu;
    protected int position;

    @Override
    protected BaseAdapter<Menu> getAdapter() {
        return new MenuAdapter().setSecond(true);
    }

    @Override
    protected PagingLoadModel getPagingLoadModel() {
        return null;
    }

    @Override
    protected void initData() {
        super.initData();
        mMenu = (Menu) getArguments().get(ActivityRouter.EXTRA_NAME_MENU);
        position = getArguments().getInt(ActivityRouter.EXTRA_NAME_POSITION);
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTopDividerHeight(getResources().getDimensionPixelOffset(R.dimen.common_list_top_height));

        if (mMenu != null) {
            setTitle(mMenu.getName());
        }

        mMenuModel = new MenuModel();
        mMenuModel.addCallback(this);

        mRecyclerView.setVisibility(View.VISIBLE);

        setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Object data) {

                Menu menu = (Menu) data;
                if ("canshuguanli".equals(menu.getNameId())) {//1.1 参数管理
                    ActivityRouter.openConfigManageActivity(getContext(), menu.getName());
                } else if ("baipanguanli".equals(menu.getNameId())) {//1.2 排班管理
                    ActivityRouter.openDoctorManageActivity(getContext(), menu.getName());
                } else if ("renyuanguanli".equals(menu.getNameId())) {//1.3 人员管理
                    ActivityRouter.openStaffManageActivity(getContext(), menu.getName());
                } else if ("shoufeixiangmu".equals(menu.getNameId())) {//1.4 收费项目
                    ActivityRouter.openFeeManageActivity(getContext(), menu.getName(), null);
                } else if ("zidianguanli".equals(menu.getNameId())) {//1.5 字典管理
                    ActivityRouter.openZiDianActivity(getContext(), menu.getName());
                } else if ("rizhichaxun".equals(menu.getNameId())) {//1.6 日志查询
                    ActivityRouter.openLogActivity(getContext(), menu.getName());
                } else if ("anquantuichu".equals(menu.getNameId())) {//1.9 安全退出
                    new ConfirmDialog(getContext()).setContent("安全退出？").setSureListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            UserManager.getInstance().logout();
                            finish();
                        }
                    }).show();
                } else if ("mimaxiugai".equals(menu.getNameId())) {//1.8 密码修改
                    ActivityRouter.openModifyPasswordActivity(getContext(), menu.getName());
                } else if ("wanshanmenzhenbuxinxi".equals(menu.getNameId())) {//1.7 完善门诊部信息
                    ActivityRouter.openCompleteInfoActivity(getContext(), null);
                }
                else if ("yuyueguanli".equals(menu.getNameId())) {//2.2 预约管理
                    int id = UserManager.getInstance().getUserInfo().getFendDianId();
                    ActivityRouter.openWebViewActivity(getContext(), "http://ecom214935344.cn13.mydns114.net/Fendian.aspx?fdid=" + id);
                }

                else if ("chufangluru".equals(menu.getNameId())) {//3.1 处方录入
                    ActivityRouter.openPrescribeActivity(getContext());
                } else if ("wodebingren".equals(menu.getNameId())) {//3.2我的病人
                    ActivityRouter.openMyPatientActivity(getContext());
                } else if ("wodegongzuoliang".equals(menu.getNameId())) {//3.3我的工作量
                    ActivityRouter.openWorkloadActivity(getContext(), menu.getName());
                } else if ("wodemuban".equals(menu.getNameId())) {//3.4我的模板
                    ActivityRouter.openPrescriptionTempListActivity(getContext(), false);
                }

                else if ("shoufei".equals(menu.getNameId())) {//4.1收费
                    ActivityRouter.openWorkloadActivity(getContext(), menu.getName());
                }

                else if ("yaopinruku".equals(menu.getNameId())) {//5.1 药品入库
                    ActivityRouter.openMedicineVoucherActivity(getContext(), menu.getName());
                } else if ("yaopinkucunchaxun".equals(menu.getNameId())) {//5.2 药品库存查询
                    ActivityRouter.openStockQueryActivity(getContext(), menu.getName());
                }

                else if ("shoufeichaxun".equals(menu.getNameId())) {//8.1收费查询
                    ActivityRouter.openReportChargeDetailActivity(getContext(), menu.getName());
                } else if ("feiyongfenleitongji".equals(menu.getNameId())) {//8.2费用分类统计
                    ActivityRouter.openReportFeeActivity(getContext(), menu.getName());
                } else if ("yishengyeji".equals(menu.getNameId())) {//8.3医生业绩
                    ActivityRouter.openReportRiBaoActivity(getContext(), menu.getName());
                } else if ("menzhenrizhi".equals(menu.getNameId())) {//8.3医生业绩
                    ActivityRouter.openReportRiZhiActivity(getContext(), menu.getName());
                }

                else if ("yijianfankui".equals(menu.getNameId())) {//9.2 意见反馈
                    ActivityRouter.openFeedbackFragment(getContext(), menu.getName());
                }
            }
        });

        loadData();
    }

    @Override
    protected void loadData() {
        super.loadData();

        int id = 1;
        if (mMenu != null) {
            id = mMenu.getId();
        }
        mMenuModel.getMenu(2, id);
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mMenuModel.isGetMenuKey(key)) {
            showLoadingView();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mMenuModel.isGetMenuKey(key)) {
            hideLoadingView();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public void onUserLoginStateChange(boolean isLogin) {
        super.onUserLoginStateChange(isLogin);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mMenuModel.isGetMenuKey(key)) {
            AppResponseEntity<List<Menu>> menus = (AppResponseEntity<List<Menu>>) data;
            setAll(menus.getData());
            return true;
        }
        return super.onSuccess(key, data);
    }
}
