package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.WorkTimeItem;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class PianbanAdapter extends BaseAdapter<WorkTimeItem> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_common_info;
    }

    @Override
    public BaseViewHolder<WorkTimeItem> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView);
    }

    private static class Holder extends BaseViewHolder<WorkTimeItem> {

        private TextView mNameTV;
        private TextView mContentTV;

        public Holder(View itemView) {
            super(itemView);
            mNameTV = findViewById(R.id.name_tv);
            mContentTV = findViewById(R.id.content_tv);

        }

        @Override
        public void onBind(WorkTimeItem data, int position) {
            mNameTV.setText(data.getDate());

            StringBuilder sb = new StringBuilder();
            sb.append("是否上班：");
            sb.append(data.isShangban() ? "是" : "否");
            sb.append("     ");
            sb.append("是否请假：");
            sb.append(data.isQingjia() ? "是" : "否");

            mContentTV.setText(sb);
        }
    }
}
