package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Drug;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class FeeAdapter extends BaseAdapter<Drug> {

    private OnModifyClickListener mOnModifyClickListener;

    public FeeAdapter(OnModifyClickListener onModifyClickListener) {
        mOnModifyClickListener = onModifyClickListener;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_config;
    }

    @Override
    public BaseViewHolder<Drug> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView, mOnModifyClickListener);
    }

    private static class Holder extends BaseViewHolder<Drug> {

        private TextView mNameTV;
        private TextView mValueTV;
        private Button mModifyBtn;

        private OnModifyClickListener mOnModifyClickListener;

        public Holder(View itemView, OnModifyClickListener onModifyClickListener) {
            super(itemView);

            mOnModifyClickListener = onModifyClickListener;

            mNameTV = itemView.findViewById(R.id.name_tv);
            mValueTV = itemView.findViewById(R.id.value_tv);
            mModifyBtn = itemView.findViewById(R.id.modify_btn);

            mModifyBtn.setVisibility((mOnModifyClickListener == null) ? View.GONE : View.VISIBLE);
        }

        @Override
        public void onBind(final Drug data, int position) {
            mNameTV.setText(data.getName());
            StringBuilder sb = new StringBuilder();
            sb.append("类别：");
            sb.append(data.getFeeClsName());
            sb.append("\n");
            sb.append("规格：");
            sb.append(data.getGuige());
            sb.append("\n");
            sb.append(String.format("单价：%s元", data.getPrice()));
            sb.append("\n");
            sb.append("库存数量：");
            sb.append(data.getStock() + data.getUnit());
            mValueTV.setText(sb);
            mModifyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnModifyClickListener != null) {
                        mOnModifyClickListener.onClick(data);
                    }
                }
            });
        }
    }

    public interface OnModifyClickListener {
        void onClick(Drug config);
    }

}
