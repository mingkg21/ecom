package com.ecome.packet.ui.fragment;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.ecome.packet.R;
import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.Patient;
import com.ecome.packet.entity.Registration;
import com.ecome.packet.model.DoctorWorkspaceModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.dialog.ConfirmDialog;
import com.mk.core.util.ToastUtil;

public class AddPatientFragment extends AppFragment {

    private EditText mNameET;
    private EditText mTelET;
    private EditText mAgeET;
    private EditText mIdET;
    private EditText mAddressET;
    private RadioGroup mSexRG;

    private DoctorWorkspaceModel mDoctorWorkspaceModel;

    private Patient mPatient;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_add_patient;
    }

    @Override
    protected void initView(final View view) {
        super.initView(view);
        setTitle("新增病人");

        mNameET = view.findViewById(R.id.name_et);
        mTelET = view.findViewById(R.id.tel_et);
        mAgeET = view.findViewById(R.id.age_et);
        mIdET = view.findViewById(R.id.id_et);
        mAddressET = view.findViewById(R.id.address_et);
        mSexRG = view.findViewById(R.id.sex_rg);

        findViewById(R.id.submit_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String name = mNameET.getText().toString().trim();
                final String tel = mTelET.getText().toString().trim();
                final String age = mAgeET.getText().toString().trim();
                final String id = mIdET.getText().toString().trim();
                final String address = mAddressET.getText().toString().trim();

                final String sex = ((RadioButton) view.findViewById(mSexRG.getCheckedRadioButtonId())).getText().toString();

                if (TextUtils.isEmpty(name)) {
                    ToastUtil.showToast("请输入姓名！");
                    return;
                }
                if (TextUtils.isEmpty(tel)) {
                    ToastUtil.showToast("请输入手机号！");
                    return;
                }
                if (TextUtils.isEmpty(age)) {
                    ToastUtil.showToast("请输入年龄！");
                    return;
                }
                if (TextUtils.isEmpty(address)) {
                    ToastUtil.showToast("请输入输入收药地址！");
                    return;
                }

                mDoctorWorkspaceModel.addPatient(name, tel, sex, age, id, address);

            }
        });

        mDoctorWorkspaceModel = new DoctorWorkspaceModel();
        mDoctorWorkspaceModel.addCallback(this);
    }

    @Override
    protected void loadData() {

    }

    @Override
    public boolean onPreLoad(String key) {
        if (mDoctorWorkspaceModel.isAddPatientKey(key)
                || mDoctorWorkspaceModel.isRegistrationKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mDoctorWorkspaceModel.isAddPatientKey(key)
                || mDoctorWorkspaceModel.isRegistrationKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mDoctorWorkspaceModel.isAddPatientKey(key)
                || mDoctorWorkspaceModel.isRegistrationKey(key)) {
            hideLoadingDialog();
            AppResponseEntity responseEntity = (AppResponseEntity) data;
            ToastUtil.showToast(responseEntity.getMsg());
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mDoctorWorkspaceModel.isAddPatientKey(key)) {
            final AppResponseEntity<Patient> appResponseEntity = (AppResponseEntity<Patient>) data;
            if (appResponseEntity != null) {
                mPatient = appResponseEntity.getData();
                if (mPatient != null && mPatient.isSuccess()) {
                    new ConfirmDialog(getContext()).setContent("是否挂号").setSureListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDoctorWorkspaceModel.registration(UserManager.getInstance().getUserId(), mPatient.getId());
                        }
                    }).setCancelListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    }).show();
                    return true;
                }
            }
            ToastUtil.showToast("请求失败，请稍后重试！");
            return true;
        } else if (mDoctorWorkspaceModel.isRegistrationKey(key)) {
            AppResponseEntity<Registration> appResponseEntity = (AppResponseEntity<Registration>) data;
            if (appResponseEntity != null) {
                ActivityRouter.openRegistrationActivity(getContext(), mPatient, appResponseEntity.getData());
                finish();
            }
            return true;
        }
        return super.onSuccess(key, data);
    }
}
