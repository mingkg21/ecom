package com.ecome.packet.ui.fragment;

import android.view.View;

import com.ecome.packet.entity.Workload;
import com.ecome.packet.model.ReportModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.adapter.WorkloadAdapter;
import com.mk.core.ui.widget.BaseAdapter;

public class ReportChargeDetailFragment extends AppDateSearchFragment<Workload> {

    private ReportModel mReportModel;

    @Override
    protected void initView(View view) {
        super.initView(view);

        setTitle(getArguments().getString(ActivityRouter.EXTRA_NAME_TITLE));

        setOnItemClickListener(new BaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Object data) {
                Workload workload = (Workload) data;
                ActivityRouter.openPrescriptionActivity(getContext(), workload.getRegistrationId(), workload.getPatientName());
            }
        });
    }

    @Override
    protected void initLoadModel() {
        mReportModel = new ReportModel();
        mReportModel.addCallback(this);
    }

    @Override
    protected boolean isLoadData(String key) {
        return mReportModel.isReportChargeDetailKey(key);
    }

    @Override
    protected BaseAdapter<Workload> getAdapter() {
        return new WorkloadAdapter(null);
    }

    @Override
    protected void loadData(String startDateTime, String endDateTime) {
        mReportModel.reportChargeDetail(startDateTime, endDateTime);
    }

}
