package com.ecome.packet.ui.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Drug;
import com.ecome.packet.entity.Fried;
import com.ecome.packet.entity.PinCi;
import com.ecome.packet.entity.Use;
import com.ecome.packet.ui.dialog.DrugDetailDialog;
import com.mk.core.util.ToastUtil;

import java.util.ArrayList;

public class PrescriptionInputLayout extends LinearLayout {

    private TextView mNameET;
    private EditText mNumberET;
    private TextView mUserET;
    private TextView mDetailTV;

    private Fried mFried;
    private Drug mDrug;
    private Use mUse;
    private PinCi mPinCi;
    private String mGuige;
    private String mYongliang;
    private String mUnit;
    private int mZibei;

    private ArrayList<Fried> mFrieds = new ArrayList<>();
    private ArrayList<Use> mUses = new ArrayList<>();
    private ArrayList<PinCi> mPinCis = new ArrayList<>();

    private OnPrescriptionInputLayoutListener mOnPrescriptionInputLayoutListener;

    public PrescriptionInputLayout(Context context) {
        super(context);
        init();
    }

    public PrescriptionInputLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PrescriptionInputLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_prescription_input, this);

        mNameET = findViewById(R.id.name_et);
        mNumberET = findViewById(R.id.number_et);
        mUserET = findViewById(R.id.use_et);
        mDetailTV = findViewById(R.id.detail_et);
        mDetailTV.setVisibility(View.GONE);

        findViewById(R.id.delete_iv).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewGroup)getParent()).removeView(PrescriptionInputLayout.this);
            }
        });

        mUserET.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnPrescriptionInputLayoutListener != null) {
                    mOnPrescriptionInputLayoutListener.onFriedClick();
                }
            }
        });
        mNameET.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnPrescriptionInputLayoutListener != null) {
                    mOnPrescriptionInputLayoutListener.onNameClick();
                }
            }
        });
        mDetailTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDrug != null) {
                    new DrugDetailDialog(getContext(), mDrug, mGuige, mYongliang, mUnit, mUse, mFried,
                            mPinCi, mZibei, mFrieds, mUses, mPinCis, new DrugDetailDialog.OnDrugDetailListener() {
                        @Override
                        public void onClick(String guige, String yongliang, String unit, Use use, Fried fried, PinCi pinCi, int zibei) {
                            mUse = use;
                            mPinCi = pinCi;
                            mGuige = guige;
                            mYongliang = yongliang;
                            mUnit = unit;
                            mZibei = zibei;
                            setFried(fried);
                        }
                    }).show();
                }
            }
        });

        mNumberET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mOnPrescriptionInputLayoutListener != null) {
                    mOnPrescriptionInputLayoutListener.onNumChanged();
                }
            }
        });
    }

    public void setFrieds(ArrayList<Fried> frieds) {
        mFrieds = frieds;
    }

    public void setUses(ArrayList<Use> uses) {
        mUses = uses;
    }

    public void setPinCis(ArrayList<PinCi> pinCis) {
        mPinCis = pinCis;
    }

    public void onClickName() {
        if (mNameET != null) {
            mNameET.performClick();
        }
    }

    public String getData() {
        if (mDrug == null) {
            ToastUtil.showToast("请选择药品");
            return null;
        }
        String number = mNumberET.getText().toString().trim();
        if (TextUtils.isEmpty(number)) {
            ToastUtil.showToast("请填写数量");
            return null;
        }

        String friedId = "0";
        if (mFried != null) {
            friedId = mFried.getId() + "";
        }
        String useId = "0";
        if (mUse != null) {
            useId = mUse.getId() + "";
        }
        String pinciId = "0";
        if (mPinCi != null) {
            pinciId = mPinCi.getId() + "";
        }

        StringBuilder sb = new StringBuilder();
        sb.append(mDrug.getId());
        sb.append(',');
        sb.append(mDrug.getName());
        sb.append(",");
        sb.append(number);
        sb.append(",");
        sb.append(useId);
        sb.append(",");
        sb.append(friedId);
        sb.append(",");
        sb.append(mZibei);
        sb.append(",");
        sb.append(pinciId);
        sb.append(",");
        sb.append(TextUtils.isEmpty(mUnit) ? "0" : mUnit);
        sb.append(",0");
        return sb.toString();
    }

    public boolean hasData() {
        if (mDrug == null) {
            return false;
        }
        return true;
    }

    public void setNum(int num) {
        mNumberET.setText(num + "");
    }

    public void setDrug(Drug drug) {
        mDrug = drug;
        mNameET.setText(drug.getName());
        mNumberET.requestFocus();
        mDetailTV.setVisibility(View.VISIBLE);
    }

    public Drug getDrug() {
        return mDrug;
    }

    public void setFried(Fried fried) {
        mFried = fried;
        mUserET.setText(fried.getName());
    }

    public int getNum() {
        try {
            return Integer.valueOf(mNumberET.getText().toString().trim());
        } catch (Exception e) {

        }
        return 0;
    }

    public Fried getFried() {
        return mFried;
    }

    public void setOnPrescriptionInputLayoutListener(OnPrescriptionInputLayoutListener onPrescriptionInputLayoutListener) {
        mOnPrescriptionInputLayoutListener = onPrescriptionInputLayoutListener;
    }

    public interface OnPrescriptionInputLayoutListener {
        void onFriedClick();
        void onNameClick();
        void onNumChanged();
    }

}
