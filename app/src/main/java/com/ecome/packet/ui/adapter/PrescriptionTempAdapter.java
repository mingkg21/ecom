package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Temp;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class PrescriptionTempAdapter extends BaseAdapter<Temp> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_common_info;
    }

    @Override
    public BaseViewHolder<Temp> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView);
    }

    private static class Holder extends BaseViewHolder<Temp> {

        private TextView mNameTV;
        private TextView mContentTV;

        public Holder(View itemView) {
            super(itemView);
            mNameTV = findViewById(R.id.name_tv);
            mContentTV = findViewById(R.id.content_tv);

        }

        @Override
        public void onBind(Temp data, int position) {
            mNameTV.setText(data.getName());

            StringBuilder sb = new StringBuilder();
            sb.append("主治：");
            sb.append(data.getZhuzhi());
            sb.append("\n");
            sb.append("功效：");
            sb.append(data.getGongxiao());
            sb.append("\n");
            sb.append("创建时间：");
            sb.append(data.getCreateDatetime());

            mContentTV.setText(sb);
        }
    }
}
