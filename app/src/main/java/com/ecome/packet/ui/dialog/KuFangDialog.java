package com.ecome.packet.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.KuFang;
import com.mk.core.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class KuFangDialog extends Dialog {

    private ListView mListView;
    private Adapter mAdapter;

    private OnDataListener mOnDataListener;

    public KuFangDialog(@NonNull Context context, List<KuFang> datas, OnDataListener onDataListener) {
        super(context);

        setContentView(R.layout.dialog_kufang);

        mListView = findViewById(R.id.dialog_list);

        mOnDataListener = onDataListener;

        mAdapter = new Adapter(getContext());
        mAdapter.setAll(datas);
        mListView.setAdapter(mAdapter);

        findViewById(R.id.dialog_sure).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDataListener != null) {
                    int position = mListView.getCheckedItemPosition();
                    if (position >= 0) {
                        mOnDataListener.onSelected(mAdapter.getItem(position));
                    } else {
                        ToastUtil.showToast("请选择类别");
                        return;
                    }
                }
                dismiss();
            }
        });

        findViewById(R.id.dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnDataListener != null) {
                    mOnDataListener.onCancel();
                }
                dismiss();
            }
        });

        setCanceledOnTouchOutside(false);
    }


    public interface OnDataListener {
        void onSelected(KuFang data);
        void onCancel();
    }

    private static class Adapter extends BaseAdapter {

        private ArrayList<KuFang> mList;
        private Context mContext;

        public Adapter(Context context) {
            mContext = context;
            mList = new ArrayList<>();
        }

        public void setAll(List<KuFang> datas) {
            mList.clear();
            mList.addAll(datas);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public KuFang getItem(int position) {
            return mList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_kufang, null);
            }

            TextView tv = (TextView) convertView;

            KuFang data = getItem(position);
            tv.setText(data.getName());

            return convertView;
        }

    }
}
