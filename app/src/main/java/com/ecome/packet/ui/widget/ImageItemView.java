package com.ecome.packet.ui.widget;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ecome.packet.R;
import com.ecome.packet.app.App;
import com.mk.core.util.ImageUtil;

import java.util.List;

import static android.app.Activity.RESULT_OK;

public class ImageItemView extends RelativeLayout {

    private int REQUEST_CODE_SELECT_IMG = 2000;

    private View mDeleteView;
    private ImageView mImageView;
    private String mImagePath;

    public OnImageItemListener mOnImageItemListener;

    public ImageItemView(Context context) {
        super(context);
        init();
    }

    public ImageItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {

        LayoutInflater.from(getContext()).inflate(R.layout.item_image, this);

        mImageView = findViewById(R.id.item_image_image);
        mDeleteView = findViewById(R.id.item_image_delete);

        mDeleteView.setVisibility(View.GONE);

        mDeleteView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnImageItemListener != null) {
                    mOnImageItemListener.onDelete(ImageItemView.this);
                }
            }
        });

        mImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnImageItemListener != null) {
                    mOnImageItemListener.onChoiceImage();
                }
            }
        });
    }

//    @Override
//    protected void onAttachedToWindow() {
//        super.onAttachedToWindow();
//        if (getParent() != null) {
//            REQUEST_CODE_SELECT_IMG += ((ViewGroup) getParent()).getChildCount();
//        }
//    }

    public void setCode(int code) {
        REQUEST_CODE_SELECT_IMG += code;
    }

    public String getImagePath() {
        return mImagePath;
    }

    public boolean hasImage() {
        return mImagePath != null;
    }

    public void reset() {
        mImagePath = null;
        mImageView.setImageResource(R.drawable.ic_add_img);
    }

    public void setImage(String path) {
        mImagePath = path;
        ImageUtil.loadImage(path, mImageView);
        mDeleteView.setVisibility(View.VISIBLE);
    }

    public void choiceImage(Object object) {
        App.openPhotoListActivity(getContext(), REQUEST_CODE_SELECT_IMG);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_SELECT_IMG) {
                final List<String> pathList = data.getStringArrayListExtra("result");
                if (!pathList.isEmpty()) {
                    String path = pathList.get(0);
                    setImage(path);
                    if (mOnImageItemListener != null) {
                        mOnImageItemListener.onChoiceImage(path);
                    }
                }
            }
        }
    }

    public void setOnImageItemListener(OnImageItemListener onImageItemListener) {
        mOnImageItemListener = onImageItemListener;
    }

    public interface OnImageItemListener {
        void onDelete(ImageItemView view);
        void onChoiceImage(String path);
        void onChoiceImage();
    }
}
