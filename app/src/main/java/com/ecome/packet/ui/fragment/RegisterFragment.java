package com.ecome.packet.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.app.App;
import com.ecome.packet.db.SystemInfoPreferences;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.entity.SystemInfo;
import com.ecome.packet.entity.UserInfo;
import com.ecome.packet.model.UserModel;
import com.ecome.packet.ui.activity.ActivityRouter;
import com.ecome.packet.ui.widget.SecurityCodeText;
import com.ecome.packet.util.Constants;
import com.ecome.packet.util.QiNiuUtils;
import com.mk.core.util.ImageUtil;
import com.mk.core.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by mingkg21 on 2017/10/10.
 */

public class RegisterFragment extends AppFragment implements View.OnClickListener {

    private static final int REQUEST_CODE_CHOICE = 1000;
    private static final int REQUEST_CODE_SELECT_IMG = 3000;

    private UserModel mUserModel;

    private TextView mSystemInfoTV;
    private EditText mNameET;
    private EditText mAccountET;
    private EditText mPhoneET;
    private EditText mIntroducerIdET;
    private EditText mCodeET;
    private SecurityCodeText mCodeSCT;

    private ImageView mImageView;

    private Button mActionBtn;

    private String mImagePath0;

    private SystemInfo mSystemInfo;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_register;
    }

    @Override
    protected void initData() {
        super.initData();

    }

    @Override
    protected void initView(View view) {
        super.initView(view);
        setTitle(R.string.title_register);

        mSystemInfoTV = view.findViewById(R.id.service_info_tv);

        mNameET = view.findViewById(R.id.name_et);
        mAccountET = view.findViewById(R.id.account_et);
        mPhoneET = view.findViewById(R.id.phone_et);
        mIntroducerIdET = view.findViewById(R.id.introducer_id_et);
        mCodeET = view.findViewById(R.id.code_et);
        mCodeSCT = view.findViewById(R.id.code_sct);
        mCodeSCT.setPhoneEt(mPhoneET);

        mActionBtn = view.findViewById(R.id.register_btn);

        mActionBtn.setOnClickListener(this);

        mImageView = view.findViewById(R.id.add_image_iv);
        mImageView.setOnClickListener(this);

        mActionBtn.setText(R.string.btn_register);

        mSystemInfoTV.setOnClickListener(this);

        mUserModel = new UserModel();
        mUserModel.addCallback(this);

    }

    @Override
    protected void loadData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_btn:
                final String name = mNameET.getText().toString().trim();
                final String account = mAccountET.getText().toString().trim();
                final String phone = mPhoneET.getText().toString().trim();
                final String code = mCodeET.getText().toString().trim();
                String tempIntroducerId = mIntroducerIdET.getText().toString().trim();
                if (TextUtils.isEmpty(name)) {
                    ToastUtil.showToast(R.string.register_input_name_tip_is_null);
                    return;
                }
                if (TextUtils.isEmpty(account)) {
                    ToastUtil.showToast(R.string.register_input_account_tip_is_null);
                    return;
                }
                if (TextUtils.isEmpty(phone)) {
                    ToastUtil.showToast(R.string.register_input_phone_tip_is_null);
                    return;
                }
                if (TextUtils.isEmpty(code)) {
                    ToastUtil.showToast("验证码不能为空！");
                    return;
                }
                if (!code.equals(mCodeSCT.getCode())) {
                    ToastUtil.showToast("请输入正确的验证码！");
                    return;
                }
                if (TextUtils.isEmpty(tempIntroducerId)) {
                    tempIntroducerId = "0";
                }

                final String introducerId = tempIntroducerId;

                //先上传图片
                if (!TextUtils.isEmpty(mImagePath0) && !mImagePath0.startsWith("http://")) {
                    showLoadingDialog();
                    ArrayList<String> imgPaths = new ArrayList<>();
                    imgPaths.add(mImagePath0);
                    QiNiuUtils.putImgs(imgPaths, new QiNiuUtils.QiNiuCallback() {
                        @Override
                        public void onSuccess(List<String> picUrls) {
                            mUserModel.register(name, account, phone, introducerId, Constants.getQiniuImageUrl(picUrls.get(0)));
                        }

                        @Override
                        public void onError(String msg) {
                            hideLoadingDialog();
                            ToastUtil.showToast("上传图片失败，请稍后重试！");
                        }
                    });
                } else {
                    if (TextUtils.isEmpty(mImagePath0)) {
                        mImagePath0 = "";
                    }
                    mUserModel.register(name, account, phone, introducerId, mImagePath0);
                }
                break;
            case R.id.add_image_iv:
                choiceImage();
                break;
            case R.id.service_info_tv:
                ActivityRouter.openSystemInfoActivity(getContext(), REQUEST_CODE_CHOICE);
                break;
        }
    }

    private void choiceImage() {
        App.openPhotoListActivity(getContext(), REQUEST_CODE_SELECT_IMG);
    }

    @Override
    public boolean onPreLoad(String key) {
        if (mUserModel.isRegisterKey(key)
                || mUserModel.isCompleteInfoKey(key)) {
            showLoadingDialog();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (mUserModel.isRegisterKey(key)
                || mUserModel.isCompleteInfoKey(key)) {
            hideLoadingDialog();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (mUserModel.isRegisterKey(key)) {
            AppResponseEntity<UserInfo> responseEntity = (AppResponseEntity<UserInfo>) data;
            if (responseEntity != null) {
                UserInfo userInfo = responseEntity.getData();
                if (userInfo != null) {
                    ToastUtil.showToast(responseEntity.getMsg());
                    if (userInfo.isSuccess()) {
                        ActivityRouter.openCompleteInfoActivity(getContext(), userInfo);
                        finish();
                    }
                    return true;
                }
            }
            ToastUtil.showToast("请求失败，请稍后重试！");
            return true;
        }
        return super.onSuccess(key, data);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (mUserModel.isRegisterKey(key) || mUserModel.isCompleteInfoKey(key)) {
            AppResponseEntity responseEntity = (AppResponseEntity) data;
            ToastUtil.showToast(responseEntity.getMsg());
            return true;
        }
        return super.onFail(key, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_SELECT_IMG) {
                final List<String> pathList = data.getStringArrayListExtra("result");
                if (!pathList.isEmpty()) {

                    String path = pathList.get(0);
                    ImageView imageView = mImageView;
                    mImagePath0 = path;
                    if (imageView != null) {
                        ImageUtil.loadImage(path, imageView);
                    }
                }
            } else if (requestCode == REQUEST_CODE_CHOICE) {
                if (resultCode == Activity.RESULT_OK) {
                    mSystemInfo = (SystemInfo) data.getSerializableExtra(ActivityRouter.EXTRA_NAME_RESULT);
                    mSystemInfoTV.setText(mSystemInfo.getName());
                    SystemInfoPreferences.getInstance().choiceSystemInfo(mSystemInfo.getId());
                }
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
