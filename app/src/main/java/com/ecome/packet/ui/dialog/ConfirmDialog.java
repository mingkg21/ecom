package com.ecome.packet.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class ConfirmDialog extends Dialog {

    private TextView mTitleTV;
    private TextView mContentTV;

    private View.OnClickListener mSureListener;
    private View.OnClickListener mCancelListener;

    public ConfirmDialog(@NonNull Context context) {
        super(context);

        setContentView(R.layout.dialog_confirm);

        mTitleTV = (TextView) findViewById(R.id.dialog_confirm_title);
        mContentTV = (TextView) findViewById(R.id.dialog_confirm_content);

        mTitleTV.setVisibility(View.GONE);
        mContentTV.setVisibility(View.GONE);

        findViewById(R.id.dialog_confirm_sure).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSureListener != null) {
                    mSureListener.onClick(v);
                }
                dismiss();
            }
        });

        findViewById(R.id.dialog_confirm_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCancelListener != null) {
                    mCancelListener.onClick(v);
                }
                dismiss();
            }
        });
    }

    public ConfirmDialog setTitle(String title) {
        mTitleTV.setText(title);
        mTitleTV.setVisibility(TextUtils.isEmpty(title) ? View.GONE : View.VISIBLE);
        return this;
    }

    public ConfirmDialog setContent(String content) {
        mContentTV.setText(content);
        mContentTV.setVisibility(TextUtils.isEmpty(content) ? View.GONE : View.VISIBLE);
        return this;
    }

    public ConfirmDialog setCancelListener(View.OnClickListener cancelListener) {
        mCancelListener = cancelListener;
        return this;
    }

    public ConfirmDialog setSureListener(View.OnClickListener sureListener) {
        mSureListener = sureListener;
        return this;
    }
}
