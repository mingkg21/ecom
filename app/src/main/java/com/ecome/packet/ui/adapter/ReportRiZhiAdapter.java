package com.ecome.packet.ui.adapter;

import android.view.View;
import android.widget.TextView;

import com.ecome.packet.R;
import com.ecome.packet.entity.Patient;
import com.ecome.packet.entity.Registration;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.BaseViewHolder;

public class ReportRiZhiAdapter extends BaseAdapter<Registration> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.item_report_rizhi;
    }

    @Override
    public BaseViewHolder<Registration> getBaseViewHolder(View itemView, int viewType) {
        return new Holder(itemView);
    }

    private static class Holder extends BaseViewHolder<Registration> {

        private TextView mTitleTV;
        private TextView mDoctorInfoTV;
        private TextView mZhenDuanInfoTV;

        private MyPatientViewHolder mPatientViewHolder;

        public Holder(View itemView) {
            super(itemView);
            mTitleTV = findViewById(R.id.item_report_rizhi_title_tv);
            mDoctorInfoTV = findViewById(R.id.item_report_rizhi_doctor_info_tv);
            mZhenDuanInfoTV = findViewById(R.id.item_report_rizhi_zhenduan_info_tv);

            mPatientViewHolder = new MyPatientViewHolder(itemView);
            findViewById(R.id.item_my_patient_more_iv).setVisibility(View.GONE);

        }

        @Override
        public void onBind(Registration data, int position) {

            mTitleTV.setText(data.getState());

            Patient patient = new Patient();
            patient.setName(data.getName());
            patient.setAge(data.getAge());
            patient.setTel(data.getTel());
            patient.setSex(data.getSex());
            patient.setAddress(data.getPatientAddress());
            mPatientViewHolder.onBind(patient, position);

            StringBuilder sb = new StringBuilder();
            sb.append("挂号医生：");
            sb.append(data.getDoctorName());
            sb.append("\n");
            sb.append("挂号时间：");
            sb.append(data.getDatetime());
            mDoctorInfoTV.setText(sb);

            sb = new StringBuilder();
            sb.append("症状：");
            sb.append("\n");
            sb.append(data.getZhenduan1());
            sb.append("\n");
            sb.append(data.getZhenduan2());
            mZhenDuanInfoTV.setText(sb);
        }
    }
}
