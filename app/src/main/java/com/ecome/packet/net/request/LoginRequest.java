package com.ecome.packet.net.request;

import com.ecome.packet.entity.UserInfo;
import com.google.gson.Gson;
import com.mk.core.app.BaseApp;
import com.mk.core.entity.BaseEntity;
import com.mk.core.util.AppUtil;

import java.util.Map;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class LoginRequest extends AppRequest {

    private String mUserName;
    private String mPassword;

    public LoginRequest(String userName, String password) {
        mUserName = userName;
        mPassword = password;
    }

    @Override
    protected String getActionName() {
        return "app_login";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_name", mUserName);
        parameters.put("pwd", mPassword);
        parameters.put("location", "[安卓]-[版本号:" + AppUtil.getApkVersionName(BaseApp.getInstance()) + "]");
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        UserInfo userInfo = gson.fromJson(str, UserInfo.class);
        return userInfo;
    }

    @Override
    protected Class getParseClass() {
        return UserInfo.class;
    }
}
