package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Fee;

import java.util.Map;

public class ReportFeeRequest extends PageRequest {

    private String beginTime;
    private String endTime;

    public ReportFeeRequest(String beginTime, String endTime) {
        this.beginTime = beginTime;
        this.endTime = endTime;
    }

    @Override
    protected String getActionName() {
        return "app_report_fee_cls";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        parameters.put("as_being_datetime", beginTime);
        parameters.put("as_end_datetime", endTime);
    }

    @Override
    protected Class getParseClass() {
        return Fee.class;
    }

}
