package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Patient;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class AddPatientRequest extends AppRequest {

    private String name;
    private String tel;
    private String sex;
    private String age;
    private String id;
    private String address;

    public AddPatientRequest(String name, String tel, String sex, String age, String id, String address) {

        this.name = checkNull(name);
        this.tel = checkNull(tel);
        this.sex = checkNull(sex);
        this.age = checkNull(age);
        this.id = checkNull(id);
        this.address = checkNull(address);
    }

    @Override
    protected String getActionName() {
        return "app_pat_reg";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        parameters.put("pat_name", name);
        parameters.put("pat_tel", tel);
        parameters.put("pat_sex", sex);
        parameters.put("pat_age", age);
        parameters.put("pat_id", id);
        parameters.put("pat_address", address);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        Patient patient = gson.fromJson(str, Patient.class);
        return patient;
    }

    @Override
    protected Class getParseClass() {
        return Patient.class;
    }
}
