package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Workload;

import java.util.Map;

public class GetDoctorWorkloadRequest extends PageRequest {

    private String beginTime;
    private String endTime;

    public GetDoctorWorkloadRequest(String beginTime, String endTime) {
        this.beginTime = beginTime;
        this.endTime = endTime;
    }

    @Override
    protected String getActionName() {
        return "app_get_dorctor_yeji_detail";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("dor_id", UserManager.getInstance().getUserId() + "");
        parameters.put("begin_datetime", beginTime);
        parameters.put("end_datetime", endTime);
    }

    @Override
    protected Class getParseClass() {
        return Workload.class;
    }

}
