package com.ecome.packet.net.request;

import android.text.TextUtils;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Drug;

import java.util.Map;

public class GetCodeRequest extends PageRequest {

    private String mdClsId;

    public GetCodeRequest(String mdClsId) {
        this.mdClsId = mdClsId;
    }

    @Override
    protected String getActionName() {
        if (TextUtils.isEmpty(mdClsId)) {
            return "app_get_code_item";
        }
        return "app_get_md_kucun";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        if (!TextUtils.isEmpty(mdClsId)) {
            parameters.put("md_cls_id", mdClsId);
        }
    }

    @Override
    protected Class getParseClass() {
        return Drug.class;
    }

}
