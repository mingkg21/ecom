package com.ecome.packet.net.request;

import com.ecome.packet.entity.WorkTime;

import java.util.Map;

public class GetDoctorWorkRequest extends PageRequest {

    private String doctorId;

    public GetDoctorWorkRequest(int doctorId) {
        this.doctorId = doctorId + "";
    }

    @Override
    protected String getActionName() {
        return "app_get_dor_work";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", doctorId);
    }

    @Override
    protected Class getParseClass() {
        return WorkTime.class;
    }
}
