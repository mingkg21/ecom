package com.ecome.packet.net.request;

import android.text.TextUtils;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Patient;

import java.util.Map;

public class FindPatientRequest extends PageRequest {

    private String key;

    public FindPatientRequest(String key) {
        this.key = key;
    }

    @Override
    protected String getActionName() {
        return "app_find_pat";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        if (!TextUtils.isEmpty(key)) {
            parameters.put("text", key);
        }
    }

    @Override
    protected Class getParseClass() {
        return Patient.class;
    }

}
