package com.ecome.packet.net.request;

import com.ecome.packet.entity.DiagnoseResult;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class AddPrescriptionRequest extends AppRequest {

//    "dor_id": 152,
//            "guahao_id": 2352,
//            "gaofang": 0,
//            "db": "ecom",
//            "function": "app_insert_chufang_detail",
//            "chufang_detail": "1224,干姜,2,,3,,0,0,0|1287,黄芩,2,,8,,0,0,0|",
//            "jishu": 1,
//            "remark": "会打电话",
//            "zhenzhuan_name": "上世纪",
//            "pid": "177",
//            "daijian": 1,
//            "zhenduan_name": "顶焦度计"

    private String doctorId;
    private String registrationId;
    private String patientId;
    private String diagnose;
    private String symptom;
    private String prescriptionDetail;
    private String useNumber;
    private String gaofang;
    private String daijian;
    private String remark;

    public AddPrescriptionRequest(int doctorId, int registrationId, int patientId, String diagnose,
                                  String symptom, String prescriptionDetail, String useNumber,
                                  String gaofang, String daijian, String remark) {
        this.doctorId = String.valueOf(doctorId);
        this.registrationId = String.valueOf(registrationId);
        this.patientId = String.valueOf(patientId);
        this.diagnose = diagnose;
        this.symptom = symptom;
        this.prescriptionDetail = prescriptionDetail;
        this.useNumber = useNumber;
        this.gaofang = gaofang;
        this.daijian = daijian;
        this.remark = remark;
    }

    @Override
    protected String getActionName() {
        return "app_insert_chufang_detail";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("dor_id", doctorId);
        parameters.put("guahao_id", registrationId);
        parameters.put("gaofang", gaofang);
        parameters.put("chufang_detail", prescriptionDetail);
        parameters.put("jishu", useNumber);
        parameters.put("remark", remark);
        parameters.put("zhenzhuan_name", symptom);
        parameters.put("pid", patientId);
        parameters.put("daijian", daijian);
        parameters.put("zhenduan_name", diagnose);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        DiagnoseResult result = gson.fromJson(str, DiagnoseResult.class);
        return result;
    }

    @Override
    protected Class getParseClass() {
        return DiagnoseResult.class;
    }

}
