package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppBaseEntity;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class CmisPaibanRequest extends AppRequest {

    private String doctorId;
    private String beginDate;
    private String endDate;

    public CmisPaibanRequest(int doctorId, String beginDate, String endDate) {
        this.doctorId = doctorId + "";
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    @Override
    protected String getActionName() {
        return "cmis_paiban_gen_detail";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        parameters.put("dor_id", doctorId);
        parameters.put("return_id", "0");
        parameters.put("riqi_begin", beginDate);
        parameters.put("riqi_end", endDate);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        AppBaseEntity result = gson.fromJson(str, AppBaseEntity.class);
        return result;
    }

    @Override
    protected Class getParseClass() {
        return AppBaseEntity.class;
    }

}
