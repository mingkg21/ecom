package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.ChargeResult;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class ChargeRequest extends AppRequest {

    public static final String PAY_TYPE_CASH = "0";
    public static final String PAY_TYPE_WX = "1";
    public static final String PAY_TYPE_ALPAY = "2";
    public static final String PAY_TYPE_NONE = "-1";

    private String chargeReg;
    private String registerId;
    private String payType;
    private String fee;
    private String patientId;

    public ChargeRequest(String chargeReg, String registerId, String payType, String fee, String patientId) {
        this.chargeReg = chargeReg;
        this.registerId = registerId;
        this.payType = payType;
        this.fee = fee;
        this.patientId = patientId;
    }

    @Override
    protected String getActionName() {
        return "app_charge";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        parameters.put("charge_reg", chargeReg);
        parameters.put("guahao_id", registerId);
        parameters.put("zhifu_id", payType);
        parameters.put("fee_sum_in", fee);
        parameters.put("pid", patientId);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        ChargeResult result = gson.fromJson(str, ChargeResult.class);
        return result;
    }

    @Override
    protected Class getParseClass() {
        return ChargeResult.class;
    }
}
