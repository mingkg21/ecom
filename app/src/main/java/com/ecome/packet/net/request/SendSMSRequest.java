package com.ecome.packet.net.request;

import com.ecome.packet.entity.AppBaseEntity;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class SendSMSRequest extends AppRequest {

    private String tel;
    private String code;

    public SendSMSRequest(String tel, String code) {
        this.tel = tel;
        this.code = code;
    }

    @Override
    protected String getActionName() {
        return "app_insert_short_message";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", "0");
        parameters.put("fendian_id", "0");
        parameters.put("pid", "0");
        parameters.put("message_cls", "11");
        parameters.put("tel", tel);
        parameters.put("message_content", "您的验证码是" + code + "，有效期5分钟。若非本人操作，可不予理会。");
        parameters.put("remark", "注册验证码短信");
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        AppBaseEntity result = gson.fromJson(str, AppBaseEntity.class);
        return result;
    }

    @Override
    protected Class getParseClass() {
        return AppBaseEntity.class;
    }
}
