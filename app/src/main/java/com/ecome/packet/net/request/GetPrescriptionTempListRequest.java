package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Temp;

import java.util.Map;

public class GetPrescriptionTempListRequest extends PageRequest {

    @Override
    protected String getActionName() {
        return "app_get_chufang_temp_main";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("dor_id", UserManager.getInstance().getUserId() + "");
    }

    @Override
    protected Class getParseClass() {
        return Temp.class;
    }

}
