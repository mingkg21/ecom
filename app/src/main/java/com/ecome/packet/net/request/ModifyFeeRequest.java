package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppBaseEntity;
import com.ecome.packet.entity.Drug;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class ModifyFeeRequest extends AppRequest {

    private Drug drug;
    private int flag;

    public ModifyFeeRequest(Drug drug) {
        this.drug = drug;
    }

    @Override
    protected String getActionName() {
        return "app_alter_code_item";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        if (drug.getId() > 0) {
            flag = 2;
        } else {
            flag = 1;
        }
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        parameters.put("code_item_cls", drug.getCls() + "");
        parameters.put("code_item_id", drug.getId() + "");
        parameters.put("code_item_name", drug.getName());
        parameters.put("code_item_guige", drug.getGuige());
        parameters.put("code_item_unit", drug.getUnit());
        parameters.put("code_item_price", drug.getPrice()+ "");
        parameters.put("falg", flag + "");
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        AppBaseEntity entity = gson.fromJson(str, AppBaseEntity.class);
        return entity;
    }

    @Override
    protected Class getParseClass() {
        return AppBaseEntity.class;
    }

}
