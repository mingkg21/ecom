package com.ecome.packet.net.request;

import android.text.TextUtils;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppBaseEntity;
import com.ecome.packet.entity.Voucher;
import com.ecome.packet.entity.VoucherDrug;

import java.util.Map;

public class GetMdVoucherRequest extends PageRequest {

    public static final int FLAG_QUERY_ALL = 0;
    public static final int FLAG_CONFIRM = 1;
    public static final int FLAG_QUERY_ONE = 2;

    private int flag;
    private String voucherNo;

    public GetMdVoucherRequest(int flag, String voucherNo) {
        this.flag = flag;
        this.voucherNo = voucherNo;
        if (TextUtils.isEmpty(this.voucherNo)) {
            this.voucherNo = "";
        }
    }

    @Override
    protected String getActionName() {
        return "app_get_md_voucher_main_all";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        parameters.put("flag", flag + "");
        parameters.put("voucher_no", voucherNo);
    }

    @Override
    protected Class getParseClass() {
        if (flag == FLAG_QUERY_ONE) {
            return VoucherDrug.class;
        } else if (flag == FLAG_CONFIRM) {
            return AppBaseEntity.class;
        }
        return Voucher.class;
    }

}
