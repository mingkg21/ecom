package com.ecome.packet.net;

import com.mk.core.net.BaseAPI;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by mingkg21 on 16/1/18.
 */
public class API extends BaseAPI {

    public static final String KEY = "241B525C12362D38597D756157635374";

    @Override
    protected String getBaseUrl() {
        return "http://101.132.128.104/api/";
    }

    @Override
    protected Interceptor getCustomInterceptor() {
        return new AppInterceptor();
    }

    public static RequestBody toRequestBody(String value) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), value);
        return requestBody;
    }

    public Class getGenericType(int index) {
        Type genType = getClass().getGenericSuperclass();
        if (!(genType instanceof ParameterizedType)) {
            return Object.class;
        }
        Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
        if (index >= params.length || index < 0) {
            throw new RuntimeException("Index out of bounds(" + index + ")");
        }
        if (!(params[index] instanceof Class)) {
            return Object.class;
        }
        return (Class) params[index];
    }

}
