package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Staff;

import java.util.Map;

public class GetStaffRequest extends PageRequest {

    @Override
    protected String getActionName() {
        return "app_get_people";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("fendian_id", String.valueOf(UserManager.getInstance().getUserInfo().getFendDianId()));
    }

    @Override
    protected Class getParseClass() {
        return Staff.class;
    }
}
