package com.ecome.packet.net.request;

import com.ecome.packet.entity.UserInfo;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

/**
 * Created by mingkg21 on 2017/10/23.
 */

public class CompleteInfoRequest extends AppRequest {

    private String LoginId;

    private String name;
    private String adminName;
    private String phone;
    private String introducerId;
    private String picPath;
    private String pic1Path;
    private String pic2Path;
    private String pic3Path;
    private String payWXPath;
    private String payAlpayPath;
    private String outpatientAddress;
    private String outpatientTel;
    private String adminEamil;
    private String outpatientIntro;

    public CompleteInfoRequest(String loginId, String name, String adminName, String phone,
                               String introducerId, String picPath, String pic1Path,
                               String pic2Path, String pic3Path, String payWXPath, String payAlpayPath,
                               String outpatientAddress, String outpatientTel, String adminEamil, String outpatientIntro) {

        LoginId = loginId;
        this.name = name;
        this.adminName = adminName;
        this.phone = phone;
        this.introducerId = introducerId;
        this.picPath = checkNull(picPath);
        this.pic1Path = checkNull(pic1Path);
        this.pic2Path = checkNull(pic2Path);
        this.pic3Path = checkNull(pic3Path);
        this.payWXPath = checkNull(payWXPath);
        this.payAlpayPath = checkNull(payAlpayPath);
        this.outpatientAddress = outpatientAddress;
        this.outpatientTel = outpatientTel;
        this.adminEamil = adminEamil;
        this.outpatientIntro = outpatientIntro;
    }

    @Override
    protected String getActionName() {
        return "app_create_fendian_add";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", LoginId);
        parameters.put("cl_name", name);
        parameters.put("admin_name", adminName);
        parameters.put("admin_tel", phone);
        parameters.put("jieshaoren_id", introducerId);
        parameters.put("pic_0", picPath);
        parameters.put("pic_1", pic1Path);
        parameters.put("pic_2", pic2Path);
        parameters.put("pic_3", pic3Path);
        parameters.put("pic_weixin", payWXPath);
        parameters.put("pic_zhifubao", payAlpayPath);
        parameters.put("cl_address", outpatientAddress);
        parameters.put("cl_tel", outpatientTel);
        parameters.put("admin_email", adminEamil);
        parameters.put("cl_jieshao", outpatientIntro);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        UserInfo userInfo = gson.fromJson(str, UserInfo.class);
        return userInfo;
    }

    @Override
    protected Class getParseClass() {
        return UserInfo.class;
    }
}
