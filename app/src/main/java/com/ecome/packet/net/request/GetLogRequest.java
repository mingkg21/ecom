package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.LogInfo;

import java.util.Map;

public class GetLogRequest extends PageRequest {

    @Override
    protected String getActionName() {
        return "app_get_log";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", String.valueOf(UserManager.getInstance().getUserId()));
        parameters.put("log_cls", "0");
    }

    @Override
    protected Class getParseClass() {
        return LogInfo.class;
    }
}
