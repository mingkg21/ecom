package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Business;
import com.ecome.packet.entity.Department;
import com.ecome.packet.entity.FeeCategory;
import com.ecome.packet.entity.KuFang;
import com.ecome.packet.entity.PinCi;
import com.ecome.packet.entity.Provider;
import com.ecome.packet.entity.RegistrationRemark;
import com.ecome.packet.entity.Role;
import com.ecome.packet.entity.Unit;
import com.ecome.packet.entity.Use;
import com.ecome.packet.entity.ZhiCheng;
import com.ecome.packet.entity.ZiDian;

import java.util.Map;

public class GetClsCodeRequest extends PageRequest {

    public static final int CLS_ALL = 0;
    public static final int CLS_ROLE = 1;
    public static final int CLS_DEPARTMENT = 2;
    public static final int CLS_ZHICHENG = 3;
    public static final int CLS_FEE = 6;
    public static final int CLS_UNIT = 9;
    public static final int CLS_REMARK = 14;
    public static final int CLS_PROVIDER = 16;
    public static final int CLS_USE = 18;
    public static final int CLS_KUFANG = 19;
    public static final int CLS_BUSINESS = 20;
    public static final int CLS_PINCI = 22;

    public static final int OPER_CLS_QUERY = 0;
    public static final int OPER_CLS_MOD = 1;
    public static final int OPER_CLS_ADD = 2;
    public static final int OPER_CLS_DEL = 3;

//     -- cls = 1  获取角色列表
//     -- cls = 2  获取科室
//     -- cls = 3  获取职称
//     -- cls = 4  获取默认诊金
//     -- cls = 5  获取诊室列表
//     -- cls = 6  获取费用的类型字典
//     -- cls = 7  获取中医诊断
//     -- cls = 8  获取西医诊断
//     -- cls = 9  获取项目的最小单位
//     -- cls = 10 获取项目的药品加成率字典
//     -- cls = 11 获取国家字典
//     -- cls = 12 获取省份
//     -- cls = 13 获取城市
//     -- cls = 14 获取处方备注
//     -- cls = 15 获取生产厂家
//     -- cls = 16 取供应商
//     -- cls = 17 获取煎法
//     -- cls = 18 获取用法
//     -- cls = 19 获取库房
//     -- cls = 20 获取业务类型
//     -- cls = 22 获取频次

    private int cls;
    private int operCls;

    public GetClsCodeRequest(int cls, int operCls) {
        this.cls = cls;
        this.operCls = operCls;
    }

    @Override
    protected String getActionName() {
        return "app_get_code_for_cls";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        parameters.put("cls", cls + "");
        parameters.put("oper_cls", operCls + "");
    }

    @Override
    protected Class getParseClass() {

        if (cls == CLS_ZHICHENG) {
            return ZhiCheng.class;
        } else if (cls == CLS_ROLE) {
            return Role.class;
        } else if (cls == CLS_DEPARTMENT) {
            return Department.class;
        } else if (cls == CLS_FEE) {
            return FeeCategory.class;
        } else if (cls == CLS_UNIT) {
            return Unit.class;
        } else if (cls == CLS_KUFANG) {
            return KuFang.class;
        } else if (cls == CLS_PROVIDER) {
            return Provider.class;
        } else if (cls == CLS_BUSINESS) {
            return Business.class;
        } else if (cls == CLS_ALL) {
            return ZiDian.class;
        } else if (cls == CLS_REMARK) {
            return RegistrationRemark.class;
        } else if (cls == CLS_USE) {
            return Use.class;
        } else if (cls == CLS_PINCI) {
            return PinCi.class;
        }

        return null;
    }

}
