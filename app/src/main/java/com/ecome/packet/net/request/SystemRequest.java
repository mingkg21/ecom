package com.ecome.packet.net.request;

import com.ecome.packet.app.App;
import com.ecome.packet.entity.SystemInfo;

import java.util.Map;

public class SystemRequest extends PageRequest {

    @Override
    protected String getActionName() {
        return "app_get_system_other";
    }

    @Override
    public String getUrl() {
        return App.getBaseApi();
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {

    }

    @Override
    protected Class getParseClass() {
        return SystemInfo.class;
    }

}
