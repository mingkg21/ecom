package com.ecome.packet.net.request;

import com.ecome.packet.entity.Registration;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class RegistrationRequest extends AppRequest {

    private int doctorId;
    private int patientId;

    public RegistrationRequest(int doctorId, int patientId) {
        this.doctorId = doctorId;
        this.patientId = patientId;
    }

    @Override
    protected String getActionName() {
        return "sp_cmis_doctor_guahao";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("doctor_id", String.valueOf(doctorId));
        parameters.put("pid", String.valueOf(patientId));
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        Registration registration = gson.fromJson(str, Registration.class);
        return registration;
    }

    @Override
    protected Class getParseClass() {
        return Registration.class;
    }
}
