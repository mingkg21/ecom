package com.ecome.packet.net.request;

import com.ecome.packet.entity.AppBaseEntity;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class ModifyPatientAddressRequest extends AppRequest {

    private String patientId;
    private String tel;
    private String address;

    public ModifyPatientAddressRequest(String patientId, String tel, String address) {
        this.patientId = patientId;
        this.tel = tel;
        this.address = address;
    }

    @Override
    protected String getActionName() {
        return "app_alter_pat_address";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("tel", tel);
        parameters.put("address", address);
        parameters.put("pid", patientId);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        AppBaseEntity result = gson.fromJson(str, AppBaseEntity.class);
        return result;
    }

    @Override
    protected Class getParseClass() {
        return AppBaseEntity.class;
    }

}
