package com.ecome.packet.net.request;

import com.ecome.packet.entity.WorkDay;

import java.util.Map;

public class GetDoctorWorkWeekRequest extends PageRequest {

    private String doctorId;

    public GetDoctorWorkWeekRequest(int doctorId) {
        this.doctorId = doctorId + "";
    }

    @Override
    protected String getActionName() {
        return "app_get_dor_work_weeken";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("dor_id", doctorId);
    }

    @Override
    protected Class getParseClass() {
        return WorkDay.class;
    }
}
