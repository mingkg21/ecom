package com.ecome.packet.net.request;

import android.text.TextUtils;

import com.ecome.packet.app.App;
import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.net.ParameterizedTypeImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mk.core.entity.BaseEntity;
import com.mk.core.entity.NullStringToEmptyAdapterFactory;
import com.mk.core.entity.ResponseEntity;
import com.mk.core.net.BaseRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mingkg21 on 2017/9/26.
 */

public abstract class AppRequest extends BaseRequest<AppResponseEntity> {

    @Override
    public AppResponseEntity parse(String str) {
        if (isList()) {
            return parse(str, getParseClass());
        }
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        Gson gson = getGson();
        AppResponseEntity responseEntity = new AppResponseEntity<>();
        try {
            JSONObject jsonObject = new JSONObject(str);
            responseEntity.setMsg(jsonObject.optString("err"));
            responseEntity.setSuccess(jsonObject.optBoolean("success"));
            JSONArray jsonArray = jsonObject.optJSONArray("resultset");
            if (jsonArray.length() > 0) {
                str = jsonArray.opt(0).toString();
                responseEntity.setData(parseEntity(gson, str));
            }
        } catch (Exception e) {

        }
        return responseEntity;
    }

    protected boolean isList() {
        return false;
    }

    @Override
    public String getUrl() {
//        return "http://27.154.58.198:28099/restful/rpc";
        return App.getApi();
    }

    protected abstract String getActionName();

    @Override
    public Map<String, String> getParameter() {
        return new HashMap<>();
    }

    protected abstract void setParameters(Map<String, String> parameters);

    @Override
    public String getPostString() {
        setParameters(mParameter);
        JSONObject jsonObject = new JSONObject();

        try {
//            jsonObject.putOpt("db", "ecom");
            jsonObject.putOpt("db", "mssql");
            jsonObject.putOpt("function", getActionName());

            for (String key : mParameter.keySet()) {
                jsonObject.putOpt(key, mParameter.get(key));
            }
        } catch (Exception e) {

        }

        return jsonObject.toString();
    }

    private Gson getGson() {
        return new GsonBuilder().registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory()).create();
    }

    protected <T extends Serializable> AppResponseEntity<T> parse(String str, Class<T> clazz) {
        Gson gson = getGson();
        AppResponseEntity<T> data = null;
        try {
            data = gson.fromJson(str, getParseType(clazz));
        } catch (Exception e) {

        }
        return data;
    }

    protected BaseEntity parseEntity(Gson gson, String str) {
        return null;
    }

    protected <T extends Serializable> Type getParseType(Class<T> clazz) {
        if (clazz == null) {
            return new TypeToken<ResponseEntity>() {
            }.getType();
        }
        return new ParameterizedTypeImpl(ResponseEntity.class, new Class[]{clazz});
    }

    protected abstract Class getParseClass();

    protected String checkNull(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        return str;
    }


}
