package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppBaseEntity;
import com.ecome.packet.entity.Staff;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class ModifyStaffRequest extends AppRequest {

    private Staff staff;

    public ModifyStaffRequest(Staff staff) {
        this.staff= staff;
    }

    @Override
    protected String getActionName() {
        return "app_get_people_alter";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        parameters.put("people_id", staff.getId() + "");
        parameters.put("name", staff.getName());
        parameters.put("login_name", staff.getLoginName());
        parameters.put("tel", staff.getTel());
        parameters.put("sex", staff.getSex());
        parameters.put("dept_id", staff.getDeptId() + "");
        parameters.put("role_id", staff.getRoleId() + "");
        parameters.put("zhicheng_id", staff.getZhichengId() + "");
        parameters.put("pic_head", staff.getPeoplePic());
        parameters.put("jianjie", staff.getJianjie());
        parameters.put("shangchang", staff.getFeature());
        parameters.put("kanzheng_minute", staff.getKanzhengMinute() + "");
        parameters.put("jingyong", staff.getDisabled() + "");
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        AppBaseEntity entity = gson.fromJson(str, AppBaseEntity.class);
        return entity;
    }

    @Override
    protected Class getParseClass() {
        return AppBaseEntity.class;
    }

}
