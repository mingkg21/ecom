package com.ecome.packet.net;

import android.text.TextUtils;

import com.ecome.packet.db.UserManager;

import java.io.IOException;
import java.net.URLDecoder;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by mingkg21 on 2017/8/30.
 */

public class AppInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request oldRequest = chain.request();

        // 添加新的参数
        HttpUrl.Builder authorizedUrlBuilder = oldRequest.url()
                .newBuilder()
                .scheme(oldRequest.url().scheme())
                .host(oldRequest.url().host());

        Request.Builder requestBuilder = oldRequest.newBuilder();
        FormBody.Builder formBodyBuilder = new FormBody.Builder();

        RequestBody requestBody = null;

        boolean hasMultipart = false;

        if (oldRequest.body() instanceof FormBody) {
            FormBody oldFormBody = (FormBody) oldRequest.body();
            for (int i = 0; i < oldFormBody.size(); i++) {
                String key = oldFormBody.encodedName(i);
                String value = URLDecoder.decode(oldFormBody.encodedValue(i));
                if (TextUtils.isEmpty(value)) {
                    continue;
                }
                formBodyBuilder.add(key, value);
            }
            formBodyBuilder.add("user", "10086");
            formBodyBuilder.add("uid", String.valueOf(UserManager.getInstance().getUserId()));

            requestBody = formBodyBuilder.build();
        } else if (oldRequest.method().equals("GET")) {
            authorizedUrlBuilder.addQueryParameter("user", "10086");
            authorizedUrlBuilder.addQueryParameter("uid", String.valueOf(UserManager.getInstance().getUserId()));

            requestBody = oldRequest.body();
        }
//        else if (oldRequest.body() instanceof MultipartBody){
//            hasMultipart = true;
//            MultipartBody oldFormBody = (MultipartBody) oldRequest.body();
//
//            MultipartBody.Builder multipartBuilder = new MultipartBody.Builder();
//
//            for (MultipartBody.Part part : oldFormBody.parts()) {
//                multipartBuilder.addPart(part);
//            }
//
////            multipartBuilder.addPart(MultipartBody.Part.createFormData("user", "10086"));
////            multipartBuilder.addPart(MultipartBody.Part.createFormData("uid", String.valueOf(UserManager.getInstance().getUserId())));
//
//            requestBody = multipartBuilder.build();
//        }
        else if (oldRequest.method().equals("POST")) {
            requestBody = oldRequest.body();
        }

//        if (hasMultipart) {//有上传文件的处理
//            return chain.proceed(requestBuilder.url(authorizedUrlBuilder.build()).build());
//        }

        // 新的请求
        Request newRequest = requestBuilder
                .method(oldRequest.method(), requestBody)
                .url(authorizedUrlBuilder.build())
                .build();

        return chain.proceed(newRequest);
    }
}
