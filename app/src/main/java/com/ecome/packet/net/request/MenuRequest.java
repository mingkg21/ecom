package com.ecome.packet.net.request;

import com.ecome.packet.entity.Menu;

import java.util.Map;

public class MenuRequest extends PageRequest {

    private String loginId;
    private String levelId;
    private String levePre;

    public MenuRequest(int loginId, int levelId, int levePre) {
        this.loginId = loginId + "";
        this.levelId = levelId + "";
        this.levePre = levePre + "";
    }

    @Override
    protected String getActionName() {
        return "app_get_menu";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", loginId);
        parameters.put("level_id", levelId);
        parameters.put("level_pre", levePre);
    }

    @Override
    protected Class getParseClass() {
        return Menu.class;
    }
}
