package com.ecome.packet.net.request;

import com.ecome.packet.entity.WorkTimeItem;

import java.util.Map;

public class GetDoctorPaibanRequest extends PageRequest {

    private String doctorId;

    public GetDoctorPaibanRequest(int doctorId) {
        this.doctorId = doctorId + "";
    }

    @Override
    protected String getActionName() {
        return "app_get_dor_paiban";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("dor_id", doctorId);
    }

    @Override
    protected Class getParseClass() {
        return WorkTimeItem.class;
    }
}
