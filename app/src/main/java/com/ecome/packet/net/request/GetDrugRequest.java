package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Drug;

import java.util.Map;

public class GetDrugRequest extends PageRequest {

    @Override
    protected String getActionName() {
        return "app_get_code_item";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", String.valueOf(UserManager.getInstance().getUserId()));
    }

    @Override
    protected Class getParseClass() {
        return Drug.class;
    }
}
