package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppBaseEntity;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class InsertPicRequest extends AppRequest {

    private String patientId;
    private String guahaoId;
    private String pic;

    public InsertPicRequest(int patientId, int guahaoId, String pic) {

        this.patientId = patientId + "";
        this.guahaoId = guahaoId + "";
        this.pic = checkNull(pic);
    }

    @Override
    protected String getActionName() {
        return "app_doctor_insert_pic";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("pid", patientId);
        parameters.put("guahao_id", guahaoId);
        parameters.put("dor_id", UserManager.getInstance().getUserId() + "");
        parameters.put("pic", pic);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        AppBaseEntity patient = gson.fromJson(str, AppBaseEntity.class);
        return patient;
    }

    @Override
    protected Class getParseClass() {
        return AppBaseEntity.class;
    }
}
