package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Config;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class ConfigModifyRequest extends AppRequest {

    private String configId;
    private String configValue;

    public ConfigModifyRequest(String configId, String configValue) {
        this.configId = configId;
        this.configValue = configValue;
    }

    @Override
    protected String getActionName() {
        return "app_alter_parameter";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        parameters.put("config_id", configId);
        parameters.put("config_value", configValue);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        Config config = gson.fromJson(str, Config.class);
        config.setConfigId(configId);
        config.setValue(configValue);
        return config;
    }

    @Override
    protected Class getParseClass() {
        return Config.class;
    }
}
