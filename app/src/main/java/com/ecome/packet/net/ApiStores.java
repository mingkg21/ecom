package com.ecome.packet.net;

/**
 * Created by mingkg21 on 2017/8/25.
 */

public interface ApiStores {

//    //----------------------------------------------------------------------------------------------------------------
//    //----------------------------------------------------------------------------------------------------------------
//    //----------------------------------------------------------------------------------------------------------------
//
//    //获取管理选项信息
//    @POST("datemanage/config")
//    Call<ResponseEntity<ManageConfig>> config();
//
//    //获取管理日历每天的记录
//    @GET("datemanage")
//    Call<ResponseEntity<ArrayList<ManageDay>>> getManageInfo(@Query("year") int year, @Query("month") int month, @Query("day") int day);
//
//    //注册接口
//    @FormUrlEncoded
//    @POST("datemanage/edit")
//    Call<ResponseEntity> manageEdit(@Field("time") long time, @Field("first") int first, @Field("seconds") String seconds);
//
//
//    //注册接口
//    @FormUrlEncoded
//    @POST("user/addbycode")
//    Call<UserInfoData> register(@Field("phone") String phone, @Field("pwd") String password, @Field("code") String code);
//
//    //登录接口
//    @FormUrlEncoded
//    @POST("user/login")
//    Call<UserInfoData> login(@Field("phone") String userName, @Field("pwd") String password);
//
//    //获取用户信息
//    @POST("user/info")
//    Call<UserInfoData> getUserInfo();
//
//    //用户信息编辑接口
//    @Multipart
//    @POST("user/edit")
//    Call<UserInfoData> userInfoEdit(@PartMap Map<String, RequestBody> params);
//
//    //获取验证码接口
//    @FormUrlEncoded
//    @POST("user/sendcode")
//    Call<ResponseEntity> sendCode(@Field("phone") String userName, @Field("type") int type);
//`
//    //修改密码接口`
//    @FormUrlEncoded
//    @POST("user/modifypwdbycode")
//    Call<ResponseEntity> updatePassword(@Field("phone") String phone,
//                                        @Field("pwd") String password,
//                                        @Field("code") String code);
//
//    //获取分类接口
//    @GET("getclassify")
//    Call<CategoryListData> getClassify(@Query("type") int type, @Query("offset") int offset, @Query("limit") int limit);
//
//    //获取资讯列表接口
//    @GET("news")
//    Call<NewsListData> getNewsList(@Query("classify") int categoryId, @Query("recommend") int recommend, @Query("subject") int subjectId, @Query("type") int type,
//                                   @Query("title") String title,
//                                   @Query("offset") int offset, @Query("limit") int limit);
//
//    //资讯详情接口
//    @FormUrlEncoded
//    @POST("news/info")
//    Call<NewsData> newsInfo(@Field("id") int id, @Field("addtime") long addTime);
//
//    //资讯详情接口
//    @FormUrlEncoded
//    @POST("news/info")
//    Call<RecordData> recordInfo(@Field("id") int id, @Field("addtime") long addTime);
//
//    //添加日记接口
//    @FormUrlEncoded
//    @POST("news/addnote")
//    Call<RecordData> saveRecord(@Field("addtime") long time, @Field("title") String title, @Field("content") String content, @Field("imglist") String imageList,
//                                @Field("isprivate") int isPrivate, @Field("subject") int subjectId);
//
//    //获取日记列表接口
//    @GET("news")
//    Call<ResponseEntity<RecordList>> getRecordList(@Query("classify") int categoryId, @Query("subject") int subjectId, @Query("type") int type, @Query("offset") int offset, @Query("limit") int limit);
//
//    //获取音频列表接口
//    @GET("audio")
//    Call<FmProgramListData> getAudioList(@Query("classify") int categoryId, @Query("recommend") int recommend, @Query("subject") int subjectId,
//                                         @Query("name") String name,
//                                         @Query("offset") int offset, @Query("limit") int limit);
//
//    //获取音频列表接口
//    @GET("subject/relevant")
//    Call<SubjectListData> relevant(@Query("classify") int categoryId, @Query("limit") int limit);
//
//    //音频详情接口
//    @FormUrlEncoded
//    @POST("audio/info")
//    Call<FmProgramData> audioInfo(@Field("classify") int categoryId, @Field("subject") int subjectId, @Field("id") int id);
//
//    //获取音频列表接口
//    @GET("anchor")
//    Call<FmProgramListData> getAnchorAudioList(@Query("classify") int categoryId, @Query("offset") int offset, @Query("limit") int limit);
//
//    //作者详情接口
//    @FormUrlEncoded
//    @POST("anchor/info")
//    Call<FmAuthorData> anchorInfo(@Field("id") String id);
//
//    //订阅接口
//    @FormUrlEncoded
//    @POST("subscribe/add")
//    Call<ResponseEntity<Subscribe>> subscribeAdd(@Field("type") int type, @Field("tid") int id);
//
//    //取消订阅接口
//    @FormUrlEncoded
//    @POST("subscribe/cancel")
//    Call<ResponseEntity<Subscribe>> subscribeCancel(@Field("type") int type, @Field("tid") int id);
//
//    //获取订阅列表接口
//    @GET("subscribe/my")
//    Call<SubscribeListData> getSubscribeList(@Query("type") int type, @Query("offset") int offset, @Query("limit") int limit);
//
//    //添加收藏接口
//    @FormUrlEncoded
//    @POST("collection/add")
//    Call<ResponseEntity<Integer>> collectionAdd(@Field("type") int type, @Field("tid") int id);
//
//    //取消收藏接口
//    @FormUrlEncoded
//    @POST("collection/cancel")
//    Call<ResponseEntity<Integer>> collectionCancel(@Field("type") int type, @Field("tid") int id);
//
//    //获取收藏列表接口
//    @GET("collection")
//    Call<CollectionListData> getCollectionList(@Query("type") int type, @Query("offset") int offset, @Query("limit") int limit);
//
//    //获取评论列表接口
//    @GET("comment")
//    Call<CommentListData> getCommentList(@Query("type") int type, @Query("tid") int id, @Query("user") int user, @Query("offset") int offset, @Query("limit") int limit);
//
//    //提交评论接口
//    @FormUrlEncoded
//    @POST("comment/add")
//    Call<CommentData> commentAdd(@Field("type") int type, @Field("tid") int id, @Field("content") String content);
//
//    //获取我的评论列表接口
//    @GET("comment/my")
//    Call<CommentListData> getMyCommentList(@Query("type") int type, @Query("offset") int offset, @Query("limit") int limit);
//
//    //提交回复接口
//    @FormUrlEncoded
//    @POST("reply/add")
//    Call<ReplyData> replyAdd(@Field("id") int id, @Field("content") String content, @Field("parentid") int parentId, @Field("iscomment") int isComment);
//
//    //获取评论详情和回复列表接口
//    @GET("reply")
//    Call<ReplyListData> getReplyList(@Query("id") int id, @Query("offset") int offset, @Query("limit") int limit);
//
//    //获取我的评论列表接口
//    @GET("reply/my")
//    Call<ReplyListData> getMyReplyList(@Query("offset") int offset, @Query("limit") int limit);
//
//    //点赞接口
//    @FormUrlEncoded
//    @POST("praise/add")
//    Call<ResponseEntity<ReplyResult>> praiseAdd(@Field("type") int type, @Field("tid") int id);
//
//    //取消点赞接口
//    @FormUrlEncoded
//    @POST("praise/cancel")
//    Call<ResponseEntity<ReplyResult>> praiseCancel(@Field("type") int type, @Field("tid") int id);
//
//    //阅读接口
//    @FormUrlEncoded
//    @POST("myread/add")
//    Call<ResponseEntity<Integer>> read(@Field("type") int type, @Field("tid") int id);
//
//    //已读列表接口
//    @GET("myread")
//    Call<ReadListData> getMyReadList(@Query("type") int type, @Query("offset") int offset, @Query("limit") int limit);
//
//     //消息列表接口
//    @GET("systemmsg")
//    Call<ResponseEntity<BaseListEntity<Message>>> getSystemMsgList(@Query("offset") int offset, @Query("limit") int limit);
//
//    //取消收藏接口
//    @FormUrlEncoded
//    @POST("systemmsg/addread")
//    Call<ResponseEntity<Integer>> readMsg(@Field("sm_id") int id);
//
//    //取消收藏接口
//    @FormUrlEncoded
//    @POST("datemanage/analysis")
//    Call<ResponseEntity<AnalyzeData>> analysis(@Field("user") String user);
//
//    //批量上传接口
//    @Multipart
//    @POST("upload/batch")
//    Call<ResponseEntity<ArrayList<UploadResult>>> upload(@PartMap Map<String, RequestBody> params);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//    //采购订单列表接口
//    @FormUrlEncoded
//    @POST("purchase/order/searchPage")
//    Call<OrderListData> searchPage(@Field("purchase_user_id") String userId,
//                                   @Field("page") int page,
//                                   @Field("pagesize") int pageSize);
//
//    //采购中接口
//    @FormUrlEncoded
//    @POST("purchase/order/searchBuying")
//    Call<OrderListData> searchBuying(@Field("page") int page,
//                                     @Field("pagesize") int pageSize);
//
//    //采购员确认发货接口
//    @FormUrlEncoded
//    @POST("purchase/order/confirmDelivery")
//    Call<ResponseEntity> confirmDelivery(@Field("purchase_order_id") String orderId,
//                                         @Field("send_desc") String sendDesc);
//
//    //采购历史接口
//    @FormUrlEncoded
//    @POST("purchase/order/searchHistory")
//    Call<OrderListData> searchHistory(@Field("page") int page,
//                                      @Field("pagesize") int pageSize);
//
//    //采购员下单接口
//    @FormUrlEncoded
//    @POST("purchase/order/createOrder")
//    Call<ResponseEntity> createOrder(@Field("other_fee_name") String otherFeeName,
//                                     @Field("other_fee") float otherFee,
//                                     @Field("product_list") String productList,
//                                     @Field("image_ids") String imgIds);
//
//    @Multipart
//    @POST("/app_img_upload")
//    Call<ResponseEntity> uploadImg(@Part MultipartBody.Part body);
//
//    //关键字查询商品接口
//    @FormUrlEncoded
//    @POST("productSearch/keySearch")
//    Call<ProductListData> search(@Field("page") int page,
//                                 @Field("pagesize") int pageSize,
//                                 @Field("city_id") String cityId,
//                                 @Field("search_key") String key);
//
//    //app版本更新接口
//    @POST("version/checkVersion")
//    Call<UpdateInfoData> checkVersion();
}

