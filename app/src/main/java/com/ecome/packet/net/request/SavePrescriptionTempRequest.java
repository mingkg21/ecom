package com.ecome.packet.net.request;

import com.ecome.packet.entity.AppBaseEntity;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class SavePrescriptionTempRequest extends AppRequest {

//    {
//        "dor_id": 152,
//            "zhuzhi": "是你",
//            "gongxiao": "手机",
//            "gaofang": 1,
//            "db": "ecom",
//            "chufang_zhifa": "1",
//            "function": "app_insert_chufang_temp_main",
//            "temp_name": "病人:王五的处方模板",
//            "jishu": 1,
//            "chufang_detail": "1156,蜜甘草,1,,5,,0,0,0|1347,生龙骨,2,,3,,0,0,0|",
//            "chufang_remark": "",
//            "temp_quanxian": "1",
//            "chufang_chuchu": "1",
//            "daijian": 1
//    }

    private String doctorId;
    private String prescriptionDetail;
    private String useNumber;
    private String gaofang;
    private String daijian;

    private String zhuzhi;
    private String gongxiao;
    private String chufang_zhifa;
    private String tempName;
    private String temp_quanxian;
    private String chufang_chuchu;
    private String chufang_remark;

    public SavePrescriptionTempRequest(int doctorId, String zhuzhi, String gongxiao, String chufang_zhifa,
                                       String tempName, String temp_quanxian, String chufang_chuchu,
                                       String prescriptionDetail, String useNumber,
                                       String gaofang, String daijian, String chufang_remark) {
        this.doctorId = String.valueOf(doctorId);
        this.zhuzhi = zhuzhi;
        this.chufang_zhifa = chufang_zhifa;
        this.tempName = tempName;
        this.temp_quanxian = temp_quanxian;
        this.chufang_remark = chufang_remark;
        this.gongxiao = gongxiao;
        this.prescriptionDetail = prescriptionDetail;
        this.useNumber = useNumber;
        this.gaofang = gaofang;
        this.daijian = daijian;
        this.chufang_chuchu = chufang_chuchu;
    }

    @Override
    protected String getActionName() {
        return "app_insert_chufang_temp_main";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("dor_id", doctorId);
        parameters.put("zhuzhi", zhuzhi);
        parameters.put("gongxiao", gongxiao);
        parameters.put("chufang_zhifa", chufang_zhifa);
        parameters.put("temp_name", tempName);
        parameters.put("temp_quanxian", temp_quanxian);
        parameters.put("chufang_remark", chufang_remark);
        parameters.put("chufang_chuchu", chufang_chuchu);
        parameters.put("gaofang", gaofang);
        parameters.put("chufang_detail", prescriptionDetail);
        parameters.put("jishu", useNumber);
        parameters.put("daijian", daijian);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        AppBaseEntity result = gson.fromJson(str, AppBaseEntity.class);
        return result;
    }

    @Override
    protected Class getParseClass() {
        return AppBaseEntity.class;
    }

}
