package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Registration;

import java.util.Map;

public class ReportRiZhiRequest extends PageRequest {

    private String beginTime;
    private String endTime;

    public ReportRiZhiRequest(String beginTime, String endTime) {
        this.beginTime = beginTime;
        this.endTime = endTime;
    }

    @Override
    protected String getActionName() {
        return "app_report_rizhi";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        parameters.put("as_being_datetime", beginTime);
        parameters.put("as_end_datetime", endTime);
    }

    @Override
    protected Class getParseClass() {
        return Registration.class;
    }

}
