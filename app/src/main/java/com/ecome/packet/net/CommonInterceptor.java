package com.ecome.packet.net;

import android.text.TextUtils;

import com.mk.core.util.MD5Util;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by mingkg21 on 2017/8/30.
 */

public class CommonInterceptor implements Interceptor {

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request oldRequest = chain.request();

        // 添加新的参数
        HttpUrl.Builder authorizedUrlBuilder = oldRequest.url()
                .newBuilder()
                .scheme(oldRequest.url().scheme())
                .host(oldRequest.url().host());

        Request.Builder requestBuilder = oldRequest.newBuilder();
        FormBody.Builder formBodyBuilder = new FormBody.Builder();

        boolean hasMultipart = false;

        TreeMap<String, Object> kvMap = new TreeMap<>();

        if (oldRequest.body() instanceof FormBody) {
            FormBody oldFormBody = (FormBody) oldRequest.body();
            for (int i = 0; i < oldFormBody.size(); i++) {
                String key = oldFormBody.encodedName(i);
                String value = URLDecoder.decode(oldFormBody.encodedValue(i));
                if (TextUtils.isEmpty(value)) {
                    continue;
                }
                kvMap.put(key, value);
                formBodyBuilder.add(key, value);
            }
        } else if (oldRequest.body() instanceof MultipartBody){
            hasMultipart = true;
        }

        //添加TOKEN
//        String token = UserManager.getInstance().getToken();
//        if (!TextUtils.isEmpty(token)) {
//            kvMap.putString("token", token);
//            formBodyBuilder.add("token", token);
//            authorizedUrlBuilder.setQueryParameter("token", token);
//        }

        Iterator<Map.Entry<String, Object>> iterator = kvMap.entrySet().iterator();
        StringBuilder builder = new StringBuilder();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> entry = iterator.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            if (!(value instanceof String) || !TextUtils.isEmpty(value.toString())) {
                builder.append(key);
                builder.append("=");
                builder.append(entry.getValue());
                builder.append("&");
            }
        }
        if (builder.length() > 0) {
            builder.append("key");
            builder.append("=");
            builder.append(API.KEY);

            String md5Str = MD5Util.string2MD5(builder.toString());
            formBodyBuilder.add("sign", md5Str);
            authorizedUrlBuilder.setQueryParameter("sign", md5Str);
        }

        if (hasMultipart) {//有上传文件的处理
            return chain.proceed(requestBuilder.url(authorizedUrlBuilder.build()).build());
        }

        // 新的请求
        Request newRequest = requestBuilder
                .method(oldRequest.method(), formBodyBuilder.build())
                .url(authorizedUrlBuilder.build())
                .build();

        return chain.proceed(newRequest);
    }
}
