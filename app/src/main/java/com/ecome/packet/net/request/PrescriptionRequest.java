package com.ecome.packet.net.request;

import com.ecome.packet.entity.Prescription;

import java.util.Map;

public class PrescriptionRequest extends PageRequest {

    private String id;

    public PrescriptionRequest(String id) {
        this.id = id;
    }

    @Override
    protected String getActionName() {
        return "app_get_pat_history_chufang";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("guahao_id", id);
    }

    @Override
    protected Class getParseClass() {
        return Prescription.class;
    }

}
