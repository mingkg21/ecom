package com.ecome.packet.net.request;

import com.ecome.packet.entity.DiagnoseResult;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class ModifyDiagnoseFeeRequest extends AppRequest {

    private String registrationId;
    private String doctorId;
    private String fee;

    public ModifyDiagnoseFeeRequest(String registrationId, String doctorId, String fee) {
        this.registrationId = registrationId;
        this.doctorId = doctorId;
        this.fee = fee;
    }

    @Override
    protected String getActionName() {
        return "app_alter_zhenjin";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("guahao_id", registrationId);
        parameters.put("dor_id", doctorId);
        parameters.put("zhenjin", fee);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        DiagnoseResult result = gson.fromJson(str, DiagnoseResult.class);
        return result;
    }

    @Override
    protected Class getParseClass() {
        return DiagnoseResult.class;
    }

}
