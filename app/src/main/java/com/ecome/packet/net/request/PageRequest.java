package com.ecome.packet.net.request;

import com.ecome.packet.entity.AppResponseEntity;
import com.ecome.packet.net.ParameterizedTypeImpl;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by mingkg21 on 2017/10/14.
 */

public abstract class PageRequest extends AppRequest {

    @Override
    protected boolean isList() {
        return true;
    }

    @Override
    protected <T extends Serializable> Type getParseType(Class<T> clazz) {
        Type listType = new ParameterizedTypeImpl(getParseListClass(), new Class[]{clazz});
        Type type = new ParameterizedTypeImpl(AppResponseEntity.class, new Type[]{listType});
        return type;
    }

    protected Class getParseListClass() {
        return ArrayList.class;
    }
}
