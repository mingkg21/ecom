package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Fried;

import java.util.Map;

public class GetFriedRequest extends PageRequest {

    @Override
    protected String getActionName() {
        return "app_get_jianfa";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", String.valueOf(UserManager.getInstance().getUserId()));
    }

    @Override
    protected Class getParseClass() {
        return Fried.class;
    }
}
