package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Workload;

import java.util.Map;

public class ReportChargeDetailRequest extends PageRequest {

    private String beginTime;
    private String endTime;

    public ReportChargeDetailRequest(String beginTime, String endTime) {
        this.beginTime = beginTime;
        this.endTime = endTime;
    }

    @Override
    protected String getActionName() {
        return "app_report_charge_detail";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        parameters.put("begin_datetime", beginTime);
        parameters.put("end_datetime", endTime);
    }

    @Override
    protected Class getParseClass() {
        return Workload.class;
    }

}
