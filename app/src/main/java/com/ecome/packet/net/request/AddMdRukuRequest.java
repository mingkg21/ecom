package com.ecome.packet.net.request;

import android.text.TextUtils;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Voucher;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class AddMdRukuRequest extends AppRequest {

    private String providerId;
    private String fapiao;
    private String remark;
    private String detail;
    private String voucherNo;
    private String kuFangId;
    private String businessId;

    public AddMdRukuRequest(int providerId, String voucherNo, String fapiao, String remark, String detail, int kuFangId, int businessId) {
        this.providerId = providerId + "";
        this.voucherNo = voucherNo;
        this.fapiao = checkNull(fapiao);
        this.remark = checkNull(remark);
        this.detail = checkNull(detail);
        this.kuFangId = kuFangId + "";
        this.businessId = businessId + "";
    }

    @Override
    protected String getActionName() {
        return "app_insert_md_ruku";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", "" + UserManager.getInstance().getUserId());
        if (TextUtils.isEmpty(voucherNo)) {
            parameters.put("flag", "1");
        } else {
            parameters.put("flag", "2");
            parameters.put("voucher_no_input", voucherNo);
        }
        parameters.put("gongyingshang_id", providerId);
        parameters.put("voucher_remark", remark);
        parameters.put("voucher_fapiao", fapiao);
        parameters.put("voucher_detail", detail);
        parameters.put("kufang_cls", kuFangId);
        parameters.put("yewu_cls", businessId);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        Voucher voucher = gson.fromJson(str, Voucher.class);
        return voucher;
    }

    @Override
    protected Class getParseClass() {
        return Voucher.class;
    }
}
