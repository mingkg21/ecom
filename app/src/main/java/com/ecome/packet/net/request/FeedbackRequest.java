package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppBaseEntity;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class FeedbackRequest extends AppRequest {

    private String content;

    public FeedbackRequest(String content) {
        this.content = content;
    }

    @Override
    protected String getActionName() {
        return "app_ecom_suggest";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        parameters.put("content", content);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        AppBaseEntity appBaseEntity = gson.fromJson(str, AppBaseEntity.class);
        return appBaseEntity;
    }

    @Override
    protected Class getParseClass() {
        return AppBaseEntity.class;
    }
}
