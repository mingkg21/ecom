package com.ecome.packet.net.request;

import com.ecome.packet.entity.UserInfo;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

/**
 * Created by mingkg21 on 2017/10/23.
 */

public class RegisterRequest extends AppRequest {

    private String name;
    private String adminName;
    private String phone;
    private String introducerId;
    private String picPath;

    public RegisterRequest(String name, String adminName, String phone, String introducerId, String picPath) {
        this.name = name;
        this.adminName = adminName;
        this.phone = phone;
        this.introducerId = introducerId;
        this.picPath = picPath;
    }

    @Override
    protected String getActionName() {
        return "app_create_fendian";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("cl_name", name);
        parameters.put("admin_name", adminName);
        parameters.put("admin_tel", phone);
        parameters.put("jieshaoren_id", introducerId);
        parameters.put("pic_0", picPath);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        UserInfo userInfo = gson.fromJson(str, UserInfo.class);
        return userInfo;
    }

    @Override
    protected Class getParseClass() {
        return UserInfo.class;
    }
}
