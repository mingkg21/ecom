package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.AppBaseEntity;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class ModifyPasswordRequest extends AppRequest {

    private String oldPwd;
    private String newPwd;
    private String newPwd2;

    public ModifyPasswordRequest(String oldPwd, String newPwd, String newPwd2) {
        this.oldPwd = oldPwd;
        this.newPwd = newPwd;
        this.newPwd2 = newPwd2;
    }

    @Override
    protected String getActionName() {
        return "app_alter_pwd";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", UserManager.getInstance().getUserId() + "");
        parameters.put("old_pwd", oldPwd);
        parameters.put("new_pwd", newPwd);
        parameters.put("new_pwd2", newPwd2);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        AppBaseEntity result = gson.fromJson(str, AppBaseEntity.class);
        return result;
    }

    @Override
    protected Class getParseClass() {
        return AppBaseEntity.class;
    }
}
