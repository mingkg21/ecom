package com.ecome.packet.net.request;

import com.ecome.packet.db.UserManager;
import com.ecome.packet.entity.Staff;

import java.util.Map;

public class GetDoctorRequest extends PageRequest {

    @Override
    protected String getActionName() {
        return "app_get_dor_list";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("login_id", String.valueOf(UserManager.getInstance().getUserId()));
    }

    @Override
    protected Class getParseClass() {
        return Staff.class;
    }
}
