package com.ecome.packet.net.request;

import com.ecome.packet.entity.Registration;

import java.util.Map;

public class GetPatientHistoryRegistrationRequest extends PageRequest {

    private String id;

    public GetPatientHistoryRegistrationRequest(String id) {
        this.id = id;
    }

    @Override
    protected String getActionName() {
        return "app_get_pat_history_guahao";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("pid", id);
    }

    @Override
    protected Class getParseClass() {
        return Registration.class;
    }

}
