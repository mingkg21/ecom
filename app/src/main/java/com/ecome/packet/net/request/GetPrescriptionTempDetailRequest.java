package com.ecome.packet.net.request;

import com.ecome.packet.entity.TempDetail;

import java.util.Map;

public class GetPrescriptionTempDetailRequest extends PageRequest {

    private String id;

    public GetPrescriptionTempDetailRequest(int id) {
        this.id = String.valueOf(id);
    }

    @Override
    protected String getActionName() {
        return "app_get_chufang_temp_detail";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("temp_id", id);
    }

    @Override
    protected Class getParseClass() {
        return TempDetail.class;
    }

}
