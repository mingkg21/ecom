package com.ecome.packet.net.request;

import com.ecome.packet.entity.AppBaseEntity;
import com.google.gson.Gson;
import com.mk.core.entity.BaseEntity;

import java.util.Map;

public class ModifyDoctorWorkRequest extends AppRequest {

    private String doctorId;
    private String morningTimeStart;
    private String morningTimeEnd;
    private String afternoonTimeStart;
    private String afternoonTimeEnd;
    private String nightTimeStart;
    private String nightTimeEnd;

    private String day1Morning;
    private String day1Afternoon;
    private String day1Night;

    private String day2Morning;
    private String day2Afternoon;
    private String day2Night;

    private String day3Morning;
    private String day3Afternoon;
    private String day3Night;

    private String day4Morning;
    private String day4Afternoon;
    private String day4Night;

    private String day5Morning;
    private String day5Afternoon;
    private String day5Night;

    private String day6Morning;
    private String day6Afternoon;
    private String day6Night;

    private String day7Morning;
    private String day7Afternoon;
    private String day7Night;

    public ModifyDoctorWorkRequest(int doctorId, String morningTimeStart, String morningTimeEnd,
                                   String afternoonTimeStart, String afternoonTimeEnd, String nightTimeStart,
                                   String nightTimeEnd, String day1Morning, String day1Afternoon,
                                   String day1Night, String day2Morning, String day2Afternoon,
                                   String day2Night, String day3Morning, String day3Afternoon,
                                   String day3Night, String day4Morning, String day4Afternoon,
                                   String day4Night, String day5Morning, String day5Afternoon,
                                   String day5Night, String day6Morning, String day6Afternoon,
                                   String day6Night, String day7Morning, String day7Afternoon, String day7Night) {

        this.doctorId = doctorId + "";
        this.morningTimeStart = morningTimeStart;
        this.morningTimeEnd = morningTimeEnd;
        this.afternoonTimeStart = afternoonTimeStart;
        this.afternoonTimeEnd = afternoonTimeEnd;
        this.nightTimeStart = nightTimeStart;
        this.nightTimeEnd = nightTimeEnd;
        this.day1Morning = day1Morning;
        this.day1Afternoon = day1Afternoon;
        this.day1Night = day1Night;
        this.day2Morning = day2Morning;
        this.day2Afternoon = day2Afternoon;
        this.day2Night = day2Night;
        this.day3Morning = day3Morning;
        this.day3Afternoon = day3Afternoon;
        this.day3Night = day3Night;
        this.day4Morning = day4Morning;
        this.day4Afternoon = day4Afternoon;
        this.day4Night = day4Night;
        this.day5Morning = day5Morning;
        this.day5Afternoon = day5Afternoon;
        this.day5Night = day5Night;
        this.day6Morning = day6Morning;
        this.day6Afternoon = day6Afternoon;
        this.day6Night = day6Night;
        this.day7Morning = day7Morning;
        this.day7Afternoon = day7Afternoon;
        this.day7Night = day7Night;
    }

    @Override
    protected String getActionName() {
        return "app_alter_dor_work";
    }

    @Override
    protected void setParameters(Map<String, String> parameters) {
        parameters.put("dor_id", doctorId);
        parameters.put("kanzhen_min", "5");
        parameters.put("morning_time1", morningTimeStart);
        parameters.put("morning_time2", morningTimeEnd);
        parameters.put("afternon_time1", afternoonTimeStart);
        parameters.put("afternon_time2", afternoonTimeEnd);
        parameters.put("night_time1", nightTimeStart);
        parameters.put("night_time2", nightTimeEnd);

        parameters.put("week_1_1", day1Morning);
        parameters.put("week_1_2", day1Afternoon);
        parameters.put("week_1_3", day1Night);

        parameters.put("week_2_1", day2Morning);
        parameters.put("week_2_2", day2Afternoon);
        parameters.put("week_2_3", day2Night);

        parameters.put("week_3_1", day3Morning);
        parameters.put("week_3_2", day3Afternoon);
        parameters.put("week_3_3", day3Night);

        parameters.put("week_4_1", day4Morning);
        parameters.put("week_4_2", day4Afternoon);
        parameters.put("week_4_3", day4Night);

        parameters.put("week_5_1", day5Morning);
        parameters.put("week_5_2", day5Afternoon);
        parameters.put("week_5_3", day5Night);

        parameters.put("week_6_1", day6Morning);
        parameters.put("week_6_2", day6Afternoon);
        parameters.put("week_6_3", day6Night);

        parameters.put("week_7_1", day7Morning);
        parameters.put("week_7_2", day7Afternoon);
        parameters.put("week_7_3", day7Night);
    }

    @Override
    protected BaseEntity parseEntity(Gson gson, String str) {
        AppBaseEntity entity = gson.fromJson(str, AppBaseEntity.class);
        return entity;
    }

    @Override
    protected Class getParseClass() {
        return AppBaseEntity.class;
    }

}
