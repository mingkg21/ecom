package com.mk.core.entity;

import java.util.ArrayList;

/** 请求数据列表响应类
 * Created by mingkg21 on 2017/8/26.
 */

public abstract class AbsListEntity<Data> extends BaseEntity {

    private static final long serialVersionUID = -6896355260101592297L;

    public abstract ArrayList<Data> getDataList();

    public abstract boolean hasNext();

    public abstract int getNextStartPos();
}
