package com.mk.core.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/** 请求数据列表响应类
 * Created by mingkg21 on 2017/8/26.
 */

public class BaseListEntity<Data> extends AbsListEntity<Data> {

    private static final long serialVersionUID = -2399439966718912344L;

    @SerializedName("page_no")
    private int pageNo;

    @SerializedName("page_size")
    private int pageSize;

    @SerializedName("total_num")
    private int totalNum;

    @SerializedName("total_page")
    private int totalPage;

    @SerializedName("count")
    private int count;

    @SerializedName("next")
    private int next;

    @SerializedName("list")
    private ArrayList<Data> dataList;

    public int getPageNo() {
        return pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public int getCount() {
        return count;
    }

    public int getNext() {
        return next;
    }

    @Override
    public ArrayList<Data> getDataList() {
        return dataList;
    }

    @Override
    public boolean hasNext() {
        return next > 0;
    }

    @Override
    public int getNextStartPos() {
        return next + 1;
    }
}
