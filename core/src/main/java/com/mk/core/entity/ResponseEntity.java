package com.mk.core.entity;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/** 请求数据响应类
 * Created by mingkg21 on 2017/8/25.
 */

public class ResponseEntity<Data extends Serializable> extends AbsResponseEntity<Data> {

    private static final long serialVersionUID = -4378844811166132960L;

    @SerializedName("code")
    private int code;

    @SerializedName("status")
    private String status;

    @SerializedName("msg")
    private String msg;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private Data data;

    public int getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }

    public String getMsg() {
        if (!TextUtils.isEmpty(msg)) {
            return msg;
        }
        return message;
    }

    @Override
    public Data getData() {
        return data;
    }

    @Override
    public boolean isSuccess() {
        return code >= 200 && code < 300;
    }

    public boolean isRelogin() {
        return "-1".equals(code);
    }
}
