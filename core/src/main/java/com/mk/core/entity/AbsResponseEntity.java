package com.mk.core.entity;

import java.io.Serializable;

/** 请求数据响应类
 * Created by mingkg21 on 2017/8/25.
 */

public abstract class AbsResponseEntity<Data> implements Serializable {

    private static final long serialVersionUID = -3988173519592618499L;

    public abstract Data getData();

    public abstract boolean isSuccess();

}
