package com.mk.core.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Parcelable;
import android.text.Editable;
import android.text.SpanWatcher;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ScrollView;

import com.mk.core.util.DimensionUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by mingkg21 on 2017/11/22.
 */

public class HtmlTextView extends EditText implements ISpanDrawableView, ViewTreeObserver.OnScrollChangedListener {

    private ChangeWatcher mChangeWatcher;
    private ArrayList<AsyncDrawableSpan> mDrawableSpans;
    private HashMap<AsyncDrawableSpan, Integer> mDrawableHeights;

    private View mScrollView;
    private View mAbsListViewItem;
    private int mAbsListViewItemTop;

    private int mImgPadding = 0;
    private int mPaddingLeft;
    private int mPaddingRight;
    private int mPaddingTop;
    private int mPaddingBottom;
    private int mLastWidthMeasure;

    private boolean isRequestViewLayout;

    private boolean mTouching = false;
    private boolean isFill = true;

    public HtmlTextView(Context context) {
        super(context);
        init();
    }

    public HtmlTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {

        mChangeWatcher = new ChangeWatcher();
        mDrawableSpans = new ArrayList<>();
        mDrawableHeights = new HashMap<>();
//        getText().setSpan(mChangeWatcher, 0, 0, Spanned.SPAN_INCLUSIVE_INCLUSIVE | (100 << Spanned.SPAN_PRIORITY_SHIFT));

        addTextChangedListener(new TextWatcher() {
            int needAddIndex = -2;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (count > 0) {
                    Object[] spans = getText().getSpans(start, start + count, Object.class);
                    if (spans != null) {
                        for (Object span : spans) {
                            if (span instanceof AsyncDrawableSpan) {
                                getText().removeSpan(span);
                            }
//                            else if(span.equals(mHeadHintSpan)){
//                                removeHeadHintSpan();
//                            }
                        }
                        invalidate();
                    }
                }
                if (needAddIndex == -1) {
                    needAddIndex = -2;
                } else {
                    if (start < s.length() && after > 0 && s.charAt(start) == '\uFFFC') {
                        needAddIndex = start + after;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (needAddIndex > 0) {
                    int index = needAddIndex;
                    needAddIndex = -1;
                    s.insert(index, "\n");
                    setSelection(index);
                }
            }
        });
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        getViewTreeObserver().addOnScrollChangedListener(this);
//        for (AsyncDrawableSpan span : mDrawableSpans) {
//            span.onAttachedToWindow();
//        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getViewTreeObserver().removeOnScrollChangedListener(this);
//        for (AsyncDrawableSpan span : mDrawableSpans) {
//            span.onDetachedFromWindow();
//        }
    }

    public void addImage(Bitmap bitmap, String url) {
        addImage(bitmap, url, 0, 0, false);
    }

    private void addImage(Bitmap bitmap, String url, int width, int height, boolean isInit) {
        int start = 0;
        if (isInit) {
            start = getText().length();
        } else {
            start = getSelectionStart();
        }
        boolean isEnd = start == getText().length();
        if (start > 0 && getText().charAt(start - 1) != '\n' && getText().charAt(start - 1) != '\uFFFC' && isFill) {
            getText().insert(start, "\n");
            start++;
        }
        if (start < 0) {
            start = 0;
        }
        String addStr = "\uFFFC";
        getText().insert(start, addStr);
        AsyncDrawableSpan span = null;
        if (bitmap != null) {
            span = new AsyncDrawableSpan(bitmap, url, this);
        } else {
            span = new AsyncDrawableSpan(url, width, height, this);
        }
        if (!isFill) {
            int padding = DimensionUtil.DIPToPX(1);
            span.setPadding(padding, padding, padding, padding);
        } else {
            int padding = DimensionUtil.DIPToPX(mImgPadding);
            span.setPadding(padding, DimensionUtil.DIPToPX(5), padding, DimensionUtil.DIPToPX(5));
        }
        getText().setSpan(span, start, start + addStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        if (!isInit && isEnd && isFill) {
            getText().insert(start + addStr.length(), "\n");
        }
        if (!isInit) {
            requestLayout();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean hand = false;
        mTouching = true;
        if(!hand){
            hand = super.onTouchEvent(event);
        }
        mTouching = false;
        return hand;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        mPaddingLeft = getPaddingLeft();
        mPaddingRight = getPaddingRight();
        mPaddingTop = getPaddingTop();
        mPaddingBottom = getPaddingBottom();
        int newLastWidthMeasure = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec) - getPaddingLeft() - getPaddingRight();
        if (mLastWidthMeasure != newLastWidthMeasure) {
            mLastWidthMeasure = newLastWidthMeasure;
            if (!mDrawableSpans.isEmpty()) {
                requestViewLayout(mDrawableSpans.get(0));
            }
        }
        try {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } catch (Exception e) {
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        findScrollView(this);
    }

    @Override
    public void invalidate() {
        super.invalidate();
        Log.v("AsyncDrawableSpan", "invalidate");
    }

    private void findScrollView(View view) {
        if (view instanceof ScrollView) {
            mScrollView = (ScrollView) view;
            return;
        } else if (view.getParent() instanceof AbsListView) {
            mAbsListViewItem = view;
            return;
        } else if (view.getParent() != null && view.getParent() instanceof View) {
            findScrollView((View) view.getParent());
        }
        if (view.equals(this)) {
            mAbsListViewItemTop = 0;
        }
        mAbsListViewItemTop += view.getTop();
    }

    private int findScrollY(View view) {
        if (mScrollView != null) {
            return mScrollView.getScrollY();
        } else if (mAbsListViewItem != null) {
            return -mAbsListViewItemTop - mAbsListViewItem.getTop();
        }
        return getScrollY();
    }

    @Override
    public void requestViewLayout(final Object span) {
        post(new Runnable() {
            @Override
            public void run() {
                if (mDrawableSpans.contains(span)) {
                    isRequestViewLayout = true;
                    int start = getText().getSpanStart(span);
                    int end = getText().getSpanEnd(span);
                    int flags = getText().getSpanFlags(span);
                    if (start >= 0) {
                        char charAt = getText().charAt(start);
                        getText().delete(start, start + 1);
                        getText().insert(start, String.valueOf(charAt));
                        getText().setSpan(span, start, end, flags);
                    }
                    isRequestViewLayout = false;
                }
            }
        });
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int scrollY = findScrollY(this);
        for (AsyncDrawableSpan span : mDrawableSpans) {
            span.drawLastLocal(canvas, scrollY, getViewHeight());
        }
        super.dispatchDraw(canvas);
    }

    private int getViewHeight(){
        if(mDrawableSpans.size() <= 2){//XXX listView Item View 不是第一项的时候计算释放图片有误，临时解决方案
            return Integer.MAX_VALUE / 2;
        }
        if(mScrollView != null){
            if(mScrollView.getHeight() < getHeight()){
                return mScrollView.getHeight();
            }
        }
        if(mAbsListViewItem != null){
            View parentView = (View) mAbsListViewItem.getParent();
            if(parentView.getHeight() < getHeight()){
                return parentView.getHeight();
            }
        }
        return getHeight();
    }

    @Override
    public void onScrollChanged() {
        invalidate();
    }

    private class ChangeWatcher implements SpanWatcher {
        @Override
        public void onSpanAdded(Spannable text, Object what, int start, int end) {
            if (isRequestViewLayout) {
                return;
            }
            if (what instanceof AsyncDrawableSpan && !mDrawableSpans.contains(what)) {
                AsyncDrawableSpan span = (AsyncDrawableSpan) what;
//                span.onAttachedToWindow();
                mDrawableSpans.add(span);
                mDrawableHeights.put(span, span.getHeight());
            }
        }

        @Override
        public void onSpanRemoved(Spannable text, Object what, int start, int end) {
            if (isRequestViewLayout) {
                return;
            }
            if (what instanceof AsyncDrawableSpan && mDrawableSpans.contains(what)) {
//                ((ImageSpan) what).onDetachedFromWindow();
                mDrawableSpans.remove(what);
                mDrawableHeights.remove(what);
            }
        }

        @Override
        public void onSpanChanged(Spannable text, Object what, int ostart,
                                  int oend, int nstart, int nend) {
        }
    }

    @Override
    public int getViewPaddingLeft() {
        return mPaddingLeft;
    }

    @Override
    public int getViewPaddingRight() {
        return mPaddingRight;
    }

    @Override
    public int getViewPaddingTop() {
        return mPaddingTop;
    }

    @Override
    public int getViewPaddingBottom() {
        return mPaddingBottom;
    }

    @Override
    public int getMaxViewHeight() {
        if(isFill){
            return Integer.MAX_VALUE;
        }else{
        return 0;
        }
    }

    @Override
    public int getMaxViewWidth() {
        if (getLayout() == null) {
            if (getMeasuredWidth() == 0) {
                return mLastWidthMeasure;
            } else {
                return getMeasuredWidth();
            }
        } else {
            return getLayout().getWidth();
        }
    }

    @Override
    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
//		super.dispatchSaveInstanceState(container);
    }

    @Override
    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
//		super.dispatchRestoreInstanceState(container);
    }

    public ArrayList<AsyncDrawableSpan> getDrawableSpans() {
        ArrayList<AsyncDrawableSpan> spanArrayList = new ArrayList<>();
        AsyncDrawableSpan[] spans = getText().getSpans(0, getText().length(), AsyncDrawableSpan.class);
        for (AsyncDrawableSpan span : spans) {
            spanArrayList.add(span);
        }
        return spanArrayList;
    }

    public void setHtml(String html) {
        if (TextUtils.isEmpty(html)) {
            getText().clear();
            return;
        }
        int mStepSize = 0;
        getText().clear();
        ArrayList<MixDataInfo> mixDataInfos = MixDataInfo.getMixDataInfos(html);
        if (!mixDataInfos.isEmpty()) {
            int i = 0;
            boolean isImg = true;
            for (MixDataInfo mixDataInfo : mixDataInfos) {
                if(!TextUtils.isEmpty(mixDataInfo.getText())){
                    Editable faceText = new SpannableStringBuilder();
                    faceText.append(mixDataInfo.getText());
                    if(faceText != null){
                        if(!isImg){
                            getText().append("\n");
                        }
                        getText().append(faceText);
                    }
                    isImg = false;
                }else if(!TextUtils.isEmpty(mixDataInfo.getImgUrl())){
                    addImage(null, mixDataInfo.getImgUrl(), mixDataInfo.getImgW(), mixDataInfo.getImgH(),true);
                    if(i == mixDataInfos.size() - 1 && isEnabled()){
                        getText().append("\n");
                    }
                    isImg = true;
                }
                i++;
            }
        } else {
            getText().append(html != null ? html : "");
        }
        requestLayout();
    }

    public String getHtml(){
        StringBuffer html = new StringBuffer();
        AsyncDrawableSpan[] spans = getText().getSpans(0, getText().length(), AsyncDrawableSpan.class);
        LinkedList<DrawableSpanInfo> spanList = new LinkedList<DrawableSpanInfo>();
        if(spans != null){
            for (AsyncDrawableSpan span : spans) {
                int star = getText().getSpanStart(span);
                int end = getText().getSpanEnd(span);
                DrawableSpanInfo spanInfo = new DrawableSpanInfo(span,star,end);
                int location = spanList.size();
                for (int i = 0;i < spanList.size();i++) {
                    DrawableSpanInfo drawableSpanInfo = spanList.get(i);
                    if(star < drawableSpanInfo.mStart){
                        location = i;
                        break;
                    }
                }
                spanList.add(location, spanInfo);
            }
        }
        int j = 0;
        for (DrawableSpanInfo spanInfo : spanList) {
            AsyncDrawableSpan span = spanInfo.mSpan;
            int star = spanInfo.mStart;
            int end = spanInfo.mEnd;
            if(j < star){
                CharSequence text = getText().subSequence(j, star);
                if(text.length() > 0 && text.charAt(text.length() - 1) == '\n'){
                    text = text.subSequence(0, text.length() - 1);
                }
                if(text.length() > 0){
                    html.append(textDataToString(text.toString()));
                }
            }
            html.append(photoDataToString(span.getNetUrl(), span.getImgWidth(), span.getImgHeight()));
            j = end;
        }
        if(j < getText().length()){
            CharSequence text = getText().subSequence(j, getText().length());
            if(text.length() > 0){
                if(text.charAt(0) == '\n'){
                    text = text.subSequence(1, text.length());
                }
                if(text.length() > 0){
                    html.append(textDataToString(text.toString()));
                }
            }
        }
        html.trimToSize();
        return html.toString();
    }

    public static String textDataToString(String text){
        StringBuffer html = new StringBuffer();
        html.append("<p>");
        html.append(formatCDATA(text));
        html.append("</p>");
        return html.toString();
    }

    public static String formatCDATA(CharSequence str){
        return filterPlaceholder(str);
    }

    public static String filterPlaceholder(CharSequence text){
        StringBuffer sb = new StringBuffer();
        int serialSpaceSize = 0;
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if(c != '\uFFFC'){
                if(c == '&'){
                    sb.append("＆");
                }else if(c == '<'){
                    sb.append("《");
                }else if(c == '>'){
                    sb.append("》");
                }else if(c == ' '){
                    serialSpaceSize++;
                    sb.append(c);
                    if(serialSpaceSize == 2){
                        sb.replace(sb.length() - 2, sb.length(), "　");
                        serialSpaceSize = 0;
                    }
                }else if(c == 9){
                    sb.append("　　");
                }else{
                    sb.append(c);
                }
            }
            if(c != ' '){
                serialSpaceSize = 0;
            }
        }
        return sb.toString();
    }

    public static String photoDataToString(String url, int w, int h){
        StringBuffer html = new StringBuffer();
        html.append("<p>");
//        html.append("<img src='"+formatAttribute(url)+"'/>");
        html.append("<img src='"+formatAttribute(url)+"' width='"+w+"' height='"+h+"'/>");
        html.append("</p>");
        return html.toString();
    }

    public static String formatAttribute(String str){
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if(c == '&'){
                sb.append("&amp;");
            }else if(c == '<'){
                sb.append("&lt;");
            }else if(c == '>'){
                sb.append("&gt;");
            }else if(c == '"'){
                sb.append("&quot;");
            }else if(c == '\''){
                sb.append("&apos;");
            }else{
                sb.append(c);
            }
        }
        return sb.toString();
    }

    private class DrawableSpanInfo{
        private AsyncDrawableSpan mSpan;
        private int mStart;
        private int mEnd;

        public DrawableSpanInfo(AsyncDrawableSpan span, int star, int end) {
            mSpan = span;
            mStart = star;
            mEnd = end;
        }
    }
}
