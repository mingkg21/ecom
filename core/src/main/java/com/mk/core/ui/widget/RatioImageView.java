package com.mk.core.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.mk.core.R;

/**
 * Created by mingkg21 on 2017/9/30.
 */

public class RatioImageView extends ImageView {

    protected int mRatioX = -1;
    protected int mRatioY = -1;

    public RatioImageView(Context context) {
        super(context);
    }

    public RatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RatioImageView);
        mRatioX = typedArray.getInt(R.styleable.RatioImageView_ratioX, -1);
        mRatioY = typedArray.getInt(R.styleable.RatioImageView_ratioY, -1);
        typedArray.recycle();
    }

    public void setRatio(float x, float y) {
        mRatioX = (int) (x * 10);
        mRatioY = (int) (y * 10);
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        if (mRatioY > 0 && mRatioX > 0) {
            if (widthMode == MeasureSpec.EXACTLY) {
                heightMeasureSpec = MeasureSpec.makeMeasureSpec(widthSize * mRatioY / mRatioX, MeasureSpec.EXACTLY);
            } else if (heightMode == MeasureSpec.EXACTLY) {
                widthMeasureSpec = MeasureSpec.makeMeasureSpec(heightSize * mRatioX / mRatioY, MeasureSpec.EXACTLY);
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
