package com.mk.core.ui.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by mingkg21 on 2017/8/26.
 */

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

    protected Context context;
    protected BaseAdapter.OnItemClickListener mOnItemClickListener;

    public BaseViewHolder(View itemView) {
        super(itemView);

        context = itemView.getContext();
    }

    public void bind(final T data, final int position) {
        onBind(data, position);
        if (mOnItemClickListener != null) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickListener.onItemClick(itemView, position, data);
                }
            });
        }
    }

    public abstract void onBind(T data, int position);

    public void setOnItemClickListener(BaseAdapter.OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    protected <VT extends View> VT findViewById(int id) {
        if (itemView == null)
            throw new NullPointerException("Fragment content view is null.");
        VT view = (VT) itemView.findViewById(id);
//        if (view == null)
//            throw new NullPointerException("This resource id is invalid.");
        return view;
    }

    protected Context getContext() {
        return context;
    }

    protected String getString(int resId) {
        return context.getString(resId);
    }

    protected String getString(int resId, Object... formatArgs) {
        return context.getString(resId, formatArgs);
    }

}
