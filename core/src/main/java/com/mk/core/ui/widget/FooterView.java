package com.mk.core.ui.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mk.core.R;

/**
 * Created by mingkg21 on 2017/11/16.
 */

public class FooterView extends LinearLayout {

    private TextView mTipTV;

    private ProgressBar mProgressBar;

    public FooterView(Context context) {
        super(context);
        init();
    }

    public FooterView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_common_footer_view, this);

        mProgressBar = (ProgressBar) findViewById(R.id.layout_common_footer_view_pb);
        mTipTV = (TextView) findViewById(R.id.layout_common_footer_view_tip);
    }

    public void showLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
        mTipTV.setVisibility(View.GONE);
    }

    public void showTip(String tip) {
        mProgressBar.setVisibility(View.GONE);
        mTipTV.setVisibility(View.VISIBLE);

        mTipTV.setText(tip);
    }

}
