package com.mk.core.ui.widget.swipe;

public interface SwipeToTouchAction extends SwipeToTouchCheckAction {

    void startScroll(int startX, int dx, boolean finish);

    void onTouchDown();

    void onScroll();

}
