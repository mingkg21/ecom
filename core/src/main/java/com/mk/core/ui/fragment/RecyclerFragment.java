package com.mk.core.ui.fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.mk.core.R;
import com.mk.core.entity.ResponseEntity;
import com.mk.core.model.PagingLoadModel;
import com.mk.core.ui.widget.BaseAdapter;
import com.mk.core.ui.widget.FooterView;
import com.mk.core.ui.widget.RecycleViewDivider;
import com.mk.core.util.ToastUtil;

import java.util.List;

/**
 * Created by mingkg21 on 2017/8/26.
 */

public abstract class RecyclerFragment<T> extends BaseFragment {

    protected RecyclerView mRecyclerView;
    protected BaseAdapter<T> mAdapter;
    private RecyclerView.LayoutManager mLinearLayoutManager;
    private RecycleViewDivider mRecycleViewDivider;
    private FooterView mFooterView;

    protected SwipeRefreshLayout mSwipeRefreshLayout;

    protected PagingLoadModel mPagingLoadModel;

    private boolean mIsInit;
    private boolean mIsLoading;
    private boolean mIsPage = true;
    private int visibleThreshold = 5;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_common_list;
    }

    protected int getRecyclerId() {
        return R.id.common_list_rv;
    }

    protected int getSwipeRefreshResId() {
        return R.id.common_list_srl;
    }

    @Override
    protected int getLoadingLayoutResId() {
        return getRecyclerId();
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        mRecyclerView = (RecyclerView) view.findViewById(getRecyclerId());
        mRecyclerView.setVisibility(View.INVISIBLE);
        mLinearLayoutManager = getLayoutManager();
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecycleViewDivider = new RecycleViewDivider(getContext(), LinearLayoutManager.HORIZONTAL, 1, getResources().getColor(R.color.common_gray));
        mRecyclerView.addItemDecoration(mRecycleViewDivider);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!mIsPage) {
                    return;
                }
                super.onScrolled(recyclerView, dx, dy);

                if (mLinearLayoutManager instanceof LinearLayoutManager) {
                    int totalItemCount = mLinearLayoutManager.getItemCount();
                    int lastVisibleItemPosition = ((LinearLayoutManager) mLinearLayoutManager).findLastVisibleItemPosition();

                    if (mIsInit && !mIsLoading && totalItemCount <= (lastVisibleItemPosition + visibleThreshold)) {
                        if (mPagingLoadModel != null && mPagingLoadModel.hasData()) {
                            mPagingLoadModel.loadNextPage();
                            mIsLoading = true;
                        }
                    }
                }
            }
        });
        mAdapter = getAdapter();
        if (mAdapter != null) {
            mRecyclerView.setAdapter(mAdapter);
            if (hasFooterView()) {
                mFooterView = new FooterView(getContext());
                mAdapter.setFooterView(mFooterView);
            }

        }

        if (getSwipeRefreshResId() > 0) {
            mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(getSwipeRefreshResId());
            mSwipeRefreshLayout.setEnabled(canRefresh());
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    loadData();
                }
            });
        }

        mPagingLoadModel = getPagingLoadModel();
        if (mPagingLoadModel != null) {
            mPagingLoadModel.addCallback(this);
        }
    }

    protected abstract BaseAdapter<T> getAdapter();

    protected abstract PagingLoadModel getPagingLoadModel();

    protected boolean hasFooterView() {
        return true;
    }

    protected boolean canRefresh() {
        return true;
    }

    public void setPage(boolean page) {
        mIsPage = page;
    }

    protected void setDividerColor(int color) {
        mRecycleViewDivider.setDividerColor(color);
    }

    protected void setHasTopLine(boolean hasTopLine) {
        mRecycleViewDivider.setHasTopLine(hasTopLine);
    }

    protected void setDividerHeight(int dividerHeight) {
        mRecycleViewDivider.setDividerHeight(dividerHeight);
    }

    protected void setTopDividerHeight(int topDividerHeight) {
        mRecycleViewDivider.setTopDividerHeight(topDividerHeight);
    }

    protected int getGridSpanCount() {
        return 0;
    }

    protected RecyclerView.LayoutManager getLayoutManager() {
        if (getGridSpanCount() > 0) {
            final GridLayoutManager layoutManager = new GridLayoutManager(getContext(), getGridSpanCount());
            layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if (mAdapter != null && mAdapter.isHasHeaderView()) {
                        if (position == 0) {
                            return getGridSpanCount();
                        }
                    }
//
                    if (hasFooterView()) {
                        if (position == mAdapter.getItemCount() - 1) {
                            return getGridSpanCount();
                        }
                    }

                    if (mAdapter != null && mAdapter.isHasFooterView()) {
                        if (position == mAdapter.getItemCount() - 1) {
                            return getGridSpanCount();
                        }
                    }

                    return 1;
                }
            });
            return layoutManager;
        }
        return new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
    }

    @Override
    protected void loadData() {
        if (mPagingLoadModel != null) {
            mPagingLoadModel.forceLoadPage();
        }
    }

    public void setOnItemClickListener(BaseAdapter.OnItemClickListener onItemClickListener) {
        if (mAdapter != null) {
            mAdapter.setOnItemClickListener(onItemClickListener);
        }
    }

    public boolean isEmpty() {
        if (mAdapter != null) {
            return mAdapter.isEmpty();
        }
        return true;
    }

    public void setAll(List<T> datas) {
        if (mAdapter != null) {
            mAdapter.clear();
        }
        addAll(datas);
    }

    public void addAll(List<T> datas) {
        if (mAdapter != null) {
            mAdapter.addAll(datas);
        }
    }

    public void add(int position, T data) {
        if (mAdapter != null) {
            mAdapter.add(position, data);
            mAdapter.hideFooterView();
        }
    }

    public void notifyDataSetChanged() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void setHeaderView(View view) {
        if (mAdapter != null) {
            mAdapter.setHeaderView(view);
        }
    }

    public void setFooterView(View view) {
        if (mAdapter != null) {
            mAdapter.setFooterView(view);
        }
    }

    protected boolean isPagingRequest(String key) {
        if (mPagingLoadModel != null && mPagingLoadModel.isPagingRequest(key)) {
            return true;
        }
        return false;
    }

    @Override
    public void onRetry() {
        super.onRetry();
        loadData();
    }

    protected String getEmptyTip() {
        return "";
    }

    @Override
    public boolean onPreLoad(String key) {
        if (isPagingRequest(key)) {
            if (mPagingLoadModel.isEmpty()) {
                showLoadingView();
            }
        }
        return super.onPreLoad(key);
    }

    protected void stopRefresh() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public boolean onPostLoad(String key) {
        if (isPagingRequest(key)) {
            mIsInit = true;
            mIsLoading = false;
            stopRefresh();
            if (mPagingLoadModel.hasData()) {
                mAdapter.showFooterView();
                if (mFooterView != null) {
                    mFooterView.showLoading();
                }
            } else {
                mAdapter.hideFooterView();
//                if (mFooterView != null) {
//                    mFooterView.showTip("已经到底了！");
//                }
            }

            if (mPagingLoadModel.isEmpty()) {
                if (mAdapter.isHasHeaderView()) {
                    hideLoadingView();
                    if (!TextUtils.isEmpty(getEmptyTip())) {
                        if (mFooterView != null) {
                            mFooterView.showTip(getEmptyTip());
                        }
                        mAdapter.showFooterView();
                    }
                } else {
                    showEmptyView(getEmptyTip());
                }
            } else {
                hideLoadingView();
            }
            mRecyclerView.setVisibility(View.VISIBLE);
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        if (isPagingRequest(key)) {
            setAll(mPagingLoadModel.getData());
            return true;
        }
        return super.onSuccess(key, data);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (isPagingRequest(key)) {
            if (mPagingLoadModel.isEmpty()) {
                showErrorView();
                return true;
            } else {
                if (data != null && data instanceof ResponseEntity) {
                    ResponseEntity responseEntity = (ResponseEntity) data;
                    ToastUtil.showToast(responseEntity.getMsg());
                    return true;
                }
            }
        }
        return super.onFail(key, data);
    }
}
