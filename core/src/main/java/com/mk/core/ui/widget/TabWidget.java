package com.mk.core.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.TextView;

import com.mk.core.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 用padding来做item的padding
 */
public abstract class TabWidget extends TextView {

    protected float mItemWidthPadding;
    protected List<ItemBean> mBeanList;
    protected float y;
    protected float startX;
    //是否初始化高度
    protected boolean mInitLayout;

    protected int mSelectionPosition;
    protected int mPreparePosition;

    protected boolean mInitDrawable;
    protected Rect mBottomRect;
    protected float mPoint;

    public TabWidget(Context context) {
        super(context);
        init(context);
    }

    public TabWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TabWidget);
        mItemWidthPadding = typedArray.getDimension(R.styleable.TabWidget_itemPaddingWidth, 0) * 2;
        typedArray.recycle();
        init(context);
    }

    public TabWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TabWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context) {
        mBottomRect = new Rect();
        mBeanList = new ArrayList<>();
        setClickable(true);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public void setStringArray(List<String> items) {
        mInitLayout = false;
        for (String item : items) {
            ItemBean bean = new ItemBean();
            bean.mItem = item;
            bean.mRectF = new RectF();
            bean.textWidth = getPaint().measureText(item);
            addTotalWidth(bean.textWidth);
            mBeanList.add(bean);
        }
        requestLayout();
        invalidate();
    }

    public void setStringArray(String[] items) {
        setStringArray(Arrays.asList(items));
    }

    protected void addTotalWidth(float width) {

    }

    public void setPoint(int currentPosition, int position, float point) {
        mSelectionPosition = currentPosition;
        mPreparePosition = position;
        mPoint = point;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
//	   super.onDraw(canvas);
        initItemLayout();
        float x = startX;
        int offSetX = (int) startX;
        boolean isSelection = false;

        int index = mSelectionPosition;
        for (int i = 0; i < mBeanList.size(); i++) {
            ItemBean bean = mBeanList.get(i);
            int rectWidth = (int) bean.mRectF.width();

            if (mPreparePosition != mSelectionPosition) {
                index = mPreparePosition;
            }

            if (index == i) {
                isSelection = true;
                offSetX += rectWidth / 2;
                offSetX += bean.dis * mPoint;
            } else {
                if (!isSelection) {
                    offSetX += rectWidth;
                }
            }

            if (mSelectionPosition == i) {
                getPaint().setColor(getTextColors().getColorForState(new int[]{android.R.attr.state_selected}, getTextColors().getDefaultColor()));
            } else {
                getPaint().setColor(getTextColors().getDefaultColor());
            }
            x += (bean.mRectF.width() - bean.textWidth) / 2;
            canvas.drawText(bean.mItem, x, y, getPaint());
            x = bean.mRectF.right;
        }
        Drawable bottomDrawable = getCompoundDrawables()[3];
        if (bottomDrawable != null) {
            if (!mInitDrawable && getHeight() > 0) {
                mInitDrawable = true;
                int left = 0;
                int top = getHeight() - getPaddingBottom() - bottomDrawable.getIntrinsicHeight();
                int right = left + bottomDrawable.getIntrinsicWidth();
                int bottom = top + bottomDrawable.getIntrinsicHeight();
                mBottomRect.set(left, top, right, bottom);
            }
            offSetX -= bottomDrawable.getIntrinsicWidth() / 2;
            mBottomRect.offsetTo(offSetX, mBottomRect.top);
            bottomDrawable.setBounds(mBottomRect);
            bottomDrawable.draw(canvas);
        }
    }

    @Override
    public void setCompoundDrawables(Drawable leftDrawable, Drawable topDrawable, Drawable rightDrawable, Drawable bottomDrawable) {
        super.setCompoundDrawables(leftDrawable, topDrawable, rightDrawable, bottomDrawable);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        y = (getHeight() - (getPaint().ascent() + getPaint().descent())) / 2;
    }

    protected abstract void initItemLayout();

    private int mTouchSlop;
    private int mTouchPosition;
    private float mDownX;
    private float mDownY;

    private static final int DOUBLE_TAP_TIMEOUT = 200;
    private static final int TAP_TIMEOUT = 100;
    private long mDoubleClickTime = 0;
    private long mDownloadTime = 0;
    private int mClickCount = 0;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final int action = event.getAction();
        float x = event.getX();
        float y = event.getY();
        long time = System.currentTimeMillis();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mDownloadTime = time;
                if (time - mDoubleClickTime < DOUBLE_TAP_TIMEOUT) {
                    removeCallbacks(mSingleClick);
                    if (mTouchPosition != mSelectionPosition) {
                        releaseClick();
                    } else {
                        mClickCount++;
                    }
                } else if (mClickCount == 0) {
                    setPressed(true);
                    mDoubleClickTime = time;
                    postDelayed(mSingleClick, DOUBLE_TAP_TIMEOUT);
                } else {
                    releaseClick();
                    return false;
                }
                for (int i = 0; i < mBeanList.size(); i++) {
                    ItemBean itemBean = mBeanList.get(i);
                    if (itemBean.mRectF.contains(x, y)) {
                        mTouchPosition = i;
                        break;
                    }
                }
                mDownX = x;
                mDownY = y;
                break;
            case MotionEvent.ACTION_MOVE:
                if (isInView(x, y)) {
                    releaseClick();
                }
                break;
            case MotionEvent.ACTION_UP:
                if (!isPressed()) {
                    releaseClick();
                } else if (mClickCount > 0) {
                    mDoubleClick.run();
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                releaseClick();
                break;
        }
        return true;
    }

    private void releaseClick() {
        mTouchPosition = -1;
        mClickCount = 0;
        mDoubleClickTime = 0;
        setPressed(false);
        removeCallbacks(mSingleClick);
        removeCallbacks(mDoubleClick);
    }

    private OnTabWidgetAction mAction;

    public void setOnTabWidgetAction(OnTabWidgetAction action) {
        mAction = action;
    }

    private Runnable mSingleClick = new Runnable() {
        @Override
        public void run() {
            if (mAction != null) {
                mAction.setCurrentItem(mTouchPosition);
            }
            releaseClick();
        }
    };

    private Runnable mDoubleClick = new Runnable() {
        @Override
        public void run() {
            if (mAction != null) {
                mAction.gotoTop();
            }
            releaseClick();
        }
    };

    public boolean isInView(float x, float y) {
        return Math.abs(mDownX - x) >= Math.abs(mTouchSlop) || Math.abs(mDownY - y) >= Math.abs(mTouchSlop);
    }

    public interface OnTabWidgetAction {

        void setCurrentItem(int item);

        void gotoTop();

    }

    public int getCurrentScroll(int position) {
        try {
            int scroll = 0;
            for (int i = 0; i < position - 1; i++) {
                scroll += mBeanList.get(i).mRectF.width();
            }
            return scroll;
        } catch (Exception e) {
            return 0;
        }
    }

    protected class ItemBean {
        String mItem;
        float textWidth;
        RectF mRectF;
        float dis;
    }

}
