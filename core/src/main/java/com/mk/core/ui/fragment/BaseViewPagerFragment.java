package com.mk.core.ui.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.HorizontalScrollView;

import com.mk.core.R;
import com.mk.core.ui.widget.FragmentAdapter;
import com.mk.core.ui.widget.TabWidget;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mingkg21 on 2017/9/30.
 */

public abstract class BaseViewPagerFragment extends BaseFragment implements ViewPager.OnPageChangeListener, TabWidget.OnTabWidgetAction {

    protected ViewPager mViewPager;
    protected List<Fragment> mFragments;
    protected FragmentAdapter mAdapter;
    protected TabWidget mTabWidget;
    protected HorizontalScrollView mTabHSV;
    protected int mIdx = -1;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_viewpager;
    }

    protected int getViewPagerResId() {
        return R.id.fragment_viewpager_vp;
    }

    protected int getTabResId() {
        return R.id.fragment_viewpager_tab;
    }

    protected int getTabLayoutResId() {
        return R.id.fragment_viewpager_tab_layout;
    }

    @Override
    protected int getLoadingLayoutResId() {
        return R.id.fragment_viewpager_vp;
    }

    protected int getTabArrays() {
        return 0;
    }

    protected boolean isNetDataTab() {
        return false;
    }

    @Override
    protected void initView(View view) {
        super.initView(view);

        if (getViewPagerResId() != 0) {
            mViewPager = findViewById(getViewPagerResId());
            if (mViewPager != null) {
                mFragments = new ArrayList<>();
                mAdapter = new FragmentAdapter(getChildFragmentManager(), mFragments);
                mViewPager.setAdapter(mAdapter);
                mViewPager.setOnPageChangeListener(this);
                if (!isNetDataTab()) {
                    addFragments();
                    mAdapter.notifyDataSetChanged();
                    mViewPager.setOffscreenPageLimit(mFragments.size());
                    setCurrentItem(mIdx);
                }
            }
        }

        if (getTabResId() != 0) {
            mTabWidget = findViewById(getTabResId());
            if(mTabWidget != null){
                mTabWidget.setOnTabWidgetAction(this);
            }
            if (getTabArrays() > 0) {
                setTabWidgetContent(Arrays.asList(getResources().getStringArray(getTabArrays())));
            }
        }

        if (getTabLayoutResId() != 0) {
            mTabHSV = findViewById(getTabLayoutResId());
        }
    }

    protected void setNetDataTabsAndFragments(List<String> tabs, List<Fragment> fragments) {
        setTabWidgetContent(tabs);
        if (mViewPager != null) {
            mFragments.clear();
            mFragments.addAll(fragments);
            mAdapter.notifyDataSetChanged();
            mViewPager.setOffscreenPageLimit(mFragments.size());
            setCurrentItem(mIdx);
        }
    }

    protected void setTabWidgetContent(List<String> tabs) {
        if (mTabWidget != null) {
            mTabWidget.setStringArray(tabs);
            if (mTabHSV != null) {
                mTabHSV.setVisibility(View.VISIBLE);
            }
        }
    }

    public abstract void addFragments();

    @Override
    public void onRetry() {
        super.onRetry();
        loadData();
    }

    @Override
    protected void loadData() {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mTabWidget != null) {
            mTabWidget.setPoint(mViewPager.getCurrentItem(), position, positionOffset);
        }
    }

    @Override
    public void onPageSelected(int position) {
        setCurrentFragment(position);
    }

    public void setCurrentFragment(final int position) {
        if (mIdx != position) {
            setSelection(mIdx, false);
        }
        mIdx = position;
        setSelection(mIdx, true);

        if (mTabHSV != null) {
            mTabHSV.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mTabHSV.smoothScrollTo(mTabWidget.getCurrentScroll(position), 0);
                }
            }, 150);
        }
    }

    protected void setSelection(int idx, boolean show) {
        try {
            ((BaseFragment) mFragments.get(idx)).lazyLoad();
        } catch (Exception e) {
        }
    }

    protected final void addFragment(BaseFragment fragment) {
        if (mFragments != null) {
            mFragments.add(fragment);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void setCurrentItem(int item) {
        if (mViewPager != null) {
            mViewPager.setCurrentItem(item);
        }
    }

    @Override
    public void gotoTop() {

    }

    protected final int getCurrentItem() {
        if (mViewPager != null) {
            return mViewPager.getCurrentItem();
        }
        return -1;
    }

    protected Fragment getCurrentFragment() {
        if (mFragments != null) {
            return mFragments.get(getCurrentItem());
        }
        return null;
    }

    protected boolean isViewPagerModelKey(String key) {
        return false;
    }

    @Override
    public boolean onPreLoad(String key) {
        if (isViewPagerModelKey(key)) {
            showLoadingView();
            return true;
        }
        return super.onPreLoad(key);
    }

    @Override
    public boolean onPostLoad(String key) {
        if (isViewPagerModelKey(key)) {
            hideLoadingView();
            return true;
        }
        return super.onPostLoad(key);
    }

    @Override
    public boolean onFail(String key, Object data) {
        if (isViewPagerModelKey(key)) {
            showErrorView();
            return true;
        }
        return super.onFail(key, data);
    }
}
