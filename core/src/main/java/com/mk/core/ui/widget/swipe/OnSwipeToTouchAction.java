package com.mk.core.ui.widget.swipe;

interface OnSwipeToTouchAction {

    void startScroll(int startX, int dx, boolean finish);

    void onScroll();

}
