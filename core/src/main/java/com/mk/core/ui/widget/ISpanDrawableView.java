package com.mk.core.ui.widget;

import android.content.res.Resources;

/**
 * Created by mingkg21 on 2017/11/22.
 */

public interface ISpanDrawableView {
    public void invalidate();
    public int getViewPaddingLeft();
    public int getViewPaddingRight();
    public int getViewPaddingTop();
    public int getViewPaddingBottom();
    public int getMaxViewWidth();
    public int getMaxViewHeight();
    public void requestViewLayout(Object span);
    public Resources getResources();
}
