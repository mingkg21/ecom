package com.mk.core.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.mk.core.R;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class AlertDialog extends Dialog {

    private DoneListener mDoneListener;

    public AlertDialog(@NonNull Context context, String title, String content) {
        super(context);

        setContentView(R.layout.dialog_alert);

        TextView titleTV = (TextView) findViewById(R.id.dialog_alert_title);
        TextView contentTV = (TextView) findViewById(R.id.dialog_alert_content);

        titleTV.setText(title);
        contentTV.setText(content);

        findViewById(R.id.dialog_alert_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDoneListener != null) {
                    mDoneListener.onDone(null);
                }
                dismiss();
            }
        });

        findViewById(R.id.dialog_alert_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void setDoneListener(DoneListener doneListener) {
        mDoneListener = doneListener;
    }

    public interface DoneListener {
        void onDone(String content);
    }

}
