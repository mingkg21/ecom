package com.mk.core.ui.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mk.core.R;

/**
 * Created by mingkg21 on 2017/8/28.
 */

public class LoadingView extends RelativeLayout {

    private View mLoadingLayout;
    private View mErrorLayout;
    private View mEmptyLayout;

    private TextView mEmptyView;

    private OnLoadingListener mOnLoadingListener;

    public LoadingView(Context context) {
        super(context);
        init();
    }

    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.layout_loading_view, this);

        mLoadingLayout = findViewById(R.id.layout_loading_view_loading_layout);
        mErrorLayout = findViewById(R.id.layout_loading_view_err_layout);
        mEmptyLayout = findViewById(R.id.layout_loading_view_empty_layout);

        mEmptyView = (TextView) findViewById(R.id.layout_loading_view_empty);

        mErrorLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnLoadingListener != null) {
                    mOnLoadingListener.onRetry();
                }
            }
        });
    }

    public void showLoding() {
        mLoadingLayout.setVisibility(View.VISIBLE);
        mErrorLayout.setVisibility(View.GONE);
        mEmptyLayout.setVisibility(View.GONE);
    }

    public void showError() {
        mLoadingLayout.setVisibility(View.GONE);
        mErrorLayout.setVisibility(View.VISIBLE);
        mEmptyLayout.setVisibility(View.GONE);
    }

    public void showEmpty(String tip) {
        mLoadingLayout.setVisibility(View.GONE);
        mErrorLayout.setVisibility(View.GONE);
        mEmptyLayout.setVisibility(View.VISIBLE);

        mEmptyView.setText(TextUtils.isEmpty(tip) ? "暂无数据" : tip);
    }

    public void hide() {
        mLoadingLayout.setVisibility(View.GONE);
        mErrorLayout.setVisibility(View.GONE);
        mEmptyLayout.setVisibility(View.GONE);
    }

    public void attachView(View view, int childId) {
        try {
            View contentView = view.findViewById(childId);
            ViewGroup viewGroup = (ViewGroup) contentView.getParent();
            if (viewGroup == null) {
                throw new Exception("the view must has a parent");
            }
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                if (child.getId() == childId) {
                    viewGroup.removeView(contentView);
                    viewGroup.addView(this, i, contentView.getLayoutParams());
                    break;
                }
            }
            addView(contentView, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void attachViewForFragment(View view, int childId) {
        try {
            View contentView = view.findViewById(childId);
            ViewGroup viewGroup = (ViewGroup) contentView.getParent();
            if (viewGroup == null) {
                addView(contentView, 0);
                return;
            }
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View child = viewGroup.getChildAt(i);
                if (child.getId() == childId) {
                    ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
                    viewGroup.removeView(contentView);
                    viewGroup.addView(this, i, layoutParams);
                    break;
                }
            }
            addView(contentView, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setOnLoadingListener(OnLoadingListener onLoadingListener) {
        mOnLoadingListener = onLoadingListener;
    }

    public interface OnLoadingListener {
        void onRetry();
    }

}
