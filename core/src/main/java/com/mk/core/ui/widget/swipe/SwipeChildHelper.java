package com.mk.core.ui.widget.swipe;

import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;


public class SwipeChildHelper implements SwipeChild {

    private boolean mOnTouch;
    private SwipeChild mSwipeChild;

    public SwipeChildHelper() {
        super();
    }

    public SwipeChildHelper setSwipeChild(SwipeChild swipeChild) {
        this.mSwipeChild = swipeChild;
        return this;
    }

    public void onTouchEvent(MotionEvent ev) {
        final int action = ev.getAction() & MotionEventCompat.ACTION_MASK;
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mOnTouch = true;
                SwipeObserver.getInst().addSwipeChild(this);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                mOnTouch = false;
                SwipeObserver.getInst().removeSwipeChild(this);
                break;
        }
    }

    @Override
    public boolean onScrollToClose() {
        return !mOnTouch || mSwipeChild.onScrollToClose();
    }

    @Override
    public int getCurrentItem() {//无作用
        return mSwipeChild == null ? 0 : mSwipeChild.getCurrentItem();
    }
}
