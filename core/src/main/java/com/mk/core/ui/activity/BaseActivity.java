package com.mk.core.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.mk.core.R;
import com.mk.core.app.AppBroadcast;
import com.mk.core.app.BaseApp;
import com.mk.core.app.observer.AppContextObservable;
import com.mk.core.app.observer.IAppContextObservable;
import com.mk.core.app.observer.IAppContextObserver;
import com.mk.core.model.LoadCallback;
import com.mk.core.ui.dialog.LoadingDialog;
import com.mk.core.ui.fragment.BaseFragment;
import com.mk.core.ui.widget.swipe.OnSwipeToCloseLayoutAction;
import com.mk.core.ui.widget.swipe.SwipeToCloseLayout;

import java.util.HashMap;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class BaseActivity extends FragmentActivity implements LoadCallback, IAppContextObservable, IAppContextObserver, OnSwipeToCloseLayoutAction {

    private BaseFragment mBaseFragment;

    private AppContextObservable mAppContextObservable;
    private LoadingDialog mLoadingDialog;

    private HashMap<String, BroadcastReceiver> mBroadcastReceivers = new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBroadcastReceiver();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        mAppContextObservable = new AppContextObservable();
        registerContextObservable(this);

        View contentView = LayoutInflater.from(this).inflate(R.layout.activity_common, null);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String fragmentName = bundle.getString(BaseActivityRouter.EXTRA_NAME_FRAGMENT_CLASS);

            if (!TextUtils.isEmpty(fragmentName)) {
                mBaseFragment = (BaseFragment) Fragment.instantiate(this, fragmentName, bundle);
                mBaseFragment.setArguments(bundle);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(R.id.activity_common_content, mBaseFragment);
                ft.commitAllowingStateLoss();
            }
        }

        setContentView(addDecorLayout(contentView));

    }

    private View addDecorLayout(View contentView) {
        LinearLayout linearLayout = null;
        //是否可以左划关闭
        if (isSwipeToClose()) {
            contentView.setBackgroundColor(Color.WHITE);
            SwipeToCloseLayout swipeToCloseLayout = new SwipeToCloseLayout(this);
            swipeToCloseLayout.setNestScroll(true);
            swipeToCloseLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            swipeToCloseLayout.setOnSwipeToCloseLayoutAction(this);
            linearLayout = swipeToCloseLayout;
            linearLayout.setOrientation(LinearLayout.VERTICAL);
        }
        //设置titleBar
//        if (mBaseFragment != null && mBaseFragment.getActionBarResId() > 0) {
//            if (linearLayout == null) {
//                linearLayout = new LinearLayout(this);
//                linearLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
//            }
//            mActionBarImp = (ActionBarImp) SystemUtils.inflateView(this, mBaseFragment.getActionBarResId());
//            linearLayout.addView(mActionBarImp.getView());
//            setSupportActionBar(mActionBarImp.getToolbar());
//            mActionBarImp.setNavigationIcon(R.mipmap.ufanr_icon_back_black);
//        }
        if (linearLayout != null) {
            linearLayout.addView(contentView, new LinearLayout.LayoutParams(-1, -1));
            contentView = linearLayout;
        }
        return contentView;
    }

    protected boolean isSwipeToClose() {
        if (mBaseFragment != null) {
            return mBaseFragment.isSwipeToClose();
        }
        return true;
    }

    protected void showLoadingDialog() {
        hideLoadingDialog();
        mLoadingDialog = new LoadingDialog(this);
        mLoadingDialog.show();
    }

    protected void hideLoadingDialog() {
        if (mLoadingDialog != null) {
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        hideLoadingDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterAllBroadcastReceiver();
        mAppContextObservable.release();
    }

    @Override
    public boolean onPreLoad(String key) {
        return false;
    }

    @Override
    public boolean onPostLoad(String key) {
        return false;
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        return false;
    }

    @Override
    public boolean onFail(String key, Object data) {
        return false;
    }

    @Override
    public boolean onDataChange(String key, Object data) {
        return false;
    }

    @Override
    public final void dispatchUserLoginStateChange(boolean isLogin) {
        mAppContextObservable.dispatchUserLoginStateChange(isLogin);
    }

    @Override
    public final void registerContextObservable(IAppContextObserver contextCallBack) {
        mAppContextObservable.registerContextObservable(contextCallBack);
    }

    @Override
    public final void unregisterContextObservable(IAppContextObserver contextCallBack) {
        mAppContextObservable.unregisterContextObservable(contextCallBack);
    }

    private void initBroadcastReceiver() {
        //初始化登录状态变更监听
        registerBroadcastReceiver(AppBroadcast.ACTION_USER_LOGIN_STATE_CHANGE, new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                dispatchUserLoginStateChange(BaseApp.getInstance().isLogin());
            }
        });
    }

    protected void registerBroadcastReceiver(String action, BroadcastReceiver receiver) {
        BroadcastReceiver oldReceiver = mBroadcastReceivers.get(action);
        if (oldReceiver == null) {
            mBroadcastReceivers.put(action, receiver);
            super.registerReceiver(receiver, new IntentFilter(action));
        }
    }

    protected void unregisterBroadcastReceiver(String action) {
        BroadcastReceiver receiver = mBroadcastReceivers.get(action);
        if (receiver != null) {
            mBroadcastReceivers.remove(action);
            super.unregisterReceiver(receiver);
        }
    }

    protected void unregisterAllBroadcastReceiver() {
        for (BroadcastReceiver receiver : mBroadcastReceivers.values()) {
            super.unregisterReceiver(receiver);
        }
        mBroadcastReceivers.clear();
    }

    @Override
    public void onUserLoginStateChange(boolean isLogin) {

    }

    @Override
    public void onCloseAction() {
        finish();
    }

    public void checkLogin(Runnable runnable) {
        BaseApp.getInstance().checkLogin(this, runnable);
    }

    @Override
    public void onBackPressed() {
        if (mBaseFragment != null && mBaseFragment.onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mBaseFragment != null) {
            mBaseFragment.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
