package com.mk.core.ui.widget.swipe;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.Scroller;

public class SwipeToCloseLayout extends LinearLayout implements OnSwipeToTouchAction {

    private Scroller mScroller;
    private SwipeToCloseHelper mSwipeToClose;
    private boolean mToCloseActivity;
    private Paint mPaint;
    private boolean mFinish;
    private boolean mNestScroll;
    private boolean mStart;

    public SwipeToCloseLayout(Context context) {
        super(context);
        init(context);
    }

    public SwipeToCloseLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void setNestScroll(boolean nestScroll) {
        mNestScroll = nestScroll;
    }

    private void init(Context context) {
        mStart = false;
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mScroller = new Scroller(context);
        mSwipeToClose = new SwipeToCloseHelper(context, mScroller);

        mSwipeToClose.setContentView(this);
        mSwipeToClose.setSwipeToTouchAction(this);
        setWillNotDraw(false);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!mNestScroll) return false;
        if (mFinish) return false;
        if (MotionEvent.ACTION_DOWN == ev.getAction()) {
            ChangeThemeUtils.convertActivityToTranslucent((Activity) getContext());
        }
        if (mSwipeToClose != null && mSwipeToClose.onInterceptTouchEvent(ev)) {
            return true;
        }
        boolean onInterceptTouchEvent = super.onInterceptTouchEvent(ev);
        return onInterceptTouchEvent;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (!mNestScroll) return false;
        if (mFinish) return false;
        return (mSwipeToClose != null && mSwipeToClose.onTouchEvent(ev));
    }

    @Override
    public void computeScroll() {
        super.computeScroll();
        if (mScroller.computeScrollOffset()) {
            scrollTo(mScroller.getCurrX(), mScroller.getCurrY());
            postInvalidate();
        } else {
            if (mToCloseActivity) {
                if (mSwipeToCloseLayoutAction != null) {
                    mSwipeToCloseLayoutAction.onCloseAction();
                }
            }
        }
    }

    @Override
    public void scrollTo(int x, int y) {
        super.scrollTo(x, y);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int scroll = getScrollX();
        if (Math.abs(scroll) >= getWidth()) {
            return;
        }
        canvas.save();
        int alpha = 160 * (getWidth() - Math.abs(scroll)) / getWidth();
        int color = alpha << 24;
        mPaint.setColor(color);
        canvas.drawRect(scroll, 0, 0, getHeight(), mPaint);
        canvas.restore();
    }

    @Override
    public void startScroll(int startX, int dx, boolean finish) {
        mToCloseActivity = finish;
        mScroller.startScroll(startX, 0, dx, 0, 500 * Math.abs(dx) / getWidth());
        postInvalidate();
    }

    public void startScroll() {
        if (mStart) return;
        mStart = true;
        startScroll(getScrollX(), getScrollX() - getWidth(), true);
    }

    private OnSwipeToCloseLayoutAction mSwipeToCloseLayoutAction;

    public void setOnSwipeToCloseLayoutAction(OnSwipeToCloseLayoutAction action) {
        this.mSwipeToCloseLayoutAction = action;
    }

    public void setFinish(boolean finish) {
        this.mFinish = finish;
    }

    @Override
    public void onScroll() {

    }

}
