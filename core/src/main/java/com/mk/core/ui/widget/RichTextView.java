package com.mk.core.ui.widget;

import android.content.Context;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * Created by mingkg21 on 2017/10/3.
 */

public class RichTextView extends TextView {

    private int initTop;

    public RichTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setHtml(final String source) {
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                getViewTreeObserver().removeGlobalOnLayoutListener(this);
                if (TextUtils.isEmpty(source)) {
                    setText("");
                } else {
                    String content = source.replaceAll("\\t", "").replaceAll("</p>", "<br>").replaceAll("</P>", "<br>");
                    SpannableStringBuilder stringBuilder = (SpannableStringBuilder) Html.fromHtml(content,
                            new GlideImageGetter(getContext(), Glide.with(getContext()), RichTextView.this, false), null);
                    setText(stringBuilder);
                }
            }
        });
    }

}
