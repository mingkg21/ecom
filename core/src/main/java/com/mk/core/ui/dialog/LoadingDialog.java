package com.mk.core.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;

import com.mk.core.R;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class LoadingDialog extends Dialog {

    public LoadingDialog(@NonNull Context context) {
        super(context);

        setContentView(R.layout.dialog_loading);

        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

}
