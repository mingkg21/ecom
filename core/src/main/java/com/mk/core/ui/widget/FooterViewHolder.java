package com.mk.core.ui.widget;

import android.view.View;
import android.widget.ProgressBar;

import com.mk.core.R;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class FooterViewHolder extends BaseViewHolder {

    private ProgressBar mProgressBar;

    public FooterViewHolder(View itemView) {
        super(itemView);
        mProgressBar = (ProgressBar) itemView.findViewById(R.id.layout_common_footer_view_pb);
    }

    @Override
    public void onBind(Object data, int position) {
        if (mProgressBar != null) {
            mProgressBar.setIndeterminate(true);
        }
    }
}
