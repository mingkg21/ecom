package com.mk.core.ui.widget;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class FragmentAdapter extends FragmentPagerAdapter {

    private List<Fragment> nFragmentList;

    public FragmentAdapter(FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);

        nFragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return nFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return nFragmentList!=null ? nFragmentList.size() : 0;
    }

}
