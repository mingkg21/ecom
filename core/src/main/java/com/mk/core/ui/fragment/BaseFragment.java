package com.mk.core.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mk.core.app.BaseApp;
import com.mk.core.app.observer.IAppContextObservable;
import com.mk.core.app.observer.IAppContextObserver;
import com.mk.core.model.LoadCallback;
import com.mk.core.ui.dialog.LoadingDialog;
import com.mk.core.ui.widget.LoadingView;

/**
 * Created by mingkg21 on 2017/8/26.
 */

public abstract class BaseFragment extends Fragment implements LoadCallback, LoadingView.OnLoadingListener, IAppContextObserver {

    //Fragment的View加载完毕的标记
    private boolean isViewCreated;
    //Fragment对用户可见的标记
    private boolean isUIVisible;

    private LoadingView mLoadingView;
    private LoadingDialog mLoadingDialog;

    protected View mView;

    protected TextView mTitleTV;

    protected boolean isMain() {
        return false;
    }

    protected boolean hasTitleBar() {
        return true;
    }

    protected int getTitleBarBackResId() {
        return 0;
    }

    protected int getTitleBarTitleResId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(getLayoutResId(), container, false);
        if (getLoadingLayoutResId() > 0) {
            mLoadingView = new LoadingView(getContext());
            mLoadingView.attachViewForFragment(mView, getLoadingLayoutResId());
            mLoadingView.setOnLoadingListener(this);
            hideLoadingView();
        }
        View backView = mView.findViewById(getTitleBarBackResId());
        if (backView != null) {
            backView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            if (isMain()) {
                backView.setVisibility(View.INVISIBLE);
            }
        }
        mTitleTV = mView.findViewById(getTitleBarTitleResId());
        initData();
        initView(mView);
        return mView ;
    }

    protected <VT extends View> VT findViewById(int id) {
        return findViewById(mView, id);
    }

    protected <VT extends View> VT findViewById(View v, int id) {
        if (v == null)
            throw new NullPointerException("Fragment content view is null.");
        VT view = (VT) v.findViewById(id);
        if (view == null)
            throw new NullPointerException("This resource id is invalid.");
        return view;
    }

    protected abstract int getLayoutResId();

    protected int getLoadingLayoutResId() {
        return 0;
    }

    protected void initData() {

    }

    protected void initView(View view) {

    }

    protected void setTitle(int resId) {
        setTitle(getContext().getString(resId));
    }

    protected void setTitle(String title) {
        if (mTitleTV != null) {
            mTitleTV.setText(title);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isViewCreated = true;
        lazyLoad();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //isVisibleToUser这个boolean值表示:该Fragment的UI 用户是否可见
        if (isVisibleToUser) {
            isUIVisible = true;
            lazyLoad();
        } else {
            isUIVisible = false;
        }
    }

    public boolean isSwipeToClose() {
        return true;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        if (!hidden) {
            isUIVisible = true;
            lazyLoad();
        } else {
            isUIVisible = false;
        }
    }

    public void finish() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    public void finish(int resultCode, Intent data) {
        Activity activity = getActivity();
        activity.setResult(resultCode, data);
        activity.finish();
    }

    public void lazyLoad() {
        //这里进行双重标记判断,是因为setUserVisibleHint会多次回调,并且会在onCreateView执行前回调,必须确保onCreateView加载完毕且页面可见,才加载数据
        if (isViewCreated && isUIVisible) {
            loadData();
            //数据加载完毕,恢复标记,防止重复加载
            isViewCreated = false;
            isUIVisible = false;
        }
    }

    protected abstract void loadData();

    protected void hideLoadingView() {
        if (mLoadingView != null) {
            mLoadingView.hide();
        }
    }

    protected void showLoadingView() {
        if (mLoadingView != null) {
            mLoadingView.setVisibility(View.VISIBLE);
            mLoadingView.showLoding();
        }
    }

    protected void showEmptyView(String tip) {
        if (mLoadingView != null) {
            mLoadingView.setVisibility(View.VISIBLE);
            mLoadingView.showEmpty(tip);
        }
    }

    protected void showErrorView() {
        if (mLoadingView != null) {
            mLoadingView.setVisibility(View.VISIBLE);
            mLoadingView.showError();
        }
    }

    protected void showLoadingDialog() {
        hideLoadingDialog();
        mLoadingDialog = new LoadingDialog(getContext());
        mLoadingDialog.show();
    }

    protected void hideLoadingDialog() {
        if (mLoadingDialog != null) {
            mLoadingDialog.dismiss();
            mLoadingDialog = null;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(activity instanceof IAppContextObservable){
            ((IAppContextObservable) activity).registerContextObservable(this);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (getActivity() instanceof IAppContextObservable) {
            ((IAppContextObservable) getActivity()).unregisterContextObservable(this);
        }
    }

    @Override
    public void onUserLoginStateChange(boolean isLogin) {

    }

    public void checkLogin(Runnable runnable) {
        BaseApp.getInstance().checkLogin(getContext(), runnable);
    }

    @Override
    public boolean onPreLoad(String key) {
        return false;
    }

    @Override
    public boolean onPostLoad(String key) {
        return false;
    }

    @Override
    public boolean onSuccess(String key, Object data) {
        return false;
    }

    @Override
    public boolean onFail(String key, Object data) {

//        if (data != null && data instanceof ResponseEntity) {
//            ResponseEntity responseEntity = (ResponseEntity) data;
//            ToastUtil.showToast(responseEntity.getMsg());
//        }

        return false;
    }

    @Override
    public void onRetry() {

    }

    @Override
    public boolean onDataChange(String key, Object data) {
        return false;
    }

    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //页面销毁,恢复标记
        hideLoadingView();
        isViewCreated = false;
        isUIVisible = false;
    }
}
