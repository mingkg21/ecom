package com.mk.core.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

/**
 * Created by mingkg21 on 2018/2/5.
 */

public class BaseActivityRouter {

    public static final String EXTRA_NAME_FRAGMENT_CLASS = "EXTRA_NAME_FRAGMENT_CLASS";

    public static void startActivity(Context context, Class<?> fragmentCls) {
        startActivity(context, fragmentCls.getName());
    }

    public static void startActivityForResult(Context context, Class<?> fragmentCls, int requestCode) {
        startActivityForResult(context, fragmentCls.getName(), requestCode);
    }

    public static void startActivity(Context context, Class<?> fragmentCls, Intent intent) {
        startActivity(context, fragmentCls.getName(), intent);
    }

    public static void startActivity(Context context, String fragmentName) {
        startActivity(context, fragmentName, new Intent());
    }

    public static void startActivityForResult(Context context, String fragmentName, int requestCode) {
        startActivity(context, fragmentName, new Intent(), requestCode);
    }

    public static void startActivity(Context context, String fragmentName, Intent intent) {
        startActivity(context, fragmentName, intent, 0);
    }

    public static void startActivity(Context context, String fragmentName, Intent intent, int requestCode) {
        intent.setClass(context, BaseActivity.class);
        intent.putExtra(EXTRA_NAME_FRAGMENT_CLASS, fragmentName);
        if (!(context instanceof Activity)) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        if (requestCode > 0) {
            if (context instanceof Activity) {
                ((Activity) context).startActivityForResult(intent, requestCode);
                return;
            }
        }
        context.startActivity(intent);
    }

}
