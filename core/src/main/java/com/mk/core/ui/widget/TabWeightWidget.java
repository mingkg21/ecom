package com.mk.core.ui.widget;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by mingkg21 on 2017/10/5.
 */

public class TabWeightWidget extends TabWidget {

    public TabWeightWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TabWeightWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TabWeightWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void initItemLayout() {
        if (getWidth() > 0 && !mInitLayout && !mBeanList.isEmpty()) {
            mInitLayout = true;
            int itemWidth = getWidth() / mBeanList.size();
            float left = 0;
            float top = getPaddingTop();
            float right = itemWidth;
            float bottom = getHeight();
            for (int i = 0; i < mBeanList.size(); i++) {
                ItemBean itemBean = mBeanList.get(i);
                itemBean.mRectF.set(left, top, right, bottom);
                left = right;
                right = left + itemWidth;
            }
            startX = 0;
        }
        for (int i = 0; i < mBeanList.size() - 1; i++) {
            ItemBean itemBean = mBeanList.get(i);
            ItemBean itemBean2 = mBeanList.get(i + 1);
            itemBean.dis = itemBean.mRectF.width() / 2 + itemBean2.mRectF.width() / 2;
        }
    }

}
