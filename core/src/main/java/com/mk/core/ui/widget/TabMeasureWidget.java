package com.mk.core.ui.widget;

import android.content.Context;
import android.util.AttributeSet;


public class TabMeasureWidget extends TabWidget {

    protected float mTotalWidth;

    public TabMeasureWidget(Context context, AttributeSet attrs) {
	   super(context, attrs);
    }

    public TabMeasureWidget(Context context, AttributeSet attrs, int defStyleAttr) {
	   super(context, attrs, defStyleAttr);
    }

    public TabMeasureWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
	   super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void addTotalWidth(float width) {
	   mTotalWidth += width + mItemWidthPadding;
    }

    @Override
    protected void initItemLayout() {
	   if (getWidth() > 0 && !mInitLayout && !mBeanList.isEmpty()) {
		  mInitLayout = true;
		  float left   = (getMeasuredWidth() - mTotalWidth) / 2;
		  float top    = getPaddingTop();
		  float right  = 0;
		  float bottom = getHeight();

		  startX = left;
		  for (int i = 0; i < mBeanList.size(); i++) {
			 ItemBean itemBean = mBeanList.get(i);
			 right = left + mItemWidthPadding + itemBean.textWidth;
			 itemBean.mRectF.set(left, top, right, bottom);
			 left = right;
		  }
	   }
	   for (int i = 0; i < mBeanList.size() - 1; i++) {
		  ItemBean itemBean  = mBeanList.get(i);
		  ItemBean itemBean2 = mBeanList.get(i + 1);
		  itemBean.dis = itemBean.mRectF.width() / 2 + itemBean2.mRectF.width() / 2;
	   }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	   int widthMode = MeasureSpec.getMode(widthMeasureSpec);
	   if (widthMode != MeasureSpec.EXACTLY) {
		  widthMeasureSpec = MeasureSpec.makeMeasureSpec((int) (mTotalWidth), MeasureSpec.EXACTLY);
	   }
	   super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }


}
