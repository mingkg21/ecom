package com.mk.core.ui.widget.swipe;

import java.util.ArrayList;
import java.util.List;

public class SwipeObserver {

    private static SwipeObserver mInst = null;

    private List<SwipeChild> mActions;

    private SwipeObserver() {
        mActions = new ArrayList<>();
    }

    public static SwipeObserver getInst() {
        synchronized (SwipeObserver.class) {
            if (mInst == null) {
                mInst = new SwipeObserver();
            }
        }
        return mInst;
    }

    public void addSwipeChild(SwipeChild action) {
        if (!mActions.contains(action)) {
            mActions.add(action);
        }
    }

    public void removeSwipeChild(SwipeChild action) {
        if (mActions != null) {
            mActions.remove(action);
        }
    }

    public boolean onScrollToClose() {
        boolean onScrollToClose = true;
        int size = mActions.size();
        for (int i = size - 1; i >= 0; i--) {
            try {
                onScrollToClose = mActions.get(i).onScrollToClose();
                if (!onScrollToClose) {
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return onScrollToClose;
    }

    public void clear() {
        mActions.clear();
    }

}
