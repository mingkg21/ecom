package com.mk.core.ui.widget.swipe;

public interface OnSwipeToCloseLayoutAction {
    /**
     * 关闭界面
     */
    void onCloseAction();
}