package com.mk.core.ui.widget;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mingkg21 on 2017/8/26.
 */

public abstract class BaseAdapter<T> extends RecyclerView.Adapter<BaseViewHolder<T>> {

    private static final int TYPE_FOOTER = 0;
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_NORMAL = 2;

    protected List<T> mDatas;

    protected LayoutInflater mLayoutInflater;

    protected View mFooterView;
    protected boolean mShowFooterView;

    protected View mHeaderView;
    protected boolean mShowHeaderView;

    protected OnItemClickListener mOnItemClickListener;

    public BaseAdapter() {
        mDatas = new ArrayList<>();
    }

    public void add(T data) {
        mDatas.add(data);
    }

    public void add(int position, T data) {
        mDatas.add(position, data);
        notifyDataSetChanged();
    }

    public void addAll(List<T> datas) {
        mDatas.addAll(datas);
        notifyDataSetChanged();
    }

    public void setAll(List<T> datas) {
        clear();
        addAll(datas);
    }

    public void remove(T item) {
        if (item != null) {
            mDatas.remove(item);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        mDatas.clear();
    }

    public List<T> getDatas() {
        return mDatas;
    }

    public boolean isEmpty() {
        return mDatas.isEmpty();
    }

    public boolean isHasHeaderView() {
        return mHeaderView != null;
    }

    public void setHeaderView(View headerView) {
        mHeaderView = headerView;
        notifyDataSetChanged();
    }

    public boolean isHasFooterView() {
        return mFooterView != null;
    }

    public void setFooterView(View footerView) {
        mFooterView = footerView;
        mShowFooterView = true;

        notifyDataSetChanged();
    }

    public void hideFooterView() {
        mShowFooterView = false;
        notifyDataSetChanged();
    }

    public void showFooterView() {
        mShowFooterView = true;
        notifyDataSetChanged();
    }

    protected boolean hasFooterView() {
        if (mFooterView == null || !mShowFooterView) {
            return false;
        }
        return true;
    }

    @Override
    public int getItemViewType(int position) {
        if (mHeaderView != null && position == 0) {
            return TYPE_HEADER;
        }
        if (hasFooterView() && (position == getItemCount() - 1)) {
            return TYPE_FOOTER;
        }
        return TYPE_NORMAL;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_HEADER) {
            BaseViewHolder viewHolder = new HeaderViewHolder(mHeaderView);
            viewHolder.itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            return viewHolder;
        }
        if(hasFooterView() && viewType == TYPE_FOOTER) {
            BaseViewHolder viewHolder = new FooterViewHolder(mFooterView);
            viewHolder.itemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            return viewHolder;
        }
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(parent.getContext());
        }
        View view = mLayoutInflater.inflate(getItemLayoutId(viewType), parent, false);
        return getBaseViewHolder(view, viewType);
    }

    public abstract int getItemLayoutId(int viewType);

    public abstract BaseViewHolder<T> getBaseViewHolder(View itemView, int viewType);

    @Override
    public void onBindViewHolder(BaseViewHolder<T> holder, int position) {
        if(getItemViewType(position) == TYPE_FOOTER) {
            holder.onBind(null, position);
            return;
        }
        if(getItemViewType(position) == TYPE_HEADER) {
            holder.onBind(null, position);
            return;
        }
        if (mHeaderView != null) {
            position = position - 1;
        }
        holder.setOnItemClickListener(mOnItemClickListener);
        holder.bind(getItem(position), position);
    }

    protected T getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public int getItemCount() {
        int dataSize = 0;
//        if (mDatas == null || mDatas.isEmpty()) {
//            if (isHasHeaderView()) {
//                return 1;
//            }
//            return 0;
//        }
        if (mDatas != null) {
            dataSize = mDatas.size();
        }
        if (isHasHeaderView()) {
            dataSize += 1;
        }
        return hasFooterView() ? dataSize + 1 : dataSize;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position, Object data);
    }

}
