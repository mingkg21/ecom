package com.mk.core.ui.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.graphics.drawable.DrawableWrapper;
import android.text.Html;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.load.resource.transcode.BitmapToGlideDrawableTranscoder;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

/**
 * Created by mingkg21 on 2017/10/3.
 */

final class GlideImageGetter implements Html.ImageGetter {

    private final GenericRequestBuilder<String, ?, ?, GlideDrawable> glide;
    private final TextView targetView;
    private final Context context;

    public GlideImageGetter(Context context, RequestManager glide, TextView targetView, boolean animated) {
        this.context = context.getApplicationContext();
        this.glide = createGlideRequest(glide, animated);
        this.targetView = targetView;
        targetView.setTag(this);
    }

    private GenericRequestBuilder<String, ?, ?, GlideDrawable> createGlideRequest(RequestManager glide,
                                                                                  boolean animated) {
        GenericRequestBuilder<String, ?, ?, GlideDrawable> load;
        if (animated) {
            load = glide
                    .fromString()
                    //".asDrawable()" default loading handles animated GIFs and still images as well
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE) // animated GIFs need source cache
                    // show full image when animating
                    .fitCenter()
            ;
        } else {
            load = glide
                    .fromString()
                    // force still images
                    .asBitmap()
                    // make compatible with target
                    .transcode(new BitmapToGlideDrawableTranscoder(context), GlideDrawable.class)
                    // cache resized images (RESULT), and re-use SOURCE cached GIFs if any
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    // show part of the image when still
                    .centerCrop()
            ;
        }
        return load
                // common settings
                //.listener(new LoggingListener<String, GlideDrawable>())
                ;
    }

    @Override public Drawable getDrawable(String url) {
        final DrawableWrapper
                wrapper = new DrawableWrapper(new ColorDrawable(Color.GRAY));

        // start Glide's async load
        glide.load(url).into(new SimpleTarget<GlideDrawable>() {
            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                int width = targetView.getMeasuredWidth() - targetView.getPaddingLeft() - targetView.getPaddingRight();
                int height = width * resource.getIntrinsicHeight() / resource.getIntrinsicWidth();
                wrapper.setWrappedDrawable(resource);
                wrapper.setBounds(0, 0, width, height);
                targetView.setText(targetView.getText());
            }
        });
        return wrapper;
    }

}
