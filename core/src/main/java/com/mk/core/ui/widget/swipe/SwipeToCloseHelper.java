package com.mk.core.ui.widget.swipe;

import android.content.Context;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Scroller;

class SwipeToCloseHelper {

    private static final int INVALID_POINTER = -1;
    private boolean mIsBeingDragged;
    private int mTouchSlop;
    private int mActivePointerId;
    private int mLastMotionX;
    private int mLastMotionY;
    private int mMoveMotionX;
    private int mMoveMotionY;
    private int mMaximumVelocity;
    private int mMinimumVelocity;

    private View mView;
    private VelocityTracker mVelocityTracker;
    private Scroller mScroller;
    private OnSwipeToTouchAction mAction;

    private boolean mDownOver;//距离左侧1/2才生效
    private int mScreenWidth;

    public SwipeToCloseHelper(Context context, Scroller scroller) {
        mDownOver = false;
        mScroller = scroller;
        mScreenWidth = context.getResources().getDisplayMetrics().widthPixels;
        final ViewConfiguration configuration = ViewConfiguration.get(context);
        mTouchSlop = configuration.getScaledTouchSlop();
        mMinimumVelocity = configuration.getScaledMinimumFlingVelocity();
        mMaximumVelocity = configuration.getScaledMaximumFlingVelocity();
    }

    public void setContentView(View view) {
        this.mView = view;
    }

    public void setSwipeToTouchAction(OnSwipeToTouchAction action) {
        this.mAction = action;
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final int action = ev.getAction();
        if ((action == MotionEvent.ACTION_MOVE) && (mIsBeingDragged)) {
            return true;
        }
        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_MOVE: {
                if (!mDownOver) {
                    break;
                }
                final int activePointerId = mActivePointerId;
                if (activePointerId == INVALID_POINTER) {
                    break;
                }

                final int pointerIndex = ev.findPointerIndex(activePointerId);
                if (pointerIndex == -1) {
                    break;
                }

                final int x = (int) ev.getX(pointerIndex);
                final int y = (int) ev.getY(pointerIndex);
                final int xDiff = Math.abs(x - mLastMotionX);
                final int yDiff = Math.abs(y - mLastMotionY);
                boolean hasScrollToRight = x > mLastMotionX;
                boolean hasCanScroll = (hasScrollToRight && SwipeObserver.getInst().onScrollToClose());
                if (xDiff > yDiff && xDiff > mTouchSlop && hasCanScroll) {
                    mIsBeingDragged = true;
                    mLastMotionX = x;
                    mMoveMotionX = x;
                    mLastMotionY = y;
                    mMoveMotionY = y;
                    initVelocityTrackerIfNotExists();
                    mVelocityTracker.addMovement(ev);
                    final ViewParent parent = mView.getParent();
                    if (parent != null) {
                        parent.requestDisallowInterceptTouchEvent(true);
                    }
                }
                break;
            }

            case MotionEvent.ACTION_DOWN: {
                SwipeObserver.getInst().clear();
                final int x = (int) ev.getX();
                final int y = (int) ev.getY();
                mLastMotionX = x;
                mMoveMotionX = x;
                mLastMotionY = y;
                mMoveMotionY = y;
                mActivePointerId = ev.getPointerId(0);

                initOrResetVelocityTracker();
                mVelocityTracker.addMovement(ev);

                mIsBeingDragged = !mScroller.isFinished();
                mDownOver = mLastMotionX <= startPosition();
                break;
            }

            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                mIsBeingDragged = false;
                mActivePointerId = INVALID_POINTER;
                recycleVelocityTracker();
                mDownOver = false;
                SwipeObserver.getInst().clear();
                break;
            case MotionEvent.ACTION_POINTER_UP:
                onSecondaryPointerUp(ev);
                break;
        }
        return mIsBeingDragged;
    }

    private int startPosition() {
//        return mScreenWidth / 2;
        return 30;
    }

    private void onSecondaryPointerUp(MotionEvent ev) {
        final int pointerIndex = (ev.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
        final int pointerId = ev.getPointerId(pointerIndex);
        if (pointerId == mActivePointerId) {
            final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
            mLastMotionX = (int) ev.getX(newPointerIndex);
            mActivePointerId = ev.getPointerId(newPointerIndex);
            if (mVelocityTracker != null) {
                mVelocityTracker.clear();
            }
        }
    }

    public boolean onTouchEvent(MotionEvent ev) {
        initVelocityTrackerIfNotExists();
        MotionEvent vtev = MotionEvent.obtain(ev);
        final int actionMasked = ev.getActionMasked();
        switch (actionMasked) {
            case MotionEvent.ACTION_DOWN: {
                SwipeObserver.getInst().clear();
                if (mView instanceof ViewGroup) {
                    if (((ViewGroup) mView).getChildCount() == 0)
                        return false;
                }
                if ((mIsBeingDragged = !mScroller.isFinished())) {
                    final ViewParent parent = mView.getParent();
                    if (parent != null) {
                        parent.requestDisallowInterceptTouchEvent(true);
                    }
                }

                if (!mScroller.isFinished()) {
                    mScroller.abortAnimation();
                }

                mLastMotionX = (int) ev.getX();
                mMoveMotionX = mLastMotionX;

                mLastMotionY = (int) ev.getY();
                mMoveMotionY = mLastMotionY;
                mActivePointerId = ev.getPointerId(0);
                break;
            }
            case MotionEvent.ACTION_MOVE:
                final int activePointerIndex = ev.findPointerIndex(mActivePointerId);
                if (activePointerIndex == -1) {
                    break;
                }

                final int x = (int) ev.getX(activePointerIndex);
                int deltaX = mMoveMotionX - x;
                final int y = (int) ev.getY(activePointerIndex);
                int deltaY = mMoveMotionY - y;
                if (!mIsBeingDragged && Math.abs(deltaX) >= Math.abs(deltaY) && Math.abs(deltaX) > mTouchSlop) {
                    final ViewParent parent = mView.getParent();
                    if (parent != null) {
                        parent.requestDisallowInterceptTouchEvent(true);
                    }
                    mIsBeingDragged = true;
                    if (deltaX > 0) {
                        deltaX -= mTouchSlop;
                    } else {
                        deltaX += mTouchSlop;
                    }
                }
                if (mIsBeingDragged) {
                    if (mView.getScrollX() + deltaX >= 0) {
                        deltaX = -mView.getScrollX();
                    }
                    mView.scrollBy(deltaX, 0);
                }
                mMoveMotionX = x;
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                SwipeObserver.getInst().clear();
                if (mIsBeingDragged) {
                    boolean finish = false;
                    final VelocityTracker velocityTracker = mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
                    int initialVelocity = (int) velocityTracker.getXVelocity(mActivePointerId);
                    int scrollX = mView.getScrollX();
                    int dx = Math.abs(scrollX);
                    if (Math.abs(initialVelocity) > mMinimumVelocity) {
                        if (initialVelocity > 0) {
                            finish = true;
                            dx = -(mView.getWidth() - dx);
                        } else {
                            dx = -scrollX;
                        }
                    } else if (Math.abs(mLastMotionX - ev.getX()) > mView.getWidth() / 2) {
                        finish = true;
                        dx = -(mView.getWidth() - dx);
                    } else {
                        dx = -scrollX;
                    }
                    scrollTo(scrollX, dx, finish);
                    mActivePointerId = INVALID_POINTER;
                }
                mDownOver = false;
                break;
            case MotionEvent.ACTION_POINTER_DOWN: {
                final int index = ev.getActionIndex();
                mLastMotionX = (int) ev.getX(index);
                mMoveMotionX = mLastMotionX;
                mActivePointerId = ev.getPointerId(index);
                break;
            }
            case MotionEvent.ACTION_POINTER_UP:
                onSecondaryPointerUp(ev);
                mLastMotionX = (int) ev.getX(ev.findPointerIndex(mActivePointerId));
                mMoveMotionX = mLastMotionX;
                break;
        }

        if (mVelocityTracker != null) {
            mVelocityTracker.addMovement(vtev);
        }
        vtev.recycle();
        if (mAction != null) {
            mAction.onScroll();
        }
        return true;
    }

    private void scrollTo(int startX, int dx, boolean finish) {
        if (mAction != null) {
            mAction.startScroll(startX, dx, finish);
        }
    }

    private void initOrResetVelocityTracker() {
        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityTracker.obtain();
        } else {
            mVelocityTracker.clear();
        }
    }

    private void initVelocityTrackerIfNotExists() {
        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityTracker.obtain();
        }
    }

    private void recycleVelocityTracker() {
        if (mVelocityTracker != null) {
            mVelocityTracker.recycle();
            mVelocityTracker = null;
        }
    }

}
