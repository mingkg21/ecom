package com.mk.core.ui.widget.swipe;

/**
 * Created by  2017/5/17 0017.
 */

public interface SwipeChild {

    boolean onScrollToClose();

    int getCurrentItem();
}