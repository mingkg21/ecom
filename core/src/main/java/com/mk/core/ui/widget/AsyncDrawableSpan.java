package com.mk.core.ui.widget;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.style.ReplacementSpan;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.mk.core.app.BaseApp;

/**
 * Created by mingkg21 on 2017/11/22.
 */

public class AsyncDrawableSpan extends ReplacementSpan {

    private ISpanDrawableView mSpanDrawableView;

    private int mImgWidth;
    private int mImgHeight;
    private int mWidth;
    private int mHeight;

    private float mLastDrawX;
    private int mLastDrawY;
    private int mLastDrawTop;
    private int mLastDrawBottom;
    boolean isNeedDraw = true;
    private int paddingL;
    private int paddingT;
    private int paddingB;
    private int paddingR;

    private Bitmap mBitmap;
    private Paint mPaint;
    private String mUrl;
    private String mNetUrl;

    public AsyncDrawableSpan(Bitmap bitmap, String url, ISpanDrawableView spanDrawableView) {
        mBitmap = bitmap;
        mUrl = url;
        mImgWidth = mBitmap.getWidth();
        mImgHeight = mBitmap.getHeight();
        mSpanDrawableView = spanDrawableView;
    }

    public AsyncDrawableSpan(String url, int width, int height, ISpanDrawableView spanDrawableView) {
        mNetUrl = mUrl = url;
        mImgWidth = width;
        mImgHeight = height;
        mSpanDrawableView = spanDrawableView;
        Glide.with(BaseApp.getInstance()).load(url).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                mBitmap = resource;
                Log.v("AsyncDrawableSpan", "load bitmap");
                BaseApp.getHandler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSpanDrawableView.invalidate();
                    }
                }, 1000);
            }
        });
    }

    public String getUrl() {
        return mUrl;
    }

    public String getNetUrl() {
        return mNetUrl;
    }

    public void setNetUrl(String netUrl) {
        mNetUrl = netUrl;
    }

    public void setPadding(int left, int top, int right, int bottom) {
        paddingL = left;
        paddingR = right;
        paddingT = top;
        paddingB = bottom;
    }

    public int getImgWidth() {
        return mImgWidth;
    }

    public int getImgHeight() {
        return mImgHeight;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    @Override
    public int getSize(@NonNull Paint paint, CharSequence text, int start, int end, @Nullable Paint.FontMetricsInt fm) {
        mPaint = paint;
        int imgWidth = mImgWidth;
        int imgHeight = mImgHeight;
        int vWidth = mSpanDrawableView.getMaxViewWidth();
        if (paddingL == 0 && paddingR == 0) {
            vWidth += mSpanDrawableView.getViewPaddingLeft()
                    + mSpanDrawableView.getViewPaddingRight();
        }
        int vHeight = mSpanDrawableView.getMaxViewHeight();
        if(vHeight == 0){
            Paint.FontMetricsInt fontMetricsInt = paint.getFontMetricsInt();
            vHeight = fontMetricsInt.bottom - fontMetricsInt.top;
        }
        vWidth -= paddingL + paddingR;
        vHeight -= paddingT + paddingB;
        if (imgWidth > 0 && imgHeight > 0) {
            int gapW = imgHeight * (imgWidth - vWidth) / imgWidth;
            int gapH = imgHeight - vHeight;
            if (gapW > gapH) {
                imgHeight = imgHeight * vWidth / imgWidth;
                imgWidth = vWidth;
            } else {
                imgWidth = imgWidth * vHeight / imgHeight;
                imgHeight = vHeight;
            }
            if (fm != null) {
                fm.ascent = -(imgHeight + paddingT + paddingB);
                fm.descent = 0;

                fm.top = fm.ascent;
                fm.bottom = 0;
            }
        }
        mWidth = imgWidth;
        mHeight = imgHeight;
        if (paddingL == 0 && paddingR == 0) {
            return mSpanDrawableView.getMaxViewWidth();
        } else {
            return imgWidth + paddingL + paddingR;
        }
    }

    @Override
    public void draw(@NonNull Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, @NonNull Paint paint) {
        Log.v("AsyncDrawableSpan", "draw ========");
        mPaint = paint;
        mLastDrawX = x;
        mLastDrawY = y;
        mLastDrawTop = top;
        mLastDrawBottom = bottom;
        draw(canvas, x, top, y, bottom);
    }

    public boolean drawLastLocal(Canvas canvas, int scrollTop, int viewHeight) {
        boolean isBeyond = false;
        if (scrollTop - mLastDrawTop - viewHeight / 2 > mHeight) {
            isBeyond = true;
        } else if (scrollTop + viewHeight + viewHeight / 2 < mLastDrawTop) {
            isBeyond = true;
        }
        if (mLastDrawBottom - mLastDrawTop > 0 && !isBeyond) {
            draw(canvas, mLastDrawX, mLastDrawTop, mLastDrawY, mLastDrawBottom);
            isNeedDraw = false;
        }
        return isBeyond;
    }

    private void draw(Canvas canvas, float x, int top, int y, int bottom) {
        if (mPaint == null || mBitmap == null) {
            return;
        }
//        if (mAsyncImageViewManager.getUrl() == null) {
//            mAsyncImageViewManager.setImageUrl(mSrc, mImgWidth, mImgHeight);
//        }
        int pb = (bottom - top - mHeight) / 2;
        int translateX = (int) (x + paddingL);
        int translateY = bottom - pb - mHeight;
        translateY += mSpanDrawableView.getViewPaddingTop();
        if (paddingL != 0 || paddingR != 0) {
            translateX += mSpanDrawableView.getViewPaddingLeft();
        }
        canvas.save();
//        canvas.translate(translateX, translateY);
//        mAsyncImageViewManager.onDraw(canvas, mWidth, mHeight);
        Rect srcRect = new Rect(0, 0, mImgWidth, mImgHeight);
        RectF dstRect = new RectF(x, top, x + mWidth, top + mHeight);
        canvas.drawBitmap(mBitmap, srcRect, dstRect, mPaint);
        canvas.restore();
    }

}
