package com.mk.core.app.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mingkg21 on 2017/10/23.
 */

public class AppContextObservable extends AbsObservable<IAppContextObserver> implements IAppContextObservable {

    @Override
    public void registerContextObservable(IAppContextObserver contextCallBack) {
        registerObserver(contextCallBack);
    }

    @Override
    public void unregisterContextObservable(IAppContextObserver contextCallBack) {
        unregisterObserver(contextCallBack);
    }

    @Override
    public void dispatchUserLoginStateChange(boolean isLogin){
        List<IAppContextObserver> list = new ArrayList<IAppContextObserver>(getObservers());
        for(IAppContextObserver contextCallBack : list){
            contextCallBack.onUserLoginStateChange(isLogin);
        }
    }

}
