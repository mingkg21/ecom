package com.mk.core.app.observer;

/**
 * Created by mingkg21 on 2017/10/23.
 */

public interface IAppContextObservable {

    /**
     * 注册回调
     * @param contextCallBack
     */
    public void registerContextObservable(IAppContextObserver contextCallBack);
    /**
     * 注销回调
     * @param contextCallBack
     */
    public void unregisterContextObservable(IAppContextObserver contextCallBack);
    /**
     * 派遣登录状态变更事件
     */
    public void dispatchUserLoginStateChange(boolean isLogin);

}
