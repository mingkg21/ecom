package com.mk.core.app.observer;

/**
 * Created by mingkg21 on 2017/10/23.
 */

public interface IAppContextObserver {

    /**
     * 用户登录状态变更回调
     * @param isLogin 是否是登录
     */
    public void onUserLoginStateChange(boolean isLogin);

}
