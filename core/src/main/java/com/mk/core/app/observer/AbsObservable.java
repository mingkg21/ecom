package com.mk.core.app.observer;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by mingkg21 on 2017/10/23.
 */

public class AbsObservable<Observer> {

    private LinkedList<Observer> mObservers = new LinkedList<>();

    public void registerObserver(Observer observer) {
        if(!mObservers.contains(observer)){
            mObservers.add(observer);
        }
    }

    public void unregisterObserver(Observer observer) {
        if(mObservers.contains(observer)){
            mObservers.remove(observer);
        }
    }

    public void release() {
        mObservers.clear();
    }

    public List<Observer> getObservers(){
        return mObservers;
    }
}
