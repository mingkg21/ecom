package com.mk.core.app;

/**
 * Created by mingkg21 on 2017/10/23.
 */

public class AppBroadcast {

    /** 用户登录状态改变的ACTION */
    public static final String ACTION_USER_LOGIN_STATE_CHANGE = "com.mk.android.action.ACTION_USER_LOGIN_STATE_CHANGE";

}
