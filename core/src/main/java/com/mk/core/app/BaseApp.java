package com.mk.core.app;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

/**
 * Created by mingkg21 on 2017/9/26.
 */

public abstract class BaseApp extends Application {

    private static BaseApp mInstance;

    private Handler mHandler;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        mHandler = new Handler(Looper.getMainLooper());

    }

    public static BaseApp getInstance() {
        return mInstance;
    }

    /**
     * 获取全局主线程Handler
     * @return
     */
    public static Handler getHandler(){
        return mInstance.mHandler;
    }

    public abstract void onEnterApp();

    public abstract void onExitApp();

    public abstract boolean isLogin();

    protected abstract void onLogin(Context context);

    public void checkLogin(Context context, Runnable runnable) {
        if(runnable == null) {
            return;
        }
        if(!isLogin()){
            onLogin(context);
        }else{
            runnable.run();
        }
    }
}
