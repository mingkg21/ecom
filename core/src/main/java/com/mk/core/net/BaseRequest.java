package com.mk.core.net;

import com.mk.core.entity.AbsResponseEntity;
import com.mk.core.model.BaseDataModel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public abstract class BaseRequest<T extends AbsResponseEntity> {

    public static final int POST = 0;
    public static final int GET = 1;

    private BaseDataModel mBaseDataModel;

    protected Map<String, String> mParameter = new HashMap<>();

    private String mDataKey = createKey();

    private int mMethod;

    public BaseRequest() {
//        mDataKey = createKey();
    }

    public int getMethod() {
        return mMethod;
    }

    public void setMethod(int method) {
        mMethod = method;
    }

    protected String createKey(String ... params) {
        StringBuilder key = new StringBuilder();
        key.append(this);
        key.append(":");
        for (String str : params) {
            key.append("_");
            key.append(str);
        }
        return key.toString();
    }

    public String getDataKey() {
        return mDataKey;
    }

    public abstract String getUrl();

    public Map<String, String> getParameter() {
        return mParameter;
    }

    public String getPostString() {
        return null;
    }

    public abstract T parse(String str);

    public BaseDataModel getBaseDataModel() {
        return mBaseDataModel;
    }

    public void setBaseDataModel(BaseDataModel baseDataModel) {
        mBaseDataModel = baseDataModel;
    }
}
