package com.mk.core.net;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.mk.core.entity.AbsResponseEntity;
import com.mk.core.model.BaseDataModel;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class ServiceProxy {

    private static ServiceProxy mInstance;
    private OkHttpClient mOkHttpClient;
    private Handler mHandler;

    protected ArrayList<WeakReference<BaseDataModel>> mBaseDataModelList = new ArrayList<>();

    private ServiceProxy() {
        mHandler = new Handler(Looper.getMainLooper());

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        mOkHttpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .connectTimeout(12, TimeUnit.SECONDS)
                .build();
    }

    public static ServiceProxy getInstance() {
        if (mInstance == null) {
            mInstance = new ServiceProxy();
        }
        return mInstance;
    }

    public void addModel(BaseDataModel baseDataModel) {
        if (!containsBaseDataModel(baseDataModel)) {
            mBaseDataModelList.add(new WeakReference<BaseDataModel>(baseDataModel));
        }
    }

    private boolean containsBaseDataModel(BaseDataModel baseDataModel) {
        for (WeakReference<BaseDataModel> dataModel : mBaseDataModelList) {
            BaseDataModel model = dataModel.get();
            if (model != null && model == baseDataModel) {
                return true;
            }
        }
        return false;
    }

    public void traversalCallBacks(CallBackHandler<BaseDataModel> handler){
        boolean isHandle = false;
        for (int i = 0; i < mBaseDataModelList.size(); i++) {
            WeakReference<BaseDataModel> callBack = mBaseDataModelList.get(i);
            if(callBack.get() != null){
//                if(!isHandle){
                    isHandle = handler.handle(callBack.get());
//                }
            }else{
                mBaseDataModelList.remove(i);
                i--;
            }
        }
    }

    public void dispatchPreLoad(final String key) {
        traversalCallBacks(new CallBackHandler<BaseDataModel>() {
            @Override
            public boolean handle(BaseDataModel callback) {
                return callback.dispatchPreLoad(callback.getKey(key));
            }
        });
    }

    public void dispatchPostLoad(final String key) {
        traversalCallBacks(new CallBackHandler<BaseDataModel>() {
            @Override
            public boolean handle(BaseDataModel callback) {
                return callback.dispatchPostLoad(callback.getKey(key));
            }
        });
    }

    public void dispatchSuccess(final String key, final Object data) {
        traversalCallBacks(new CallBackHandler<BaseDataModel>() {
            @Override
            public boolean handle(BaseDataModel callback) {
                return callback.dispatchSuccess(callback.getKey(key), data);
            }
        });
    }

    public void dispatchFail(final String key, final Object data) {
        traversalCallBacks(new CallBackHandler<BaseDataModel>() {
            @Override
            public boolean handle(BaseDataModel callback) {
                return callback.dispatchFail(callback.getKey(key), data);
            }
        });
    }

    public void dispatchDataChange(final String key, final Object data) {
        traversalCallBacks(new CallBackHandler<BaseDataModel>() {
            @Override
            public boolean handle(BaseDataModel callback) {
                callback.dispatchDataChange(key, data);
                return false;
            }
        });
    }

    public void notifyDataChange(String key, Object data) {
        dispatchDataChange(key, data);
    }

    public void requestData(final BaseRequest baseRequest) {
        final String key = baseRequest.getDataKey();
        final BaseDataModel dataModel = baseRequest.getBaseDataModel();
        dataModel.dispatchPreLoad(key);

        String url = baseRequest.getUrl();

        Request.Builder requestBuilder = new Request.Builder();
        //设置HEADER

        //设置请求参数
        Map<String, String> parameter = baseRequest.getParameter();
        if (!parameter.isEmpty()) {
            if (baseRequest.getMethod() == BaseRequest.POST) {
                FormBody.Builder formBodyBuilder = new FormBody.Builder();
                Set<String> keys = parameter.keySet();
                for (String k : keys) {
                    String value = parameter.get(k);
                    if (value != null) {
                        formBodyBuilder.add(k, value);
                    }
                }
                requestBuilder.post(formBodyBuilder.build());
            } else {
                StringBuilder sb = new StringBuilder();
                Set<String> keys = parameter.keySet();
                for (String k : keys) {
                    String value = parameter.get(k);
                    if (value != null) {
                        if (sb.length() > 0) {
                            sb.append("&");
                        }
                        sb.append(k);
                        sb.append("=");
                        sb.append(value);
                    }
                }
                if (sb.length() > 0) {
                    url = url + "?" + sb.toString();
                }
            }
        } else if (baseRequest.getPostString() != null) {
            if (baseRequest.getMethod() == BaseRequest.POST) {
                requestBuilder.post(RequestBody.create(MediaType.parse("text/plain"), baseRequest.getPostString()));
            }
        }

        requestBuilder.url(url);
        Request request = requestBuilder.build();

        Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                if (dataModel != null) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            dataModel.dispatchPostLoad(key);
                            dataModel.dispatchFail(key, null);
                        }
                    });
                }
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                if (dataModel != null) {
                    String str = response.body().string();
                    if (TextUtils.isEmpty(str)) {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                dataModel.dispatchPostLoad(key);
                                dataModel.dispatchFail(key, response);
                            }
                        });
                    } else {
                        final AbsResponseEntity responseEntity = baseRequest.parse(str);
                        if (responseEntity != null) {
                            if (responseEntity.isSuccess()) {
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        dataModel.dispatchSuccess(key, responseEntity);
                                        dataModel.dispatchPostLoad(key);
                                    }
                                });
                                return;
                            } else {
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        dataModel.dispatchPostLoad(key);
                                        dataModel.dispatchFail(key, responseEntity);
                                    }
                                });
                            }
                        } else {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    dataModel.dispatchPostLoad(key);
                                    dataModel.dispatchFail(key, response);
                                }
                            });
                        }
                    }
                }
            }
        });

    }

}
