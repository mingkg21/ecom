package com.mk.core.net;

import okhttp3.Interceptor;

/**
 * Created by mingkg21 on 16/1/18.
 */
public abstract class BaseAPI {

    protected Interceptor getCustomInterceptor() {
        return new DefaultInterceptor();
    }

    protected abstract String getBaseUrl();

}
