package com.mk.core.net;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public interface CallBackHandler<CallBack>{
    public boolean handle(CallBack callBack);
}