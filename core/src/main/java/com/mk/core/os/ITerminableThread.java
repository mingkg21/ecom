package com.mk.core.os;

public interface ITerminableThread {
	public void start();
	public void cancel();
	public boolean isCancel();
}
