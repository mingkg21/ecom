package com.mk.core.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class IMMUtils {

    public static void hideSoftInput(Activity activity) {
	   try {
		  InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		  inputMethodManager.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
	   } catch (Exception e) {
		  e.printStackTrace();
	   }
    }

    public static void hideSoftInput(Context context, View view) {
	   try {
		  InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		  inputMethodManager.hideSoftInputFromInputMethod(view.getWindowToken(), 0);
		  IMMUtils.hideSoftInput((Activity) context);
	   } catch (Exception e) {
		  e.printStackTrace();
	   }
    }

    public static void showSoftInput(Activity activity) {
	   try {
		  InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		  inputMethodManager.toggleSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0, InputMethodManager.SHOW_FORCED);
	   } catch (Exception e) {
		  e.printStackTrace();
	   }
    }

    public static void showSoftInput(Context context, View view) {
	   try {
		  InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		  inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_FORCED);
	   } catch (Exception e) {
		  e.printStackTrace();
	   }
    }

    public static void showSoftDialogInput(Context context, View view) {
	   try {
		  InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		  inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
	   } catch (Exception e) {
		  e.printStackTrace();
	   }
    }

}
