package com.mk.core.util;

import android.widget.Toast;

import com.mk.core.app.BaseApp;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class ToastUtil {

    public static void showToast(int resId) {
        showToast(BaseApp.getInstance().getString(resId));
    }

    public static void showToast(String content) {
        Toast.makeText(BaseApp.getInstance(), content, Toast.LENGTH_SHORT).show();
    }

}
