package com.mk.core.util;

import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.mk.core.app.BaseApp;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class ImageUtil {

    public static void loadImage(String url, ImageView imageView) {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        Glide.with(BaseApp.getInstance()).load(url).into(imageView);
    }

    public static void loadImage(String url, ImageView imageView, int defaultResId) {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        Glide.with(BaseApp.getInstance()).load(url).placeholder(defaultResId).into(imageView);
    }

}
