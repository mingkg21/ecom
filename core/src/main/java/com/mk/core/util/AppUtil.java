package com.mk.core.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.AppTask;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RecentTaskInfo;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;

import com.mk.core.app.BaseApp;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class AppUtil {
	private static String sUMengChannel;

	/**
	 * 是否模拟器
	 * @param context
	 * @return
	 */
	public static boolean isSimulator(Context context){
		TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		String deviceId = tm.getDeviceId();
		if(TextUtils.isEmpty(deviceId) || "000000000000000".equalsIgnoreCase(deviceId)){
			return true;
		}
		if("Android".equalsIgnoreCase(tm.getSimOperatorName())){
			return true;
		}
		if("310260".equalsIgnoreCase(tm.getSimOperator())){
			return true;
		}
		WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		if(wifiManager == null){
			return true;
		}else{
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			if(wifiInfo == null || TextUtils.isEmpty(wifiInfo.getMacAddress())){
				return true;
			}
		}
		return false;
	}
	/**
	 * 是否已安装
	 * @param context
	 * @param packageName
	 * @return
	 */
	public static boolean isInstalled(Context context, String packageName){
		PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
        } catch (NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }
        if(packageInfo == null){
    		return false;
        }else{
    		return true;
        }
	}
	/**
	 * 获取ActivityManager
	 * @param context
	 * @return
	 */
	public static ActivityManager getActivityManager(Context context){
		return (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
	}
	/**
	 * 获取最近用户过的软件
	 * @param context
	 * @param taskSize
	 * @return
	 */
	public static List<RecentTaskInfo> getRecentTasks(Context context, int taskSize){
		return getActivityManager(context).getRecentTasks(taskSize, ActivityManager.RECENT_WITH_EXCLUDED);
	}
	/**
	 * 获取运行的应用程序
	 * @param context
	 * @param taskSize
	 * @return
	 */
	public static List<RunningTaskInfo> getRunningTasks(Context context, int taskSize){
		return getActivityManager(context).getRunningTasks(taskSize);
	}
	/**
	 * 获取最近用户过的软件
	 * @param context
	 * @param taskSize
	 * @return
	 */
	public static List<AppTask> getAppTasks(Context context){
		return getActivityManager(context).getAppTasks();
	}
	/**
	 * 获取当前运行进程数
	 * @param context
	 * @return
	 */
	public static List<RunningAppProcessInfo> getRunningAppProcesses(Context context){
		return getActivityManager(context).getRunningAppProcesses();
	}
	/**
	 * 判断应用程序是否显示在最前端
	 * @param context
	 * @param packageName
	 * @return
	 */
	public static boolean isTopAppTask(Context context, String packageName){
		List<RunningAppProcessInfo> processInfos = getRunningAppProcesses(context);
		if(processInfos != null){
			for (RunningAppProcessInfo processInfo : processInfos) {
				if(processInfo != null && processInfo.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND
						&& processInfo.processName.equals(packageName)){
					return true;
				}
			}
		}
		return false;
	}
	/** 卸载软件
	 * @param context
	 * @param packageName
	 */
	public static void gotoUninstall(Context context, String packageName) {
		if(TextUtils.isEmpty(packageName)) {
			return;
		}
		Uri packageUri = Uri.parse("package:" + packageName);
		Intent intent = new Intent(Intent.ACTION_DELETE, packageUri);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}
	
	/** 安装软件
	 * @param context
	 * @param apkPath
	 */
	public static void gotoInstall(Context context, String apkPath) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setDataAndType(Uri.parse("file://" + apkPath),"application/vnd.android.package-archive");
		context.startActivity(intent); 
	}
	/**
	 * 获取root权限
	 * @param pkgCodePath
	 * @return
	 */
	public static boolean getRootPermission(Context context) {
	    Process process = null;
	    DataOutputStream os = null;
	    try {  
	        String cmd="chmod 777 " + context.getPackageCodePath();
	        process = Runtime.getRuntime().exec("su"); //切换到root帐号
	        os = new DataOutputStream(process.getOutputStream());
	        os.writeBytes(cmd + "\n");  
	        os.writeBytes("exit\n");  
	        os.flush();  
	        process.waitFor();
	    } catch (Exception e) {
	        return false;  
	    } finally {
	        try {
	            if (os != null) {  
	                os.close();  
	            }  
	            process.destroy();  
	        } catch (Exception e) {
	        }  
	    }  
	    return true;  
	}  
	/**
	 * 静默安装
	 * @param context
	 * @param apkPath
	 * @return
	 */
	public static boolean installApk(Context context, String apkPath) {
		 return execRootCmdSilent("pm install -r " + apkPath) == 0;
	}
	
	public static int execRootCmdSilent(String cmd) {
		int result = -1;
		DataOutputStream dos = null;
		try {
			Process p = Runtime.getRuntime().exec("su");
			dos = new DataOutputStream(p.getOutputStream());
			dos.writeBytes(cmd + "\n");
			dos.flush();
			dos.writeBytes("exit\n");
			dos.flush();
			p.waitFor();
			result = p.exitValue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (dos != null) {
				try {
					dos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	/**
	 * 打开应用程序
	 * @param context
	 * @param packageName
	 */
	public static void openApp(Context context, String packageName){
		try{
	        Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
	        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        context.startActivity(intent);
	    }catch(Exception e){
	    }
	}
	/** 到系统的设置网络界面
	 * @param context
	 */
	public static void gotoSettingActivity(Context context){
		try {
			String action = android.provider.Settings.ACTION_WIRELESS_SETTINGS;
			if(Build.VERSION.SDK_INT >= 14){//4.0的设置
				action = android.provider.Settings.ACTION_SETTINGS;
			}
			Intent intent = new Intent(action);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		} catch (Exception e) {
		}
	}
	
	/**
	 * 获取APK信息
	 * @param context
	 */
	public static String getApkVersionName(Context context) {
		PackageManager pm = context.getPackageManager();
		String packageName = context.getPackageName();
		PackageInfo apkInfo = null;
		try {
			apkInfo = pm.getPackageInfo(packageName, 0);
		} catch (NameNotFoundException e) {
			apkInfo = null;
			e.printStackTrace();
		}
		String versionName = "0.0.0";
		
		if (apkInfo != null && !TextUtils.isEmpty(apkInfo.versionName)) {
			versionName = apkInfo.versionName;
		}
		
		return versionName;
	}
	
	/**
	 * 获取APK信息
	 * @param context
	 */
	public static int getApkVersionCode(Context context) {
		PackageManager pm = context.getPackageManager();
		String packageName = context.getPackageName();
		PackageInfo apkInfo = null;
		try {
			apkInfo = pm.getPackageInfo(packageName, 0);
		} catch (NameNotFoundException e) {
			apkInfo = null;
			e.printStackTrace();
		}
		
		return apkInfo != null ? apkInfo.versionCode : 0;
	}
	
	public static String getUMengChannel(Context context) {
		if(sUMengChannel != null){
			return sUMengChannel;
		}
		
        ApplicationInfo appinfo = context.getApplicationInfo();
        String sourceDir = appinfo.sourceDir;
        ZipFile zipfile = null;
        final String start_flag = "META-INF/channel_";
        try {
            zipfile = new ZipFile(sourceDir);
            Enumeration<?> entries = zipfile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = ((ZipEntry) entries.nextElement());
                String entryName = entry.getName();
                if (entryName.contains(start_flag)) {
                	sUMengChannel = entryName.replaceAll(start_flag, "");
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (zipfile != null) {
                try {
                    zipfile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        if(sUMengChannel == null){
        	Object data = null;
    		try {
    			ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
    			data = ai.metaData.get("CHANNEL");
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
    		sUMengChannel = data != null ? data.toString() : null;
        }
		
		return sUMengChannel;
	}
	
	public static String getFirstChannel(Context context) {
		Object data = null;
		try {
			ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
			data = ai.metaData.get("FIRST_CHANNEL");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data != null ? data.toString() : null;
	}
	
	public static void closeInputMethod(Activity activity){
		try {  
            InputMethodManager inputMethodManager = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {}
	}
	
	public static String getDeviceId(Context context){
		try {
			TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			return tm.getDeviceId();
		} catch (Exception e) {
			return "";
		}
	}
	
	public static String getDeviceName(){
		return android.os.Build.MODEL;
	}

	public static String getSysVersionName(){
		return android.os.Build.VERSION.RELEASE;
	}

	public static int getSysVersionCode(){
		return android.os.Build.VERSION.SDK_INT;
	}

	public static String getMac(Context context) {
		String wifiMacAddress = "";
		WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		if(wifiManager != null){
			WifiInfo wifiInfo = wifiManager.getConnectionInfo();
			if(wifiInfo != null){
				wifiMacAddress = wifiInfo.getMacAddress();
			}
		}
		return wifiMacAddress;
	}
	
	public static void clearAllAppData(Context context){
		try {
			FileUtil.delFiles(context.getCacheDir().getParentFile());
		} catch (Exception e) {}
	}
	
	public static void freeMemory(int importance,boolean freeSelf){
		try {
			Context context = BaseApp.getInstance();
			ActivityManager activityManger = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			List<RunningAppProcessInfo> list = activityManger.getRunningAppProcesses();
			if (list != null){
				for (int i = 0; i < list.size(); i++) {
					RunningAppProcessInfo apinfo = list.get(i);
					String[] pkgList = apinfo.pkgList;
					if (pkgList != null && apinfo.processName != null && apinfo.importance > importance) {
						if(freeSelf || apinfo.processName.indexOf(context.getPackageName()) == -1){
							for (int j = 0; j < pkgList.length; j++) {
								activityManger.killBackgroundProcesses(pkgList[j]);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static long getAvailMemory(Context context) {
		// 获取android当前可用内存大小
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		MemoryInfo mi = new MemoryInfo();
		am.getMemoryInfo(mi);
		// mi.availMem; 当前系统的可用内存

		// return Formatter.formatFileSize(context, mi.availMem);//
		// 将获取的内存大小规格化
		return mi.availMem / (1024 * 1024);
	}

	public static long getTotalMemory(Context context) {
		String str1 = "/proc/meminfo";// 系统内存信息文件
		String str2;
		String[] arrayOfString;
		long initial_memory = 0;

		try {
			FileReader localFileReader = new FileReader(str1);
			BufferedReader localBufferedReader = new BufferedReader(localFileReader, 8192);
			str2 = localBufferedReader.readLine();// 读取meminfo第一行，系统总内存大小
			arrayOfString = str2.split("\\s+");
			initial_memory = Integer.valueOf(arrayOfString[1]).intValue() * 1024;// 获得系统总内存，单位是KB，乘以1024转换为Byte
			localBufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// return Formatter.formatFileSize(context, initial_memory);//
		// Byte转换为KB或者MB，内存大小规格化
		return initial_memory / (1024 * 1024);
	}
}
