package com.mk.core.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.mk.core.app.BaseApp;

/**
 * Created by mingkg21 on 2017/9/30.
 */

public class DimensionUtil {

    private static Context getContext(){
        return BaseApp.getInstance();
    }
    private static Resources getResources(){
        return getContext().getResources();
    }

    public static float sizeToPX(int unit, float size){
        return TypedValue.applyDimension(unit, size, getResources().getDisplayMetrics());
    }

    public static int DIPToPX(float size){
        return (int) DIPToPXF(size);
    }

    public static float DIPToPXF(float size){
        return sizeToPX(TypedValue.COMPLEX_UNIT_DIP, size);
    }

    public static int PXToDIP(float pxValue){
        final float scale = getResources().getDisplayMetrics().density;
        return (int)(pxValue / scale);
    }

    public static DisplayMetrics getDisplayMetrics() {
        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        return dm;
    }

    public static int getScreenWidth() {
        return getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return getDisplayMetrics().heightPixels;
    }

}
