package com.mk.core.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mingkg21 on 2017/10/16.
 */

public class TimeUtil {

    public static final long SECOND = 1000;
    public static final long MINUTE = 60 * SECOND;
    public static final long HOUR   = 60 * MINUTE;
    public static final long DAY    = 24 * HOUR;
    public static final long DAY_15  = 15 * DAY;
    public static final long MONTH_2 = 61 * DAY;

    public static String formatComment(long time) {
        if (isYesterday(time)) {
            return "昨天";
        } else {
            long currentTime = System.currentTimeMillis() - time;
            if (currentTime > MONTH_2) {
                return formatYYYY_MM_DD(time);
            } else if (currentTime > DAY_15) {
                return formatMM_DD(time);
            } else if (currentTime > DAY) {
                return currentTime / DAY + "天前";
            } else if (currentTime > HOUR) {
                return currentTime / HOUR + "小时前";
            } else if (currentTime > MINUTE) {
                return currentTime / MINUTE + "分钟前";
            } else {
                return "刚刚";
            }
        }
    }

    public static boolean isYesterday(long timestamp) {
        Calendar c = Calendar.getInstance();
        clearCalendar(c, Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND);
        c.add(Calendar.DAY_OF_MONTH, -1);
        long firstOfDay = c.getTimeInMillis(); // 昨天最早时间

        c.setTimeInMillis(timestamp);
        clearCalendar(c, Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND, Calendar.MILLISECOND); // 指定时间戳当天最早时间

        return firstOfDay == c.getTimeInMillis();
    }

    private static void clearCalendar(Calendar c, int... fields) {
        for (int f : fields) {
            c.set(f, 0);
        }
    }

    public static String formatYYYY_MM_DD_HH_MM_SS(long time) {
        if (String.valueOf(time).length() == 10) {
            time *= 1000;
        }
        SimpleDateFormat simpleFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleFormatter.format(new Date(time));
    }

    public static String formatYYYY_MM_DD(long time) {
        if (String.valueOf(time).length() == 10) {
            time *= 1000;
        }
        SimpleDateFormat simpleFormatter = new SimpleDateFormat("yyyy-MM-dd");
        return simpleFormatter.format(new Date(time));
    }

    public static String formatMM_DD(long time) {
        if (String.valueOf(time).length() == 10) {
            time *= 1000;
        }
        SimpleDateFormat simpleFormatter = new SimpleDateFormat("MM-dd");
        return simpleFormatter.format(new Date(time));
    }

    public static String formatHH_MM(long time) {
        if (String.valueOf(time).length() == 10) {
            time *= 1000;
        }
        SimpleDateFormat simpleFormatter = new SimpleDateFormat("HH:mm");
        return simpleFormatter.format(new Date(time));
    }

    public static int getMaxDayOfMonth(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, 1);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

}
