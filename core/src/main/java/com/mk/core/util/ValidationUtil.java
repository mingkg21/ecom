package com.mk.core.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mingkg21 on 2017/10/10.
 */

public class ValidationUtil {

    public static boolean matchingMobil(String phone) {
        String regStr = "1[34578][0-9]{9}";
        Pattern pattern = Pattern.compile(regStr);
        Matcher matcher = pattern.matcher(phone.trim());
        if (!matcher.matches()) {
            ToastUtil.showToast("电话号码输入有误~");
            return false;
        }
        return true;
    }

    public static boolean matchingSecurity(String code) {
        int length = code.trim().length();
        if (length != 6) {
            ToastUtil.showToast("验证码长度不够~");
            return false;
        }
        return true;
    }

    public static boolean matchingPassword(String password) {
        int length = password.trim().length();
        if (length < 6) {
            ToastUtil.showToast("密码长度至少6个字符~");
            return false;
        } else if (length > 18) {
            ToastUtil.showToast("密码长度小于18个字符~");
            return false;
        }
        return true;
    }
}
