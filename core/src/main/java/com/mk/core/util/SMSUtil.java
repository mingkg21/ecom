package com.mk.core.util;

import java.security.SecureRandom;

public class SMSUtil {

    private static final String SYMBOLS = "0123456789"; // 数字

    public static String getSMSCode(int num) {

        char[] nonceChars = new char[num];

        for (int index = 0; index < nonceChars.length; ++index) {
            nonceChars[index] = SYMBOLS.charAt(new SecureRandom().nextInt(SYMBOLS.length()));
        }

        return new String(nonceChars);
    }


}
