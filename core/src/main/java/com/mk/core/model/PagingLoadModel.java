package com.mk.core.model;

import com.mk.core.net.BaseRequest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public abstract class PagingLoadModel<Result extends Serializable, ResultItem> extends BaseDataModel {

    private ArrayList<ResultItem> mDatas;

    private boolean isNext;
    private boolean isClearData;
    private boolean hasDate;
    private boolean isLoadData ;
    private int mTempIndex;
    protected boolean isForce;

    private Result mCurrentResult;
    private BaseRequest mDateProvider;

    public PagingLoadModel() {
        mDatas = new ArrayList<>();
    }

    public boolean loadPage(boolean isClearData, boolean isForce) {
        boolean isSuccess = false;
        this.isForce = isForce;
        if (isClearData || hasData(mCurrentResult)) {
            Result result = isClearData ? null : mCurrentResult;
            BaseRequest baseRequest = getBaseRequest(result);
            if (baseRequest != null) {
                this.mDateProvider = baseRequest;
                this.isClearData = isClearData;
                startTask(baseRequest);
                isSuccess = true;
            }
        }
        return isSuccess;
    }

    public boolean loadPage() {
        return loadPage(true, false);
    }

    public boolean forceLoadPage() {
        return loadPage(true, true);
    }

    public boolean loadNextPage() {
        return loadPage(false, false);
    }

    public boolean hasData() {
        return hasData(mCurrentResult);
    }

    protected abstract boolean hasData(Result result);

    public boolean isClear() {
        return isClearData;
    }

    public boolean isForceRequest() {
        return isForce;
    }

    public boolean isEmpty() {
        return mDatas.isEmpty();
    }

    public void addData(ResultItem item) {
        mDatas.add(item);
    }

    public void addData(int index, ResultItem item) {
        mDatas.add(index, item);
    }

    public void addAllData(ArrayList<ResultItem> items) {
        mDatas.addAll(items);
    }

    public void setAllData(ArrayList<ResultItem> items) {
        mDatas.clear();
        mDatas.addAll(items);
    }

    public ArrayList<ResultItem> getData(){
        return mDatas;
    }

    public Result getCurrentResult() {
        return mCurrentResult;
    }

    protected abstract BaseRequest getBaseRequest(Result result);

    public String getRequestKey(){
        return mDateProvider != null ? mDateProvider.getDataKey() : "";
    }

    public boolean isPagingRequest(String key){
        return isKey(key, getRequestKey());
    }

    protected abstract List<ResultItem> getListData(Result result);

    @Override
    public boolean dispatchPostLoad(String key) {
        if(isPagingRequest(key)) {
            isLoadData = false;
        }
        return super.dispatchPostLoad(key);
    }

    @Override
    public boolean dispatchSuccess(String key, Object data) {
        if(isPagingRequest(key)) {
            Result currentResult = (Result) data;

            if (isClearData) {
                mDatas.clear();
            }
            mCurrentResult = currentResult;

            List<ResultItem> resultItemList = getListData(currentResult);
            if (resultItemList != null) {
                mDatas.addAll(resultItemList);
            }
        }
        return super.dispatchSuccess(key, data);
    }
}
