package com.mk.core.model;

import com.mk.core.entity.AbsListEntity;
import com.mk.core.entity.AbsResponseEntity;
import com.mk.core.net.BaseRequest;

import java.util.List;

/**
 * Created by mingkg21 on 2017/10/22.
 */

public abstract class BaseAppPagingLoadModel<ENTITY extends AbsResponseEntity<LIST>, LIST extends AbsListEntity<T>, T> extends PagingLoadModel<ENTITY, T> {

    @Override
    protected boolean hasData(ENTITY entity) {
        if (entity == null || entity.getData() == null) {
            return false;
        }
        return entity.getData().hasNext();
    }

    @Override
    protected BaseRequest getBaseRequest(ENTITY entity) {
        if (entity == null || entity.getData() == null) {
            return getBaseRequest(0);
        }
        return getBaseRequest(entity.getData().getNextStartPos());
    }

    @Override
    protected List<T> getListData(ENTITY entity) {
        return entity.getData().getDataList();
    }

    protected abstract BaseRequest getBaseRequest(int offset);

}
