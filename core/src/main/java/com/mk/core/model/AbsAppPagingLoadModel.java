package com.mk.core.model;

import com.mk.core.entity.AbsListEntity;
import com.mk.core.entity.ResponseEntity;

/**
 * Created by mingkg21 on 2017/10/22.
 */

public abstract class AbsAppPagingLoadModel<LIST extends AbsListEntity<T>, T> extends BaseAppPagingLoadModel<ResponseEntity<LIST>, LIST, T> {

}
