package com.mk.core.model;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public interface LoadCallback {

    boolean onPreLoad(String key);
    boolean onPostLoad(String key);
    boolean onSuccess(String key, Object data);
    boolean onFail(String key, Object data);
    boolean onDataChange(String key, Object data);

}
