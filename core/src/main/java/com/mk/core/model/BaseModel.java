package com.mk.core.model;

import android.content.Context;

import com.mk.core.app.BaseApp;
import com.mk.core.net.CallBackHandler;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class BaseModel {

    protected ArrayList<WeakReference<LoadCallback>> mCallbackList = new ArrayList<>();

    public void addCallback(LoadCallback callback) {
        if (!containsCallback(callback)) {
            mCallbackList.add(new WeakReference<LoadCallback>(callback));
        }
    }

    public void removeCallback(LoadCallback callback) {
        for (WeakReference<LoadCallback> cb : mCallbackList) {
            LoadCallback c = cb.get();
            if (c != null && c == callback) {
                mCallbackList.remove(cb);
                return;
            }
        }
    }

    private boolean containsCallback(LoadCallback callback) {
        for (WeakReference<LoadCallback> cb : mCallbackList) {
            LoadCallback c = cb.get();
            if (c != null && c == callback) {
                return true;
            }
        }
        return false;
    }

    public boolean traversalCallBacks(CallBackHandler<LoadCallback> handler){
        boolean isHandle = false;
        for (int i = 0; i < mCallbackList.size(); i++) {
            WeakReference<LoadCallback> callBack = mCallbackList.get(i);
            if(callBack.get() != null){
                if(!isHandle){
                    isHandle = handler.handle(callBack.get());
                }
            }else{
                mCallbackList.remove(i);
                i--;
            }
        }
        return isHandle;
    }

    public Context getContext() {
        return BaseApp.getInstance();
    }

}
