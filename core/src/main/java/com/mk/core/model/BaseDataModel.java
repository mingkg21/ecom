package com.mk.core.model;

import com.mk.core.net.BaseRequest;
import com.mk.core.net.CallBackHandler;
import com.mk.core.net.ServiceProxy;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public class BaseDataModel extends BaseModel {

    public BaseDataModel() {
        ServiceProxy.getInstance().addModel(this);
    }

    protected boolean isKey(String key, String requestKey) {
        String tmpKey = getKey(requestKey);
        return getKey(requestKey).equals(key);
    }

    public String getKey(String key) {
//        return getClass().getName() + ":" + key;
        if (key == null) {
            key = "";
        }
        return key;
    }

    public boolean dispatchPreLoad(final String key) {
        return traversalCallBacks(new CallBackHandler<LoadCallback>() {
            @Override
            public boolean handle(LoadCallback callback) {
                return callback.onPreLoad(key);
            }
        });
    }

    public boolean dispatchPostLoad(final String key) {
        return traversalCallBacks(new CallBackHandler<LoadCallback>() {
            @Override
            public boolean handle(LoadCallback callback) {
                return callback.onPostLoad(key);
            }
        });
    }

    public boolean dispatchSuccess(final String key, final Object data) {
        return traversalCallBacks(new CallBackHandler<LoadCallback>() {
            @Override
            public boolean handle(LoadCallback callback) {
                return callback.onSuccess(key, data);
            }
        });
    }

    public boolean dispatchFail(final String key, final Object data) {
        return traversalCallBacks(new CallBackHandler<LoadCallback>() {
            @Override
            public boolean handle(LoadCallback callback) {
                return callback.onFail(key, data);
            }
        });
    }

    public boolean dispatchDataChange(final String key, final Object data) {
        return traversalCallBacks(new CallBackHandler<LoadCallback>() {
            @Override
            public boolean handle(LoadCallback callback) {
                return callback.onDataChange(key, data);
            }
        });
    }

    public void notifyDataChange(String key,Object data) {
        ServiceProxy.getInstance().notifyDataChange(key, data);
    }

    protected void startTask(BaseRequest baseRequest) {
        baseRequest.setBaseDataModel(this);
        ServiceProxy.getInstance().requestData(baseRequest);
    }
}
