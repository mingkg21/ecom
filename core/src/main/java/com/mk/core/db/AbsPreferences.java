package com.mk.core.db;

import android.content.Context;
import android.content.SharedPreferences;

import com.mk.core.app.BaseApp;

/**
 * Created by mingkg21 on 2017/8/27.
 */

public abstract class AbsPreferences {

    protected abstract String getFileName();

    protected SharedPreferences getSharedPreferences() {
        return BaseApp.getInstance().getSharedPreferences(getFileName(), Context.MODE_PRIVATE);
    }

    protected SharedPreferences.Editor getEdit() {
        return getSharedPreferences().edit();
    }

    public void putString(String key, String value) {
        getEdit().putString(key, value).apply();
    }

    public String getString(String key) {
        return getSharedPreferences().getString(key, "");
    }

    public void putInt(String key, int value) {
        getEdit().putInt(key, value).apply();
    }

    public int getInt(String key) {
        return getSharedPreferences().getInt(key, 0);
    }

}
